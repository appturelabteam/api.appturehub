<?php
$controller = new Controller(array());
$responseBody = $controller->_createJsonResponseBody();
$responseBody["success"] = true;
$responseBody["message"] = "Start up time: ". (microtime(true)-$_SESSION["start_time"]);
header("Content-Type: application/json");
print json_encode($responseBody);
exit();
?>