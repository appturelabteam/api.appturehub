<?php
// API mode def
define('API_MODE_DEVELOPMENT',0);
define('API_MODE_PREPRODUCTION',50);
define('API_MODE_PRODUCTION',100);
define('API_MODE',API_MODE_DEVELOPMENT);

// our API title
define('API_SITE_NAME',"Another Appture Hub API");

// API's default anonymous Client ID; generated at install in ClientController
//  - used by frontends for oauth2 access
define('API_DEFAULT_CLIENT_ID', "another_appture_hub_web");

// versions
define('API_VERSION',"1");
define('VERSION_WEB',"0");
define('VERSION_ANDROID',"0");
define('VERSION_IOS',"0");

// path to uploads folder
define('API_PATH_UPLOADS',"uploads");

// urls for frontend and admin
define('UI_URL',"../");
define('ADMIN_UI_URL',"../admin");

// google signin ids
define('API_GOOGLE_WEB_CLIENT_ID','');
//define('API_GOOGLE_IOS_CLIENT_ID',''); // used for the IOS app version

// database connection
define('API_DB_HOST',"localhost");
define('API_DB_DATABASE',API_MODE !== API_MODE_PRODUCTION ? "appturehub_basic" : "");
define('API_DB_USER',API_MODE !== API_MODE_PRODUCTION ? "root" : "");
define('API_DB_PASSWORD',API_MODE !== API_MODE_PRODUCTION ? "root" : "");

// set some php settings
ini_set('error_reporting', E_ALL);
ini_set('display_errors', API_MODE === API_MODE_DEVELOPMENT ? 1 : 0);
ini_set('display_startup_errors',1);
ini_set('log_errors',1);
ini_set('log_errors_max_len',2048);
ini_set('ignore_repeated_errors',0);
ini_set('ignore_repeated_source',0);
ini_set('report_memleaks',1);
ini_set('track_errors',1);
ini_set('error_log',"php_error_log.txt" );
ini_set('post_max_size','128M');
ini_set('upload_max_filesize', '128M');
date_default_timezone_set("Africa/Johannesburg");

// Fake GET Method when running script from CMD
if(isset($argc) && $argc > 1) {
    $_SERVER["REQUEST_METHOD"] = "GET";
    parse_str(implode('&',array_slice($argv, 1)), $_GET);
}

// allow required HTTP methods for our API
if(isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Max-Age: 86400'); // cache for 1 day
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    //header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding');
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    
    //if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
    //   header("Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
}

// get start time so we can time the request
$_SESSION["start_time"] = microtime(true);

// include all composer packages
include_once("vendor/autoload.php");

// include our appture hub helper classes
include_once("includes/PushNotifications.php");
include_once("includes/DataSource_PDO.php");
include_once('includes/OAuth2Wrap/OAuth2Wrap.php');
include_once("includes/Session.php");
include_once("includes/Image.php");
include_once("includes/Mailer.php");
include_once("includes/Message.php");
include_once("includes/Permissions.php");
include_once("includes/Forms.php");
include_once("includes/Controller.php");
include_once("includes/Template.php");
include_once("includes/Router.php");

// connect to db
DS::connect(API_DB_HOST, API_DB_USER, API_DB_PASSWORD, API_DB_DATABASE);

// setup/install controller manager class
Controller::_install();

// setup/install permissions manager class
Permissions::install();

/* start the session */
$time = $_SESSION["start_time"];
Session::start(str_slugify(API_SITE_NAME));
$_SESSION["start_time"] = $time;

/* initialize utilities and services */
Permissions::load();
Forms::setUploadPath(API_PATH_UPLOADS);

/* Create Router */
$router = new Router();
$router->setDefaultRoute("home");
$router->init();

// set the webcontent folder for automatically loaded routes (see $router->loadAll();)
// - for dev mode, add API test routes which generates some experimental live documentation
$router->setWebContentFolder((API_MODE === API_MODE_DEVELOPMENT ? "tests" : "live"));

/* Add routes to Router */
$router->loadAll(); // loads all php files under the web content folder as routes, excluding files that start with an _

// set router on base Controller class
Controller::setRouter($router);

/* load controllers */
Controller::addRoute("log","LogController",0);
Controller::addRoute("variable","VariableController",1);
Controller::addRoute("cron","CronController",0);

Controller::addRoute("auth","AuthController",0);
Controller::addRoute("user","UserController",1);
Controller::addRoute("client","ClientController",5);
Controller::addRoute("user_profile","UserProfileController",5);

Controller::addRoute("menu","MenuController",0);
Controller::addRoute("event","EventController",0);
Controller::addRoute("file","FileController",1);
Controller::addRoute("email","EmailController",1);
Controller::addRoute("contact","ContactController",5);

//Controller::addRoute("template","TemplateController",5); // in dev
Controller::addRoute("page","PageController",5);
Controller::addRoute("node","NodeController",5); // in dev
Controller::addRoute("faq","FaqController",5);

Controller::addRoute("announcement","AnnouncementController",5);
Controller::addRoute("announcement_group","AnnouncementGroupController",5);

Controller::includeAll(true);

/* Run the router */
$router->run();