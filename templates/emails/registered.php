<p>Thank you for registering an account with <a style="text-decoration: none; color:<?=VariableController::_getItemValue("Settings", "SITE_COLOR")?>;" href="<?=UI_URL?>"><?=VariableController::_getItemValue("Settings", "SITE_NAME")?></a>.</p>

<?php if($account_type === "Local") { ?>
    <p>Your account information is as follows:</p>
    <ul>
        <li><b>Username:</b> <a style="text-decoration: none; color:<?=VariableController::_getItemValue("Settings", "SITE_COLOR")?>;" href="mailto:<?=strtolower($username)?>"><?=strtolower($username)?></a></li>
        <li><b>Password:</b> <i>(as entered during registration)</i></li>
    </ul>
    
    <p>
        Please click on the link below to validate your email address:<br>
        <a style="text-decoration: none; color:<?=VariableController::_getItemValue("Settings", "SITE_COLOR")?>;" href="<?=Router::getIndex()."user/validate_email?id=".base64_encode($validation_guid)?>">Validate email address...</a>
    </p>
<?php } else { ?>
    <p>You have signed up using your <?=$account_type?> account.</p>
<?php } ?>