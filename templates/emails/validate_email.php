<p>
    Please click on the link below to validate your email address:<br>
    <a style="text-decoration: none; color:<?=VariableController::_getItemValue("Settings", "SITE_COLOR")?>;" href="<?=Router::getIndex()."user/validate_email?id=".base64_encode($validation_guid)?>">Validate email address...</a>
</p>