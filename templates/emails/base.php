<!DOCTYPE html>
<html>
    <head>
        <style>
            html {
                margin: 0;
                padding: 0;
            }

            body {
                margin: 0;
                padding: 0;
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }

            table {
                border-spacing: 0;
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
            }

            table td,
            table th {
                border-collapse: collapse;
                text-align: left;
            }

            .ExternalClass {
                width: 100%;
            }

            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }
            /* Outermost container in Outlook.com */

            .ReadMsgBody {
                width: 100%;
            }

            img {
                -ms-interpolation-mode: bicubic;
            }
      
            a {
                color: <?=VariableController::_getItemValue("Settings", "SITE_COLOR")?>;
            }
            html {
                font-family: sans-serif;
            }
        </style>
    </head>
    <body>
        <table style="width:100%;" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td>&nbsp;</td>
                <td style="min-width: 700px; width: 700px; max-width: 100%;">
                    <table style="min-width: 700px; width: 700px; max-width: 100%;" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td style="text-align: center;">
                                <a border="0" href="<?=UI_URL?>"><img src='templates/emails/img/logo.png' title='<?=VariableController::_getItemValue("Settings", "SITE_NAME")?>'/></a><br><br>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: <?=VariableController::_getItemValue("Settings", "SITE_COLOR")?>; color: white; text-align: left; height: 120px;">
                                <table style='width: 100%; min-width: 100%;' cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td style='width: 10px'>&nbsp;</td>
                                        <td><h2 style='color: white;'><?=$subject?></h2></td>
                                        <td style='width: 10px'>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:#333; text-align: left;">
                                <table width='100%' cellspacing="0" cellpadding="0" border="0" style='width: 100%; min-width: 100%; border-left: solid 1px <?=VariableController::_getItemValue("Settings", "SITE_COLOR")?>; border-right: solid 1px <?=VariableController::_getItemValue("Settings", "SITE_COLOR")?>;'>
                                    <tr><td colspan="3">&nbsp;</td></tr>
                                    <tr>
                                        <td style='width: 10px'>&nbsp;</td>
                                        <td><?=$message?></td>
                                        <td style='width: 10px'>&nbsp;</td>
                                    </tr>
                                    <tr><td colspan="3">&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #777; color: white; text-align: center; height: 50px;">
                                <br><a style="color: white;" href="<?=UI_URL?>"><?=VariableController::_getItemValue("Settings", "SITE_NAME")?></a><br>
                                <a style="color: white;" href="mailto:<?=VariableController::_getItemValue("Settings", "SITE_EMAIL_ADMIN")?>"><?=VariableController::_getItemValue("Settings", "SITE_EMAIL_ADMIN")?></a> | <a style="color: white;" href="tel:<?= str_replace(" ", "", VariableController::_getItemValue("Settings", "SITE_TEL"))?>"><?=VariableController::_getItemValue("Settings", "SITE_TEL")?></a><br>&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>