<table style="width:100%;" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td>&nbsp;</td>
        <td style="min-width: 700px; width: 700px; max-width: 100%;">
            <table style="min-width: 700px; width: 700px; max-width: 100%;" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td style="text-align: center;">
                        <a border="0" href="<?=UI_URL?>"><img src='<?=Router::getIndex()?>templates/emails/img/logo.png' title='<?=VariableController::_getItemValue("Settings", "SITE_NAME")?>'/></a><br><br>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: <?=VariableController::_getItemValue("Settings", "SITE_COLOR")?>; color: white; text-align: left; height: 120px;">
                        <table style='width: 100%; min-width: 100%;' cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td style='width: 10px'>&nbsp;</td>
                                <td><h2 style='color: white;'>{subject}</h2></td>
                                <td style='width: 10px'>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="color:#333; text-align: left;">
                        <table width='100%' cellspacing="0" cellpadding="0" border="0" style='width: 100%; min-width: 100%; border-left: solid 1px <?=VariableController::_getItemValue("Settings", "SITE_COLOR")?>; border-right: solid 1px <?=VariableController::_getItemValue("Settings", "SITE_COLOR")?>;'>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            <tr>
                                <td style='width: 10px'>&nbsp;</td>
                                <td>{message}</td>
                                <td style='width: 10px'>&nbsp;</td>
                            </tr>
                            <tr><td colspan="3">&nbsp;</td></tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #777; color: white; text-align: center; height: 50px;">
                        <br><a style="color: white;" href="<?=UI_URL?>"><?=VariableController::_getItemValue("Settings", "SITE_NAME")?></a><br>
                        <a style="color: white;" href="mailto:<?=VariableController::_getItemValue("Settings", "SITE_EMAIL_ADMIN")?>"><?=VariableController::_getItemValue("Settings", "SITE_EMAIL_ADMIN")?></a> | <a style="color: white;" href="tel:<?= str_replace(" ", "", VariableController::_getItemValue("Settings", "SITE_TEL"))?>"><?=VariableController::_getItemValue("Settings", "SITE_TEL")?></a><br>&nbsp;
                    </td>
                </tr>
            </table>
        </td>
        <td>&nbsp;</td>
    </tr>
</table>