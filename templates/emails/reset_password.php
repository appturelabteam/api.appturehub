<p>Your account password has been reset.</p>
    
<p>Your account information is as follows:</p>
<ul>
    <li><b>Username:</b> <a style="text-decoration: none; color:#4ebc7a;" href="mailto:<?=strtolower($username)?>"><?=strtolower($username)?></a></li>
    <li><b>Password:</b> <i><?=$password?></i></li>
</ul>

<p>View our <a style="text-decoration: none; color:#4ebc7a;" href="https://www.craftedarts.co.za/faq">FAQ online</a> for further details.</p>