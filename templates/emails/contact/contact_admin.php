<p>Good day <?=$user["first_name"] ? $user["first_name"].($user["last_name"] ? " ".$user["last_name"] : "") : $user["username"]?>,</p>
<p>A contact form has been submitted with the following details:</p>
<ul>
    <li>Email: <?=$data["email"]?></li>
    <li>Phone: <?=$data["phone"]?></li>
    <li>Contact Name: <?=$data["contact_name"]?></li>
    <li>Message: <?=$data["message"]?></li>
    <li>Width: <?=$data["width"]?></li>
    <li>Profile: <?=$data["profile"]?></li>
    <li>Rim Size: <?=$data["rim_size"]?></li>
</ul>