<p>Good day <?=(isset($user["contact_name"]) ? $user["contact_name"] : (isset($user["first_name"]) ? $user["first_name"].($user["last_name"] ? " ".$user["last_name"] : "") : (isset($user["username"]) ? $user["username"] : "")))?>,</p>
<p>This is to confirm, you have submitted a contact form with the following details:</p>
<ul>
    <li>Email: <?=$data["email"]?></li>
    <li>Phone: <?=$data["phone"]?></li>
    <li>Contact Name: <?=$data["contact_name"]?></li>
    <li>Message: <?=$data["message"]?></li>
    <li>Width: <?=$data["width"]?></li>
    <li>Profile: <?=$data["profile"]?></li>
    <li>Rim Size: <?=$data["rim_size"]?></li>
</ul>
<p>We will be in contact shortly.</p>