var Message = {
    options: {
        expires: 5000,
        create_callback:null,
        parentTo:null,
        replaceWithId:null,
        id:null
    },
    
    alert: "<div class='alert alert-dismissable fade show'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><br>",
    
    add: function(text,alert_type,options){
        var self = this;
        
        var opts = $.extend({},self.options,options);
        if(!opts.parentTo) {
            opts.parentTo = $("#messages");
        }
        
        if(opts.id !== null) {
            $(opts.parentTo).find(".alert[data-id='"+opts.id+"']").remove();
        }
        
        var alert = $(self.alert);
        alert.addClass("alert-"+alert_type+"");
        alert.append(text);
        
        if(opts.id !== null) {
            alert.attr("data-id",opts.id);
        }
        
        if(opts.expires !== 0) {
            alert.find("button").remove();
            alert.delay(opts.expires).fadeOut(500,function(){$(this).remove();});
            alert.on("mouseenter",function(){
                alert.stop(true,true);
            });
            alert.on("mouseleave",function(){
                alert.delay(opts.expires).fadeOut(500,function(){$(this).remove();});
            });
        }
        
        $(opts.parentTo).append(alert);
        
        if(opts.create_callback !== null) {
            create_callback(alert);
        }
    }
};