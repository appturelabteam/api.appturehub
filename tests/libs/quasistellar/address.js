// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.
//
// Remember to update API_KEY
var Address = {
    API_KEY:"AIzaSyCqsle9GikUPQ80wPCOw85arVGSyIPrE1c",
    gMapsLoaded: 0,
    addressFields:{},
    callbacks:{},
    placeSearch:null,
    autocomplete:null,
    idsToInit:new Array(),
    
    gMapsCallback: function(){
        Address.gMapsLoaded = 2; // done
        for(var x = 0; x < Address.idsToInit.length; x++) {
            Address.initialize(Address.idsToInit[x],Address.callbacks[Address.idsToInit[x]]);
        }
    },
    
    componentForm: {
        street_number: 'long_name',
        route: 'long_name',
        sublocality_level_1: 'long_name', // suburb
        sublocality_level_2: 'long_name', // suburb
        locality: 'long_name', // city
        administrative_area_level_1: 'long_name', // province
        country: 'long_name',
        postal_code: 'long_name'
    },
    
    initialize: function (id, callback) {
        
        Address.callbacks[id] = callback;
        
        if(Address.gMapsLoaded !== 2) { // not loaded or in progress
            Address.idsToInit.push(id);
        }
        
        if(Address.gMapsLoaded === 0) { // 0 = not loaded and load not started
            Address.gMapsLoaded = 1; // in progress
            var script_tag = document.createElement('script');
            script_tag.setAttribute("type","text/javascript");
            script_tag.setAttribute("src","https://maps.googleapis.com/maps/api/js?key="+Address.API_KEY+"&libraries=geometry,places&callback=Address.gMapsCallback");
            (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
        }
        
        if(Address.gMapsLoaded !== 2) { // not loaded or in progress
            return;
        }
        
        Address.addressFields[id] = {};
        
        var index = Address.idsToInit.indexOf(id);
        if(index > 0) {
            Address.idsToInit.splice(index, 1);
        }
        
        // Create the autocomplete object, restricting the search
        // to geographical location types.
        //console.log(document.getElementById(id))
        //id.replace('_autocomplete','')
        
        // to display the address when we've saved a json value use the following
        //var value = document.getElementById(id.replace('_autocomplete','')).value;
        //var address_info = value == "" ? {} : JSON.parse(value);
        //document.getElementById(id).value = (address_info.entered_value ? address_info.entered_value : document.getElementById(id).value);
        
        if(typeof(google) !== 'undefined') {
            Address.addressFields[id].autocomplete = new google.maps.places.Autocomplete(
                    (document.getElementById(id)),
                    {types: ['geocode']});
            // When the user selects an address from the dropdown,
            // populate the address fields in the form.
            
            document.getElementById(id).placeholder = ""

            google.maps.event.addListener(Address.addressFields[id].autocomplete, 'place_changed', function () {
                Address.fillInAddress(id);
            });
            
            var geolocation = new google.maps.LatLng(
                    -34.2958839, 18.2334052);
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: 10000
            });
            Address.addressFields[id].autocomplete.setBounds(circle.getBounds());
        }
    },
    
    // [START region_fillform]
    fillInAddress: function (id) {
        id = (id || Address.currentId);
        // Get the place details from the autocomplete object.
        var place = Address.addressFields[id].autocomplete.getPlace();
        
        document.getElementById(id+'_json').value = '';
        
        //console.log(place.address_components)
        
        var address_info = {};
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (Address.componentForm[addressType]) {
                var val = place.address_components[i][Address.componentForm[addressType]];
                address_info[addressType] = val;
            }
        }
        
        address_info['location'] = place.geometry.location;
        address_info['entered_value'] = document.getElementById(id).value;
        
        //console.log(address_info)
        
        document.getElementById(id+'_json').value = JSON.stringify(address_info);
        
        if(document.getElementById(id+'_latitude')) {
            
            document.getElementById(id+'_latitude').value = place.geometry.location.lat();
            document.getElementById(id+'_longitude').value = place.geometry.location.lng();

            document.getElementById(id+'_street').value = address_info.street_number+" "+address_info.route;
            document.getElementById(id+'_suburb').value = address_info.sublocality_level_1 || address_info.sublocality_level_2;
            document.getElementById(id+'_city').value = address_info.locality;
            document.getElementById(id+'_province').value = address_info.administrative_area_level_1;
            document.getElementById(id+'_country').value = address_info.country;
            document.getElementById(id+'_postal_code').value = address_info.postal_code;
            
        } else {
            
            if(document.getElementById('latitude'))
                document.getElementById('latitude').value = place.geometry.location.lat();
            if(document.getElementById('longitude'))
                document.getElementById('longitude').value = place.geometry.location.lng();
            
            if(document.getElementById('street'))
                document.getElementById('street').value = address_info.street_number+" "+address_info.route;
            if(document.getElementById('suburb'))
                document.getElementById('suburb').value = address_info.sublocality_level_1 || address_info.sublocality_level_2;
            if(document.getElementById('city'))
                document.getElementById('city').value = address_info.locality;
            if(document.getElementById('province'))
                document.getElementById('province').value = address_info.administrative_area_level_1;
            if(document.getElementById('country'))
                document.getElementById('country').value = address_info.country;
            if(document.getElementById('postal_code'))
                document.getElementById('postal_code').value = address_info.postal_code;
        }
        
        if(typeof(Address.callbacks[id]) !== 'undefined') {
            Address.callbacks[id]();
        }
    },
    // [END region_fillform]

    // [START region_geolocation]
    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    geolocate: function () {
        var id = this.id;
        if (navigator.geolocation && !Address.addressFields[id].gelocateReady) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = new google.maps.LatLng(
                        position.coords.latitude, position.coords.longitude);
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                Address.addressFields[id].autocomplete.setBounds(circle.getBounds());
            });
            
            Address.addressFields[id].gelocateReady = true;
        }
    }
    // [END region_geolocation]
};