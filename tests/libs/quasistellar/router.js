/**
 * router.js
 * 
 * By Dewald Bodenstein
 * May 2017
 * 
 * This router taps into the hashchange event to load the appropriate page from
 * the Router.pages object.
 * 
 * 
 * Pages:
 * 
 * The pageslider.js loads the HTML file set on page.htmlView from the 
 * 'app/views' folder. template.js is used to merge the view with the hashmap at
 * page.dataValues.
 * 
 * Each page has three events that can be utilized, page.onReady, page.onShown 
 * and page.onRefresh;
 * 
 * Pages can be added at any time to the Router.pages object as long as the hash
 * is only requested after the appropriate page has been added.
 * 
 * 
 * Getting started:
 * 
 * Adding a page called 'home', place it under 'router.pages.home' as a
 * function. Then within the function create a 'page' var, set the page.htmlView 
 * to the home view. Then return the 'page' var from the function.
 * 
 * Router.pages.home = function(args) {
 *     var page = new Page();
 *     page.html = "<h1>Home Page</h1>";
 *     return page;
 * };
 * 
 * To get it working, have the router.onReady event called when the document is
 * ready.
 * 
 * Requirements:
 *  pageslider.js
 *  template.js
 * 
 */
var Router = {
    container: null,
    pageSlider: null,
    baseUrl: null,
    page: null,
    hashString:null,
    routeString: null,
    routeFirstPart: null,
    prevRouteString: null,
    prevRouteFirstPart: null,
    prevHashString: null,
    routeParts: null,
    routeCallback: undefined,
    routeUniversalCallback: null,
    
    /**
     * route
     * Send the viewer to the page requested.
     * 
     * @param {type} callback
     * @param {boolean} force
     * @returns {undefined}
     */
    route: function (routeWithHash,callback,force) {
        
        // cancel this call if we only got a hash
        if(routeWithHash === "#") {
            this.hashString = "";
            return;
        }
        
        // prep vars
        this.routeCallback = callback;
        this.hashString = "";
        var route = routeWithHash; // keep as local variable for further processing
        
        // we receive a routeWithHash that may contain a route name and hash string
        // so we need to split it into a route and a hash
        if(routeWithHash.indexOf("#") !== -1) {
            var split = routeWithHash.split("#");
            //console.log("split",split,split.length)
            if(split.length > 1) {
                this.hashString = "#"+split[1];
                route = split[0];
                //console.log("split done",route,this.hashString)
            } else {
                this.hashString = "#"+split[0];
                route = "";
                //console.log("split done",route,this.hashString)
            }
        }
        
        //console.log("route "+routeWithHash,this.hashString)
        
        var baseParts = this.baseUrl.split("/");
        var fullRouteParts = route ? route.split("/") : [];
        //console.log(baseParts)
        //console.log(fullRouteParts)
        this.routeParts = new Array();
        for(var i = fullRouteParts.length-1; i >= 0; i--) {
            if(baseParts.indexOf(fullRouteParts[i]) === -1 && fullRouteParts[i] !== "." && fullRouteParts[i] !== "") {
                this.routeParts.push(fullRouteParts[i]);
            }
        }
        this.routeParts = this.routeParts.reverse();
        this.routeParts = this.routeParts.length ? this.routeParts : ['home'];
        
        //console.log(this.routeParts)
        
        // lets fix url where route camein before questionmark - paypal redirect issue
        /*if(window.location.route.indexOf("?") > 0) {
            var route = window.location.route.substr(0,window.location.route.indexOf("?"));
            var params = window.location.route.substr(window.location.route.indexOf("?"));
            window.location.href = window.location.origin+params+route;
        }*/
        
        // get the current window location route and split it
        //this.cleanHash = route.replace("#",""); // window.location.route.replace("#","");
        //this.routeParts = route;//this.cleanHash.split("/");
        this.routeString = this.routeParts.join("/");
        this.routeFirstPart = this.routeParts.length ? this.routeParts[0] : 'home';
        
        // default to home page, if none was specified
        //if (this.routeFirstPart === '') {// this.cleanHash === '') {
        //    this.routeFirstPart = "home";
        //}
        
        //console.log(this.routeString+" === "+this.prevRouteString)
        
        // add to history
        //this.history.push({from:this.prevRouteString,to:window.location.route});
        window.history.pushState(null, '', (this.routeString === '' ? "./" : this.routeString)+this.hashString);
        
        // skip reloading the same page
        if(this.routeString === this.prevRouteString && this.hashString === this.prevHashString && force !== true) { // if(window.location.route === this.prevRouteString && force !== true) {
            return;
        }
        
        if(this.prevRouteString !== this.routeString) {
            
            // set previous route strings
            this.prevRouteFirstPart = this.routeFirstPart;
            this.prevHashString = this.hashString;
            this.prevRouteString = this.routeString; //window.location.route;

            if(typeof this.pages[this.routeFirstPart] === 'undefined') {
                this.routeFirstPart = "not_found"; // override and show not found page
                //window.location.route = "#not_found";
                //return;
            }

            // now we are sure a page to show exists
            this.page = this.pages[this.routeFirstPart]( this.routeParts, this.hashString );
            
            if(this.page) {

                // add the page element to the dom and page container
                this.container.appendChild(this.page.element);

                // start the creation process which ends with sliding the page in
                this.page._onCreate(function (page) {
                    Router.pageSlider.slidePage(page);

                    $(page.element).find("a").on("click",Router.onLinkClicked);

                    if(Router.routeCallback) {
                        Router.routeCallback();
                    }

                    if(Router.routeUniversalCallback) {
                        Router.routeUniversalCallback();
                    }
                });
            }
            
        } else {
            
            if(this.page) {
                
                this.prevHashString = this.hashString;
                
                this.page.onRecall(this.routeParts,this.hashString);
            }
            
        }
        
        
    },
    
    /**
     * pages
     * 
     * The pages object contains all the available routes that can be called.
     * This is a dynamic object and pages can be added at any time.
     */
    pages: {},/*{
        home: function() {
            var page = {};
            page.onCreate = function(page) {
                page.find("a.exit").on("click",function(e){
                    e.preventDefault();
                    navigator.app.exitApp();
                    return false;
                });
            };
            return page;
        },
        read: function() {
            var page = {};
            page.onCreate = function(page) {
                page.find("a.back").attr("href","#home");
            };
            return page;
        }
    };*/
    
    /**
     * 
     * 
     * @returns {object} assoc array of parameters
     */
    params: function() {
        //alert("called")
        var params = (window.location.href.substr(window.location.href.indexOf("?")+1).replace(window.location.hash,"").split("&"));
        var assoc = {};
        for(var i in params) {
            var split = params[i].split("=");
            assoc[split[0]] = (typeof(split[1]) !== 'undefined' ? split[1] : "");
        }
        return assoc;
    },
    
    onLinkClicked: function(e){
        e.preventDefault();
        
        if($(this).attr("href").indexOf("http") === -1/* && $(this).attr("href").indexOf("#") !== 0*/) {
            //console.log("global "+$(this).attr("href"))
            return Router.route($(this).attr("href"));
        }
    },
    
    onReady: function (container, routeUniversalCallback, doneCallback) {
        this.container = container;
        this.routeUniversalCallback = routeUniversalCallback;
        this.pageSlider = new PageSlider();
        
        if(typeof cordova !== 'undefined') {
            this.baseUrl = location.pathname;
        } else {
            this.baseUrl = document.querySelector("base").getAttribute("href");
        }
        
        /*$(window).on('hashchange', function (e) {
            e.preventDefault();
            Router.route(window.location.hash,hashChangeCallback);
        });*/
        
        $("a").on("click",this.onLinkClicked);
        
        window.onpopstate = function(event) {
            //event.preventDefault();
            //console.log("pop state")
            
            Router.route(location.pathname); 
        };
        
        this.route(location.pathname+location.hash,doneCallback);
        
        // add onBeforeUnload event to browser
        /*$(window).bind("beforeunload", function(e) {
            if (typeof Router.page !== 'undefined' && typeof Router.page.onBeforeUnload !== 'undefined' && Router.page.onBeforeUnload !== null) {
                console.log(Router.page)
                // The event may get fired more than once if you have transition on 
                // multiple properties, ie using opacity and transform will fire 
                // this event twice, therefore we make sure we haven't called onShown before.
                return Router.page.onBeforeUnload(Router.page);
                
            }
            
            return true;
        });*/
    }
};