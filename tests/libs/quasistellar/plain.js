var pjs = {};
var hasClass, addClass, removeClass;
var extend;
var addEventsListiner;

if ('classList' in document.documentElement) {
    hasClass = function (el, className) { return el.classList.contains(className); };
    addClass = function (el, className) { el.classList.add(className); };
    removeClass = function (el, className) { el.classList.remove(className); };
} else {
    hasClass = function (el, className) {
        return new RegExp('\\b'+ className+'\\b').test(el.className);
    };
    addClass = function (el, className) {
        if (!hasClass(el, className)) { el.className += ' ' + className; }
    };
    removeClass = function (el, className) {
        el.className = el.className.replace(new RegExp('\\b'+ className+'\\b', 'g'), '');
    };
}

function elOffset(el,container) {
    container = container || window;
    var rect = el.getBoundingClientRect(),
        scrollLeft = container === window ? (window.pageXOffset || document.documentElement.scrollLeft) : container.scrollLeft,
        scrollTop = container === window ? (window.pageYOffset || document.documentElement.scrollTop) : container.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}

function outerHeight(el) {
    var totalHeight = 0;
    for (var i = 0; i < el.childNodes.length; i++) {
        //var style = window.getComputedStyle ? getComputedStyle(el, null) : el.currentStyle;
        //console.log(el.childNodes[i].style.height)
        totalHeight += el.childNodes[i].style.height;
    }
    //console.log(totalHeight)
    return totalHeight;
}


function isVisible(el,top,bottom) {
    var rect = el.getBoundingClientRect();
    
    if(((rect.top + top+10) >= top && (rect.top + top+10) <=bottom) ||
            ((rect.bottom + top-10) >= top && (rect.bottom + top-10) <=bottom) ) {
        //console.log(animOnVisibleEls[i])
        return true;
    }
    return false;
}


/**
 * getElWidth
 * 
 * Calculate the width of an element.
 * 
 * @param {DOM Element} el
 * @returns {Number} width
 */
getElWidth = function(el) {
    var rect = el.getBoundingClientRect();
    
    var width;
    if (rect.width) {
      // `width` is available for IE9+
      width = rect.width;
    } else {
      // Calculate width for IE8 and below
      width = rect.right - rect.left;
    }
    
    return width;
};

getChildrenHeight = function(el) {
    var height = 0;
    for(var i = 0; i < el.children.length; i++) {
        height += el.children[i].offsetHeight;
    }
    return height;
}

/**
 * Get the current top/left coordinates of an element relative to the document.
 * 
 * @param {type} el
 * @returns {getOffset.plainAnonym$0}
 */
getOffset = function(el) {
    var rect = el.getBoundingClientRect(),
    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}



/**
 * extend
 * 
 * Similar to jQuery's $.extend function. Merges 2 objects into one.
 * 
 * To keep DOM events, pass the element first for which events should be kept.
 * The second's events are lost.
 * 
 * @param {object} obj
 * @param {object} src
 * @returns {object} merged object
 */
extend = function (obj, src) {
    for (var key in src) {
        if (src.hasOwnProperty(key)) obj[key] = src[key];
    }
    return obj;
};




/**
 * addEventsListener
 * Allows adding multiple event listeners to an element with one call.
 * 
 * @param {type} element
 * @param {type} arEvent
 * @param {type} handler
 * @param {type} useCapture or options
 * @param {type} args
 * @returns {none}
 */
addEventsListener = function(element, arEvent, handler, useCapture, args) {
    if (typeof arEvent === 'string') {
        arEvent = arEvent.split(" ");
    }

    if (!(arEvent instanceof Array)) {
        throw 'addEventListener: requires an array of event names or string with multiple event names separated by a space.';
    }
    
    var handlerWrapper = function(event) {
        var normalArgs = args && args instanceof Array ? args : [];

        normalArgs.unshift(event);
        //console.log(typeof(handler))
        if(typeof(handler) === 'function') {
            handler.apply(this, normalArgs);
        } else {
            if(typeof(handler.handleEvent) === 'function') {
                handler.handleEvent(event, normalArgs);
            }
        }
    }

    for (var i=0;i<arEvent.length;i+=1){
        element.addEventListener(arEvent[i],handlerWrapper,useCapture);
    }
};




Select = function(key_array,object){
    var list = {};
    
    for(var i in key_array) {
        list[(typeof(key_array[i]) === 'object' ? i : key_array[i])] = (typeof(key_array[i]) === 'object' ? key_array[i] : object[key_array[i]]);
    }
    
    return list;
};

SplitIn2 = function (obj) {
    var first = {};
    var second = {};
    var x = 1;
    for(var i in obj) {
        if(x % 2 === 0) {
            second[i] = obj[i];
        } else {
            first[i] = obj[i];
        }
        x++;
    }
    return [first,second];
};

String.prototype.leftPad = function(padString, length) {
    var str = this;
    while (str.length < length) {
        str = padString + str;
    }
    return str;
};


Math.randomSeed = function(seed) {
    var x = Math.sin(seed++) * 10000;
    return x - Math.floor(x);
};


String.prototype.toCurrencyFormat = function() {
    return Number(Number(this).toFixed(2));
};

Number.prototype.toCurrencyFormat = function() {
    return Number(this.toFixed(2));
};


/**
 * Function to produce UUID.
 * See: http://stackoverflow.com/a/8809472
 */
function generateUUID()
{
	var d = new Date().getTime();
	
	if( window.performance && typeof window.performance.now === "function" )
	{
		d += performance.now();
	}
	
	var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c)
	{
		var r = (d + Math.random()*16)%16 | 0;
		d = Math.floor(d/16);
		return (c=='x' ? r : (r&0x3|0x8)).toString(16);
	});

return uuid;
}


function slugify(text,spaceReplacement)
{
    spaceReplacement = spaceReplacement || '-';
  return text.toString().toLowerCase()
    .replace(/\s+/g, spaceReplacement)           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, spaceReplacement)         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}


// ES5 15.2.3.14
// http://es5.github.com/#x15.2.3.14
if (!Object.keys) {
    // http://whattheheadsaid.com/2010/10/a-safer-object-keys-compatibility-implementation
    var hasDontEnumBug = true,
        dontEnums = [
            "toString",
            "toLocaleString",
            "valueOf",
            "hasOwnProperty",
            "isPrototypeOf",
            "propertyIsEnumerable",
            "constructor"
        ],
        dontEnumsLength = dontEnums.length;

    for (var key in {"toString": null}) {
        hasDontEnumBug = false;
    }

    Object.keys = function keys(object) {

        if (
            (typeof object != "object" && typeof object != "function") ||
            object === null
        ) {
            throw new TypeError("Object.keys called on a non-object");
        }

        var keys = [];
        for (var name in object) {
            if (owns(object, name)) {
                keys.push(name);
            }
        }

        if (hasDontEnumBug) {
            for (var i = 0, ii = dontEnumsLength; i < ii; i++) {
                var dontEnum = dontEnums[i];
                if (owns(object, dontEnum)) {
                    keys.push(dontEnum);
                }
            }
        }
        return keys;
    };

}