(function ( $ ) {
 
    $.fn.timestamp = function( args ) {
        args = (typeof args === 'undefined' ? {} : args);
        var options = $.extend({updateInterval: 60000}, args );
        
        function secondsToString (seconds) {
            var numyears = Math.floor(seconds / 31536000);
            var numdays = Math.floor((seconds % 31536000) / 86400); 
            var numhours = Math.floor(((seconds % 31536000) % 86400) / 3600);
            var numminutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
            var numseconds = (((seconds % 31536000) % 86400) % 3600) % 60;
            return (numyears > 0 ? numyears + " years " : "") + (numdays > 0 ? numdays + " days " : "") + numhours + " hour"+(numhours === 0 || numhours > 1 ? "s" : "")+" and " + numminutes + " minute"+(numminutes === 0 || numminutes > 1 ? "s" : "");// + numseconds;
        }
        
        function updateTimestamp($this){
            if(!$this.length) {
                // element has been removed...
                return;
            }
            // update with current time
            $this.attr("title",$this.timestamp + ((new Date().getTime()/1000) - $this.createdTimestamp));
            $this.html(secondsToString ($this.timestamp + ((new Date().getTime()/1000) - $this.createdTimestamp)));
            //continue with updates
            $this.timeout = setTimeout(function(){
                updateTimestamp($this);
            },$this.updateInterval);
        }
        
        function init($this) {
            clearTimeout($this.timeout);
            $this.updateInterval = options.updateInterval;
            $this.timestamp = $this.data("timestamp");
            $this.running = $this.data("running");
            $this.createdTimestamp = (new Date().getTime()/1000);
            
            $this.attr("title",$this.timestamp);
            $this.html(secondsToString ($this.timestamp));
            
            if($this.running) {
                $this.timeout = setTimeout(function(){
                    updateTimestamp($this);
                },$this.updateInterval);
            }
        };
        
        return this.each(function() {
            init($(this));
        });
    };
 
}( jQuery ));