// templates
var tpl = {
    page: "<div>\n\
                <div class='content'>\n\
                </div>\n\
            </div>",
    
    modal: "<div class='modal fade' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>\n\
                <div class='modal-dialog modal-lg' role='document'>\n\
                    <div class='modal-content'>\n\
                        <div class='modal-header'>\n\
                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>\n\
                            <h4 class='modal-title' id='myModalLabel'>Modal title</h4>\n\
                        </div>\n\
                        <div class='modal-body'>\n\
                        </div>\n\
                        <div class='modal-footer'>\n\
                            <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>\n\
                            <span class='buttons'></span>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
            </div>",
    
    table_limit: "<select name='generic-listing-limit' class='form-control input-sm'><option value='1'>1</option><option value='5'>5</option><option value='10'>10</option><option value='25'>25</option><option value='50'>50</option><option value='100'>100</option><option value='All'>All</option></select>",
    table_filter: "<input name='generic-listing-filter' class='form-control input-sm'/><button class='btn' name='generic-listing-filter-btn'><i class='fa fa-search'></i></button>",
    table_generic: "<table id='generic-listing' class='table table-rounded table-striped table-bordered'></table>",
    table_action: "<button class='btn btn-sm'></button>",
    
    list_generic: "<div id='generic-listing'></div>",
    list_action: "<button class='btn'></button>",
    
    // append the input and help text to the controls div
    bootstrap: "<div class='form-group'><label class='control-label bmd-label-floating'></label><div class='controls'></div></div>",
    // insert a label and input into this variable
    bootstrap_inline: "<div class='control-inline'></div>",
    bootstrap_help: "<span class='help-inline'></span>",
    
    alert: "<div class='alert fade in'><div class='container'><button type='button' class='close' data-dismiss='alert'>×</button></div></div>",
    
    input: "<input class='form-control'/>",
    textarea: "<textarea class='form-control'></textarea>",
    select: "<div class='combobox'><select class='form-control'></select></div>",
    multi_select: "<select class='form-control' multiple='multiple'></select>",
    select_add: "<div class='select-add'><div class='combobox'><select class='form-control'></select></div><button class='btn select-add-btn'><i class='fa fa-plus'></i></button><input type='hidden'/><ul></ul></div>",
    checkbox_wrap: "<div class='checkbox'><label></label></div>",
    checkbox: "<input type='checkbox'>",
    radio: "<input type='radio'/>",
    color_display: "<div class='form-control color-display'></div>",
    
    label: "<label></label>",
    
    option: "<option></option>",
    
    // if required add a red astirix
    required: "<font color='red'>*</font>",
}

var Render = {
    fixedNumber: function(n) {
        return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    },
    
    secondsToString: function(seconds) {
        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400); 
        var numhours = Math.floor(((seconds % 31536000) % 86400) / 3600);
        var numminutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
        var numseconds = (((seconds % 31536000) % 86400) % 3600) % 60;
        return (numyears > 0 ? numyears + " years " : "") + (numdays > 0 ? numdays + " days " : "") + numhours + " hour"+(numhours > 0 ? "s" : "")+" and " + (numminutes+"").padStart(2,"0") + ":" + (numseconds+"").padStart(2,"0");
    },
    
    dateFromString: function(dateStr) {
        if(typeof(dateStr) === 'undefined' || dateStr === null) return new Date();
        // unix time date?
        if(!isNaN(dateStr)) {
            return new Date(Number(dateStr)*1000);
        }
        // otherwise date string
        var a=dateStr.split(" ");
        var d=a[0].split("-");
        if(a.length > 1) {
            var t=a[1].split(":");
            return new Date(d[0],(d[1]-1),d[2],t[0],t[1],t[2]);
        } else {
            return new Date(d[0],(d[1]-1),d[2]);
        }
    },
    
    dateFromNumber: function(dateUnixTime) {
        return new Date(dateUnixTime);
    },
    
    dateISOString: function(dateStr) {
        return Render.dateFromString(dateStr).toISOString();
    },
    
    dateLocaleString: function(dateStr) {
        return Render.dateFromString(dateStr).toLocaleString();
    },
    
    toTitleCase: function(text) {
        var words = text.match(/[A-Za-z][a-z]*/g);
        return words.map(Render.capitalize).join(" ");
    },
    
    capitalize: function(word) {
        return word.charAt(0).toUpperCase() + word.substring(1);
    },
    
    invalidFields: function(fields, invalid) {
        
        var message = "<strong>Some fields are invalid:</strong><br><ul>";
        for(var i in invalid) {
            if(typeof(invalid[i].message) !== 'undefined') {
                message+= "<li>"+(typeof(fields[i].Label) !== 'undefined' ? fields[i].Label : Render.toTitleCase(i))+": "+invalid[i].message+"</li>";
            }
        }
        message+= "</ul>";
        
        return message;
    },
    
    formFields: function(form_name, fields, form_state, readonly_all) {
        //Debug.log(form_state);
        var $container = $("<div></div>");
        for(var field in fields) {
            var value = fields[field];
            
            if(value.Permission==="none") {continue;}
            
            var readonly = (value.Permission==="view" ? true : (readonly_all || false));
            
            if(value.Extra.toLowerCase().indexOf("auto_increment")===-1 && !readonly) {
                
                var required = (value.Null.toLowerCase().indexOf("no")!==-1 ? true : false);
                var label = (typeof(value.Label) !== 'undefined' ? value.Label : Render.toTitleCase(field));
                
                var $control = $(tpl.bootstrap);
                if(value.Class) {
                    $control.addClass(value.Class);
                }
                $control.find("label.control-label").html(label+(required ? " "+tpl.required : ""));
                var $input = "";
                
                if(value.Type.toLowerCase().indexOf("varchar")!==-1 || 
                        value.Type.toLowerCase().indexOf("textbox")!==-1 || 
                        value.Type.toLowerCase().indexOf("email")!==-1 || 
                        value.Type.toLowerCase().indexOf("double")!==-1 || 
                        value.Type.toLowerCase().indexOf("float")!==-1 || 
                        value.Type.toLowerCase().indexOf("decimal")!==-1 ||
                        value.Type.toLowerCase().indexOf("int")===0) {
                    // text
                    $input = $(tpl.input);
                    $input.attr("type","text");
                    $input.val(form_state[field]);
                } else if(value.Type.toLowerCase().indexOf("password")!==-1) {
                    // password
                    $input = $(tpl.input);
                    $input.attr("type","password");
                    $input.val(form_state[field]);
                } else if(value.Type.toLowerCase() === "color") {
                    // color
                    $input = $(tpl.input);
                    $input.attr("type","color");
                    $input.val(form_state[field]);
                } else if(value.Type.toLowerCase().indexOf("hidden")!==-1) {
                    // hidden
                    $input = $(tpl.input);
                    $input.attr("type","hidden");
                    $input.val(form_state[field]);
                } else if(value.Type.toLowerCase().indexOf("color_display")!==-1) {
                    // color display
                    $input = $(tpl.color_display);
                    $input.css("background-color",form_state[field]);
                } else if(value.Type.toLowerCase().indexOf("text")!==-1 || value.Type.toLowerCase().indexOf("textarea")!==-1) {
                    // textarea
                    $input = $(tpl.textarea);
                    $input.val(form_state[field]);
                } else if(value.Type.toLowerCase().indexOf("ckeditor")!==-1) {
                    // textarea
                    $input = $(tpl.textarea);
                    $input.val(form_state[field]);
                } else if(value.Type.toLowerCase().indexOf("address")!==-1) {
                    var $hidden = $(tpl.input);
                    $hidden.attr("type","hidden");
                    $hidden.val(form_state[field]);
                    
                    var $address = $(tpl.input);
                    $address.attr("type","text");
                    $address.attr("placeholder","Start typing an address");
                    
                    $input = $("<div></div>").append($address).append($hidden);
                } else if(value.Type.toLowerCase().indexOf("select_add")===0) {
                    // select list with add button
                    $input = $(tpl.select_add);
                    $select = $input.find("select");
                    for(var i in value.Items) {
                        var $option = $(tpl.option);
                        $option.val(i);
                        $option.html(value.Items[i][0]); // add label
                        if(form_state[field]==value.Items[i][0]) {
                            $option.prop("selected",true);
                        }
                        $option.attr("disabled",!value.Items[i][1]);
                        $select.append($option);
                    }
                    
                    for(var i in value.ItemsDefaultStart) {
                        var title = $select.find("option[value="+value.ItemsDefaultStart[i]+"]").html();
                        SelectAdd.AddDefaultStart(value.ItemsDefaultStart[i],title,$input);
                    }
                    
                    //console.log(form_state[field])
                    for(var i in form_state[field]) {
                        var title = $select.find("option[value="+form_state[field][i]+"]").html();
                        SelectAdd.Insert(form_state[field][i],title,$input);
                    }
                    
                    for(var i in value.ItemsDefaultEnd) {
                        var title = $select.find("option[value="+value.ItemsDefaultEnd[i]+"]").html();
                        SelectAdd.AddDefaultEnd(value.ItemsDefaultEnd[i],title,$input);
                    }
                } else if(value.Type.toLowerCase().indexOf("select")===0) {
                    // select
                    $input = $(tpl.select);
                    $select = $input.find("select");
                    for(var i in value.Items) {
                        var $option = $(tpl.option);
                        $option.val(i);
                        $option.html(value.Items[i][0]); // add label
                        if(form_state[field]==i) {
                            $option.prop("selected",true);
                        }
                        $option.attr("disabled",!value.Items[i][1]);
                        $select.append($option);
                    }
                } else if(value.Type.toLowerCase().indexOf("multi_select")!==-1) {
                    // select
                    $input = $(tpl.multi_select);
                    for(var i in value.Items) {
                        var $option = $(tpl.option);
                        $option.val(i);
                        $option.html(value.Items[i][0]); // add label
                        if(form_state[field].indexOf(i)!==-1) {
                            $option.prop("selected",true);
                        }
                        $option.attr("disabled",!value.Items[i][1]);
                        $input.append($option);
                    }
                } else if(value.Type.toLowerCase().indexOf("tinyint")!==-1 || value.Type.toLowerCase().indexOf("bool")!==-1) {
                    // boolean
                    $input = $(tpl.checkbox);
                    $input.val("1");
                    
                    $input.attr("checked",(form_state[field] == 1));
                } else if(value.Type.toLowerCase().indexOf("checkbox")!==-1) {
                    // checkbox list
                    $input = $("<ul class='checkbox-list'></ul>");
                    for(var i in value.Items) {
                        var $checkbox = $(tpl.checkbox);
                        var $label = $(tpl.label);
                        $checkbox.val(i);
                        if(form_state[field].indexOf(i.toString())!==-1) {
                            $checkbox.prop("checked",true);
                        }
                        $checkbox.attr("disabled",!value.Items[i][1]);
                        $label.append($checkbox).append(value.Items[i][0]); // add label
                        $input.append($("<li></li>").append($label));
                    }
                } else if(value.Type.toLowerCase().indexOf("timestamp")!==-1 || value.Type.toLowerCase().indexOf("date")!==-1 || value.Type.toLowerCase().indexOf("time")!==-1) {
                    // datepicker
                    $input = $(tpl.input);
                    $input.attr("type","text");
                    $input.val(form_state[field]);
                    
                    if(value.Type.toLowerCase() == "time") {
                        $input.datetimepicker({
                            format: "hh:mm:ss",
                            allowInputToggle: true
                        });
                    } else if(value.Type.toLowerCase() == "date") {
                        $input.datetimepicker({
                            format: "Y-MM-DD",
                            allowInputToggle: true
                        });
                    } else {
                        $input.datetimepicker({
                            format: "Y-MM-DD hh:mm:ss",
                            allowInputToggle: true
                        });
                    }
                } else if(value.Type.toLowerCase().indexOf("image")!==-1) {
                    // image upload
                    $input = $(tpl.input);
                    $input.attr("type","hidden");
                    $input.val(form_state[field]);
                } else if(value.Type.toLowerCase().indexOf("file")!==-1) {
                    // file upload
                    $input = $(tpl.input);
                    $input.attr("type","hidden");
                    $input.val(form_state[field]);
                } else {
                    $input = $("<span class='flat-text'>"+form_state[field]+"</span>");
                }
                
                $control.find("div.controls").append($input);
                $container.append($control);
                
                if(typeof($input)!=='string') {
                    if(required) {
                        $input.attr("required","required");
                    }
                    
                    if(value.Type.toLowerCase().indexOf("checkbox")!==-1) {
                        
                        $input.find("input:checkbox").attr("name",field+"[]");
                        
                    } else if(value.Type.toLowerCase().indexOf("select")===0) {
                        
                        $input.find("select").attr("name",field);
                        
                    } else if(value.Type.toLowerCase().indexOf("address")!==-1) {
                        
                        $input.find("input[type=hidden]").attr("name",field);
                        $input.find("input[type=hidden]").attr("id",field);
                        $input.find("input[type=text]").attr("name",field+"_autocomplete");
                        $input.find("input[type=text]").attr("id",field+"_autocomplete");
                        $input.on("focus",Address.geolocate);
                        $input.find("input[type=hidden]").after("<style>.pac-container{z-index:1100;}</style>");
                        
                    } else if(value.Type.toLowerCase().indexOf("image") !== -1 || value.Type.toLowerCase().indexOf("file") !== -1) {
                        
                        var $dropzone = $("<div class='generic-dropzone allow-files-drop drag-n-drop lazy-image'>Drag and drop files here or click to select</div>");
                        $dropzone.attr("data-target","[name="+field+"]");
                        $dropzone.attr("data-style","max-size");
                        $dropzone.attr("data-image-style","max-size");
                        $dropzone.attr("data-image-type","background");
                        $dropzone.attr("data-image-id",form_state[field]);
                        
                        $input.parent(".controls").append($dropzone);
                        
                        $input.attr("name",field);
                        $input.attr("id",field);
                        if(value.Placeholder) {
                            $input.attr("placeholder",value.Placeholder);
                        }
                        
                    } else if(value.Type.toLowerCase().indexOf("tinyint") === -1 || value.Type.toLowerCase().indexOf("bool") === -1) {
                        
                        $input.attr("name",field);
                        $input.attr("id",field);
                        if(value.Placeholder) {
                            $input.attr("placeholder",value.Placeholder);
                        }
                        
                    }
                }
            } else {
                
                $input = $("<span class='flat-text'>"+form_state[field]+"</span>");
                $control.addClass("is-filled").find("div.controls").append($input);
                $container.append($control);
            }
        }
        return $container.contents();
    },
    
    fieldMultiselect: function(form_name,field,value,form_state,readonly_all) {
        if(value.Permission==="none") {return;}
        
        var readonly = (value.Permission==="view" ? true : (readonly_all || false));

        if(value.Extra.toLowerCase().indexOf("auto_increment")===-1 && !readonly) {

            var required = (value.Null.toLowerCase().indexOf("no")!==-1 ? true : false);
            var label = Render.toTitleCase((typeof(value.Label) !== 'undefined' ? value.Label : field));

            var $control = $(tpl.bootstrap);
            if(label.trim() === "") {
                $control.find("label.control-label").remove();
            } else {
                $control.find("label.control-label").html(label+(required ? " "+tpl.required : ""));
            }
            var $input = "";

            // multi select
            $input = $(tpl.multi_select);
            for(var i in value.Items) {
                var $option = $(tpl.option);
                $option.val(value.Items[i][0]);
                $option.html(i);
                if(form_state[field]==value.Items[i][0]) {
                    $option.prop("selected",true);
                }
                $input.append($option);
            }

            if(typeof($input)!=='string') {
                $input.attr("name",form_name+"_"+field);
                $input.attr("id",form_name+"_"+field);
            }

            $control.find("div.controls").append($input);
            return $control;
        }
        return null;
    },
    
    menuLinks: function(links,level) {
        if(typeof(level)==="undefined") level = -1;
        level++;
        var output = "";
        for(var i = 0; i < links.length; i++) {
            var link = links[i];
            
            if(link == null) {continue;}
            var has_sub = (link.sub_menu !== null);
            var classes = (typeof(link.classes) !== 'undefined' ? link.classes : "");
            
            output+= "<li class='"+(has_sub && link.url !== "" ? "nav-group " : "")+(has_sub && link.url === "" ? "dropdown" : "")+(window.location.hash === link.url || (link.url === "#" && (window.location.hash === "#home" || window.location.hash === "#")) ? " active" : "")+"' >";
            if((has_sub && link.url === "") || !has_sub) {
                output+= "<a "+(has_sub ? "class='dropdown-toggle "+classes+"' data-toggle='dropdown'" : "class='"+classes+"'")+" data-menu='"+Render.slugify(link.menu_title)+"-menu' "+(link.url==="" ? "" : "href='"+(link.url[0] === "/" ? link.url : "#"+link.url)+"'")+">"+link.menu_title+" "+(has_sub && link.url === "" ? "<i class='fa fa-"+(level===0 ? "caret-down" : "caret-right")+"'></i>" : "")+"</a>";
            }
            if(has_sub && link.url !== "") {
                output+= "<a class='"+classes+"' href='#"+link.url+"'>"+link.menu_title+"</a>";
                output+= " <a class='dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>\n\
                                <span class='caret'></span>\n\
                                <span class='sr-only'>Toggle Dropdown</span>\n\
                            </a>";
            }
            if(has_sub) {
                output+= "<ul id='"+Render.slugify(link.menu_title)+"-menu' class='dropdown-menu' role='menu'>";
                output+= Render.menuLinks(link.sub_menu,level);
                output+= "</ul>";
            }
            output+= "</li>";
        }
        return output;
    },
    
    slugify: function(text) {
        return text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-')
            ;
    },
    
    list: function(headerField, columns,rows,row_count,actions,selected_limit,selected_page,total_rows,filter,total_filtered_rows) {
        var $table_group = $("<div id='generic-listing-wrapper'></div>");
        
        // build table top row inputs
        var $table_top = $("<div class='row'></div>");
        var $limit_select = $("<div class='col-auto'><label class='generic-listing-input'>Show "+tpl.table_limit+" entries</label></div>");
        $table_top.append($limit_select);
        $limit_select.find("select").val(selected_limit);
        var $filter_input = $("<div class='col-auto ml-auto'><label class='generic-listing-input'>Search: "+tpl.table_filter+"</label></div>");
        $table_top.append($filter_input);
        $filter_input.find("input").val(filter);
        
        // build table bottom row
        var $table_bottom = $("<div class='row'></div>");
        var pages = Math.ceil(total_rows/(selected_limit !== "All" ? selected_limit : total_rows));
        //console.log(pages)
        var page_start_from = (selected_page*(selected_limit !== "All" ? selected_limit : 1));
        var $num_rows = $("<div class='col-auto'>Showing "+(page_start_from+1)+" to "+(page_start_from+row_count)+" of "+(filter !== "" ? total_filtered_rows : total_rows)+" entries"+ (filter !== "" ? " ("+total_rows+" total entries)" : "")+"</div>")
        $table_bottom.append($num_rows);
        $table_bottom.append("<div class='col-auto ml-auto'>"+this.pager(selected_page,selected_limit,(filter !== "" ? total_filtered_rows : total_rows))+"</div>");
        
        // build table and contents
        var $table = $(tpl.list_generic);
        var $thead = $("<div></div>");
        var $tbody = $("<div></div>");
        $table.append($thead).append($tbody);
        /*var $row = $("<tr></tr>");
        var count = 0;*/
        var max = Object.keys(columns).length;
        /*for(var field in columns) {
            count++;
            if((count < 9 || count === max) && columns[field].Type!=="ckeditor") {
                $row.append("<th>"+(columns[field].Label ? columns[field].Label : Render.toTitleCase(field))+"</th>");
            }
        }
        if(actions.length) {
            $row.append("<th>Actions</th>");
        }
        $thead.append($row);*/

        var sortColumn = 0;
        var keys = Object.keys(rows).sort(function(a,b){return a-b;});
        //console.log(rows)
        //console.log(keys)
        for(var i in keys) {
            i = keys[i];
            
            var $card = $("<div class='card margin-bottom-sml'></div>");
            var $cardHeader = $("<h5 class='card-header'>"+rows[i][headerField]+"</h5>");
            var $cardBody = $("<div class='card-body'></div>");
            var $row = $("<div class='row'></div>");
            
            count = 0;
            for(var field in columns) {
                if(field=="createDate") {
                    sortColumn = count;
                }
                count++;
                if((count < 12 || count == max) && field !== headerField) {
                    var $col = $("<div class='col-6'></div>");
                    
                    // add a label
                    $col.append("<b class='d-block d-sm-inline'>"+(columns[field].Label ? columns[field].Label : Render.toTitleCase(field))+":</b> ");
                    
                    if(columns[field].Type.indexOf("image") !== -1) {
                        if(rows[i][field] && rows[i][field].length > 0) {
                            if(columns[field].Type.toLowerCase() === "image") {
                                $col.append("<img src='"+App.url+"file/"+rows[i][field]+"?style=admin-table'/>");
                            } else {
                                for(var image in rows[i][field].split(",")) {
                                    $col.append("<img src='"+App.url+"file/"+image+"?style=admin-table'/>");
                                }
                            }
                        } else {
                            $col.append("");
                        }
                    } else if(columns[field].Type==="ckeditor") {
                        var val = atob(rows[i][field]);
                        val = rows[i][field].length > 1024 ? val.substr(0,1024)+"..." : val;
                        $col.append(val);
                    } else if(columns[field].Type.toLowerCase().indexOf("tinyint")!==-1 || columns[field].Type==="boolean") {
                        $col.append(rows[i][field]==1 ? "Yes" : "No");
                    } else if((columns[field].Type.indexOf("select")!==-1 || columns[field].Type==="multi_select") && columns[field].ItemsLabeled) {
                        $col.append(columns[field].ItemsLabeled[rows[i][field]]);
                    } else if(columns[field].Type.indexOf("color_display")!==-1) {
                        $col.append("<span class='colour-display' style='background-color:"+rows[i][field]+";'></span>");
                    } else {
                        if(rows[i][field]) {
                            var val = rows[i][field].length > 1024 ? rows[i][field].substr(0,1024)+"..." : rows[i][field];
                            if(columns[field].LeftPad) {
                                val = (val+"").leftPad(columns[field].LeftPad[0],columns[field].LeftPad[1]);
                            }
                            if(columns[field].Prefix) {
                                val = columns[field].Prefix + val;
                            }
                            $col.append(val);
                        } else {
                            $col.append("");
                        }
                    }
                    $row.append($col);
                }
            }
            
            $card.append($cardHeader).append($cardBody.append($row));
            
            if(actions.length) {
                var $actions = $("<div class='card-footer text-right'></div>");
                for(var j in actions) {
                    var action = $(tpl.list_action).attr("data-action",actions[j].Action).attr("data-item-id",rows[i][actions[j].IdField]).addClass(actions[j].Class).html(actions[j].Label);
                    $actions.append(action);
                }
                $card.append($actions);
                //$row.append($actions);
            }
            
            
            $tbody.append($card);
        }
        
        $table_group.append($table_top).append($table).append($table_bottom);
        
        return {table:$table_group,sortColumn:sortColumn};
    },
    
    table: function(columns,rows,row_count,actions,selected_limit,selected_page,total_rows,filter,total_filtered_rows) {
        var $table_group = $("<div id='generic-listing-wrapper'></div>");
        
        // build table top row inputs
        var $table_top = $("<div class='row'></div>");
        var $limit_select = $("<div class='col-auto'><label class='generic-listing-input'>Show "+tpl.table_limit+" entries</label></div>");
        $table_top.append($limit_select);
        $limit_select.find("select").val(selected_limit);
        var $filter_input = $("<div class='col-auto ml-auto'><label class='generic-listing-input'>Search: "+tpl.table_filter+"</label></div>");
        $table_top.append($filter_input);
        $filter_input.find("input").val(filter);
        
        // build table bottom row
        var $table_bottom = $("<div class='row'></div>");
        var pages = Math.ceil(total_rows/(selected_limit !== "All" ? selected_limit : total_rows));
        //console.log(pages)
        var page_start_from = (selected_page*(selected_limit !== "All" ? selected_limit : 1));
        var $num_rows = $("<div class='col-auto'>Showing "+(page_start_from+1)+" to "+(page_start_from+row_count)+" of "+(filter !== "" ? total_filtered_rows : total_rows)+" entries"+ (filter !== "" ? " ("+total_rows+" total entries)" : "")+"</div>")
        $table_bottom.append($num_rows);
        $table_bottom.append("<div class='col-auto ml-auto'>"+this.pager(selected_page,selected_limit,(filter !== "" ? total_filtered_rows : total_rows))+"</div>");
        
        // build table and contents
        var $table = $(tpl.table_generic);
        var $thead = $("<thead></thead>");
        var $tbody = $("<tbody></tbody>");
        $table.append($thead).append($tbody);
        var $row = $("<tr></tr>");
        var count = 0;
        var max = Object.keys(columns).length;
        for(var field in columns) {
            count++;
            if((count < 9 || count === max)) {
                $row.append("<th>"+(columns[field].Label ? columns[field].Label : Render.toTitleCase(field))+"</th>");
            }
        }
        
        if(actions.length) {
            $row.append("<th>Actions</th>");
        }
        $thead.append($row);

        var sortColumn = 0;
        var keys = Object.keys(rows).sort(function(a,b){return a-b;});
        //console.log(rows)
        //console.log(keys)
        for(var i in keys) {
            i = keys[i];
            var $row = $("<tr></tr>");
            count = 0;
            for(var field in columns) {
                if(field=="createDate") {
                    sortColumn = count;
                }
                count++;
                if((count < 9 || count == max)) {
                    var $col = $("<td></td>");
                    if(columns[field].Type.indexOf("image") !== -1) {
                        if(rows[i][field] && rows[i][field].length > 0) {
                            if(columns[field].Type.toLowerCase() === "image") {
                                $col.append("<img src='"+App.url+"file?"+rows[i][field]+"&style=admin-table'/>");
                            } else {
                                for(var image in rows[i][field].split(",")) {
                                    $col.append("<img src='"+App.url+"file?"+image+"&style=admin-table'/>");
                                }
                            }
                        } else {
                            $col.append("");
                        }
                    } else if(columns[field].Type==="ckeditor") {
                        var val = "";
                        try {
                            val = atob(rows[i][field]);
                        } catch(e) {
                            //console.log(e)
                            val = rows[i][field];
                        }
                        val = rows[i][field].length > 1024 ? val.substr(0,1024)+"..." : val;
                        $col.append(val);
                    } else if(columns[field].Type.toLowerCase().indexOf("tinyint")!==-1 || columns[field].Type==="boolean") {
                        $col.append(rows[i][field]==1 ? "<i class='fa fa-check'></i>" : "");
                    } else if((columns[field].Type.indexOf("select")!==-1 || columns[field].Type==="multi_select") && columns[field].ItemsLabeled) {
                        $col.append(columns[field].ItemsLabeled[rows[i][field]]);
                    } else if(columns[field].Type.indexOf("color_display")!==-1) {
                        $col.append("<span class='colour-display' style='background-color:"+rows[i][field]+";'></span>");
                    } else if(columns[field].Type.indexOf("JobNumber")!==-1) {
                        $col.append(Render.jobNumber(rows[i][field]));
                    } else {
                        if(rows[i][field]) {
                            var val = rows[i][field].length > 50 ? rows[i][field].substr(0,50)+"..." : rows[i][field];
                            if(columns[field].LeftPad) {
                                val = (val+"").leftPad(columns[field].LeftPad[0],columns[field].LeftPad[1]);
                            }
                            if(columns[field].Prefix) {
                                val = columns[field].Prefix + val;
                            }
                            $col.append(val);
                        } else {
                            $col.append("");
                        }
                    }
                    $row.append($col);
                }
            }
            
            if(actions.length) {
                var $actions = $("<td></td>");
                for(var j in actions) {
                    var action = $(tpl.table_action).attr("data-action",actions[j].Action).attr("data-item-id",rows[i][actions[j].IdField]).addClass(actions[j].Class).html(actions[j].Label);
                    $actions.append(action);
                }
                $row.append($actions);
            }
            
            $tbody.append($row);
        }
        
        $table_group.append($table_top).append($table).append($table_bottom);
        
        return {table:$table_group,sortColumn:sortColumn};
    },
    
    pager: function(page,limit,total_rows) {
        var items = 3;
        var body = "";
        
        if(total_rows > limit) {
            var pages = Math.ceil(total_rows/limit);
            
            body+= "<nav>";
            body+= "<ul class='pagination'>";
            
            for($x=-items;$x<=items;$x++) {
                var pageNum = page+$x;
                if(pageNum >= 0 && pageNum < pages) {
                    
                    body+= "<li class='page-item "+ (pageNum==page ? "active" : "")+"'>";
                    
                    if($x==-items && pageNum!=0) {
                        body+= "<a class='page-link' href='#' data-page='0'><i class='fa fa-angle-double-left'></i></a>";
                    } else if($x==items && pageNum!=pages-1) {
                        body+= "<a class='page-link' href='#' data-page='"+(pages-1)+"'><i class='fa fa-angle-double-right'></i></a>";
                    } else {
                        body+= "<a class='page-link' href='#' data-page='"+pageNum+"'>";
                        body+= (pageNum==page ? ""+(pageNum+1)+ " / "+ pages : (pageNum+1))+ " <span class='sr-only'>(current)</span></a>";
                    }
                    
                    body+= "</li>";
                }
            }
            
            body+= "</ul>";
            body+= "</nav>";
        }
        
        return body;
    }
};