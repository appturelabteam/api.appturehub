function Page() {
    this.element = document.createElement("div");
    this.dataValues = { /* AppName: App.appName */ };
    this.htmlView = "app/views/page.html";
    this.footerView = null;
    this.html = "";
    this.created = false;

    this.element.removeAfterHide = true;
    
    this.setRemoveAfterHide = function (removeAfterHide) {
        this.element.removeAfterHide = removeAfterHide;
    };
    
    this.getRemoveAfterHide = function () {
        return this.element.removeAfterHide;
    };
    
    /*
    Called by router after the route has returned a Page object.
        
    The onCreateCallback callback is passed in by the router which adds the page to the (DOM) page
    stack without content and starts the pageslider functionality in order to bring the page into view.

    At this point the page's content is not ready to manipulate yet, wait for onReady.

    Page loading process:
        _onCreate -> getDataValues -> _mergeDataAndHtml -> _setHTML -> onReady
    */
    this._onCreate = function (onCreateCallback) {
        
        if(this.created === false) {
            this.getDataValues(this._getDataValuesDone);
        }
        
        // we must call this function
        onCreateCallback(this);
        
        this.created = true;
    };

    /*
    Load all data here into this.dataValues.
    If overriden, remember the callback so the page creation process can continue.
    */
    this.getDataValues = function (done) {
        // remember the callback so the page creation process can continue
        done();
    };

    this._getDataValuesDone = function () {
        this._mergeDataAndHTML();
    }.bind(this);

    /*
    Called when data is loaded and html content is ready.
    Virtual.
    */
    this.onReady = function () { };
    
    /*
    Called when page slider css animation starts to make page visible.
    Virtual.
    */
    this.onShow = function () { };
    
    /*
    Called when page slider css animation is complete and page is fully visible.
    Virtual.
    */
    this.onShown = function () { };

    /*
    Called when page slider css animation for hiding a page, starts.
    Virtual.
    */
    this.onHide = function () { };
    
    /*
    Called when page slider css animation for hiding a page, ends.
    Virtual.
    */
    this.onHidden = function () { };
    
    /*
    Called when page is same as current.
    Virtual.
    */
    this.onRecall = function (args,hash) { console.log("recall "+hash) };
    

    /*
    Fired by window.onbeforeunload event.
    If the return is not true, a prompt will be created by the browser. Its behaviour is Browser dependent.
    */
    this.onBeforeUnload = function (e) {
        return true;
    };

    this.refresh = function () {
        this.getDataValues(this._getDataValuesDone);
    };

    this._mergeDataAndHTML = function () {
        
        var page = this;
        
        if (this.footerView) {
            
            template.load(page.footerView, page.dataValues, function (footer_content) {
                
                // If a view has been specified ignore page.html and load the view instead
                if (page.htmlView) {
                    
                    template.load(page.htmlView, page.dataValues, function (content) {
                        // pass generated html into setContent function
                        page._setHTML(content+footer_content);
                    });

                } else {

                    // pass generated html into setContent function
                    page._setHTML(template.dataValues(page.html, page.dataValues)+footer_content);
                    
                }
                
            });
            
        } else {
            
            // If a view has been specified ignore page.html and load the view instead
            if (page.htmlView) {

                template.load(page.htmlView, page.dataValues, function (content) {
                    // pass generated html into setContent function
                    page._setHTML(content);
                });

            } else {

                // pass generated html into setContent function
                page._setHTML(template.dataValues(page.html, page.dataValues));

            }
            
        }
    };

    this._setHTML = function (content) {
        var page_content = this.element.querySelector(".content") || document.createElement("div");
        page_content.innerHTML = content;
        addClass(page_content, "content");
        this.element.appendChild(page_content);
        this.onReady();
    };
    
    this.handleEvent = function(event,args) {
        if (event.target === this.element) {
            if('webkitTransitionEnd transitionend oTransitionEnd'.indexOf(event.type) > -1) {
                
                if (this.shown === false) {
                    
                    this.shown = true;
                    this.onShown();
                    
                } else if(this.shown === true) {
                    
                    this.shown = false;
                    this.onHidden();
                    
                }
                
            }
            
        }
    }
}