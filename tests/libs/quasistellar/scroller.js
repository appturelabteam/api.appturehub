var Scroller = function(container) {
    var self = this;
    self.scrollCallback = null;
    self.scrollEl = null;
    self.scrollTop = 0.0;
    self.scrollOffset = 0.0;
    self.active = false;
    self.close = 0;
    self.speed = 0.5;
    self.container = container || window;
    
    self.scrollLoop = function (){
        //console.log("looping")
        if(self.active) {
            requestAnimFrame(self.scrollLoop);
            self.scroll();
        }
    };
    
    self.scroll = function() {
        var currentTop  = (self.container === window ? (window.pageYOffset || document.documentElement.scrollTop) : self.container.scrollTop);
        
        self.scrollTop = Math.max(0,elOffset(self.scrollEl, self.container).top + self.scrollOffset);
        
        //console.log(self.scrollTop,currentTop);
        
        //console.log(Math.max(0.25,(currentTop-self.scrollTop)*0.25))
        var dif = Math.abs((currentTop - self.scrollTop));
        if(dif !== 0 && self.close < 5) {
            //console.log("scroll amount "+(currentTop-self.scrollTop));
            var change = ( currentTop > self.scrollTop ? Math.min(-1,-(dif*self.speed)) : Math.max(1,(dif*self.speed)) );
            if(Math.abs((currentTop+change) - self.scrollTop) < self.speed * 50) {
                self.close ++;
            }
            
            if(self.close > 5) {
                container.scrollTo(0,self.scrollTop);
            } else {
                container.scrollTo(0,currentTop+change);
            }
        } else {
            self.active = false;
            self.close = 0;
            if(self.scrollCallback !== null) {
                self.scrollCallback(self.scrollTop);
            }
        }
    };
    
    self.scrollTo = function(el,scrollOffset) {
        var currentTop  = (self.container === window ? (window.pageYOffset || document.documentElement.scrollTop) : self.container.scrollTop);
        
        self.scrollEl = el;
        self.scrollOffset = scrollOffset;
        self.scrollTop = Math.max(0,elOffset(self.scrollEl).top + self.scrollOffset);
        
        //console.log(self.scrollTop,currentTop);
        
        if(self.scrollTop !== currentTop) {
            self.active = true;
            self.close = 0;
            self.scrollLoop();
        }
    };
    
    self.scrollInputEvent = function(e) {
        self.active = false;
        if(self.scrollCallback !== null) {
            self.scrollCallback(self.scrollTop);
        }
    };
};

// shim layer with setTimeout fallback
window.requestAnimFrame = (function(){
  return function( callback ){
            window.setTimeout(callback, 1000 / 30);
         };
})();