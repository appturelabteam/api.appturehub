/**
 * @author Dewald Bodenstein
 * @description From an example by Christophe Coenraets, {@link http://coenraets.org/blog/2013/03/hardware-accelerated-page-transitions-for-mobile-web-apps-phonegap-apps/|Link to article}.
 * Converted to plain js by Dewald Bodenstein, May 2017.
 * @class
 * @requires plain.js
 * 
 *  Notes:
 * - History management is currently done using window.location.hash.  This could easily be changed to use Push State instead.
 */
function PageSlider() {

    var currentPage,
        stateHistory = [];

    /**
     * @description Use this function if you want PageSlider to automatically determine the sliding direction based on the state history
     * 
     * @param {object} page Page object, see {@link App}'s default pages.
     */
    this.slidePage = function(page) {
        
        var l = stateHistory.length,
            state = window.location.hash;

        if (l === 0) {
            stateHistory.push(state);
            this.slidePageFrom(page, null);
            return;
        }
        
        var from;
        if(typeof page.slideFrom !== 'undefined') {
            from = page.slideFrom;
        } else if (state === stateHistory[l-2]) {
            from = 'left';
        } else {
            from = 'right';
        }
        
        if(from === 'left') {
            stateHistory.pop();
        } else {
            stateHistory.push(state);
        }
        this.slidePageFrom(page, from);
    };

    /**
     * @description Use this function directly if you want to control the sliding direction outside PageSlider
     * 
     * Triggers the following events on the page object, by calling these
     * functions if they exist on the page object: onCreate(page), onShown(page) and onHide(page).
     * 
     * @param {object} page
     * @param {string} from
     */
    this.slidePageFrom = function (page, from) {
        
        page.shown = false;
        
        if (!currentPage || !from) {
            page.element.className = "page center";
            page.shown = true;
            
            if(typeof page.onShown !== 'undefined' && page.onShown !== null) {
                page.onShown(page);
            }
            
            currentPage = page;
            return;
        }

        // Position the page at the starting position of the animation
        page.element.className = "page " + from;
        
        addEventsListener(currentPage.element, 'webkitTransitionEnd transitionend oTransitionEnd', function(e) {
            // The event may get fired more than once if you have transition on 
            // multiple properties, ie using opacity and transform will fire 
            // this event twice, therefore we make sure its not been removed.
            if(e.target.parentNode && e.target.className.indexOf("page") > -1) {
                if(e.target.removeAfterHide) {
                    e.target.parentNode.removeChild(e.target);
                }
            }
        });
        /*currentPage.on('webkitTransitionEnd transitionend oTransitionEnd', function(e) {
            $(e.target).remove();
        });*/

        // Force reflow. More information here: http://www.phpied.com/rendering-repaint-reflowrelayout-restyle/
        container.offsetWidth;
        
        // fire the onHide event for the current page because we are going to start hiding the page now
        if(typeof currentPage.onHide !== 'undefined' && currentPage.onHide !== null) {
            currentPage.onHide(currentPage);
        }
        // add the onHidden event listener for the current page
        if(typeof currentPage.onHidden !== 'undefined' && currentPage.onHidden !== null) {
            currentPage.shown = true;
            addEventsListener(currentPage.element, 'webkitTransitionEnd transitionend oTransitionEnd', currentPage, {once:true});
        }

        // Position the new page and the current page at the ending position of their animation with a transition class indicating the duration of the animation
        page.element.className = "page transition center";
        currentPage.element.className = "page transition " + (from === "left" ? "right" : "left");
        currentPage = page;
        
        if(typeof page.onShow !== 'undefined' && page.onShow !== null) {
            page.onShow();
        }
        
        if(typeof page.onShown !== 'undefined' && page.onShown !== null) {
            page.shown = false;
            addEventsListener(page.element, 'webkitTransitionEnd transitionend oTransitionEnd', page, {once:true});
        }
    };
    
}