var Forms = {
    init: function(fields,fieldsdata) {
        var form_state = {};
        form_state["invalid"] = {}; // initialize validation array
        
        for(var field in fields) {
            var value = fields[field];
            
            if(value.Type.toLowerCase().indexOf("varchar")!==-1 || 
                    value.Type.toLowerCase().indexOf("textbox")!==-1 || 
                    value.Type.toLowerCase().indexOf("text")!==-1 || 
                    value.Type.toLowerCase().indexOf("color")!==-1 || 
                    value.Type.toLowerCase().indexOf("textarea")!==-1 ||
                    value.Type.toLowerCase().indexOf("ckeditor")!==-1 || 
                    value.Type.toLowerCase().indexOf("email")!==-1 || 
                    value.Type.toLowerCase().indexOf("float")!==-1 || 
                    value.Type.toLowerCase().indexOf("decimal")!==-1 ||
                    value.Type.toLowerCase().indexOf("hidden")!==-1 ||
                    value.Type.toLowerCase().indexOf("username")!==-1 ||
                    value.Type.toLowerCase().indexOf("int")===0 ||
                    value.Type.toLowerCase().indexOf("double")===0 ||
                    value.Type.toLowerCase().indexOf("integer")===0 ||
                    value.Type.toLowerCase().indexOf("address")!==-1 || 
                    value.Type.toLowerCase().indexOf("file")===0 ||
                    value.Type.toLowerCase().indexOf("image")===0 ||
                    value.Type.toLowerCase().indexOf("multi_image")===0 ||
                    value.Type.toLowerCase().indexOf("tinyint")===0 ||
                    value.Type.toLowerCase().indexOf("radio")===0 ||
                    value.Type.toLowerCase().indexOf("boolean")===0) {
                // text
                form_state[field] = (fieldsdata ? fieldsdata[field] : (value["Default"] === null ? "" : value["Default"]));
            } else if(value.Type.toLowerCase().indexOf("password")===0) {
                // password
                form_state[field] = "";
            } else if(value.Type.toLowerCase().indexOf("checkbox")===0 ||
                    value.Type.toLowerCase().indexOf("select")===0 ||
                    value.Type.toLowerCase().indexOf("multi_select")===0) {
                // checkbox or select
                if(fieldsdata) {
                    form_state[field] = (
                            typeof(fieldsdata[field]) === 'string' ? fieldsdata[field].split(",") : 
                            (fieldsdata[field] === null ? [] : fieldsdata[field])
                        );
                } else {
                    form_state[field] = (typeof(value["Default"]) === 'string' ? value["Default"].split(",") : (value["Default"] === null ? "" : value["Default"]));
                    /*foreach($value['Items'] as $itemLabel=>$itemValue) {
                        if($itemValue[1]===true) {
                            Forms::$form_state[$form_name][$key][] = $itemValue[0];
                        }
                    }*/
                }
            } else if(value.Type.toLowerCase().indexOf("timestamp")!==false ||
                    value.Type.toLowerCase().indexOf("date")===0 ||
                    value.Type.toLowerCase().indexOf("datetime")===0) {
                // datepicker
                form_state[field] = (fieldsdata ? fieldsdata[field] : (value["Default"] == "CURRENT_TIMESTAMP" ? date() : value["Default"]));
            }
        }
        
        return form_state;
    },
    
    getValues: function(formName, fields, validate, skip_null) {
        //console.log(fields)
        var fieldsData = {};
        if(validate === true) {
            fieldsData.invalid = {};
            fieldsData.invalid.invalid_amount = 0;
        }
        for(var field in fields) {
            var value = fields[field];
            var $input = $("form[name='"+formName+"'] [name='"+field+"']");
            
            //if(value.Permission !== "edit") {continue;}
            
            if(value.Type.toLowerCase().indexOf("varchar")!==-1 || 
                    value.Type.toLowerCase().indexOf("textbox")!==-1 || 
                    value.Type.toLowerCase() === "color" || 
                    value.Type.toLowerCase().indexOf("email")!==-1 || 
                    value.Type.toLowerCase().indexOf("float")!==-1 || 
                    value.Type.toLowerCase().indexOf("decimal")!==-1 ||
                    value.Type.toLowerCase().indexOf("double")!==-1 ||
                    value.Type.toLowerCase().indexOf("hidden")!==-1 ||
                    value.Type.toLowerCase().indexOf("int")===0 ||
                    value.Type.toLowerCase() === "image" ||
                    value.Type.toLowerCase() === "file_id") {
                
                // text
                fieldsData[field] = $input.val();
            } else if(value.Type.toLowerCase().indexOf("password")!==-1) {
                // password
                fieldsData[field] = $input.val();
            } else if(value.Type.toLowerCase().indexOf("color_display")!==-1) {
                // color display
                fieldsData[field] = $input.css("background-color");
            } else if(value.Type.toLowerCase().indexOf("text")!==-1 || value.Type.toLowerCase().indexOf("ckeditor")!==-1 || value.Type.toLowerCase().indexOf("textarea")!==-1) {
                // textarea
                fieldsData[field] = $input.val();
            } else if(value.Type.toLowerCase().indexOf("address")!==-1) {
                // address
                fieldsData[field] = $input.val();
            } else if(value.Type.toLowerCase().indexOf("select_add")===0) {
                // select add
                //console.log($input)
                fieldsData[field] = $input.parent(".combobox").parent(".select-add").find("input").val();
            } else if(value.Type.toLowerCase().indexOf("select")===0) {
                // select
                fieldsData[field] = $input.val();
            } else if(value.Type.toLowerCase().indexOf("tinyint")!==-1 || value.Type.toLowerCase().indexOf("bool")!==-1) {
                // boolean
                fieldsData[field] = ($input.prop("checked") ? "1" : "0");
            } else if(value.Type.toLowerCase().indexOf("checkbox")!==-1) {
                // checkbox list
                $input = $("form[name='"+formName+"'] [name='"+field+"[]']");
                fieldsData[field] = new Array();
                $input.each(function(){
                    if($(this).prop("checked")) {
                        fieldsData[field].push($(this).val());
                    }
                });
            } else if(value.Type.toLowerCase().indexOf("multi_select")===0) {
                // multi_select list
                fieldsData[field] = new Array();
                $input.find(":selected").each(function(){
                    fieldsData[field].push($(this).val());
                });
            } else if(value.Type.toLowerCase().indexOf("radio")!==-1) {
                // radio list
                if($("form[name='"+formName+"'] [name='"+field+"']:checked").length) {
                    fieldsData[field] = $("form[name='"+formName+"'] [name='"+field+"']:checked").val();
                } else {
                    fieldsData[field] = null;
                }
                
            } else if(value.Type.toLowerCase().indexOf("timestamp")!==-1 || value.Type.toLowerCase().indexOf("date")!==-1 || value.Type.toLowerCase().indexOf("time")!==-1) {
                // datepicker
                fieldsData[field] = $input.val();
            }/* else if(stripos($value["Type"],"image")===0) {
                // image upload
                $output.= Render::inputImage($form_name, $field, $label, $fieldsData[$field], (isset($fieldsData["invalid"][$field]) ? $fieldsData["invalid"][$field] : ""), $required, "", $readonly);
            } else if(stripos($value["Type"],"file")===0) {
                // image upload
                $output.= Render::inputFile($form_name, $field, $label, $fieldsData[$field], (isset($fieldsData["invalid"][$field]) ? $fieldsData["invalid"][$field] : ""), $required, "", $readonly);
            }*/ else {
                //$input = form_state[field];
            }
            //console.log(fieldsData.invalid)
            if(validate===true) {
                var result = Forms.validateField(formName, field, value, fieldsData[field], skip_null);
                if(result !== true) {
                    fieldsData.invalid[field] = result;
                    fieldsData.invalid.invalid_amount ++;
                }
            }
        }
        return fieldsData;
    },
    
    validateField: function(formName, field, details, value, skip_null) {
        //console.log(field+" "+(typeof(skip_null) === 'undefined' || skip_null === false))
        if( (typeof(skip_null) === 'undefined' || skip_null === false) && details.Null.toLowerCase().indexOf('no')!==-1 ) {
            // see if valid
            var error = true;
            if(
                value &&
                (
                    ( typeof(value) === "object" && (value.length && value.indexOf('')===-1 && value.indexOf('0')===-1)) ||
                    ( typeof(value) !== "object" && 
                        (
                            (
                                (details.Type.toLowerCase().indexOf('int')===0 ||
                                details.Type.toLowerCase().indexOf('float')===0 ||
                                details.Type.toLowerCase().indexOf('double')===0) && !isNaN(value)
                            ) || 
                            (
                                (details.Type.toLowerCase().indexOf('file_id')===0 ||
                                details.Type.toLowerCase() === "select") && value != 0 && value != ""
                            ) || value != false
                        )
                    )
                )
                ) {
                error = false;
            }
            if(error ) {
                return {
                    'status':'error',
                    'message':'This field is required'
                };
            }
        }

        /*if(details.Type.indexOf('typeahead')===0) {
            if(!array_key_exists(strtolower(Forms::$form_state[$form_name][$field]), array_change_key_case($fields[$field]['Items']))) {
                Forms::$form_state[$form_name]["invalid"][$field]=array(
                    'status'=>'error',
                    'message'=>'Value not found.'
                );
            } else {
                Forms::$form_state[$form_name][$field] = $fields[$field]['Items'][Forms::$form_state[$form_name][$field]][0];
            }
        }*/
        
        if(details.Type.toLowerCase().indexOf('email')===0) {
            var parts = value.split("@");
            var domParts = (parts.Length > 1 ? parts[1].split(".") : [""]);
            if(value !== "" && parts.Length === 2 && domParts.Length === 2 && (
                    parts[1].Length > 2 && domParts[1].Length > 2 && domParts[2].Length > 2)) {
                return {
                    'status': 'error',
                    'message': 'Not a valid email address.'
                };
            }/* else if(stripos($fields[$field]['Extra'],'account')!==false){
                if(count(DS::select("users", "WHERE email=?s",Forms::$form_state[$form_name][$field]))) {
                    Forms::$form_state[$form_name]["invalid"][$field]=array(
                        'status'=>'error',
                        'message'=>'An account with this address already exists.'
                    );
                }
            }*/
        }
        
        if(details.Type.toLowerCase().indexOf('password')===0) {
            var $confPass = $("form[name='"+formName+"'] [name='confirm_"+field+"']");
            if($confPass.length > 0) {
                if(value !== $confPass.val()) {
                    return {
                        'status': 'error',
                        'message': 'Passwords do not match.'
                    };
                }
            }
        }
        
        if(details.Type.toLowerCase().indexOf('datepicker')===0 && !Date.parse(value)) {
            return {
                'status': 'error',
                'message': 'Not a valid date.'
            };
        }
        
        if(details.Type.toLowerCase().indexOf('int')===0) {
            if(isNaN(value)) {
                return {
                    'status': 'error',
                    'message': 'Not a valid integer value.'
                };
            } else if(typeof(details.Min) !== 'undefined' && value < parseInt(details.Min)) {
                return {
                    'status': 'error',
                    'message': 'Minimum is '+details.Min
                };
            } else if(typeof(details.Max) !== 'undefined' && value > parseInt(details.Max)) {
                return {
                    'status': 'error',
                    'message': 'Maximum is '+details.Max
                };
            }
        }
        
        if(details.Type.toLowerCase().indexOf('float')===0) {
            if(isNaN(value)) {
                return {
                    'status': 'error',
                    'message': 'Not a valid floating point number.'
                };
            } else if(typeof(details.Min) !== 'undefined' && value < parseFloat(details.Min)) {
                return {
                    'status': 'error',
                    'message': 'Minimum is '+details.Min
                };
            } else if(typeof(details.Max) !== 'undefined' && value > parseFloat(details.Max)) {
                return {
                    'status': 'error',
                    'message': 'Maximum is '+details.Max
                };
            }
        }
        
        if(details.Type.toLowerCase().indexOf('double')===0) {
            if(isNaN(value)) {
                return {
                    'status': 'error',
                    'message': 'Not a valid double value.'
                };
            } else if(typeof(details.Min) !== 'undefined' && value < parseFloat(details.Min)) {
                return {
                    'status': 'error',
                    'message': 'Minimum is '+details.Min
                };
            } else if(typeof(details.Max) !== 'undefined' && value > parseFloat(details.Max)) {
                return {
                    'status': 'error',
                    'message': 'Maximum is '+details.Max
                };
            }
        }
        
        return true;
    },
    
    updateForm: function(formName,fields,form_state) {
        $("form[name='"+formName+"'] .invalid-feedback").remove();
        for(var field in fields) {
            var value = fields[field];
            var $input = $("form[name='"+formName+"'] [name="+field+"]");
            
            //if(value.Permission !== "edit") {continue;}
            //Debug.log(value.Type.toLowerCase().indexOf("multi_select"))
            if(value.Type.toLowerCase().indexOf("varchar")!==-1 || 
                    value.Type.toLowerCase().indexOf("textbox")!==-1 || 
                    value.Type.toLowerCase().indexOf("email")!==-1 || 
                    value.Type.toLowerCase().indexOf("float")!==-1 || 
                    value.Type.toLowerCase().indexOf("double")!==-1 || 
                    value.Type.toLowerCase().indexOf("decimal")!==-1 ||
                    value.Type.toLowerCase().indexOf("hidden")!==-1 ||
                    value.Type.toLowerCase().indexOf("int")===0 ||
                    value.Type.toLowerCase() === "file_id") {
                // text
                $input.val(form_state[field]);
            } else if(value.Type.toLowerCase().indexOf("password")!==-1) {
                // password
                $input.val(form_state[field]);
            } else if(value.Type.toLowerCase().indexOf("text")!==-1 || value.Type.toLowerCase().indexOf("textarea")!==-1) {
                // textarea
                $input.val(form_state[field]);
            } else if(value.Type.toLowerCase().indexOf("address")!==-1) {
                // address
                $input.find("input[type=hidden]").val(form_state[field]);
            } else if(value.Type.toLowerCase().indexOf("select_add")===0) {
                // select add
                //$input.val(form_state[field]);
            } else if(value.Type.toLowerCase().indexOf("select")===0) {
                // select
                $input.val(form_state[field]);
            } else if(value.Type.toLowerCase().indexOf("tinyint")!==-1 || value.Type.toLowerCase().indexOf("bool")!==-1) {
                // boolean
                if($input.val()==form_state[field]) {
                    $input.prop("checked",true);
                } else {
                    $input.prop("checked",false);
                }
            } else if(value.Type.toLowerCase().indexOf("multi_select")!==-1) {
                // multi_select
                for(var l in value.Items) {
                    if(form_state[field].indexOf(value.Items[l][0]) !== -1) {
                        $input.find("option").prop("selected",true);
                    }
                }
            } else if(value.Type.toLowerCase().indexOf("checkbox")!==-1) {
                // checkbox list
                $input = $("form[name='"+formName+"'] [name='"+field+"[]']");
                $input.each(function(){
                    var $this = $(this);
                    if(form_state[field].indexOf($this.val().toString()) !== -1) {
                        $this.prop("checked",true);
                    }
                });
                //$output.= Render::inputCheckbox($form_name, $field, $label, $fieldsData[$field], $value["Items"], (isset($fieldsData["invalid"][$field]) ? $fieldsData["invalid"][$field] : ""), false, "", $readonly);
            } else if(value.Type.toLowerCase().indexOf("radio")!==-1) {
                // radio list
                $("form[name='"+formName+"'] [name='"+field+"'][value='"+form_state[field]+"']").prop("checked",true);
                
            } else if(value.Type.toLowerCase().indexOf("timestamp")!==-1 || value.Type.toLowerCase().indexOf("date")!==-1 || value.Type.toLowerCase().indexOf("time")!==-1) {
                // datepicker
                $input.val(form_state[field]);
            }/* else if(stripos($value["Type"],"image")===0) {
                // image upload
                $output.= Render::inputImage($form_name, $field, $label, $fieldsData[$field], (isset($fieldsData["invalid"][$field]) ? $fieldsData["invalid"][$field] : ""), $required, "", $readonly);
            } else if(stripos($value["Type"],"file")===0) {
                // image upload
                $output.= Render::inputFile($form_name, $field, $label, $fieldsData[$field], (isset($fieldsData["invalid"][$field]) ? $fieldsData["invalid"][$field] : ""), $required, "", $readonly);
            }*/ else {
                //$input = form_state[field];
            }
            
            
            if(typeof(form_state.invalid) !== 'undefined' && typeof(form_state.invalid[field]) !== 'undefined') {
                $input.addClass("is-invalid");
                $input.removeClass("is-valid");
                
                var $group = $input.closest(".bmd-form-group, .form-group");
                //console.log($input)
                if($group.length) {
                    $group.addClass("has-"+form_state.invalid[field].status);
                    $group.append($('<div class="invalid-feedback">').html(form_state.invalid[field].message));
                } else {
                    //$input.parent(".control-inline").addClass(form_state.invalid[field].status);
                }
            } else {
                $input.addClass("is-valid");
                $input.removeClass("is-invalid");
                
                var $group = $input.closest(".bmd-form-group");
                if($group.length) {
                    $group.removeClass("has-error");
                    //console.log($group.find(".invalid-feedback"))
                    $group.find(".invalid-feedback").remove();
                } else {
                    //$input.parent(".control-inline").removeClass("has-error");
                }
            }
        }
    },
    
    updateSelectOption: function($select, selected_value, options) {
         // select
        $select.empty();
        for(var i in options) {
            var $option = $(tpl.option);
            $option.val(i);
            $option.html(options[i][0]); // add label
            if(selected_value == i) {
                $option.prop("selected",true);
            }
            $option.attr("disabled",!options[i][1]);
            $select.append($option);
        }
    }
};