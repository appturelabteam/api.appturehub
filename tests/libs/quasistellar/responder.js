/**
 * responder
 * non-jquery version 0.2
 * 
 * By Dewald Bodenstein
 * May 2017 v0.1
 * July 2018 v0.2
 * 
 * @type type
 */
var responder = {
    items : {},
    
    init: function() {
        // add responder event
        window.addEventListener("resize", responder.resize);
    },
    
    resize: function(){
        clearTimeout(responder.resizeTimer);
        responder.resizeTimer = setTimeout(responder.respond,250);
    },
    
    respond: function() {
        for(var i in responder.items) {
            responder.items[i][0](responder.items[i][1]);
        }
    },
    
    // what happens to variable scope here?
    set: function(identifier,func,args) {
        responder.items[identifier] = [func, args];
    },
    
    remove: function(identifier) {
        delete responder.items[identifier];
    }
};