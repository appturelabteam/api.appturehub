<h2 id="getting-started">Getting Started</h2>
<div class="card mb-3">
    <div class="card-body">
        <p>Coming soon...</p>
    </div>
</div>

<h2 id="api-testing-setup">API Testing Setup</h2>
<div class="card mb-3">
    <div class="card-body">
        <p>Coming soon...</p>
    </div>
</div>

<h2 id="lists-and-filters">Lists and filters</h2>
<div class="card mb-3">
    <div class="card-body">
        <p>The filter parameter that is available on standard endpoints using the GET method, without an <code>id</code> parameter, allows the following options:</p>
        <ul>
            <li>By default, the <code>LIKE</code> operator is used find the filter value in all columns.</li>
            <li>In order to pass multiple filter values, separate each with a <code>+</code>. The resulting SQL query uses the <code>OR</code> operator for each passed filter on all columns.</li>
            <li>If no <code>+</code> operator is used, the whole filter passed will be used as one value regardless of spaces.</li>
            <li>In order to filter on a specific column the following formats are available for each passed filter:
                <ul>
                    <li><code>{column}:{value}</code> using a colon will cause the query to use the <code>LIKE</code> operator.</li>
                    <li><code>{column}|{value}</code> using a line will cause the query to use the <code>=</code> operator for exact value matches.</li>
                </ul>
            </li>
        </ul>
    </div>
</div>
