<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="tests/libs/bootstrap/css/bootstrap.min.css">
        
        <script src="tests/libs/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
        <script src="tests/libs/quasistellar/template.js"></script>
        <script src="tests/libs/quasistellar/render.js"></script>
        <script src="tests/libs/quasistellar/forms.js"></script>
        
        <style>
            .bottom {
                padding-bottom: 15px;
            }
            
            pre {
                border-radius: 4px;
            }
            
            pre.success {
                background-color: lightgreen;
            }
            
            pre.danger {
                background-color: darksalmon;
            }
            .list-group-item {
                border: none;
            }
            .list-group-item:first-child {
                border-top-left-radius: 0;
                border-top-right-radius: 0;
            }
            .list-group-item:last-child {
                border-bottom-right-radius: 0;
                border-bottom-left-radius: 0;
            }
        </style>
        
        <script>
            var API_URL = "<?=Router::getIndex()?>";
            var sessionPrefix = "<?=API_DEFAULT_CLIENT_ID?>_tests_";
            var client_id = sessionStorage.getItem(sessionPrefix+"client_id") || "<?=API_DEFAULT_CLIENT_ID?>";
            var session = {};

            function authenticateClient(done) {
                $.ajax({
                    url: API_URL + "auth/access_token",
                    method: "POST",
                    headers: {"Authorization" : "Basic "+btoa(client_id+":")},
                    data: {
                        grant_type: "client_credentials"
                    },
                    dataType:"json",
                    contentType: 'application/x-www-form-urlencoded',
                    crossDomain: true
                }).done(function(response){
                    //console.log(response)

                    session = response;
                    session.username = "anonymous";

                    sessionStorage.setItem(sessionPrefix+'username', session.username);
                    sessionStorage.setItem(sessionPrefix+'token_type', session.token_type);
                    sessionStorage.setItem(sessionPrefix+'access_token', session.access_token);
                    sessionStorage.setItem(sessionPrefix+'expires_in', session.expires_in);
                    sessionStorage.setItem(sessionPrefix+'refresh_token', session.refresh_token);
                    sessionStorage.setItem(sessionPrefix+'scope', session.scope);

                    if(done)
                        done(response);

                }).fail(function(response){
                    console.log(response.responseText)
                    response = JSON.parse(response.responseText);
                    //Message.add("<strong>Authentication failed:</strong> "+response.message,"danger",{id:"user"});
                });
            }

            function getSessionUser(callback) {

                // try and read session from cookies
                session.username = sessionStorage.getItem(sessionPrefix+'username');
                session.token_type = sessionStorage.getItem(sessionPrefix+'token_type');
                session.access_token = sessionStorage.getItem(sessionPrefix+'access_token');
                session.refresh_token = sessionStorage.getItem(sessionPrefix+'refresh_token');
                session.expires_in = sessionStorage.getItem(sessionPrefix+'expires_in');
                session.scope = sessionStorage.getItem(sessionPrefix+'scope');

                // if no session found
                if(session.username === null) {

                    // authenticate the transaction for anonymous access
                    authenticateClient(function(response){
                        callback();
                    });

                } else {

                    // else pass back the session user
                    callback();

                }
            }
        </script>
        
        <script type="text/html" id="section-template">
            <div id="{ACTION}" class="card mb-3">
                <div class="card-body">
                    <div class="row bottom">
                        <div class="col-sm-12">
                            <h3>{title}</h3>
                            <p>{description}</p>
                            <b>Method:</b> <code>{METHOD}</code><br>
                            <b>URI:</b> <code>{url}<b>{subEndpoint}{uri_parameters}</b></code><br>
                            <b>Headers:</b>
                            {headers}
                            <b>Parameters:</b>
                            {parameters}
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <h4>Parameters:</h4>
                            <div class="{endpoint}_{METHOD}_parameters_messages"></div>
                            <form name="{endpoint}_{METHOD}_parameters" class="input-parameters"></form>
                            <h4>Request:</h4>
                            <div class="{endpoint}_{METHOD}_messages"></div>
                            <form name="{endpoint}_{METHOD}" class="input-fields"></form>
                            <button class="btn btn-primary" id="button-{ACTION}">{button_text} <i class="fa fa-ajax fa-spin"></i></button>
                        </div>
                        <div class="col-md-8">
                            <h4>Response<span id="response-result-{ACTION}"></span>:</h4>
                            <pre id="response-{ACTION}" style="max-height: calc(100vh - 60px)"></pre>
                        </div>
                    </div>
                </div>
            </div>
        </script>
    </head>
    <body>
        <aside style="width: 300px; padding: 5px 10px; max-height: 100%; overflow: auto; position: fixed; top: 0; bottom: 0;">
            <h2><a href='./'><?=API_SITE_NAME?></a></h2>
            <div id="accordion">
                <div class="list-group">
                    <a class="list-group-item active" id="accordianHeaderDoc" data-toggle="collapse" aria-expanded="true" data-target="#accordianCollapseDoc" aria-controls="accordianCollapseDoc" href="#"><b>Documentation</b></a>
                    <div id="accordianCollapseDoc" aria-labelledby="accordianHeaderDoc" class="collapse show" data-parent="#accordion">
                        <a class="list-group-item" href="./#getting-started">Getting Started</a>
                        <a class="list-group-item" href="./#api-testing-setup">API Testing Setup</a>
                        <a class="list-group-item" href="./#lists-and-filters">Lists and Filters</a>
                    </div>
                </div>
                <span></br></span>
            </div>
            <br>&nbsp;
        </aside>

        <div class="body" style="margin-left: 320px; width: calc(100% - 340px);">
            <h1><a href='./'><?=API_SITE_NAME?> Documentation</a></h1>
            
            <?php print $body; ?>
            
            <div id="page-top"></div>
        </div>
        
        <footer style="margin-left: 320px; width: calc(100% - 340px); padding-top: 26px; padding-bottom: 46px;">
            API Version: <?=API_VERSION?><br>
            Web Version: <?=VERSION_WEB?><br>
            Android Version: <?=VERSION_ANDROID?><br>
            iOS Version: <?=VERSION_IOS?><br>
            Start up time: <?=(microtime(true)-$_SESSION["start_time"])?>
        </footer>
        
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>-->
        <!--<script src="../libs/bootstrap/js/bootstrap.bundle.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>-->
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        
        <script>
            var $accordian = $("#accordion");
            var endpoints = [
                "auth",
                "client",
                
                "-",
                
                "user",
                "user_profile",
                
                "-",
                
                "menu",
                "file",
                "contact",
                "page",
                "node",
                
                //"-",
                
                //"announcement",
                //"announcement_group",
                //"log"
            ];
            var options = {};
            
            getSessionUser(function(){
                
                var atEndpoint = 0;
                
                function nextEndpoint() {
                    atEndpoint++;
                    if(atEndpoint < endpoints.length) {
                        buildEndpoint();
                    } else {
                        $('.collapse').collapse();
                        if(window.location.hash)
                            window.location = window.location.hash;
                    }
                }
                
                function buildEndpoint() {
                    var endpoint = endpoints[atEndpoint];
                    
                    if(endpoint === "-") {
                        
                        $accordian.append("<span></br></span>");
                        nextEndpoint();
                        
                    } else {
                        
                        var $accordianGroup = $("<div>").addClass("list-group");
                        $accordian.append($accordianGroup);
                        $accordianGroup.append($("<a>").addClass("list-group-item").addClass("active").attr("href","./#"+endpoint).attr("id","accordianHeader"+endpoint).attr("data-toggle","collapse").attr("data-target","#accordianCollapse"+endpoint).attr("aria-controls","accordianCollapse"+endpoint).html("<b>"+Render.toTitleCase(endpoint)+" Endpoint</b>"));
                        var $listGroup = $("<div>").attr("id","accordianCollapse"+endpoint).attr("aria-labelledby","accordianHeader"+endpoint).attr("data-parent","#accordion").addClass("collapse");
                        $accordianGroup.append($listGroup);

                        if($("#page-top").siblings().length === 0) {
                            $("#page-top").after($("<h2>").text(Render.toTitleCase(endpoint)+" Endpoint").attr("id",endpoint));
                        } else {
                            $("#page-top").siblings().last().after($("<h2>").text(Render.toTitleCase(endpoint)+" Endpoint").attr("id",endpoint));
                        }

                        $.ajax({
                            type: 'OPTIONS',
                            url: API_URL + endpoint,
                            dataType:"json",
                            headers: {"Authorization": session.token_type + " " + session.access_token},
                            crossOrigin: true
                        }).done(function (response) {
                            console.log(response);

                            options[endpoint] = response.data;

                            // build doc
                            for(var subEndpoint in options[endpoint]) {
                                for(var method in options[endpoint][subEndpoint]) {

                                    // add link
                                    var action = Render.slugify(options[endpoint][subEndpoint][method].title);
                                    $listGroup.append($("<a>").addClass("list-group-item").attr("href","./#"+action).html(options[endpoint][subEndpoint][method].title));

                                    // generate doc sections
                                    var headers = "";
                                    if(typeof(options[endpoint][subEndpoint][method].request.headers) !== 'undefined') {
                                        for(var header in options[endpoint][subEndpoint][method].request.headers) {
                                            headers+= "<li>"+header+": "+options[endpoint][subEndpoint][method].request.headers[header]+"</li>";
                                        }
                                    }

                                    var parameters = "";
                                    var uri_parameters = "";
                                    for(var parameter in options[endpoint][subEndpoint][method].request.parameters) {
                                        parameters+= "<li>"+parameter+": "+options[endpoint][subEndpoint][method].request.parameters[parameter].Description+"</li>";
                                        //console.log(options[endpoint][subEndpoint][method].request.parameters[parameter].Default)
                                        uri_parameters+= (subEndpoint.indexOf("{"+parameter+"}") === -1 ? (uri_parameters !== "" ? "&" : "")+parameter+"="+(options[endpoint][subEndpoint][method].request.parameters[parameter].Default !== null ? options[endpoint][subEndpoint][method].request.parameters[parameter].Default : "[no default]") : "");
                                    }

                                    var data = {
                                        METHOD: method,
                                        ACTION: Render.slugify(options[endpoint][subEndpoint][method].title),
                                        parameters: parameters === "" ? "None<br>" : "<ul>"+parameters+"</ul>",
                                        headers: headers === "" ? "None<br>" : "<ul>"+headers+"</ul>",
                                        uri_parameters: (uri_parameters !== "" ? "?" : "")+uri_parameters,
                                        url: API_URL,
                                        subEndpoint: subEndpoint,
                                        endpoint: endpoint,
                                        title: options[endpoint][subEndpoint][method].title,
                                        description: options[endpoint][subEndpoint][method].description,
                                        button_text: Render.toTitleCase(method.toLowerCase()+" "+endpoint)
                                    };

                                    var $content = $(template.dataValues($("#section-template").text(),data));
                                    $("#page-top").siblings().last().after($content);

                                    if(options[endpoint][subEndpoint][method].request.parameters) {
                                        if(subEndpoint === 'auth/access_token') {
                                            options[endpoint][subEndpoint][method].request.parameters.client_id.Default = client_id;
                                        }
                                        var formState = Forms.init(options[endpoint][subEndpoint][method].request.parameters, null);
                                        $("#"+data.ACTION+" .input-parameters").append(Render.formFields(endpoint+"_"+method+"_parameters", options[endpoint][subEndpoint][method].request.parameters, formState));
                                    } else {
                                        $("#"+data.ACTION+" .input-parameters").append("None<br><br>");
                                    }

                                    if(options[endpoint][subEndpoint][method].request.body.length !== 0) {
                                        var formState = Forms.init(options[endpoint][subEndpoint][method].request.body, null);
                                        $("#"+data.ACTION+" .input-fields").append(Render.formFields(endpoint+"_"+method, options[endpoint][subEndpoint][method].request.body, formState));
                                    } else {
                                        $("#"+data.ACTION+" .input-fields").append("None<br><br>");
                                    }

                                    $("#"+data.ACTION+" #button-"+action).attr("data-method",method);
                                    $("#"+data.ACTION+" #button-"+action).attr("data-endpoint",subEndpoint);
                                    $("#"+data.ACTION+" #button-"+action).on("click",function(e){
                                        e.preventDefault();

                                        var $this = $(this);
                                        var method = $this.attr("data-method");
                                        var subEndpoint = $this.attr("data-endpoint");
                                        var action = Render.slugify(options[endpoint][subEndpoint][method].title);
                                        var formState = null;
                                        var parametersFormState = null;
                                        var formStateValid = false;
                                        var parametersFormStateValid = false;

                                        clear(action);

                                        //console.log(method,subEndpoint);

                                        if(options[endpoint][subEndpoint][method].request.parameters) {
                                            $("#"+action+" ."+endpoint+"_"+method+"_parameters_messages").removeClass("alert alert-danger").empty();
                                            parametersFormState = Forms.getValues(endpoint+"_"+method+"_parameters", options[endpoint][subEndpoint][method].request.parameters, true, false);
                                            //console.log(parametersFormState)
                                            if(parametersFormState.invalid.invalid_amount > 0) {
                                                Forms.updateForm(endpoint+"_"+method+"_parameters", options[endpoint][subEndpoint][method].request.parameters, parametersFormState);
                                                $("#"+action+" ."+endpoint+"_"+method+"_parameters_messages").addClass("alert alert-danger").append(Render.invalidFields(options[endpoint][subEndpoint][method].request.parameters, parametersFormState.invalid));
                                                window.location.hash = "#"+action;
                                                parametersFormState = null;
                                            } else {
                                                parametersFormStateValid = true;
                                                delete parametersFormState.invalid;
                                            }
                                        }

                                        if(options[endpoint][subEndpoint][method].request.body.length !== 0) {
                                            $("#"+action+" ."+endpoint+"_"+method+"_messages").removeClass("alert alert-danger").empty();
                                            formState = Forms.getValues(endpoint+"_"+method, options[endpoint][subEndpoint][method].request.body, true, method === "PUT");
                                            if(formState.invalid.invalid_amount > 0) {
                                                Forms.updateForm(endpoint+"_"+method, options[endpoint][subEndpoint][method].request.body, formState);
                                                $("#"+action+" ."+endpoint+"_"+method+"_messages").addClass("alert alert-danger").append(Render.invalidFields(options[endpoint][subEndpoint][method].request.body, formState.invalid));
                                                window.location.hash = "#"+action;
                                                formState = null;
                                            } else {
                                                formStateValid = true;
                                                delete formState.invalid;
                                            }
                                        }

                                        //console.log(parametersFormState, formState)
                                        //console.log((options[endpoint][subEndpoint][method].request.parameters === null || parametersFormStateValid) , (options[endpoint][subEndpoint][method].request.body.length === 0 || formStateValid))

                                        if((options[endpoint][subEndpoint][method].request.parameters === null || parametersFormStateValid) && (options[endpoint][subEndpoint][method].request.body.length === 0 || formStateValid)) {

                                            var endpointWithId = subEndpoint;
                                            var parameters = "";

                                            if(parametersFormState) {
                                                endpointWithId = template.dataValues(subEndpoint,{id:(typeof parametersFormState.id !== 'undefined' ? parametersFormState.id : 0)});
                                                delete parametersFormState.id;
                                                
                                                for(var parameter in parametersFormState) {
                                                    parameters+= (parameters !== "" ? "&" : "")+parameter+"="+parametersFormState[parameter];
                                                }
                                                
                                            }

                                            // allow sparse payloads for updates
                                            if(method === "PUT" && formState) {
                                                for(var key in formState) {
                                                    if(formState[key] === "") {
                                                        delete formState[key];
                                                    }
                                                }
                                            }
                                            
                                            if(subEndpoint === "auth/access_token") {

                                                if(formState.grant_type === "client_credentials") {
                                                    delete formState.username;
                                                    delete formState.password;
                                                }

                                                $.ajax({
                                                    type: method,
                                                    url: API_URL + endpointWithId,
                                                    dataType:"json",
                                                    headers: {"Authorization" : "Basic "+btoa(parametersFormState.client_id+":")},
                                                    data: formState,
                                                    contentType: 'application/x-www-form-urlencoded',
                                                    crossOrigin: true
                                                }).done(function (response) {
                                                    console.log("done",response);

                                                    session = response;
                                                    session.username = formState.username || client_id;

                                                    sessionStorage.setItem(sessionPrefix+'username', session.username);
                                                    sessionStorage.setItem(sessionPrefix+'token_type', session.token_type);
                                                    sessionStorage.setItem(sessionPrefix+'access_token', session.access_token);
                                                    sessionStorage.setItem(sessionPrefix+'expires_in', session.expires_in);
                                                    sessionStorage.setItem(sessionPrefix+'refresh_token', session.refresh_token);
                                                    sessionStorage.setItem(sessionPrefix+'scope', session.scope);

                                                    doneAuth(response, action, null);

                                                    window.location.hash = "#"+action;

                                                }).fail(function(response){
                                                    console.log("fail",response);

                                                    fail(response, action, null);

                                                    window.location.hash = "#"+action;
                                                });

                                            } else {
                                                
                                                var data = (formState ? JSON.stringify(formState) : "");
                                                
                                                $.ajax({
                                                    type: method,
                                                    url: API_URL + endpointWithId + (parameters !== "" ? "?"+parameters : ""),
                                                    dataType:"json",
                                                    headers: {"Authorization": session.token_type + " " + session.access_token},
                                                    data: data,
                                                    crossOrigin: true
                                                }).done(function (response) {
                                                    console.log(response);

                                                    done(response, action, null);

                                                    window.location.hash = "#"+action;

                                                }).fail(function(response){
                                                    console.log(response);

                                                    fail(response, action, null);

                                                    window.location.hash = "#"+action;
                                                });

                                            }


                                        }
                                    });
                                }
                            }

                            nextEndpoint();

                        }).fail(function(response){
                            console.log(response);

                            //alert(JSON.stringify(response,4,' '));

                            nextEndpoint();
                        });
                    }
                }
                
                buildEndpoint();
            });
            
            var clear = function(classSuffix) {
                //console.log(data);
                $("#response-"+classSuffix).removeClass("danger").removeClass("success").empty();
                $("#response-result-"+classSuffix).removeClass("text-primary").removeClass("text-error").empty();
            };
            var done = function(data, classSuffix, callback) {
                //console.log(data);
                $("#response-"+classSuffix).removeClass((data.success ? "danger" : "success")).addClass((data.success ? "success" : "danger")).html(JSON.stringify(data, null, 4));
                $("#response-result-"+classSuffix).addClass("text-primary").removeClass("text-error").html(" (Done)");
                
                if(callback){
                    callback(data);
                }
            };
            var doneAuth = function(data, classSuffix, callback) {
                //console.log(data);
                $("#response-"+classSuffix).removeClass("danger").addClass("success").html(JSON.stringify(data, null, 4));
                $("#response-result-"+classSuffix).addClass("text-primary").removeClass("text-error").html(" (Done)");
                
                if(callback){
                    callback(data);
                }
            };
            var fail = function(data, elSelector, callback) {
                
                $("#response-"+elSelector).removeClass("success").addClass("danger").html(JSON.stringify(data, null, 4));
                $("#response-result-"+elSelector).addClass("text-error").removeClass("text-success").html(" (Fail)");
                
                if(callback){
                    callback(data);
                }
            };
            
            $(document).ready(function(){
                console.log("ready")
                
            })
            
        </script>
    </body>
</html>