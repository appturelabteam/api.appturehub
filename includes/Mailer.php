<?php
class Mailer {
    public static $lastError = null;
    
    static function send($to, $cc, $bcc, $from, $subject, $message, $replyto = null, $files = array()) {
        // use swift for dev
        /*if(API_MODE === API_MODE_DEVELOPMENT) {
            $to = "admin@quasistellar.co.za";
            $cc = "dewaldbodenstein@gmail.com";
            $bcc = "";
            return Mailer::swiftSend($to, $cc, $bcc, $from, $subject, $message, $replyto, $files);
        }*/
        
        //if(API_MODE === API_MODE_DEVELOPMENT) {
            return Mailer::swiftSend($to,$cc,$bcc,$from,$subject,$message, $replyto, $files);
        //}
        
        /*$xheaders = "";
        $xheaders .= "From: <$from>\n";
        $xheaders .= "X-Sender: <$from>\n";
        $xheaders .= "X-Mailer: PHP\n"; // mailer
        $xheaders .= "X-Priority: 3\n"; //1 Urgent Message, 3 Normal
        $xheaders .= "Content-Type:text/html; charset=\"iso-8859-1\"\n";
        $xheaders .= "Bcc:{$bcc}\n";
        $xheaders .= "Cc:{$cc}\n";
        return mail($to, $subject, $message, $xheaders);*/
    }
    
    static function swiftSend($to,$cc,$bcc,$from,$subject,$message, $replyto = null, $files = array()) {
        // Create the Transport
        //$transport = Swift_SmtpTransport::newInstance('whips-co-za.mail.protection.outlook.com', 25);
        $transport = new Swift_SmtpTransport('mail.clickatyre.online', 465, 'ssl');
        $transport->setUsername('website@clickatyre.online');
        $transport->setPassword('ysp57gwurCqj');
        
        // SPLIT THE INPUT
        $from = is_array($from) ? $from : array_filter(explode(",",$from));
        $to = is_array($to) ? $to : array_filter(explode(",",$to));
        $cc = is_array($cc) ? $cc : array_filter(explode(",",$cc));
        $bcc = is_array($bcc) ? $bcc : array_filter(explode(",",$bcc));

        /*
        You could alternatively use a different transport such as Sendmail or Mail:

        // Sendmail
        $transport = Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs');

        // Mail
        $transport = Swift_MailTransport::newInstance();
        */

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);
        
        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
        $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

        // Create a message
        $swiftMessage = new Swift_Message($subject);
        $swiftMessage->setFrom($from);
        $swiftMessage->setTo($to);
        
        if($replyto) {
            $swiftMessage->addReplyTo($replyto);
        }
        
        if(count($cc)) {
            $swiftMessage->setCc($cc);
        }
        
        if(count($bcc)) {
            $swiftMessage->setBcc($bcc);
        }
        
        // embed inline images
        $doc = new DOMDocument();
        $doc->loadHTML($message);
        
        $imgs = $doc->getElementsByTagName("img");
        $count = 0;
        foreach($imgs as $img){
            //print $img->getAttribute('src')." - ".realpath($img->getAttribute('src'))."-<br>";
            
            // try and download and embed an external file
            if(stripos($img->getAttribute('src'),"http") !== false) {
                
                $count ++;
                $image_data = Mailer::dataUri($img->getAttribute('src'));
                $extension = explode("/",$image_data["mime"]);
                $image = new Swift_Image($image_data["data"], "image_attachement_$count.".(count($extension) == 2 ? $extension[1] : "png"), $image_data["mime"]);
                $cid = $swiftMessage->embed($image);
                $img->setAttribute( 'src' , $cid );
                
            } else if(file_exists(realpath($img->getAttribute('src')))) {
                
                $image_data = Mailer::dataUri(realpath($img->getAttribute('src')));
                $image = new Swift_Image($image_data["data"], $img->getAttribute('src'), $image_data["mime"]);
                $cid = $swiftMessage->embed($image);
                $img->setAttribute( 'src' , $cid );
                
            }
        }
        
        //print $doc->saveHTML();
        //die();
        
        $swiftMessage->setBody($doc->saveHTML(),'text/html');
        
        $files = array_filter($files);
        foreach($files as $file) {
            $swiftMessage->attach(Swift_Attachment::fromPath($file));
        }
        
        try {

            $result = $mailer->send($swiftMessage);
            
        } catch(Exception $exception) {
            
        }
        
        Mailer::$lastError = $logger->dump();
        
        LogController::log("MAILER", "SEND", 0, 0, true, json_encode(compact("to","result","error","cc","bcc","from","subject","tags")), "");

        // Send the message
        return $result;
    }
    
    static function dataUri($file) {
        $extension = substr($file,strripos($file, ".")+1);
        $type = strtolower($extension);
        if($type == 'jpeg') {$type = 'jpg';}
        $mime = "image/{$type}";
            
        $contents = file_get_contents($file);
        //$base64 = base64_encode($contents);
        
        return array("data"=>$contents,"mime"=>$mime);
    }
}