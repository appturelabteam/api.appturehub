<?php
/**
 * Description of Image
 *
 * @author dewald
 */
use League\ColorExtractor\Color;
use League\ColorExtractor\ColorExtractor;
use League\ColorExtractor\Palette;

if ( ! function_exists( 'exif_imagetype' ) ) {
    function exif_imagetype ( $filename ) {
        if ( ( list($width, $height, $type, $attr) = getimagesize( $filename ) ) !== false ) {
            return $type;
        }
        return false;
    }
}

class Image {
    static private $cache_folder = "uploads/img/cache";
    static public $last_error = null;


    static public $presets = array(
        'max-size' => array('width'=>1920,'height'=>1920,'type'=>'fit-width'),
        'lazy-color'=>array('width'=>2,'height'=>2,'type'=>'fit'),
        'admin-table'=>array('width'=>36,'height'=>36,'type'=>'fit-crop'),
        'admin-list'=>array('width'=>64,'height'=>64,'type'=>'fit-crop'),
        'file-selector-preview'=>array('width'=>240,'height'=>180,'type'=>'fit-crop'),
        
        // frontend
        'product-thumb'=>array('width'=>48,'height'=>48,'type'=>'fit-crop'),
        //'product-preview'=>array('width'=>136,'height'=>136,'type'=>'fit-height'),
        'product-preview'=>array('width'=>180,'height'=>136,'type'=>'fit-width'),
        'product-brand'=>array('width'=>180,'height'=>24,'type'=>'fit-height'),
        'product-image'=>array('width'=>350,'height'=>350,'type'=>'fit-height'),
        'overview-image' => array('width'=>360,'height'=>180,'type'=>'fit-pad'),
        
        // cart
        'cart-product'=>array('width'=>64,'height'=>64,'type'=>'fit'),
        'order-product'=>array('width'=>48,'height'=>48,'type'=>'fit'),
        'checkout-product'=>array('width'=>40,'height'=>40,'type'=>'fit-crop'),
        
        // announcements
        'announcement'=>array('width'=>720,'height'=>720,'type'=>'fit-width'),
    );
    
    /*
     * Resizes an image according to the specified preset and saves it. If the resized file 
     * already exists it is loaded instead.
     */
    static function get($src,$size_preset) {
        
        if(isset(Image::$presets[$size_preset]) && $src!="") {
            //print "</br>src: ".$src."</br>";
            $fullFilename = substr($src,strripos($src, "/")+1);
            //print "full filename: ".$fullFilename."</br>";
            $filename = substr($fullFilename,0,strripos($fullFilename, "."));
            //print "filename: ".$filename."</br>";
            $extension = substr($fullFilename,strripos($fullFilename, ".")+1);
            
            if(!file_exists($src) && is_readable($filename)) {
                Image::$last_error = "File does not exist or isn't readable";
                error_log("File does not exist: $src - ".print_r(Image::$presets[$size_preset],1));
                return false;
            }
            
            $imageType = exif_imagetype ($src);
            switch($imageType) {
                case IMAGETYPE_JPEG:
                case IMAGETYPE_JPEG2000:
                    $type = "jpg";
                    break;
                case IMAGETYPE_PNG:
                    $type = "png";
                    break;
                case IMAGETYPE_GIF:
                    $type = "gif";
                    break;
                default:
                    Image::$last_error = "Unsupported image type";
                    error_log("Unsupported image type ($imageType): $src");
                    return false;
            }
            
            if(stripos($src,"http://")!==false || stripos($src,"https://")!==false) {
                // check the file header first
                $headers = get_headers($src,1);
                if(stripos($headers["Content-Type"],"image")===false) {
                    Image::$last_error = "Bad external image";
                    error_log("Bad external image: $src - ".print_r(Image::$presets[$size_preset],1));
                    return false; // bad image
                }
                
                $folder = Image::$cache_folder."/external";
                if(!file_exists($folder)) {
                    mkdir($folder,0755);
                }
            } else {
                $folder = substr($src,0,strripos($src, "/"));
            }
            
            //print $folder."</br>";
            $presetFolder = Image::$cache_folder."/".$size_preset;
            $newFilename = $presetFolder.
                    "/".
                    md5(
                        $src."1_".
                        Image::$presets[$size_preset]["width"]."_".
                        Image::$presets[$size_preset]["height"]."_".
                        Image::$presets[$size_preset]["type"]."_".
                        (isset(Image::$presets[$size_preset]["crop-trasparency"]) ? Image::$presets[$size_preset]["crop-trasparency"]."_" : "").
                        (isset(Image::$presets[$size_preset]["crop-background"]) ? Image::$presets[$size_preset]["crop-background"]."_" : "").
                        (isset(Image::$presets[$size_preset]["background"]) && is_array(Image::$presets[$size_preset]["background"]) ? implode("",Image::$presets[$size_preset]["background"]) : "")
                    ).
                    ".".$type;
            
            if((file_exists($newFilename) && file_exists($src)) && (filemtime($newFilename) > filemtime($src))) {
                return $newFilename;
            }
            
            if(!file_exists(Image::$cache_folder)) {
                mkdir(Image::$cache_folder,0755,true);
            }
            
            if(!file_exists($presetFolder)) {
                mkdir($presetFolder,0755,true);
            }
            
            if(!list($width, $height) = getimagesize($src)) {
                Image::$last_error = "Unsupported picture type";
                error_log("Unsupported picture type: $src - ".print_r(Image::$presets[$size_preset],1));
                return false;
            }
            
            $widthOffset = 0;
            $heightOffset = 0;
            
            $finalWidth = $newWidth = Image::$presets[$size_preset]['width'];
            $finalHeight = $newHeight = Image::$presets[$size_preset]['height'];
            
            // fit the width or height into the desired width and height, for fit-crop, fit-width or fit-height
            if(stripos(Image::$presets[$size_preset]['type'],'fit')!==false && Image::$presets[$size_preset]['type']!=='fit') {
                $original_aspect = $width / $height;
                $thumb_aspect = $finalWidth / $finalHeight;
                
                if ($original_aspect >= $thumb_aspect) {
                    // If image is wider than thumbnail (in aspect ratio sense)
                    $newHeight = $finalHeight;
                    $newWidth = $width / ($height / $finalHeight);
                } else {
                    // If the thumbnail is wider than the image
                    $newWidth = $finalWidth;
                    $newHeight = $height / ($width / $finalWidth);
                }
            }
            
            // fit-crop requires the offset so it can crop correctly
            if(Image::$presets[$size_preset]['type']=='fit-crop') {
                $widthOffset = - ($newWidth - $finalWidth) / 2;
                $heightOffset = - ($newHeight - $finalHeight) / 2;
            }
            //print $newWidth."x".$newHeight;
            
            // centre the position to copy from
            if(Image::$presets[$size_preset]['type']=='crop') {
                $widthOffset = ($width/2)-($newWidth/2);
                $heightOffset = ($height/2)-($newHeight/2);
            }
            
            // fit the image completely within the desired height or width
            if(Image::$presets[$size_preset]['type']=='fit' || Image::$presets[$size_preset]['type']=='fit-pad') {
                // Set the width of the area and height of the area
                //$finalWidth = 460;
                //$finalHeight = 280;

                // So then if the image is wider rather than taller, set the width and figure out the height
                if (($width / $height) > ($finalWidth / $finalHeight)) {
                    $finalWidth = $finalWidth;
                    $finalHeight = ceil(($finalWidth * $height) / $width);
                }
                // And if the image is taller rather than wider, then set the height and figure out the width
                elseif (($width / $height) < ($finalWidth / $finalHeight)) {
                    $finalWidth = ceil(($finalHeight * $width) / $height);
                    $finalHeight = $finalHeight;
                }
                // And because it is entirely possible that the image could be the exact same size/aspect ratio of the desired area, so we have that covered as well
                elseif (($width / $height) == ($finalWidth / $finalHeight)) {
                    $finalWidth = $finalWidth;
                    $finalHeight = $finalHeight;
                }
            }
            
            if(Image::$presets[$size_preset]['type']=='fit-width') {
                $a = $finalWidth/$finalHeight;
                $b = $width/$height;
                
                //print $a. "x". $b. "</br>";
                
                //if($a >= $b) {
                    //if($finalWidth >= $finalHeight) {
                        $finalWidth = ((1/$width)*$finalWidth)*$width;
                        $finalHeight = ((1/$width)*$finalWidth)*$height;
                    //} else {
                        //$finalHeight = ((1/$height)*$finalWidth)*$width;
                    //}
                /*} else {
                    if($finalWidth >= $finalHeight) {
                        $finalHeight = ((1/$width)*$finalWidth)*$height;
                    } else {
                        $finalWidth = ((1/$width)*$finalHeight)*$height;
                    }
                }*/
                /*
                if($width >= $height && $finalWidth >= $finalHeight) {
                    $scale = (1/$height)*$finalHeight;

                    $finalWidth = $width * $scale;
                    $finalHeight = $height * $scale;
                }*/
                //print $filename.": ".$finalWidth. "x". $finalHeight."</br>";
                /*
                if($width > $height) {
                    $scale = (1/$width)*$smallest;

                    $finalWidth = $width * $scale;
                    $finalHeight = $height * $scale;
                    die(print $filename.": ".$scale. "*".$finalWidth. "x". $finalHeight);
                }*/
            }
            
            if(Image::$presets[$size_preset]['type']=='fit-height') {
                //$a = $finalWidth/$finalHeight;
                //$b = $width/$height;
                
                //print $a. "x". $b. "</br>";
                
                //if($a >= $b) {
                    //if($finalWidth >= $finalHeight) {
                        $finalHeight = ((1/$height)*$finalHeight)*$height;
                        $finalWidth = ((1/$height)*$finalHeight)*$width;
                        
                    //} else {
                        //$finalHeight = ((1/$height)*$finalWidth)*$width;
                    //}
                /*} else {
                    if($finalWidth >= $finalHeight) {
                        $finalHeight = ((1/$width)*$finalWidth)*$height;
                    } else {
                        $finalWidth = ((1/$width)*$finalHeight)*$height;
                    }
                }*/
                /*
                if($width >= $height && $finalWidth >= $finalHeight) {
                    $scale = (1/$height)*$finalHeight;

                    $finalWidth = $width * $scale;
                    $finalHeight = $height * $scale;
                }*/
                //print $filename.": ".$finalWidth. "x". $finalHeight."</br>";
                /*
                if($width > $height) {
                    $scale = (1/$width)*$smallest;

                    $finalWidth = $width * $scale;
                    $finalHeight = $height * $scale;
                    die(print $filename.": ".$scale. "*".$finalWidth. "x". $finalHeight);
                }*/
            }
            
            //$type = strtolower($extension);
            if($type == 'jpeg') $type = 'jpg';
            switch($type){
                case 'gif': $image = imagecreatefromgif($src); break;
                case 'jpg': $image = imagecreatefromjpeg($src); break;
                case 'png': $image = imagecreatefrompng($src); break;
                default :
                    Image::$last_error = "Unsupported picture type";
                    error_log("Unsupported picture type!");
                    return false;
            }
            
            if(Image::$presets[$size_preset]['type']=='fit') {
                $new = imagecreatetruecolor($finalWidth, $finalHeight);
            } else if(Image::$presets[$size_preset]['type']=='fit-pad') {
                $new = imagecreatetruecolor(Image::$presets[$size_preset]["width"], Image::$presets[$size_preset]["height"]);
            } else {
                $new = imagecreatetruecolor($finalWidth, $finalHeight);
            }
            
            // preserve transparency and set background
            if(!isset(Image::$presets[$size_preset]["background"])) {
                Image::$presets[$size_preset]["background"] = [0,0,0,127];
            }
            
            if(Image::$presets[$size_preset]["background"] === "avg-gcolor-1") {
                Image::$presets[$size_preset]["background"] = array_values(Image::getAverageColor(Image::get($src,'card-product'),true));
            }
            
            if($type == "gif" || $type == "png"){
                imagealphablending($new, false);
                imagesavealpha($new, true);
                imagefill($new, 0, 0, imagecolorallocatealpha($new, Image::$presets[$size_preset]["background"][0], Image::$presets[$size_preset]["background"][1], Image::$presets[$size_preset]["background"][2], Image::$presets[$size_preset]["background"][3]));
            } else {
                imagefill($new, 0, 0, imagecolorallocate($new, Image::$presets[$size_preset]["background"][0], Image::$presets[$size_preset]["background"][1], Image::$presets[$size_preset]["background"][2]));
            }
            
            if(Image::$presets[$size_preset]['type']=='crop') {
                imagecopyresampled($new, $image, 0, 0, $widthOffset, $heightOffset, $finalWidth, $finalHeight, $newWidth, $newHeight);
            } else if(Image::$presets[$size_preset]['type']=='fit-crop') {
                imagecopyresampled($new, $image, $widthOffset, $heightOffset, 0, 0, $newWidth, $newHeight, $width, $height);
            } else if(Image::$presets[$size_preset]['type']=='fit') {
                imagecopyresampled($new, $image, 0, 0, 0, 0, $finalWidth, $finalHeight, $width, $height);
            } else if(Image::$presets[$size_preset]['type']=='fit-width') {
                imagecopyresampled($new, $image, 0, 0, $widthOffset, $heightOffset, $finalWidth, $finalHeight, $width, $height);
            } else if(Image::$presets[$size_preset]['type']=='fit-height') {
                imagecopyresampled($new, $image, 0, 0, 0, 0, $finalWidth, $finalHeight, $width, $height);
            } else if(Image::$presets[$size_preset]['type']=='fit-pad') {
                imagecopyresampled($new, $image, (Image::$presets[$size_preset]["width"]-$finalWidth)/2, (Image::$presets[$size_preset]["height"]-$finalHeight)/2, $widthOffset, $heightOffset, $finalWidth, $finalHeight, $width, $height);
            }
            
            if(isset(Image::$presets[$size_preset]["crop-trasparency"]) &&
                    ($type === 'gif' || $type === 'png')) {
                
                $result = imagecropauto($new, IMG_CROP_TRANSPARENT);
                if($result !== false) {
                    $new  = $result;
                }
                
                switch($type){
                    case 'gif': imagegif($new, $newFilename); break;
                    case 'png': imagepng($new, $newFilename); break;
                }
                
            } else if(isset(Image::$presets[$size_preset]["crop-background"])) {
                
                $result = imagecropauto($new, IMG_CROP_THRESHOLD, Image::$presets[$size_preset]["crop-background"], 16777215);
                if($result !== false) {
                    $new  = $result;
                }
                
                switch($type){
                    case 'gif': imagegif($new, $newFilename); break;
                    case 'jpg': imagejpeg($new, $newFilename,75); break;
                    case 'png': imagepng($new, $newFilename); break;
                }
            
            } else {
                
                switch($type){
                    case 'gif': imagegif($new, $newFilename); break;
                    case 'jpg': imagejpeg($new, $newFilename,75); break;
                    case 'png': imagepng($new, $newFilename); break;
                }
                
            }
            
            imagedestroy($image);
            imagedestroy($new);
            
            return $newFilename;
        }
    }
    
    static function resizeAndSave($src) {
        $resized = Image::get($src, "max-size");
        
        if($resized !== false) {
            
            $fullFilename = substr($src,strripos($src, "/")+1);
            $filename = substr($fullFilename,0,strripos($fullFilename, "."));
            $extension = substr($fullFilename,strripos($fullFilename, ".")+1);
            
            $type = strtolower($extension);
            if($type == 'jpeg') $type = 'jpg';
            switch($type){
                case 'gif': $image = imagecreatefromgif($resized); break;
                case 'jpg': $image = imagecreatefromjpeg($resized); break;
                case 'png': $image = imagecreatefrompng($resized); break;
                default : return "Unsupported picture type!";
            }
            
            switch($type){
                case 'gif': imagegif($image, $src); break;
                case 'jpg': imagejpeg($image, $src); break;
                case 'png': imagepng($image, $src); break;
            }
            
            imagedestroy($image);
            
            return true;
            
        }
        
        return $resized;
    }


    static function cropAndSave($src) {
        $fullFilename = substr($src,strripos($src, "/")+1);
        $filename = substr($fullFilename,0,strripos($fullFilename, "."));
        $extension = substr($fullFilename,strripos($fullFilename, ".")+1);
        
        $type = strtolower($extension);
        switch($type){
            case 'gif': $image = imagecreatefromgif($src); break;
            case 'png': $image = imagecreatefrompng($src); break;
            default : return false;
        }
        
        $new = imagecropauto($image);
        
        if($new !== false) {
            switch($type){
                case 'gif': return imagegif($new, $src);
                case 'png': return imagepng($new, $src);
                default : return false;
            }
        }
        
        return false;
    }

    static function saveBase64Image($image_data, $path, $filename){

        list($type, $data) = explode(';', $image_data); // exploding data for later checking and validating 

        if (preg_match('/^data:image\/(\w+);base64,/', $image_data, $type)) {
            $data = substr($data, strpos($data, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif

            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
                throw new \Exception('invalid image type');
            }

            $data = base64_decode($data);

            if ($data === false) {
                throw new \Exception('base64_decode failed');
            }
        } else {
            throw new \Exception('did not match data URI with image data');
        }

        $fullname = $path."/".$filename.".".$type;
        
        if(!file_exists($path)) {
            mkdir($path,0755);
        }

        if(file_put_contents($fullname, $data)){
            $result = $fullname;
        }else{
            $result =  "error";
        }
        /* it will return image name if image is saved successfully 
        or it will return error on failing to save image. */
        return $result; 
    }

    static function getBase64Image($path) {
        if(file_exists($path)) {
            $data = file_get_contents($path);
            $type = strtolower(pathinfo($path, PATHINFO_EXTENSION));
            return 'data:image/' . $type . ';base64,' . base64_encode($data);
        }
        return null;
    }

    static function getFolder($folder,$preset,$allfiles = false) {
        $list = array();
        if ($handle = opendir($folder)) {
            /* This is the correct way to loop over the directory. */
            while (false !== ($entry = readdir($handle))) {
                
                if(stripos($entry,".")!==0) {
                    
                    if($allfiles) {
                        
                        $list[] = $folder."/".$entry;
                        
                    } else {
                        
                        $extension = substr($entry,strripos($entry, ".")+1);
                        if(strcasecmp($extension,'png') === 0 ||
                                strcasecmp($extension,'jpg') === 0 ||
                                strcasecmp($extension,'gif') === 0 ) {
                            $list[] = Image::get($folder."/".$entry,$preset);
                        }
                        
                    }
                }
            }
            closedir($handle);
        }
        return $list;
    }
    
    static function colorPalette($src, $numColors, $granularity = 2) {
        $granularity = max(1, abs((int)$granularity)); 
        $colors = array(); 
        $size = @getimagesize($src); 
        if($size === false) {
            user_error("Unable to get image size data"); 
            return false; 
        }

        // Get the width and height.
        $width = $size[0];
        $height = $size[1];

        $img = imagecreatefromstring(file_get_contents($src));
        
        if(!$img) {
           user_error("Unable to open image file"); 
           return false; 
        }

        // Create a white background, the same size as the original.
        $bg = imagecreatetruecolor($width, $height);
        $white = imagecolorallocate($bg, 255, 255, 255);
        imagefill($bg, 0, 0, $white);

        // Merge the two images.
        imagecopyresampled(
            $bg, $img,
            0, 0, 0, 0,
            $width, $height,
            $width, $height);

        imagefilter($img, IMG_FILTER_GRAYSCALE);

        for($x = 0; $x < $size[0]; $x += $granularity) {
           for($y = 0; $y < $size[1]; $y += $granularity) {
              $thisColor = imagecolorat($img, $x, $y); 
              $rgb = imagecolorsforindex($img, $thisColor); 
              $red = round(round(($rgb['red'] / 0x33)) * 0x33); 
              $green = round(round(($rgb['green'] / 0x33)) * 0x33); 
              $blue = round(round(($rgb['blue'] / 0x33)) * 0x33); 
              $thisRGB = sprintf('%02X%02X%02X', $red, $green, $blue); 
              if(array_key_exists($thisRGB, $colors)) {
                 $colors[$thisRGB]++;
              } else {
                 $colors[$thisRGB] = 1; 
              }
           }
        }

        arsort($colors);

        return array_slice(array_keys($colors), 0, $numColors);
    }
    
    static function getAverageColor($sourceURL,$returnArray = false) {
        
        $palette = Palette::fromFilename($sourceURL);
        
        if($palette->count()) {
        // an extractor is built from a palette
        $extractor = new ColorExtractor($palette);
        
        // it defines an extract method which return the most “representative” colors
            $colors = $extractor->extract(1);
            
            $hex = Color::fromIntToHex($colors[0],false);

            $hsl = Image::hexToHsl(str_split($hex));
            $hsl[1]*= 0.75;
            $hsl[2]*= 0.75;

            $hex = Image::hslToHex($hsl);

            if($returnArray) {
                return Image::hex2rgb($hex);
            } else {
                return "#".$hex;
            }
        }
        
            if($returnArray) {
            return Image::hex2rgb("000000");
            } else {
            return "#000000";
            }
        //var_dump($colors);
    }
    
    static function adjustBrightness($hex, $steps) {
        // Steps should be between -255 and 255. Negative = darker, positive = lighter
        $steps = max(-255, min(255, $steps));

        // Normalize into a six character long hex string
        $hex = str_replace('#', '', $hex);
        if (strlen($hex) == 3) {
            $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
        }

        // Split into three parts: R, G and B
        $color_parts = str_split($hex, 2);
        $return = '#';

        foreach ($color_parts as $color) {
            $color   = hexdec($color); // Convert to decimal
            $color   = max(0,min(255,$color + $steps)); // Adjust color
            $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
        }

        return $return;
    }
    
    /*
     * Invert an HTML color code
     */
    static function colorInverse($color,$multi=1){
        $color = str_replace('#', '', $color);
        if (strlen($color) != 6){ return '#000000'; }
        $rgb = '';
        for ($x=0;$x<3;$x++){
            $c = 255 - (hexdec(substr($color,(2*$x),2))*$multi);
            $c = ($c < 0) ? 0 : dechex($c);
            $rgb .= (strlen($c) < 2) ? '0'.$c : $c;
        }
        return '#'.$rgb;
    }
    
    /*
     * Multiply an HTML color code
     */
    static function colorMultiply($color,$multi){
        $color = str_replace('#', '', $color);
        if (strlen($color) != 6){ return '#000000'; }
        $rgb = '';
        for ($x=0;$x<3;$x++){
            $c = hexdec(substr($color,(2*$x),2)) * $multi;
            $c = dechex(max(0,min(255,$c)));
            $rgb .= (strlen($c) < 2) ? '0'.$c : $c;
        }
        return '#'.$rgb;
    }
    
    static function hex2rgb($hex_str, $return_string = false, $separator = ',') {
        $hex_str = preg_replace("/[^0-9A-Fa-f]/", '', $hex_str); // Gets a proper hex string
        $rgb_array = array();
        if( strlen($hex_str) == 6 ) {
            $color_val = hexdec($hex_str);
            $rgb_array['r'] = 0xFF & ($color_val >> 0x10);
            $rgb_array['g'] = 0xFF & ($color_val >> 0x8);
            $rgb_array['b'] = 0xFF & $color_val;
        } elseif( strlen($hex_str) == 3 ) {
            $rgb_array['r'] = hexdec(str_repeat(substr($hex_str, 0, 1), 2));
            $rgb_array['g'] = hexdec(str_repeat(substr($hex_str, 1, 1), 2));
            $rgb_array['b'] = hexdec(str_repeat(substr($hex_str, 2, 1), 2));
        } else {
            return false;
        }
        return $return_string ? implode($separator, $rgb_array) : $rgb_array;
    }
    
    static function hexToHsl($hex) {
        $hex = array($hex[0].$hex[1], $hex[2].$hex[3], $hex[4].$hex[5]);
        $rgb = array_map(function($part) {
            return hexdec($part) / 255;
        }, $hex);

        $max = max($rgb);
        $min = min($rgb);

        $l = ($max + $min) / 2;

        if ($max == $min) {
            $h = $s = 0;
        } else {
            $diff = $max - $min;
            $s = $l > 0.5 ? $diff / (2 - $max - $min) : $diff / ($max + $min);

            switch($max) {
                case $rgb[0]:
                    $h = ($rgb[1] - $rgb[2]) / $diff + ($rgb[1] < $rgb[2] ? 6 : 0);
                    break;
                case $rgb[1]:
                    $h = ($rgb[2] - $rgb[0]) / $diff + 2;
                    break;
                case $rgb[2]:
                    $h = ($rgb[0] - $rgb[1]) / $diff + 4;
                    break;
            }

            $h /= 6;
        }

        return array($h, $s, $l);
    }

    static function hslToHex($hsl)
    {
        list($h, $s, $l) = $hsl;

        if ($s == 0) {
            $r = $g = $b = 1;
        } else {
            $q = $l < 0.5 ? $l * (1 + $s) : $l + $s - $l * $s;
            $p = 2 * $l - $q;

            $r = Image::hue2rgb($p, $q, $h + 1/3);
            $g = Image::hue2rgb($p, $q, $h);
            $b = Image::hue2rgb($p, $q, $h - 1/3);
        }

        return Image::rgb2hex($r) . Image::rgb2hex($g) . Image::rgb2hex($b);
    }

    static function hue2rgb($p, $q, $t) {
        if ($t < 0) $t += 1;
        if ($t > 1) $t -= 1;
        if ($t < 1/6) return $p + ($q - $p) * 6 * $t;
        if ($t < 1/2) return $q;
        if ($t < 2/3) return $p + ($q - $p) * (2/3 - $t) * 6;

        return $p;
    }

    static function rgb2hex($rgb) {
        return str_pad(dechex($rgb * 255), 2, '0', STR_PAD_LEFT);
    }
    
    
    static function readyCaptcha($config = array()) {
        // Check for GD library
        if( !function_exists('gd_info') ) {
            throw new Exception('Required GD library is missing');
        }

        $bg_path = API_PATH_UPLOADS.'/captcha/backgrounds/';
        $font_path = API_PATH_UPLOADS.'/captcha/fonts/';
        
        // Default values
        $captcha_config = array(
            'code' => '',
            'min_length' => 5,
            'max_length' => 5,
            'backgrounds' => array(
                $bg_path . '45-degree-fabric.png',
                $bg_path . 'cloth-alike.png',
                $bg_path . 'grey-sandbag.png',
                $bg_path . 'kinda-jean.png',
                $bg_path . 'polyester-lite.png',
                $bg_path . 'stitched-wool.png',
                $bg_path . 'white-carbon.png',
                $bg_path . 'white-wave.png'
            ),
            'fonts' => array(
                $font_path . 'times_new_yorker.ttf'
            ),
            'characters' => 'ABCDEFGHJKLMNPRSTUVWXYZabcdefghjkmnprstuvwxyz23456789',
            'min_font_size' => 28,
            'max_font_size' => 28,
            'color' => '#555',
            'angle_min' => 0,
            'angle_max' => 10,
            'shadow' => true,
            'shadow_color' => '#111',
            'shadow_offset_x' => -1,
            'shadow_offset_y' => 1
        );

        // Overwrite defaults with custom config values
        if( is_array($config) ) {
            foreach( $config as $key => $value ) $captcha_config[$key] = $value;
        }

        // Restrict certain values
        if( $captcha_config['min_length'] < 1 ) $captcha_config['min_length'] = 1;
        if( $captcha_config['angle_min'] < 0 ) $captcha_config['angle_min'] = 0;
        if( $captcha_config['angle_max'] > 10 ) $captcha_config['angle_max'] = 10;
        if( $captcha_config['angle_max'] < $captcha_config['angle_min'] ) $captcha_config['angle_max'] = $captcha_config['angle_min'];
        if( $captcha_config['min_font_size'] < 10 ) $captcha_config['min_font_size'] = 10;
        if( $captcha_config['max_font_size'] < $captcha_config['min_font_size'] ) $captcha_config['max_font_size'] = $captcha_config['min_font_size'];

        // Use milliseconds instead of seconds
        srand(microtime() * 100);

        // Generate CAPTCHA code if not set by user
        if( empty($captcha_config['code']) ) {
            $captcha_config['code'] = '';
            $length = rand($captcha_config['min_length'], $captcha_config['max_length']);
            while( strlen($captcha_config['code']) < $length ) {
                $captcha_config['code'] .= substr($captcha_config['characters'], rand() % (strlen($captcha_config['characters'])), 1);
            }
        }

        // Generate HTML for image src
        $image_src = 'service/captcha?t=' . urlencode(microtime());//substr(__FILE__, strlen( realpath($_SERVER['DOCUMENT_ROOT']) )) . '?_CAPTCHA&amp;t=' . urlencode(microtime());
        $image_src = ltrim(preg_replace('/\\\\/', '/', $image_src), '/');

        $_SESSION['_CAPTCHA']['config'] = serialize($captcha_config);

        return array(
            'code' => $captcha_config['code'],
            'image_src' => $image_src
        );
    }
}