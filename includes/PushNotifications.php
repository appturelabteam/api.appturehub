<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PushNotifications
 *
 * @author dewal
 */
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Messaging;

class PushNotifications {
    
    /**
     * 
     * details {
     *      title,
     *      body,
     *      icon, // ie file-add
     *      hash, // view a specific page in app
     *      priority => 'high' or 'normal'
     * }
     * 
     * @param type $deviceToken
     * @param type $details
     */
    static public function SendToDevice($deviceToken, $details) {
        
        $serviceAccount = ServiceAccount::fromJsonFile((API_MODE === API_MODE_DEVELOPMENT ? 'notPublic/' : 'file://' . __DIR__ . '/../../../notPublic/').'crafted-arts-1517167131657-firebase-adminsdk-xdkwo-4aaa21ac17.json');
        
        if(
            $firebase = (new Factory)
                ->withServiceAccount($serviceAccount)
                ->create() ) {

            if($deviceToken) {

                $payload = [
                    //'topic' => 'my-topic',
                    // 'condition' => "'TopicA' in topics && ('TopicB' in topics || 'TopicC' in topics)",
                    'token' => $deviceToken,
                    'notification' => [
                        'title' => $details["title"],
                        'body' => $details["body"],
                    ],
                    'data' => [
                        'hash' => $details["hash"]
                    ],
                    'android' => [
                        'priority' => isset($details["priority"]) ? $details["priority"] : 'normal',
                        'notification' => [
                            'title' => $details["title"],
                            'body' => $details["body"],
                            'icon' => !empty($details["icon"]) ? $details["icon"] : 'notice_icon',
                            'color' => '#296f45',
                            'sound' => 'default'
                        ],
                    ],
                    'apns' => [
                        'headers' => [
                            'apns-priority' => !empty($details["priority"]) && $details["priority"] === 'high' ? '10' : '5',
                        ],
                        'payload' => [
                            'aps' => [
                                'alert' => [
                                    'title' => $details["title"],
                                    'body' => $details["body"],
                                ],
                                'badge' => 0,
                                'sound' => 'default'
                            ],
                        ],
                    ],
                    'webpush' => [
                        'notification' => [
                            'title' => $details["title"],
                            'body' => $details["body"],
                            'icon' => UI_URL.'img/notice/'.(!empty($details["icon"]) ? $details["icon"] : 'notice_icon').'.png',
                        ],
                    ],
                ];
                
                try {

                    $response = $firebase->getMessaging()->send($payload);

                    LogController::log("FIREBASE_FCM", "SEND_TO_DEVICE", 0, 0, 0, json_encode(array("payload" => $payload, "response"=>$response)), ""); // expires in 1 hour
                    
                } catch (Exception $e) {
                    
                    $response = array("success" => false, "message" => "Messaging Exception", "data" => json_encode($e));
                    
                }

            } else {

                $response = array("success" => false, "message" => "Empty token");

            }
            
        } else {
            
            $response = array("success" => false, "message" => "Firebase error");
            
        }
        
        return $response;
    }
}