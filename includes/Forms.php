<?php
class Forms {
    static public $form_state = array();
    static private $upload_path = "";

    static function init($form_name,$fields,$fieldsData=null,$owner_id=-1) {
        Forms::$form_state[$form_name] = array();
        Forms::$form_state[$form_name]["invalid"] = array(); // initialize valid array
        
        foreach($fields as $key=>$value) {
            $permission = "edit"; // if no permissions are set allow edit
            if(isset($value["Permissions"]["view"])) { $permission = (SessionUser::hasRoles($value["Permissions"]["view"],null,$owner_id) ? "view" : "none"); }
            if(isset($value["Permissions"]["edit"])) { $permission = (SessionUser::hasRoles($value["Permissions"]["edit"],null,$owner_id) ? "edit" : $permission); }
            
            //print $key."=".$permission."<br>";
            if($permission=="none") {continue;}
            
            if(isset($value["Extra"]) && stripos($value["Extra"],"auto_increment")!==false) { continue; }
            
            if(stripos($value["Type"],"varchar")!==false ||
                    stripos($value["Type"],"Textbox")===0 ||
                    stripos($value["Type"],"color")!==false ||
                    stripos($value["Type"],"tel")!==false ||
                    stripos($value["Type"],"captcha")!==false ||
                    stripos($value["Type"],"address")!==false ||
                    stripos($value["Type"],"hidden")!==false) {
                // text
                Forms::$form_state[$form_name][$key] = ($fieldsData ? $fieldsData[$key] : $value["Default"]);
            } else if(stripos($value["Type"],"username")===0) {
                // username
                Forms::$form_state[$form_name][$key] = ($fieldsData ? $fieldsData[$key] : $value["Default"]);
            } else if(stripos($value["Type"],"email")===0) {
                // email
                Forms::$form_state[$form_name][$key] = ($fieldsData ? $fieldsData[$key] : $value["Default"]);
            } else if(stripos($value["Type"],"Password")===0) {
                // password
                Forms::$form_state[$form_name][$key] = "";
            } else if(stripos($value["Type"],"text")!==false || stripos($value["Type"],"Textarea")===0 || stripos($value["Type"],"html")!==false) {
                // textarea
                Forms::$form_state[$form_name][$key] = ($fieldsData ? $fieldsData[$key] : $value["Default"]);
            } else if(stripos($value["Type"],"bit")===0 || stripos($value["Type"],"tinyint")===0 || stripos($value["Type"],"checkbox")===0 || stripos($value["Type"],"boolean")===0 || stripos($value["Type"],"select")===0) {
                // checkbox or select
                if($fieldsData) {
                    Forms::$form_state[$form_name][$key] = (is_array($fieldsData[$key]) ? $fieldsData[$key] : explode(',',$fieldsData[$key]));
                } else {
                    Forms::$form_state[$form_name][$key] = array($value["Default"]);
                    /*foreach($value['Items'] as $itemLabel=>$itemValue) {
                        if($itemValue[1]===true) {
                            Forms::$form_state[$form_name][$key][] = $itemValue[0];
                        }
                    }*/
                }
            } else if(stripos($value["Type"],"float")!==false || stripos($value["Type"],"Decimal")===0) {
                // decimal
                Forms::$form_state[$form_name][$key] = ($fieldsData ? $fieldsData[$key] : $value["Default"]);
            } else if(stripos($value["Type"],"int")===0 || stripos($value["Type"],"Integer")===0) {
                // integer
                Forms::$form_state[$form_name][$key] = ($fieldsData ? $fieldsData[$key] : $value["Default"]);
            } else if(stripos($value["Type"],"timestamp")!==false || stripos($value["Type"],"date")===0 || stripos($value["Type"],"datetime")===0) {
                // datepicker
                Forms::$form_state[$form_name][$key] = ($fieldsData ? $fieldsData[$key] : ($value["Default"] == "CURRENT_TIMESTAMP" ? date("Y-m-d H:i:s") : $value["Default"]));
            } else if(stripos($value["Type"],"file")===0 || stripos($value["Type"],"image")===0 || stripos($value["Type"],"multi_image")===0) {
                // file/image upload
                Forms::$form_state[$form_name][$key] = ($fieldsData ? $fieldsData[$key] : $value["Default"]);
            }
        }
    }

    static function processFieldPost($form_name,$field,$value) {
        // save post values into form_state
        if(isset($_POST["{$form_name}_{$field}"])) {
            //print $form_name."_".$field." ({$value["Type"]}) = ".$_POST["{$form_name}_{$field}"].PHP_EOL;
            //if($value["Type"] == "image") {
            //    print print_r($_FILES["{$form_name}_{$field}_filename"], true).PHP_EOL;
            //}
            $prev_value = Forms::$form_state[$form_name][$field];
            Forms::$form_state[$form_name][$field] = $_POST["{$form_name}_{$field}"];
            
            // if the field is a file upload field or image field process the upload here
            if((stripos($value["Type"],"image")!==false || stripos($value["Type"],"file")!==false) &&
                    isset($_FILES["{$form_name}_{$field}_filename"]) && $_FILES["{$form_name}_{$field}_filename"]["name"]!="") {
                if(isset($value["Extensions"]) && array_search(strtolower(substr($_FILES["{$form_name}_{$field}_filename"]["name"],strripos($_FILES["{$form_name}_{$field}_filename"]["name"], ".")+1)),$value["Extensions"]) === false) {
                    Forms::$form_state[$form_name]["invalid"][$field]=array(
                        'status'=>'error',
                        'message'=>"Bad file extension; allowed extensions: ".implode(",",$value["Extensions"])
                    );
                } else {
                    //print "upload file: ".$_FILES["{$form_name}_{$field}_filename"]["name"].PHP_EOL;
                    if($result = Forms::upload($_FILES["{$form_name}_{$field}_filename"],"",array("prev_filename"=>$prev_value))) {
                        if(strpos($result,"ERROR")!==false) {
                            // file could not be uploaded
                            Forms::$form_state[$form_name]["invalid"][$field]=array(
                                'status'=>'error',
                                'message'=>str_replace("ERROR: ", "", $result. " Max allowed file size: ".  ini_get("upload_max_filesize").".")
                            );
                        } else {
                            if(stripos($value["Type"],"multi")!==false) {
                                $items = array_filter(explode(",",Forms::$form_state[$form_name][$field]));
                                if(!in_array($result, $items)) {
                                    $items[] = $result;
                                }
                                Forms::$form_state[$form_name][$field] = implode(",",$items);
                            } else {
                                Forms::$form_state[$form_name][$field] = $result;
                            }
                        }
                    }
                }
            }
        } else if(stripos($value["Null"],"no")!==false) {
            Forms::$form_state[$form_name][$field] = $value["Default"];
        } else if(!isset($_POST["{$form_name}_{$field}"]) && (stripos($value["Type"],"boolean")!==false || stripos($value["Type"],"tinyint(1)")!==false || stripos($value['Type'],'checkbox')!==false || stripos($value['Type'],'SELECT_MULTI')!==false || stripos($value['Type'],'SELECT_MULTI_ADD')!==false)) {
            // some input types are not posted if they have no set value, like a boolean checkbox, if so we need to set it to an empty array here so it can be saved properly
            Forms::$form_state[$form_name][$field] = array("");
        }
    }
    
    /*
     * validate
     * 
     * Returns null if not valid else an array of $fieldsData.
     */
    static function validate($form_name,$fields,$owner_id=-1) {
        Forms::$form_state[$form_name]["invalid"] = array();
        
        $fieldsData = array();
        
        // validate the posted values
        foreach($fields as $field=>$value) {
            if(isset($value["Extra"]) && stripos($value["Extra"],"auto_increment")!==false) { continue; }
            
            $permission = "edit"; // if no permissions are set allow edit
            if(isset($value["Permissions"]["view"])) { $permission = (SessionUser::hasRoles($value["Permissions"]["view"],null,$owner_id) ? "view" : "none"); }
            if(isset($value["Permissions"]["edit"])) { $permission = (SessionUser::hasRoles($value["Permissions"]["edit"],null,$owner_id) ? "edit" : $permission); }
            
            if($permission=="none") {continue;}
            //print $field."=".$permission."<br>";
            
            if(count($_POST) && !(isset($_POST["{$form_name}_submit"]) && $_POST["{$form_name}_submit"]=="Load")) {
                Forms::processFieldPost($form_name, $field, $value); 
                
                if(isset($_POST["{$form_name}_submit"]) && 
                    $_POST["{$form_name}_submit"]!="Cancel" && 
                    $_POST["{$form_name}_submit"]!="Upload" &&
                    $_POST["{$form_name}_submit"]!="Delete" &&
                    $_POST["{$form_name}_submit"]!="Remove") {
                
                    if(isset(Forms::$form_state[$form_name][$field])) {
                        $fieldsData[$field] = Forms::$form_state[$form_name][$field];
                    }

                    if(stripos($fields[$field]['Null'],'no')!==false) {
                        // see if valid
                        $error = true;
                        if(isset(Forms::$form_state[$form_name][$field]) &&
                            ( is_array(Forms::$form_state[$form_name][$field]) && (count(Forms::$form_state[$form_name][$field]) && array_search('',Forms::$form_state[$form_name][$field])===false) ||
                            (!is_array(Forms::$form_state[$form_name][$field]) && Forms::$form_state[$form_name][$field]!=''))) {
                            $error = false;
                        }

                        if(!isset(Forms::$form_state[$form_name][$field]) || $error ) {
                            Forms::$form_state[$form_name]["invalid"][$field]=array(
                                'status'=>'error',
                                'message'=>'This field is required'
                            );
                        }
                    }

                    if(stripos($fields[$field]['Type'],'typeahead')===0) {
                        if(!array_key_exists(strtolower(Forms::$form_state[$form_name][$field]), array_change_key_case($fields[$field]['Items']))) {
                            Forms::$form_state[$form_name]["invalid"][$field]=array(
                                'status'=>'error',
                                'message'=>'Value not found.'
                            );
                        } else {
                            Forms::$form_state[$form_name][$field] = $fields[$field]['Items'][Forms::$form_state[$form_name][$field]][0];
                        }
                    }
                    
                    if(stripos($fields[$field]['Type'],'email')===0) {
                        if(Forms::$form_state[$form_name][$field] != "" && (
                                stripos(Forms::$form_state[$form_name][$field],"@") === false || 
                                stripos(Forms::$form_state[$form_name][$field],".") === false || 
                                stripos(Forms::$form_state[$form_name][$field],"@")+1 == stripos(Forms::$form_state[$form_name][$field],".") || 
                                stripos(Forms::$form_state[$form_name][$field],".")+1 == strlen(Forms::$form_state[$form_name][$field]) )) {
                            Forms::$form_state[$form_name]["invalid"][$field]=array(
                                'status'=>'error',
                                'message'=>'Not a valid email address.'
                            );
                        } else if(stripos($fields[$field]['Extra'],'account')!==false){
                            if(count(DS::select("users", "WHERE email = ?s ",Forms::$form_state[$form_name][$field]))) {
                                Forms::$form_state[$form_name]["invalid"][$field]=array(
                                    'status'=>'error',
                                    'message'=>'An account with this address already exists.'
                                );
                            }
                        }
                    }
                    
                    if(stripos($fields[$field]['Type'],'tel')===0) {
                        if(Forms::$form_state[$form_name][$field] != "" && (
                                strlen(Forms::$form_state[$form_name][$field]) < 10 || 
                                strlen(Forms::$form_state[$form_name][$field]) > 12 )) {
                            Forms::$form_state[$form_name]["invalid"][$field]=array(
                                'status'=>'error',
                                'message'=>'Not a valid phone number.'
                            );
                        }
                    }

                    if(stripos($fields[$field]['Type'],'PASSWORD')==0 &&
                            isset($_POST[$form_name."_confirm_".str_replace($form_name, "", $field)]) &&
                            $_POST[$form_name."_confirm_".str_replace($form_name, "", $field)] != $_POST["{$form_name}_{$field}"]) {

                        Forms::$form_state[$form_name]["invalid"][$field]=array(
                            'status'=>'error',
                            'message'=>'Passwords do not match.'
                        );
                    }

                    if(stripos($fields[$field]['Type'],'DATEPICKER')===0 && strtotime(Forms::$form_state[$form_name][$field])===false) {
                        Forms::$form_state[$form_name]["invalid"][$field]=array(
                            'status'=>'error',
                            'message'=>'Not a valid date.'
                        );
                    }
                    
                    if(stripos($fields[$field]['Type'],'decimal')===0 && !is_numeric(Forms::$form_state[$form_name][$field])) {
                        Forms::$form_state[$form_name]["invalid"][$field]=array(
                            'status'=>'error',
                            'message'=>'Not a valid number.'
                        );
                    }
                    
                    if(stripos($fields[$field]['Type'],'int')===0 && !is_numeric(Forms::$form_state[$form_name][$field])) {
                        Forms::$form_state[$form_name]["invalid"][$field]=array(
                            'status'=>'error',
                            'message'=>'Not a valid integer value.'
                        );
                    }
                    
                    if(stripos($fields[$field]['Type'],'float')!==false && !is_numeric(Forms::$form_state[$form_name][$field])) {
                        Forms::$form_state[$form_name]["invalid"][$field]=array(
                            'status'=>'error',
                            'message'=>'Not a valid floating point number.'
                        );
                    }
                    
                    if(stripos($fields[$field]['Type'],'double')!==false && !is_numeric(Forms::$form_state[$form_name][$field])) {
                        Forms::$form_state[$form_name]["invalid"][$field]=array(
                            'status'=>'error',
                            'message'=>'Not a valid double value.'
                        );
                    }
                }
            }
        }
        
        return (count(Forms::$form_state[$form_name]["invalid"]) ? null : (count($fieldsData) ? $fieldsData : null));
    }
    
    static function isValidEmail($address) {
        $partsA = explode("@",$address);
        if(count($partsA) >= 2 && strlen($partsA[0]) > 2 && strlen($partsA[1]) > 3) {
            $partsB = explode(".",$partsA[1]);
            if(count($partsB) >= 2 && strlen($partsB[0]) > 1 && strlen($partsB[1]) > 1) {
                return true;
            }
        }
        return false;
    }


    /*
     * validateData
     * 
     * Returns an array of invalid fields with a status and message for each
     */
    static function validateData($fields,$data,$owner_id=0) {
        $invalid = array();
        
        // validate the posted values
        foreach($fields as $field=>$value) {
            if(isset($value["Extra"]) && stripos($value["Extra"],"auto_increment")!==false) { continue; }
            
            $permission = "edit"; // if no permissions are set allow edit
            if(isset($value["Permissions"]["view"])) { $permission = (SessionUser::hasRoles($value["Permissions"]["view"],null,$owner_id) ? "view" : "none"); }
            if(isset($value["Permissions"]["edit"])) { $permission = (SessionUser::hasRoles($value["Permissions"]["edit"],null,$owner_id) ? "edit" : $permission); }
            
            if($permission=="none") {continue;}
            //print $field."=".$permission."<br>";
            
            try {
                if(stripos($fields[$field]['Null'],'no')!==false) {
                    // see if valid
                    $error = true;
                    if(isset($data[$field]) &&
                        ( is_array($data[$field]) && (count($data[$field]) && array_search('',$data[$field])===false) ||
                        (!is_array($data[$field]) && $data[$field] !== ''))) {
                        $error = false;
                    }

                    if(!isset($data[$field]) || $error ) {
                        $invalid[$field]=array(
                            'status'=>'danger',
                            'message'=>'This field is required'
                        );
                    }
                }
            } catch(Exception $e) {
                error_log("Error with field info {$field}: No Null property set".PHP_EOL.$e->getTraceAsString());
            }
            
            if(!isset($data[$field])) continue;
            
            try {
                if(stripos($fields[$field]['Type'],'email')===0) {
                    //print "email field";
                    if($data[$field] != "") {
                        if(stripos($data[$field],"@") === false || 
                                stripos($data[$field],".") === false || 
                                stripos($data[$field],"@")+1 == stripos($data[$field],".") || 
                                stripos($data[$field],".")+1 == strlen($data[$field]) ) {
                            $invalid[$field]=array(
                                'status'=>'danger',
                                'message'=>'Not a valid email address.'
                            );
                        } else
                        // account check
                        // required values: AccountsTable, AccountsEmailField, AccountsUserIDField, AccountsUserID
                        if(stripos($fields[$field]['Extra'],'account')!==false && isset($fields[$field]['AccountsTable']) && isset($fields[$field]['AccountsEmailField']) && isset($fields[$field]['AccountsUserIDField'])) {
                            if(count(DS::query("SELECT * FROM {$fields[$field]['AccountsTable']} WHERE {$fields[$field]['AccountsEmailField']} = ?s".(isset($fields[$field]['AccountsUserID']) ? " AND {$fields[$field]['AccountsUserIDField']} != ?i" : ""), $data[$field], isset($fields[$field]['AccountsUserID']) ? $fields[$field]['AccountsUserID'] : 0))) {
                                $invalid[$field]=array(
                                    'status'=>'danger',
                                    'message'=>'An account with this address already exists.'
                                );
                            }
                        }
                    }
                }
                
                if(stripos($fields[$field]['Type'],'tel')===0) {
                    //print "email field";
                    if($data[$field] != "") {
                        if(strlen($data[$field]) < 10 || 
                                strlen($data[$field]) > 12 ) {
                            $invalid[$field]=array(
                                'status'=>'danger',
                                'message'=>'Not a valid phone number.'
                            );
                        }
                    }
                }

                if(stripos($fields[$field]['Type'],'PASSWORD') !== false) {
                    if (!isset($data["confirm_password"]) || 
                        (isset($data["confirm_password"]) && $data["confirm_password"] != $data[$field])) {

                        $invalid["confirm_password"]=array(
                            'status'=>'danger',
                            'message'=>'Passwords do not match.'
                        );
                    }
                }

                if(stripos($fields[$field]['Type'],'DATEPICKER')===0 && strtotime($data[$field])===false) {
                    $invalid[$field]=array(
                        'status'=>'danger',
                        'message'=>'Not a valid date.'
                    );
                }

                if(stripos($fields[$field]['Type'],'decimal')===0 && !is_numeric($data[$field])) {
                    $invalid[$field]=array(
                        'status'=>'error',
                        'message'=>'Not a valid number.'
                    );
                }

                if(stripos($fields[$field]['Type'],'int')===0 && $data[$field] !== "" && !is_numeric($data[$field])) {
                    $invalid[$field]=array(
                        'status'=>'danger',
                        'message'=>'Not a valid integer value.'
                    );
                }

                if(stripos($fields[$field]['Type'],'file_id')===0 && stripos($fields[$field]['Null'],'no')!==false && (
                        !is_numeric($data[$field]) || ( is_numeric($data[$field]) && $data[$field] === 0 ) ) ) {
                    $invalid[$field]=array(
                        'status'=>'danger',
                        'message'=>'This field is required.'
                    );
                }

                if(stripos($fields[$field]['Type'],'float')!==false && !is_numeric($data[$field])) {
                    $invalid[$field]=array(
                        'status'=>'danger',
                        'message'=>'Not a valid floating point number.'
                    );
                }

                if(stripos($fields[$field]['Type'],'double')!==false && !is_numeric($data[$field])) {
                    $invalid[$field]=array(
                        'status'=>'danger',
                        'message'=>'Not a valid double value.'
                    );
                }

                if((stripos($fields[$field]['Type'],'text')!==false || stripos($fields[$field]['Type'],'varchar')!==false)
                        && strip_tags($data[$field], '<a>,<strong>,<br>,<small>,<em>,<p>,<b>,<h1>,<h2>,<h3>,<ol>,<ul>,<li>,<img>') !== $data[$field] ) {
                    $invalid[$field]=array(
                        'status'=>'danger',
                        'message'=>'HTML tags are not allowed.'
                    );
                }
            } catch(Exception $e) {
                error_log("Error with field info {$field}: No Type property set".PHP_EOL.$e->getTraceAsString());
            }
        }
        
        return $invalid;
    }
    
    static function upload($file,$target_path="",$sub_folder="",$filename=null) {
        if($target_path === "") {
            $target_path = Forms::$upload_path;
        }
        
        $filename = ($filename ? basename($filename) : basename($file['name']));
        
        // create the target folders
        if(!file_exists($target_path. ($sub_folder !== "" ? "/".$sub_folder : "")) ) {
            if(!mkdir($target_path. ($sub_folder !== "" ? "/".$sub_folder : ""), 0775))
                return "ERROR: Could not create folder.";
        }
        
        if(intval($file['error']) === 1 || intval($file['error']) === 2) {
            return "ERROR: File size > ".ini_get("upload_max_filesize");
        }
        
        if(!file_exists($file['tmp_name'])) {
            return "ERROR: File does not exist ({$file['tmp_name']}).";
        }
        
        if(move_uploaded_file($file['tmp_name'], $target_path. ($sub_folder !== "" ? "/".$sub_folder : ""). "/". $filename)) {
            return ($sub_folder !== "" ? "/".$sub_folder : ""). "/". $filename;
        }
        
        return "ERROR: Could not upload file ({$file['tmp_name']}).";
    }
    
    static function saveUpload($content,$target_path="",$sub_folder="",$filename=null) {
        if($target_path === "") {
            $target_path = Forms::$upload_path;
        }
        
        $filename = ($filename ? basename($filename) : basename($file['name']));
        
        // create the target folders
        if(!file_exists($target_path. ($sub_folder !== "" ? "/".$sub_folder : "")) ) {
            if(!mkdir($target_path. ($sub_folder !== "" ? "/".$sub_folder : ""), 0775))
                return "ERROR: Could not create folder.";
        }
        var_dump($content);
        $result = file_put_contents($target_path. ($sub_folder !== "" ? "/".$sub_folder : ""). "/". $filename, base64_decode($content));
        
        if($result !== false) {
            return ($sub_folder !== "" ? "/".$sub_folder : ""). "/". $filename;
        } else {
            return "ERROR: Could not save file contents";
        }
    }
    
    static function arrangeFields($fields) {
        $arrangedFields = array();
        $weightedFields = array();
        $otherFields = array();
        foreach($fields as $field=>$value) {
            if(isset($value["weight"])) {
                $weight = $value["weight"];
                while(isset($weightedFields[$weight])) {
                    $weight+="_";
                }
                $weightedFields[$weight] = $value;
            } else {
                $otherFields[] = $value;
            }
        }
        ksort($weightedFields);
        foreach(array_merge($weightedFields,$otherFields) as $x=>$value) {
            $arrangedFields[$value["Field"]] = $value;
        }
        return $arrangedFields;
    }

    static function getState($form_name) {
        return Forms::$form_state[$form_name];
    }
    
    static function getUploadPath() {
        return Forms::$upload_path;
    }
    
    static function setUploadPath($path) {
        Forms::$upload_path = $path;
    }
}