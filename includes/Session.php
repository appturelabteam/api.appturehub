<?php
//include_once 'DataSource.php';
class Session {
    static private $session_length = 60;//3600;

    static function start($session_name="sec_session_id") {
        ob_start();
        
        if(session_status() !== PHP_SESSION_ACTIVE) {
            $secure = false; // Set to true if using https.
            $httponly = true; // This stops javascript being able to access the session id.

            ini_set('session.gc_maxlifetime', Session::$session_length);
            ini_set('session.cookie_lifetime', Session::$session_length);
            ini_set('session.gc_probability', 0);
            ini_set('session.gc_divisor', 100);
            //ini_set('session.cookie_secure', FALSE);
            ini_set('session.use_only_cookies', 1); // Forces sessions to only use cookies.
            ini_set('session.use_trans_sid', 0); // session IDs should never be passed via url if cookies are disabled
            //echo ini_get("session.gc_maxlifetime");

            $cookieParams = session_get_cookie_params(); // Gets current cookies params.
            session_set_cookie_params(Session::$session_length, $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
            session_name($session_name); // Sets the session name to the one set above.
            session_start(); // Start the php session
        }
        
        $_SESSION["call"] = (isset($_SESSION["call"]) ? $_SESSION["call"] : 0)+1;
        //$_SESSION["timeout"] = (isset($_SESSION["timeout"]) ? $_SESSION["timeout"] : null);
        
        if (!isset($_SESSION['CREATED'])) {
            $_SESSION['CREATED'] = time();
        } else if (time() - $_SESSION['CREATED'] > Session::$session_length) {
            //session_abort();
            // session started more than 30 minutes ago
            //session_regenerate_id(true);    // change session ID for the current session and invalidate old session ID
            //$_SESSION['CREATED'] = time();  // update creation time
        }
    }
}