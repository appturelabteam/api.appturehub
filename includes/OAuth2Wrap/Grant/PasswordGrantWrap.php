<?php
namespace OAuth2Wrap\Grant;

use OAuth2Wrap\Entities\UserEntity;
use League\OAuth2\Server\Grant\PasswordGrant;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\RequestEvent;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;

class PasswordGrantWrap extends PasswordGrant {
    /**
     * @param ServerRequestInterface $request
     * @param ClientEntityInterface  $client
     *
     * @throws OAuthServerException
     *
     * @return UserEntityInterface
     */
    protected function validateUser(ServerRequestInterface $request, ClientEntityInterface $client)
    {
        $username = $this->getRequestParameter('username', $request);
        if (is_null($username)) {
            throw OAuthServerException::invalidRequest('username');
        }

        $password = $this->getRequestParameter('password', $request);
        if (is_null($password)) {
            throw OAuthServerException::invalidRequest('password');
        }

        $userGeneric = $this->userRepository->getUserEntityByUserCredentials(
            $username,
            $password,
            $this->getIdentifier(),
            $client
        );
        
        $user = false;
        
        if($userGeneric) {
            
            $user = new UserEntity();
            $user->setIdentifier($userGeneric['id']);
            $user->setUsername($username);
            $user->setIsActive($userGeneric["is_active"]);
            
            if(!$user->isActive()) {
                
                throw new OAuthServerException("Your account has been deactivated. Please contact site admin.", 11, 'account_deactivated', 400);
                
            } else {
            
                $accountType = $request->getAttribute("account_type");
                if($accountType != null && $userGeneric["account_type"] !== $accountType) {

                    throw new OAuthServerException("You previously used your {$userGeneric["account_type"]} account to sign in.", 12, 'incorrect_account_type', 400);

                } else {

                    if (password_verify($password.$userGeneric['salt'], $userGeneric['password']) === false) {
                        $user = false;
                    }

                    if ($user instanceof UserEntityInterface === false) {
                        $this->getEmitter()->emit(new RequestEvent(RequestEvent::USER_AUTHENTICATION_FAILED, $request));

                        throw OAuthServerException::invalidCredentials();
                    }

                }
                
            }
            
        } else {
            
            throw OAuthServerException::invalidCredentials();
            
        }
        
        return $user;
    }
}