<?php
/**
 * OAuth 2.0 Bearer Token Response.
 *
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */

namespace OAuth2Wrap\ResponseTypes;

use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\ResponseTypes\BearerTokenResponse;
use DS;

class BearerTokenResponseWithParams extends BearerTokenResponse
{
    /**
     * Add custom fields to your Bearer Token response here, then override
     * AuthorizationServer::getResponseType() to pull in your version of
     * this class rather than the default.
     *
     * @param AccessTokenEntityInterface $accessToken
     *
     * @return array
     */
    protected function getExtraParams(AccessTokenEntityInterface $accessToken)
    {
        /*$fees = DS::query("
            SELECT
                transaction_costs,
                facilitation_fee,
                vat,
                ROUND(transaction_costs*100,0) as transaction_costs_perc,
                ROUND(facilitation_fee*100,0) as facilitation_fee_perc,
                ROUND(vat*100,0) as vat_perc
            FROM fees
            WHERE id = (SELECT MAX(id) FROM fees)");
        $fees = count($fees) ? $fees[0] : null;
        
        if($fees) {
            // convert values to floats
            foreach($fees as $key => $fee) {
                $fees[$key] = floatval($fee);
            }
        }*/
        
        $user = DS::query("SELECT notifications_token FROM oauth2_users WHERE id = ?i", $accessToken->getIdentifier());
        
        return ["scope" => $accessToken->getScopes(), "notifications_token" => (count($user) && $user[0]["notifications_token"] ? true : false)];
    }
}