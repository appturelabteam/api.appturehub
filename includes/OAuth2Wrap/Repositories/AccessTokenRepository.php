<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */
namespace OAuth2Wrap\Repositories;

use DS;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use OAuth2Wrap\Entities\AccessTokenEntity;

class AccessTokenRepository implements AccessTokenRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function persistNewAccessToken(AccessTokenEntityInterface $accessTokenEntity)
    {
        // Some logic here to save the access token to a database
        if ($accessTokenEntity instanceof AccessTokenEntity) {
            try {
                DS::insert("oauth2_access_tokens", $accessTokenEntity->convertToDSData());
            } catch(Exception $e) {
                print $e->getMessage();
            }
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function revokeAccessToken($tokenId)
    {
        // Some logic here to revoke the access token
        try {
            DS::delete("oauth2_access_tokens", "WHERE access_token = ?s", $tokenId);
        } catch(Exception $e) {
            print $e->getMessage();
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function isAccessTokenRevoked($tokenId)
    {
        try {
            $access_tokens = DS::select("oauth2_access_tokens", "WHERE access_token = ?s", $tokenId);
            
            return !count($access_tokens) ? true : false; // Has the access token been revoked?
            
        } catch(Exception $e) {
            print $e->getMessage();
        }
        
        return false;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getNewToken(ClientEntityInterface $clientEntity, array $scopes, $userIdentifier = null)
    {
        $accessToken = new AccessTokenEntity();
        $accessToken->setClient($clientEntity);
        foreach ($scopes as $scope) {
            $accessToken->addScope($scope);
        }
        $accessToken->setUserIdentifier($userIdentifier);
        return $accessToken;
    }
}