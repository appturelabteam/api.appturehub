<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */
namespace OAuth2Wrap\Repositories;

use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use OAuth2Wrap\Entities\RefreshTokenEntity;
use DS;

class RefreshTokenRepository implements RefreshTokenRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function persistNewRefreshToken(RefreshTokenEntityInterface $refreshTokenEntity)
    {
        // Some logic to persist the refresh token in a database
        if ($refreshTokenEntity instanceof RefreshTokenEntity) {
            try {
                DS::insert("oauth2_refresh_tokens", $refreshTokenEntity->convertToDSData());
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
        
    }
    
    /**
     * {@inheritdoc}
     */
    public function revokeRefreshToken($tokenId)
    {
        // Some logic to revoke the refresh token in a database
        try {
            DS::delete("oauth2_refresh_tokens", "WHERE refresh_token = ?s", $tokenId);
        } catch (Exception $e) {
            print $e->getMessage();
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function isRefreshTokenRevoked($tokenId)
    {
        
        try {
            $refresh_tokens = DS::select("oauth2_refresh_tokens", "WHERE refresh_token = ?s", $tokenId);
            
            return count($refresh_tokens) ? false : true; // Has the refresh token been revoked?
            
        } catch (Exception $e) {
            print $e->getMessage();
        }
        
        return false;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getNewRefreshToken()
    {
        return new RefreshTokenEntity();
    }
}