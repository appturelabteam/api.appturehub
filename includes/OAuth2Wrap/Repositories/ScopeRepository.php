<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 * 
 * NOTE: For our wrapper we are using scopes like roles.
 */
namespace OAuth2Wrap\Repositories;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use OAuth2Wrap\Entities\ScopeEntity;
use DS;

class ScopeRepository implements ScopeRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getScopeEntityByIdentifier($scopeIdentifier)
    {
        $scopes = DS::select("roles", "WHERE role = ?s", $scopeIdentifier);
        
        // Check if scope exists
        if (!count($scopes)) {
            return;
        }
        
        $scope = new ScopeEntity();
        $scope->setIdentifier($scopeIdentifier);
        
        return $scope;
    }
    /**
     * {@inheritdoc}
     */
    public function finalizeScopes(
        array $scopes,
        $grantType,
        ClientEntityInterface $clientEntity,
        $userIdentifier = null
    ) {
        if($userIdentifier !== null) {
            $roles = DS::select("user_roles", "WHERE user_id = ?i", $userIdentifier);
            
            // all users are granted the authenticated scope
            $scope = new ScopeEntity();
            $scope->setIdentifier("authenticated");
            $scopes[] = $scope;
            
            foreach($roles as $role) {
                $scope = new ScopeEntity();
                $scope->setIdentifier($role["role"]);
                $scopes[] = $scope;
            }
        } else {
            
            if($clientEntity && $clientEntity->isConfidential()) {
                
                $scope = new ScopeEntity();
                $scope->setIdentifier("confidential");
                $scopes[] = $scope;
                
            } else {
            
                $scope = new ScopeEntity();
                $scope->setIdentifier("anonymous");
                $scopes[] = $scope;
            
            }
        }
        
        return $scopes;
    }
}