<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */
namespace OAuth2Wrap\Repositories;

use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use OAuth2Wrap\Entities\ClientEntity;
use DS;

class ClientRepository implements ClientRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getClientEntity($clientIdentifier, $grantType, $clientSecret = null, $mustValidateSecret = true)
    {
        $clients = DS::select("oauth2_clients", "WHERE identifier = ?s AND is_active = 1", $clientIdentifier);
        
        // Check if client is registered
        if (!count($clients)) {
            return;
        }
        
        if (
            $mustValidateSecret === true
            && boolval($clients[0]['is_confidential']) === true
            && password_verify($clientSecret, $clients[0]['secret']) === false
        ) {
            return;
        }
        
        $client = new ClientEntity();
        $client->setIdentifier($clientIdentifier);
        $client->setName($clients[0]['name']);
        $client->setRedirectUri($clients[0]['redirect_uri']);
        $client->setIsActive(true);
        $client->setIsConfidential(boolval($clients[0]['is_confidential']));
        
        return $client;
    }
}