<?php
/**
 * Wrapper for:
 * https://oauth2.thephpleague.com/
 */
namespace OAuth2Wrap;

include_once('includes/OAuth2Wrap/Grant/PasswordGrantWrap.php');
include_once('includes/OAuth2Wrap/Entities/AccessTokenEntity.php');
include_once('includes/OAuth2Wrap/Entities/AuthCodeEntity.php');
include_once('includes/OAuth2Wrap/Entities/ClientEntity.php');
include_once('includes/OAuth2Wrap/Entities/RefreshTokenEntity.php');
include_once('includes/OAuth2Wrap/Entities/ScopeEntity.php');
include_once('includes/OAuth2Wrap/Entities/UserEntity.php');
include_once('includes/OAuth2Wrap/Repositories/AccessTokenRepository.php');
include_once('includes/OAuth2Wrap/Repositories/AuthCodeRepository.php');
include_once('includes/OAuth2Wrap/Repositories/ClientRepository.php');
include_once('includes/OAuth2Wrap/Repositories/RefreshTokenRepository.php');
include_once('includes/OAuth2Wrap/Repositories/ScopeRepository.php');
include_once('includes/OAuth2Wrap/Repositories/UserRepository.php');
include_once('includes/OAuth2Wrap/ResponseTypes/BearerTokenResponseWithParams.php');

use OAuth2Wrap\Grant\PasswordGrantWrap;
use OAuth2Wrap\Repositories\ClientRepository;
use OAuth2Wrap\Repositories\ScopeRepository;
use OAuth2Wrap\Repositories\UserRepository;
use OAuth2Wrap\Repositories\AccessTokenRepository;
use OAuth2Wrap\Repositories\RefreshTokenRepository;
use OAuth2Wrap\ResponseTypes\BearerTokenResponseWithParams;

class OAuth2Wrap {
    
    private static $pathToKeys = (API_MODE === API_MODE_DEVELOPMENT ? 'file://' . __DIR__ . '/../../notPublic/' : 'file://' . __DIR__ . '/../../../../notPublic/');
    private static $encryptionKey = "";
    
    private static function generateRSAKeys() {
        
        if(!file_exists(OAuth2Wrap::$pathToKeys. "private.key")) {
            
            // generate keys and save to $pathToKeys, if none exist
            
            $pkGeneratePrivate;
            $pkGeneratePublic;

            // generate 2048-bit RSA key
            $pkGenerate = openssl_pkey_new(array(
                'private_key_bits' => 2048,
                'private_key_type' => OPENSSL_KEYTYPE_RSA
            ));

            // get the private key
            openssl_pkey_export($pkGenerate,$pkGeneratePrivate); // NOTE: second argument is passed by reference

            // get the public key
            $pkGenerateDetails = openssl_pkey_get_details($pkGenerate);
            $pkGeneratePublic = $pkGenerateDetails['key'];

            // free resources
            openssl_pkey_free($pkGenerate);

            // fetch/import public key from PEM formatted string
            // remember $pkGeneratePrivate now is PEM formatted...
            // this is an alternative method from the public retrieval in previous
            $pkImport = openssl_pkey_get_private($pkGeneratePrivate); // import
            $pkImportDetails = openssl_pkey_get_details($pkImport); // same as getting the public key in previous
            $pkImportPublic = $pkImportDetails['key'];
            openssl_pkey_free($pkImport); // clean up
            
            if(!file_exists(OAuth2Wrap::$pathToKeys)) {
                mkdir(OAuth2Wrap::$pathToKeys, 0600);
            }
            
            file_put_contents(OAuth2Wrap::$pathToKeys. "private.key", $pkGeneratePrivate);
            file_put_contents(OAuth2Wrap::$pathToKeys. "public.key", $pkGeneratePublic);
            
            chmod(OAuth2Wrap::$pathToKeys. "private.key", 0600);
            chmod(OAuth2Wrap::$pathToKeys. "public.key", 0600);
            
            /*echo "\n".$pkGeneratePrivate
                ."\n".$pkGeneratePublic
                ."\n".$pkImportPublic
                ."\n".'Public keys are '.(strcmp($pkGeneratePublic,$pkImportPublic)?'different':'identical').'.';
            exit();*/
            
        }
        
    }
    
    private static function generateEncryptionKey() {
        
        if(!file_exists(OAuth2Wrap::$pathToKeys. "encryption.key")) {
            
            file_put_contents(OAuth2Wrap::$pathToKeys. "encryption.key", base64_encode(random_bytes(32)));
            chmod(OAuth2Wrap::$pathToKeys. "encryption.key", 0600);
        
        }
        
        OAuth2Wrap::$encryptionKey = file_get_contents(OAuth2Wrap::$pathToKeys. "encryption.key");
        
    }

    static function getAuthorizationServer() {
        // Init our repositories
        $clientRepository = new ClientRepository(); // instance of ClientRepositoryInterface
        $scopeRepository = new ScopeRepository(); // instance of ScopeRepositoryInterface
        $accessTokenRepository = new AccessTokenRepository(); // instance of AccessTokenRepositoryInterface
        $userRepository = new UserRepository(); // instance of UserRepositoryInterface
        $refreshTokenRepository = new RefreshTokenRepository(); // instance of RefreshTokenRepositoryInterface
        
        OAuth2Wrap::generateRSAKeys();
        OAuth2Wrap::generateEncryptionKey();
        
        // Path to public and private keys
        $privateKey = new \League\OAuth2\Server\CryptKey(OAuth2Wrap::$pathToKeys. 'private.key', null, API_MODE !== API_MODE_DEVELOPMENT);
        //$privateKey = new CryptKey('file://path/to/private.key', 'passphrase'); // if private key has a pass phrase
        $encryptionKey = OAuth2Wrap::$encryptionKey; // generate using base64_encode(random_bytes(32))

        // Setup the authorization server
        $server = new \League\OAuth2\Server\AuthorizationServer(
            $clientRepository,
            $accessTokenRepository,
            $scopeRepository,
            $privateKey,
            $encryptionKey,
            new BearerTokenResponseWithParams()
        );

        $grant = new PasswordGrantWrap(
             $userRepository,
             $refreshTokenRepository
        );

        $grant->setRefreshTokenTTL(new \DateInterval('P1M')); // refresh tokens will expire after 1 month

        // Enable the password grant on the server
        $server->enableGrantType(
            $grant,
            new \DateInterval('PT1H') // access tokens will expire after 1 hour
        );
        
        // Also enable the client credentials grant on the server
        $server->enableGrantType(
            new \League\OAuth2\Server\Grant\ClientCredentialsGrant(),
            new \DateInterval('PT1H') // access tokens will expire after 1 hour
        );
        
        // Also enable the client credentials grant on the server
        $server->enableGrantType(
            new \League\OAuth2\Server\Grant\RefreshTokenGrant($refreshTokenRepository),
            new \DateInterval('P1M') // access tokens will expire after 1 month
        );
        
        return $server;
    }
    
    static function getResourceServer() {
        
        OAuth2Wrap::generateRSAKeys();
        
        $publicKeyPath = new \League\OAuth2\Server\CryptKey(OAuth2Wrap::$pathToKeys. 'public.key', null, API_MODE !== API_MODE_DEVELOPMENT);

        $server = new \League\OAuth2\Server\ResourceServer(
            new AccessTokenRepository(),
            $publicKeyPath
        );
        
        return $server;
    }
}