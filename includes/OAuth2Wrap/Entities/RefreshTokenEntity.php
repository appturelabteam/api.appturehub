<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */
namespace OAuth2Wrap\Entities;

use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use League\OAuth2\Server\Entities\Traits\EntityTrait;
use League\OAuth2\Server\Entities\Traits\RefreshTokenTrait;

class RefreshTokenEntity implements RefreshTokenEntityInterface
{
    use RefreshTokenTrait, EntityTrait;
    
    public function convertToDSData() {
        return array(
            "refresh_token" => $this->getIdentifier(),
            "access_token" => $this->getAccessToken()->getIdentifier(),
            "expiry_date_time" => $this->getExpiryDateTime()->format(DATE_DB)
        );
    }
}