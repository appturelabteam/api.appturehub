<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */
namespace OAuth2Wrap\Entities;

use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\Traits\AccessTokenTrait;
use League\OAuth2\Server\Entities\Traits\EntityTrait;
use League\OAuth2\Server\Entities\Traits\TokenEntityTrait;

class AccessTokenEntity implements AccessTokenEntityInterface
{
    use AccessTokenTrait, TokenEntityTrait, EntityTrait;
    
    public function getScopeIdentifiers() {
        $scopeIdentifiers = array();
        foreach($this->getScopes() as $scope) {
            $scopeIdentifiers[] = $scope->getIdentifier();
        }
        return $scopeIdentifiers;
    }
    
    public function convertToDSData() {
        return array(
            "access_token" => $this->getIdentifier(),
            "expiry_date_time" => $this->getExpiryDateTime()->format(DATE_DB),
            "user_id" => $this->getUserIdentifier(),
            "scopes" => implode(",",$this->getScopeIdentifiers()),
            "client_id" => $this->getClient()->getIdentifier()
        );
    }
}