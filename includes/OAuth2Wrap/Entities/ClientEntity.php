<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */
namespace OAuth2Wrap\Entities;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\Traits\ClientTrait;
use League\OAuth2\Server\Entities\Traits\EntityTrait;

class ClientEntity implements ClientEntityInterface
{
    use EntityTrait, ClientTrait;
    
    /**
     * @var boolean
     */
    protected $isActive;
    /**
     * @var boolean
     */
    protected $isConfidential;
    /**
     *
     * @var array
     */
    private $fees;
    
    
    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function setRedirectUri($uri)
    {
        $this->redirectUri = $uri;
    }
    
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }
    
    public function setIsConfidential($isConfidential)
    {
        $this->isConfidential = $isConfidential;
    }
    
    public function setFees($fees) {
        $this->fees = $fees;
    }
    
    public function isActive()
    {
        return $this->isActive;
    }
    
    public function isConfidential()
    {
        return $this->isConfidential;
    }
    
    public function getFees() {
        return $this->fees;
    }
}