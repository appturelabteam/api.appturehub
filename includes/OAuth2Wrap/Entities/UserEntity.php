<?php
/**
 * @author      Alex Bilbie <hello@alexbilbie.com>
 * @copyright   Copyright (c) Alex Bilbie
 * @license     http://mit-license.org/
 *
 * @link        https://github.com/thephpleague/oauth2-server
 */
namespace OAuth2Wrap\Entities;

use League\OAuth2\Server\Entities\UserEntityInterface;

class UserEntity implements UserEntityInterface
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $username;
    /**
     * @var boolean
     */
    private $isActive;
    
    
    /**
     * Return the user's identifier.
     *
     * @return mixed
     */
    public function getIdentifier() {
        return $this->id;
    }
    
    public function getUsername() {
        return $this->username;
    }
    
    public function isActive() {
        return $this->isActive;
    }
    
    public function setIdentifier($id) {
        $this->id = $id;
    }
    
    public function setUsername($username) {
        $this->username = $username;
    }
    
    public function setIsActive($isActive) {
        $this->isActive = $isActive;
    }
}