<?php
class Controller {
    static private $router;
    static private $controller_list;
    static private $path = "controllers";
    protected $route_name;
    protected $permissions;
    
    function __construct($route_name) {
        $this->route_name = $route_name;
    }
    
    static function setRouter($router) {
        Controller::$router = $router;
    }
    
    static function getControllers() {
        return Controller::$controller_list;
    }

    /*
     * this function is called by the router to run the controller
     * 
     * the ultimate return should be get_defined_vars(); 
     * 
     * remember to add this return to an override or called function so you can access these variables from the rendered view
     */
    
    function run($params) {
        $methods = get_class_methods(get_class($this));
        
        if( !isset($params[1]) || (strpos($params[1],"getRouteName")===false && 
                strpos($params[1],"setRouteName")===false && 
                strpos($params[1],"includeAll")===false &&
                strpos($params[1],"_")!==0) ) { // dont allow pre-underscores in method names to be called
            //print "Looking for member func...";
            if(array_search($params[1], $methods)!==false) { // if a safe member function is found call it
                //print " found one.";
                return call_user_func(array($this, $params[1]), $params);
            } else if(array_search("view", $methods)!==false) { // fall back to the view member function if it exists
                //print " not found, call view func.";
                return call_user_func(array($this, "view"), $params);
            }
            //print " found none.";
        }
        
        return false;
    }
    
    function getRouteName() {
        return $this->route_name;
    }
    
    function setRouteName($route_name) {
        $this->route_name=$route_name;
    }
    
    static function setPath($path) {
        Controller::$path = $path;
    }
    
    /**
     * Use 'readFolder = true' only when adding a new controller, on init of
     * project or while in development.
     * 
     * @param type $readFolder
     */
    static function includeAll() {
        //print print_r(Controller::$controller_list,true)."<br>";
        
        // rearrange by weight
        $weightedControllers = array();
        foreach(Controller::$controller_list as $class=>$controller) {
            if(!isset($weightedControllers[$controller["weight"]])) {
                $weightedControllers[$controller["weight"]] = array();
            }
            $weightedControllers[$controller["weight"]][$class] = $controller;
        }
        
        foreach($weightedControllers as $weight=>$weightGroup) {
            //print "Group: $weight<br>";
            foreach($weightGroup as $class=>$controller) {
                //print "$class<br>";
                include(Controller::$path."/".$class.".php");
                $controllerObj = new $class($controller["route"]);
                
                // only run install or update functions when DS is available
                if(class_exists("DS")) {
                    $methods = get_class_methods($class);
                    //print "Version".$class::$version."<br>";
                    // run installer if foreced or if not installed yet
                    if($controller["version"] === -1) {
                        if(array_search("install", $methods) !== false && class_exists("DS")) {
                            //call_user_func(array($class, "install"));
                            $controllerObj->install();
                        }
                    }

                    // run update if version has updates
                    if($class::$version !== 0 && $controller["version"] < $class::$version) {
                        if(array_search("update", $methods) !== false) {
                            if(($controller["version"] = $controllerObj->update($controller["version"])) !== false) {
                            //if(($controller["version"] = call_user_func(array($class, "update"),$controller["version"])) !== false) {
                                if(isset($controller["id"])) {
                                    DS::update("controllers", $controller, "WHERE id = ?i", $controller["id"]);
                                }
                            }
                        }
                    }

                    //$controller["lastAccess"] = date(DATE_ISO8601);
                    if(!isset($controller["id"])) {
                        $controller["version"] = $class::$version;
                        DS::insert("controllers", $controller);
                    }// else {
                    //    DS::update("controllers", $controller, "WHERE id = ?i", $controller["id"]);
                    //}
                }
            }
        }
    }
    
    static function addRoute($route, $class, $weight) {
        $controller = array(
            "controller_class" => $class,
            "version" => -1,
            "weight" => $weight,
            "route" => $route
        );
        
        if(class_exists("DS")) {
            if(count($controllers = DS::select("controllers","WHERE controller_class = ?s", $class))) {
                $controller["id"] = $controllers[0]["id"];
                $controller["version"] = $controllers[0]["version"];
            }
        }
        
        Controller::$controller_list[$class] = $controller;
        Controller::$router->set($route,$class);
    }
    
    static function includeSingle($controllerClass) {
        Controller::loadData($controllerClass);
        //print print_r(Controller::$controller_list,true)."<br>";
        
        include(Controller::$path."/".$controllerClass.".php");

        // only run install or update functions when DS is available
        if(class_exists("DS")) {
            $methods = get_class_methods($controllerClass);
            //print "Version".$controllerClass::$version."<br>";
            // run installer if foreced or if not installed yet
            if(Controller::$controller_list[$controllerClass]["version"] === -1) {
                if(array_search("install", $methods) !== false && class_exists("DS")) {
                    call_user_func(array($controllerClass, "install"));
                }
            }

            // run update if version has updates
            if($controllerClass::$version !== 0 && Controller::$controller_list[$controllerClass]["version"] < $controllerClass::$version) {
                if(array_search("update", $methods) !== false) {
                    if((Controller::$controller_list[$controllerClass]["version"] = call_user_func(array($controllerClass, "update"),Controller::$controller_list[$controllerClass]["version"])) !== false) {
                        if(isset(Controller::$controller_list[$controllerClass]["id"])) {
                            DS::update("controllers", Controller::$controller_list[$controllerClass], "WHERE id = ?i", Controller::$controller_list[$controllerClass]["id"]);
                        }
                    }
                }
            }

            //$controller["lastAccess"] = date(DATE_ISO8601);
            if(!isset(Controller::$controller_list[$controllerClass]["id"])) {
                Controller::$controller_list[$controllerClass]["version"] = $controllerClass::$version;
                if(class_exists("DS")) {
                    DS::insert("controllers", Controller::$controller_list[$controllerClass]);
                }
            }// else {
            //    DS::update("controllers", $controller, "WHERE id = ?i", $controller["id"]);
            //}
        }
    }
    
    static function loadAllData($readFolder = false) {
        $found_controllers = array();
        
        if(class_exists("DS")) {
            $controllers = DS::select("controllers","ORDER BY weight");
            Controller::$controller_list = array();
            foreach($controllers as $controller) {
                if(file_exists(Controller::$path."/".$controller["controller_class"].".php")) {
                    $found_controllers[] = $controller["controller_class"];
                    Controller::$controller_list[$controller["controller_class"]] = $controller;
                } else {
                    //$controller["missing"] = 1;
                    //DS::update("controllers", $controller, "WHERE id = ?i", $controller["id"]);
                }
            }
        }
        
        if($readFolder) {
            if($handle = opendir(Controller::$path)) {
                while (false !== ($entry = readdir($handle))) {
                    if(strpos($entry, ".php") === strlen($entry)-4) {
                        $class = str_replace(".php", "", $entry);
                        if(stripos($entry,".php")!==false) {
                            Controller::$controller_list[$class] = array(
                                "controller_class" => $class,
                                "version" => $class::$version,
                                "weight" => $class::$weight
                            );
                            if(array_search($class, $found_controllers) === false) {
                                Controller::$controller_list[$class]["version"] = -1; // this is first run
                            }
                        }
                    }
                }
                closedir($handle);
            }
        }
    }
    
    static function loadData($controllerClass) {
        if(file_exists($controllerClass.".php") && Controller::$controller_list[$class] === null) {
            Controller::$controller_list[$controllerClass] = array(
                "controller_class" => $controllerClass,
                "version" => -1, // this is first run
                "weight" => $controllerClass::$weight
            );
        }
    }
    
    static function _install() {
        $tables = DS::list_tables();
        
        // check if the users table exists, if not create it.
        if(array_search('controllers',$tables)===false) {
            // generate the create table query
            $query = "CREATE TABLE controllers (
                        id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT, 
                        route VARCHAR(128) NOT NULL,
                        controller_class VARCHAR(128) NOT NULL,
                        version INTEGER UNSIGNED NOT NULL,
                        weight INTEGER NOT NULL,
                        update_date DATETIME,
                        create_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
                        PRIMARY KEY (id)
                    ) ENGINE = InnoDB;";

            if(DS::query($query)) {
                //message_add("The Users table has been created.");
            }
        }
    }
    
    function _createJsonResponseBody () {
        return array(
            "message" => "Some parameters are missing.",
            "success" => false,
            "access" => true,
            "versions" => array(
                "api_version" => API_VERSION,
                "version_web" => VERSION_WEB,
                "version_android" => VERSION_ANDROID,
                "version_ios" => VERSION_IOS
            )
        );
    }
    
    function _generateOptions ($genericName, $tableFields, $replaceOptions) {
        return array_replace_recursive(array(
            $genericName => array(
                "GET" => array(
                    "title" => Template::toTitleCase($genericName)." List",
                    "description" => "List resources.",
                    "request" => array(
                        "parameters" => array(
                            "limit" => array("Field" => "limit", "Type" => "int", "Null" => "yes", "Key" => "", "Default" => 25, "Extra" => "", "Description" => "Limit the number of items returned (default: 25)."),
                            "page" => array("Field" => "page", "Type" => "int", "Null" => "yes", "Key" => "", "Default" => 0, "Extra" => "", "Description" => "Page through the returned items (default: 0)."),
                            "filter[]" => array("Field" => "filter[]", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Description" => "Filter the returned items ({field_name:value} or {value} for searching in all fields).")
                        ),
                        "body" => array()
                    ),
                    "response" => array(
                        "list" => array($tableFields),
                        "limit" => null,
                        "page" => null,
                        "count" => null,
                        "total_filtered" => null,
                        "total" => null
                    )
                ),
                "POST" => array(
                    "title" => "Create ".Template::toTitleCase($genericName),
                    "description" => "Create a resource.",
                    "request" => array(
                        "parameters" => null,
                        "body" => $tableFields,
                    ),
                    "response" => $tableFields
                ),
            ),
            $genericName."/{id}" => array(
                "GET" => array(
                    "title" => "Get ".Template::toTitleCase($genericName),
                    "description" => "Get a resource.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array()
                    ),
                    "response" => $tableFields,
                ),
                "PUT" => array(
                    "title" => "Update ".Template::toTitleCase($genericName),
                    "description" => "Update a resource. Allows sparse payloads.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => $tableFields
                    ),
                    "response" => $tableFields,
                ),
                "DELETE" => array(
                    "title" => "Delete ".Template::toTitleCase($genericName),
                    "description" => "Permanently remove a resource from the database.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array()
                    ),
                    "response" => array()
                )
            )
        ),$replaceOptions);
    }
    
    function _getViewer($request) {
        
        //$request = new \Laminas\Diactoros\ServerRequest();
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        $client_id = $request->getAttribute("oauth_client_id");
        $view_as = $request->getHeader("ViewAs");
        
        if(is_array($view_as) && is_array($scopes) && count(array_intersect(["webmaster","administrator"],$scopes)) > 0 && count($view_as) && intval($view_as[0])) {
            $user_id = $view_as[0];
            
            //DS::select("users","WHERE id = ?i",$view_as);
        }
        
        return array($scopes,$user_id,$client_id);
    }
    
    /**
     * 
     * @param integer $currentVersion
     * @return integer Must return new version number.
     */
    function update($currentVersion) {
        return $currentVersion;
    }
}