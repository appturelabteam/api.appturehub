<?php
use League\OAuth2\Server\Exception\OAuthServerException;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;

class FileController extends Controller {
    static public $version = 1;
    static public $weight = 5;
    static public $genericName = "file";
    static public $tableName = "files";
    
    static public $tableFields = array(
        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
        "author_id" => array("Field" => "author_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "title" => array("Field" => "title", "Type" => "varchar(512)", "Null" => "yes", "Key" => "", "Default" => "", "Extra" => ""),
        "filepath" => array("Field" => "filepath", "Type" => "varchar(512)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "filename" => array("Field" => "filename", "Type" => "varchar(512)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "mime" => array("Field" => "mime", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "lazy_color" => array("Field" => "lazy_color", "Type" => "varchar(7)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
    );
    
    function view($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {
            if($method === "GET") {
                if(!isset($params[1])) {
                    $request = $server->validateAuthenticatedRequest($request);
                    $response = $this->_getAll($params, $request, $response, $server);
                } else {
                    $response = $this->_getSpecific($params, $request, $response, $server);
                }
            }

            if(strtoupper($method) === "POST") {
                $request = $server->validateAuthenticatedRequest($request);
                if(isset($params[1]) && $params[1] === "bulk") {
                    $response = $this->_addBulk($params, $request, $response, $server);
                } else {
                    $response = $this->_add($params, $request, $response, $server);
                }
            }

            if(strtoupper($method) === "PUT" && isset($params[1])) {
                $request = $server->validateAuthenticatedRequest($request);
                $response = $this->_update($params, $request, $response, $server);
            }

            if(strtoupper($method) === "DELETE" && isset($params[1])) {
                $request = $server->validateAuthenticatedRequest($request);
                $response = $this->_delete($params, $request, $response, $server);
            }

            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response, $server);
            }
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    function _getAll($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if(count(array_intersect(["authenticated"],$scopes)) > 0) {
            
            $options = $this->_getOptions($user_id, $scopes);
            $fields = $options[FileController::$genericName]["GET"]["response"]["list"][0];
            
            // automatically page the loaded items
            $limit_parameter = filter_input(INPUT_GET, "limit");
            $page_parameter = filter_input(INPUT_GET, "page");
            $limit = $limit_parameter === null ? 25 : intval($limit_parameter);
            $page = $page_parameter == null ? 0 : intval($page_parameter);
            $limitString = ($limit > 0 ? " LIMIT ".($page*($limit)).", $limit" : "");

            // ordering, check validity etc and parse for query
            $orderParameter = filter_input(INPUT_GET, "order");
            $orderParameter = $orderParameter == null ? "create_date:DESC" : $orderParameter;
            $order_split = explode(":",$orderParameter);
            if(array_search($order_split[0], array_keys($fields)) === false || array_search($order_split[1], array("DESC","ASC")) === false) {
                $orderParameter = "create_date:DESC";
            }
            $order = implode(" ",explode(":",$orderParameter))." ";

            // add filter
            $filterFields = array_keys($fields);

            $filters = isset($_GET["filter"]) ? array_filter($_GET["filter"]) : array();
            $filtersAnd = isset($_GET["filterAnd"]) ? array_filter($_GET["filterAnd"]) : array();

            $filterString = DS::gen_filters($filterFields, $filters, "OR");
            $filterStringAnd = DS::gen_filters($filterFields, $filtersAnd, "AND");
            $filterString = "($filterString AND $filterStringAnd)";
            
            $isAdmin = count(array_intersect(["webmaster","administrator"],$scopes)) > 0 ? 1 : 0;
            
            $totalRead = DS::query("
                SELECT
                    COUNT(id) as total
                FROM ".FileController::$tableName."
                WHERE author_id = ?i OR 1 = ?i", $user_id, $isAdmin);
            
            $totalFiltered = DS::query("
                SELECT
                    COUNT(id) as total
                FROM ".FileController::$tableName."
                WHERE $filterString AND (author_id = ?i OR 1 = ?i)", $user_id, $isAdmin);

            $generics = DS::query("SELECT
                    ".implode(", ", array_keys($fields)).",
                    IFNULL(title,'') as title
                FROM ".FileController::$tableName."
                WHERE $filterString AND (author_id = ?i OR 1 = ?i)
                ORDER BY $order
                $limitString", $user_id, $isAdmin);

            $responseBody["success"] = true;
            $responseBody["message"] = ucwords(str_camel_case_to_words(FileController::$genericName)). " list";
            $responseBody["data"] = array(
                "filter" => $filters,
                "filterAnd" => $filtersAnd,
                "order" => $orderParameter,
                "list" => $generics,
                "limit" => 0,
                "page" => 0,
                "count" => count($generics),
                "total_filtered" => intval($totalFiltered[0]["total"]),
                "total" => intval($totalRead[0]["total"])
            );
                   
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(FileController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getSpecific($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // authenticate only when no style is set
        $style = filter_input(INPUT_GET,"style");
        
        $options = $this->_getOptions();
        $fields = $options["file"]["POST"]["response"];//DS::table_info(FileController::$tableName);
        
        $generics = DS::query("SELECT
                ".implode(", ",array_keys($fields))."
            FROM files
            WHERE id = ?i", $params[1]);
        
        if(count($generics)) {
            
            if($style === null) {
                
                // if no style is passed we need to authenticate the request
                $request = $server->validateAuthenticatedRequest($request);
                
                // get current user scopes
                list($scopes,$user_id) = $this->_getViewer($request);

                if(array_intersect($scopes, ["administrator","webmaster"]) !== 0) {
                    
                    $responseBody["data"] = $generics[0];
                    $responseBody["message"] = FileController::$genericName. " loaded";
                    $responseBody["success"] = true;
                    
                } else {

                    // access denied
                    $responseBody["message"] = "Access denied.";
                    $responseBody["access"] = false;

                }
                
            } else {

                /*$fileContents = "";
                if(stripos($generics[0]["mime"],"image") !== false) {
                    $style = filter_input(INPUT_GET,"style");
                    $fileContents = file_get_contents(Image::get(API_PATH_UPLOADS.$generics[0]["filepath"],$style));
                } else {
                    $fileContents = file_get_contents(API_PATH_UPLOADS.$generics[0]["filepath"]);
                }

                $response = new Laminas\Diactoros\Response;
                $response = $response->withHeader('Content-Type', $generics[0]["mime"])
                        ->withHeader('Content-Disposition',"attachment; filename='{$generics[0]["filename"]}'")
                        ->withHeader('Lazy-Color',"{$generics[0]["lazy_color"]}")
                        ->withHeader('Access-Control-Expose-Headers',"Lazy-Color");
                $response->getBody()->write($fileContents);*/

                // log response
                //LogController::log(FileController::$genericName,$request->getMethod(),$params[1],$user_id,true,json_encode($generics[0]),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());


                $style = filter_input(INPUT_GET,"style");
                if(stripos($generics[0]["mime"],"image") !== false && stripos($generics[0]["mime"],"svg") === false && $style) {
                    if(base64_decode($style) === "none") $style = "max-size";
                    $filename = Image::get(API_PATH_UPLOADS.$generics[0]["filepath"],$style);
                } else {
                    $filename = API_PATH_UPLOADS.$generics[0]["filepath"];
                }

                if($filename) {
                    
                    header('Content-Type: '.$generics[0]["mime"]);
                    header('Content-Length: '.filesize($filename));

                    header('Lazy-Color: '.$generics[0]["lazy_color"]);
                    header('Access-Control-Expose-Headers: Lazy-Color');

                    //get the last-modified-date of this very file
                    $lastModified=filemtime($filename);
                    //get a unique hash of this file (etag)
                    $etagFile = md5_file($filename);
                    //get the HTTP_IF_MODIFIED_SINCE header if set
                    $ifModifiedSince=(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
                    //get the HTTP_IF_NONE_MATCH header if set (etag: unique file hash)
                    $etagHeader=(isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

                    //set last-modified header
                    header("Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModified)." GMT");
                    //set etag-header
                    header("Etag: $etagFile");
                    //make sure caching is turned on
                    header('Cache-Control: public');

                    //check if page has changed. If not, send 304 and exit
                    if (@strtotime($ifModifiedSince) == $lastModified || $etagHeader == $etagFile)
                    {
                           header("HTTP/1.1 304 Not Modified");
                           exit;
                    }

                    readfile($filename);

                    exit();
                    
                } else {
                    
                    $responseBody["message"] = ucwords(str_camel_case_to_words(FileController::$genericName)). " not found";
                }
                
            }
            
        } else {
            
            $responseBody["message"] = ucwords(str_camel_case_to_words(FileController::$genericName)). " not found";
            
        }
        
        // prep payload
        $response = $response->withPayload($responseBody);

        // log response
        //LogController::log(FileController::$genericName,$request->getMethod(),$params[1],$user_id,false,json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _add($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // does the user have the correct scope
        if(count(array_intersect(["authenticated"], $scopes))) {
            
            if(isset($_FILES["file"])) {
                
                // move the uploaded file
                $filename = $_FILES['file']['name'];
                $extension = substr($_FILES['file']['name'],strripos($_FILES['file']['name'], "."));
                $randomFilename = FileController::_generateUniqueName().$extension;
                $filepath = Forms::upload($_FILES['file'], API_PATH_UPLOADS, date("Ymd"), $randomFilename);
                
                // check if there were any errors
                if(stripos($filepath, "ERROR") === false) {
                    
                    $resizeError = false;
                    
                    switch(exif_imagetype (API_PATH_UPLOADS.$filepath)) {
                        case IMAGETYPE_JPEG:
                        case IMAGETYPE_JPEG2000:
                            $type = "jpg";
                            break;
                        case IMAGETYPE_PNG:
                            $type = "png";
                            break;
                        case IMAGETYPE_GIF:
                            $type = "gif";
                            break;
                        default:
                            $type = "other";
                            break;
                    }
                    
                    if($type === "other") {
                        
                        $lazy_color = "#ffffff";
                        $resized = false;
                        
                    } else {

                        list($width, $height) = getimagesize(API_PATH_UPLOADS.$filepath);
                        //if($width > 1920 || $height > 1920) {
                            $resized = Image::resizeAndSave(API_PATH_UPLOADS.$filepath);
                            
                            $resizeError = ($resized === false);
                        //}

                        $lazy_color = Image::getAverageColor(Image::get(API_PATH_UPLOADS.$filepath,'lazy-color'));
                        //list($width, $height) = getimagesize(Image::get(API_PATH_UPLOADS.$filepath,'lazy-color'));
                        
                    }
                    
                    
                    if(!$resizeError) {

                        $mime = mime_content_type(API_PATH_UPLOADS.$filepath);

                        // set current user as owner
                        $data = array(
                            "filepath" => $filepath,
                            "filename" => $filename,
                            "mime" => $mime,
                            //'preview_height' => $height,
                            "lazy_color" => $lazy_color,
                            "author_id" => $user_id,
                        );

                        // if an id was part of the parameters then we will do an update and delete the previous file
                        if(isset($params[1]) && intval($params[1])) {

                            if(count($file = DS::select(FileController::$tableName,"WHERE id = ?i", $params[1]))) {

                                unlink(API_PATH_UPLOADS.$file[0]["filepath"]);
                                DS::update(FileController::$tableName, $data, "WHERE id = ?i", $params[1]);

                                $file[0] = array_merge($file[0],$data);

                                $file[0]["resized"] = isset($resized) ? $resized : false;
                                $responseBody["message"] = ucwords(str_camel_case_to_words(FileController::$genericName)). " updated";
                                $responseBody["success"] = true;
                                $responseBody["data"] = $file[0];

                            } else {
                                $responseBody["message"] = ucwords(str_camel_case_to_words(FileController::$genericName)). " not found";
                            }

                        } else {

                            if(count($generic = DS::insert(FileController::$tableName, $data))) {
                                $generic[0]["resized"] = isset($resized) ? $resized : false;
                                $responseBody["message"] = ucwords(str_camel_case_to_words(FileController::$genericName)). " added";
                                $responseBody["success"] = true;
                                $responseBody["data"] = $generic[0];
                            } else {
                                $responseBody["message"] = "Database insert error";
                            }

                        }
                        
                    } else {
                        
                        $responseBody["message"] = Image::$last_error;
                        
                    }
                    
                } else {
                    $responseBody["message"] = str_ireplace("ERROR: ","",$filepath);
                }
                
            } // else parameters are missing - default message
               
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(FileController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _addBulk($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // does the user have the correct scope
        if(count(array_intersect(["webmaster","administrator"], $scopes))) {
            
            $uploads = (isset($_FILES["file"]) ? $_FILES["file"] : null);
            //$responseBody["data"]["post"] = $_POST;
            //$responseBody["data"]["files"] = $_FILES;
            
            if($uploads) {
                
                $uploadedCount = 0;
                foreach($uploads["name"] as $x=>$value) {
                    
                    $file = array(
                        "name" => $uploads["name"][$x],
                        "tmp_name" => $uploads["tmp_name"][$x],
                        "error" => $uploads["error"][$x],
                        "size" => $uploads["size"][$x],
                        "type" => $uploads["type"][$x]
                    );
                    
                    $bulkResponseBody = array(
                        "message" => "Some parameters are missing",
                        "success" => false,
                        "data" => $file
                    );
                    
                    $title = $_POST["title"][$x];

                    // move the uploaded file
                    $filename = $file['name'];
                    $extension = substr($file['name'],strripos($file['name'], "."));
                    $randomFilename = FileController::_generateUniqueName().$extension;
                    $filepath = Forms::upload($file, API_PATH_UPLOADS, date("Ymd"), $randomFilename);
                    
                    // check if there were any errors
                    if(stripos($filepath, "ERROR") === false) {
                        
                        list($width, $height) = getimagesize(API_PATH_UPLOADS.$filepath);
                        if($width > 1920 || $height > 1920) {
                            $resized = Image::resizeAndSave(API_PATH_UPLOADS.$filepath);
                        }
                        
                        $mime = mime_content_type(API_PATH_UPLOADS.$filepath);
                        $lazy_color = Image::getAverageColor(Image::get(API_PATH_UPLOADS.$filepath,'lazy-color'));
                        list($width, $height) = getimagesize(Image::get(API_PATH_UPLOADS.$filepath,'lazy-color'));
                        
                        // set current user as owner
                        $data = array(
                            "filepath" => $filepath,
                            "filename" => $filename,
                            "mime" => $mime,
                            "title" => $title,
                            "lazy_color" => $lazy_color,
                            "author_id" => $user_id,
                        );
                        
                        if(count($generic = DS::insert(FileController::$tableName, $data))) {
                            $generic[0]["resized"] = isset($resized) ? $resized : false;
                            
                            // now lets link a product to the image, if found
                            /*if(count($product = DS::select(ProductController::$tableName, "WHERE title = ?s", $title))) {
                                DS::update(ProductController::$tableName, array("image_id" => $generic[0]["id"]), "WHERE title = ?s", $title);
                            }*/
                            
                            $bulkResponseBody["message"] = ucwords(str_camel_case_to_words(FileController::$genericName)). " added";
                            $bulkResponseBody["success"] = true;
                            $bulkResponseBody["data"] = $generic[0];
                            
                            $uploadedCount++;
                        } else {
                            $bulkResponseBody["message"] = "Database insert error";
                        }

                    } else {
                        $bulkResponseBody["message"] = str_ireplace("ERROR: ","",$filepath);
                    }
                    
                    $responseBody["data"][] = $bulkResponseBody;
                    
                }
                
                if($uploadedCount === count($uploads["name"])) {
                    $responseBody["message"] = ucwords(str_camel_case_to_words(FileController::$genericName)). " uploaded successfully";
                    $responseBody["success"] = true;
                } else if($uploadedCount > 0) {
                    $responseBody["message"] = ucwords(str_camel_case_to_words(FileController::$genericName)). " upload failed partially";
                    $responseBody["success"] = true;
                } else {
                    $responseBody["message"] = ucwords(str_camel_case_to_words(FileController::$genericName)). " upload failed";
                }
                
            } // else parameters are missing - default message
               
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(FileController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _update($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        $generics = DS::query("SELECT
                *
            FROM files
            WHERE id = ?i", intval($params[1]));
        
        // does the user have the correct scope
        if( count(array_intersect(["administrator","webmaster"], $scopes)) ) {
            
            $requestBody = file_get_contents("php://input");
            $requestObject = json_decode($requestBody, true);
            
            $options = $this->_getOptions();
            $fields = $options["file/{id}"]["PUT"]["request"]["body"];//DS::table_info(FileController::$tableName);
            
            // filter away bad fields and don't accept file as field
            $data = array();
            $sparseFields = array();
            foreach(array_keys($fields) as $field) {
                if(isset($requestObject[$field]) && $field !== "file") {
                    $data[$field] = $requestObject[$field];
                    $sparseFields[$field] = $fields[$field];
                }
            }
            
            // still have data?
            if(count($data)) {
                
                // some fields need to meet certain criteria - validate here
                $invalid = Forms::validateData($sparseFields, $data, $user_id);
                
                // valid?
                if(!count($invalid)) {
                    
                    if(DS::update(FileController::$tableName, $data, "WHERE id = ?i", $params[1]) !== null) {
                        
                        $file = DS::select(FileController::$tableName, "WHERE id = ?i", $params[1]);
                        
                        $responseBody["message"] = ucwords(str_camel_case_to_words(FileController::$genericName)). " updated";
                        $responseBody["success"] = true;
                        $responseBody["data"] = (count($file) ? $file[0] : null);
                        
                    } else {
                        $responseBody["message"] = "Database update error";
                    }
                    
                } else {
                    
                    // had invalid fields
                    $responseBody["message"] = "Some fields are invalid";
                    $responseBody["data"] = array(
                        "values" => $data,
                        "invalid" => $invalid
                    );
                    
                }
                
            }
            
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(FileController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _delete($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        $generics = DS::query("SELECT
                id,
                author_id
            FROM files
            WHERE id = ?i", intval($params[1]));
        
        // does the user have the correct scope
        if( count($generics) && (boolval(intval($generics[0]["author_id"]) === intval($user_id))) ) {
            
            if(DS::delete(FileController::$tableName, "WHERE id = ?i", $params[1]) !== null) {
                $responseBody["message"] = ucwords(str_camel_case_to_words(FileController::$genericName)). " deleted";
                $responseBody["success"] = true;
            } else {
                $responseBody["message"] = "Database delete error";
            }
            
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(FileController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getOptions() {
        return array(
            "file" => array(
                "GET" => array(
                    "title" => "List Files",
                    "description" => "List resources.",
                    "request" => array(
                        "parameters" => array(
                            "limit" => array("Field" => "limit", "Type" => "int", "Null" => "yes", "Key" => "", "Default" => 25, "Extra" => "", "Description" => "Limit the number of items returned (default: 25)."),
                            "page" => array("Field" => "limit", "Type" => "int", "Null" => "yes", "Key" => "", "Default" => 0, "Extra" => "", "Description" => "Page through the returned items (default: 0)."),
                            "filter" => array("Field" => "limit", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Description" => "Filter the returned items ({field_name:value} or {value} for searching in all fields).")
                        ),
                        "body" => array()
                    ),
                    "response" => array(
                        "list" => array(
                            array(
                                "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
                                "author_id" => array("Field" => "author_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "title" => array("Field" => "title", "Type" => "varchar(512)", "Null" => "yes", "Key" => "", "Default" => "", "Extra" => ""),
                                "filepath" => array("Field" => "filepath", "Type" => "varchar(512)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "filename" => array("Field" => "filename", "Type" => "varchar(512)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "mime" => array("Field" => "mime", "Type" => "varchar(64)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "lazy_color" => array("Field" => "lazy_color", "Type" => "varchar(7)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "author_id" => array("Field" => "author_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                                "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                            )
                        ),
                        "limit" => null,
                        "page" => null,
                        "count" => null,
                        "total_filtered" => null,
                        "total" => null
                    )
                ),
                "POST" => array(
                    "title" => "Create File",
                    "description" => "Create a resource.",
                    "request" => array(
                        "parameters" => null,
                        "body" => array(
                            "title" => array("Field" => "title", "Type" => "varchar(512)", "Null" => "yes", "Key" => "", "Default" => "", "Extra" => ""),
                            "file" => array("Field" => "file", "Type" => "file", "Null" => "no", "Default" => "", "Extra" => "", "Description" => "File upload from HTML input with name of 'file'."),
                            "lazy_color" => array("Field" => "lazy_color", "Type" => "color", "Null" => "yes", "Key" => "", "Default" => "#dfdfdf", "Extra" => "", "Label" => "Lazy Load Color")
                        )
                    ),
                    "response" => array(
                        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
                        "author_id" => array("Field" => "author_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "title" => array("Field" => "title", "Type" => "varchar(512)", "Null" => "yes", "Key" => "", "Default" => "", "Extra" => ""),
                        "filepath" => array("Field" => "filepath", "Type" => "varchar(512)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "filename" => array("Field" => "filename", "Type" => "varchar(512)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "mime" => array("Field" => "mime", "Type" => "varchar(64)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "lazy_color" => array("Field" => "lazy_color", "Type" => "varchar(7)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "author_id" => array("Field" => "author_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                    )
                ),
            ),
            "file/{id}" => array(
                "GET" => array(
                    "title" => "Get File",
                    "description" => "Sends back the file content with correct headers.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array()
                    ),
                    "response" => null
                ),
                "PUT" => array(
                    "title" => "Update File",
                    "description" => "Update a resource. Allows sparse payloads.",
                    "request" => array(
                        "parameters" => null,
                        "body" => array(
                            "title" => array("Field" => "title", "Type" => "varchar(512)", "Null" => "yes", "Key" => "", "Default" => "", "Extra" => ""),
                            "file" => array("Field" => "file", "Type" => "file", "Null" => "no", "Default" => "", "Extra" => "", "Description" => "File upload from HTML input with name of 'file'."),
                            "lazy_color" => array("Field" => "lazy_color", "Type" => "color", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Label" => "Lazy Load Color")
                        )
                    ),
                    "response" => array(
                        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
                        "author_id" => array("Field" => "author_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "title" => array("Field" => "title", "Type" => "varchar(512)", "Null" => "yes", "Key" => "", "Default" => "", "Extra" => ""),
                        "filepath" => array("Field" => "filepath", "Type" => "varchar(512)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "filename" => array("Field" => "filename", "Type" => "varchar(512)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "mime" => array("Field" => "mime", "Type" => "varchar(64)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "lazy_color" => array("Field" => "lazy_color", "Type" => "varchar(7)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "author_id" => array("Field" => "author_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                    )
                ),
                "DELETE" => array(
                    "title" => "Delete File",
                    "description" => "Permanently remove a resource from the database.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array()
                    ),
                    "response" => array()
                )
            )
        );
    }
    
    function _options($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $options = $this->_getOptions();
        
        $responseBody["message"] = ucwords(str_camel_case_to_words(FileController::$genericName)). " options";
        $responseBody["success"] = true;
        $responseBody["data"] = $options;
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        //LogController::log(FileController::$genericName,$request->getMethod(),$params[1],$user_id,json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    static function _generateUniqueName() {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }
        
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }
    
    static function addExternalImage($url,$user_id){
        
        try {
            $ch = curl_init ($url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
            $raw = curl_exec($ch);
            curl_close ($ch);

            $randomFilename = FileController::_generateUniqueName();
            $tempFile = API_PATH_UPLOADS."/".$randomFilename;

            if(file_exists($tempFile)) {
                unlink($tempFile);
            }

            $fp = fopen($tempFile,'x');
            fwrite($fp, $raw);
            fclose($fp);

            $mime = mime_content_type($tempFile);

            $extension = substr($mime,stripos($mime,"/")+1);
            if($extension === "jpeg") {
                $extension = "jpg";
            }
            
            $folder = date("Ymd");
            if(!file_exists(API_PATH_UPLOADS."/".$folder)) {
                mkdir(API_PATH_UPLOADS."/".$folder,0755);
            }

            $filepath = "/".$folder."/".$randomFilename.".".$extension;
            
            if(file_exists($tempFile)) {
                rename($tempFile,API_PATH_UPLOADS.$filepath);

                $lazy_color = Image::getAverageColor(Image::get(API_PATH_UPLOADS.$filepath,"thumbs-product"));

                // set current user as owner
                $data = array(
                    "filepath" => $filepath,
                    "filename" => $url,
                    "mime" => $mime,
                    "lazy_color" => $lazy_color,
                    "author_id" => $user_id,
                );

                if(count($generic = DS::insert("files", $data))) {
                    return $generic[0];
                }
            }
        } catch(Exception $e) {
            
        }
        
        return null;
    }
    
    public function install() {
        $tables = DS::list_tables();
        
        if(array_search(FileController::$tableName, $tables)===false) {
            
            // generate the create table query
            $query = "CREATE TABLE ".FileController::$tableName." (";
            
            $queryFields = "";
            $primaryKey = null;
            $uniqueKey = null;
            
            foreach(FileController::$tableFields as $field) {
                $queryFields.= ($queryFields ? ", " : "")."{$field["Field"]} {$field["Type"]} ".(strtolower($field["Null"]) === "no" ? "NOT NULL" : "")." ".($field["Default"] ? "DEFAULT ".($field["Default"] === "CURRENT_TIMESTAMP" ? $field["Default"] : "'{$field["Default"]}'") : "")." ".($field["Extra"] ? $field["Extra"] : "");
                if(strtolower($field["Key"]) === "pri") {
                    $primaryKey = ", PRIMARY KEY ({$field["Field"]})";
                } else if(strtolower($field["Key"]) === "uni") {
                    $uniqueKey = ", UNIQUE `{$field["Field"]}` (`{$field["Field"]}`)";
                }
            }
            
            $query.= $queryFields.($primaryKey ? $primaryKey : "").($uniqueKey ? ($primaryKey ? ", " : "").$uniqueKey : "").") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            if(DS::query($query)) {
                //message_add("The users table has been created.");
            }
        }
        
        $pages = array();
        /*
        * file/listing
        */

        // add user page permissions including core admin
        $pages[] = array(
            'name'=>'getAll',
            'category'=>'file',
            'subcat'=>'pages',
            'ptype'=>2,
            'pview'=>'authenticated');
        
        foreach($pages as $key=>$fieldData) {
            Permissions::set($fieldData);
        }

        // add user content permissions
        $content = array();
        $content[] = array(
            'name'=>'file',
            'category'=>'file',
            'subcat'=>'content',
            'ptype'=>0,
            'pview'=>'authenticated',
            'pedit'=>'own',
            'padd'=>'authenticated',
            'pdel'=>'own');
        foreach($content as $key=>$fieldData) {
            Permissions::set($fieldData);
        }

        // add user field permissions
        /*$fields = array();
        $fields[] = array(
            'name'=>'active',
            'category'=>'user',
            'subcat'=>'fields',
            'ptype'=>1,
            'pview'=>'webmaster',
            'pedit'=>'webmaster');
        foreach($fields as $key=>$fieldData) {
            Permissions::set($fieldData);
        }*/
        
        if(($adminMenu = MenuController::getItem("Admin")) === null) {
            $adminMenu = MenuController::addItem("Admin", null, null, "", 0, ["administrator","webmaster"]);
        }
        if(($siteContentMenu = MenuController::getItem("Content & Media")) === null) {
            $siteContentMenu = MenuController::addItem("Content & Media", null, null, "", 1, ["administrator","webmaster"], $adminMenu["id"]);
        }
        MenuController::addItem("<i class='fa fa-folder-open'></i> File Manager", null, null, "file", 0, ["administrator","webmaster"], $siteContentMenu["id"]);
    }
    
    /*public function update($from_version) {
        
        try {
            
            if($from_version < 1) {
                
                $query = "ALTER TABLE files CHANGE mime mime varchar(128) AFTER filename;";
                
                DS::query($query);
                
                $from_version ++;
                
            }
            
        } catch (Exception $e) {
            print $e->getMessage();
        }
        
        return $from_version;
    }*/
}