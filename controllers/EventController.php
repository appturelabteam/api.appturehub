<?php
use League\OAuth2\Server\Exception\OAuthServerException;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Psr\Http\Message\ServerRequestInterface;

class EventController extends Controller {
    static public $version = 0;
    static public $genericName = "event";
    static public $tableName = "events";
    
    static public $tableFields = array(
        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
        "title" => array("Field" => "title", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "slug_title" => array("Field" => "slug_title", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "subject" => array("Field" => "subject", "Label" => "Email or Notification Subject line", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "description" => array("Field" => "description", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => "", "Extra" => ""),
        "sub_template" => array("Field" => "sub_template", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => "", "Extra" => ""),
        "roles" => array("Field" => "roles", "Type" => "VARCHAR(256)", "Null" => "no", "Key" => "", "Default" => 'authenticated', "Extra" => ""),
        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
    );
    
    /**
     * 
     * @param type $params
     */
    function view($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {

            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response, $server);
            } else {
                $request = $server->validateAuthenticatedRequest($request);
            }
            
            if($method === "GET") {
                if(!isset($params[1])) {
                    $response = $this->_getAll($params, $request, $response, $server);
                } else {
                    $response = $this->_getSpecific($params, $request, $response);
                }
            }

            // user profiles are created automatically on user registration
            if(strtoupper($method) === "POST") {
                if(isset($params[1]) && $params[1] === "bulk") {
                    $response = $this->_addBulk($params, $request, $response, $server);
                } else {
                    $response = $this->_add($params, $request, $response);
                }
            }

            if(strtoupper($method) === "PUT" && isset($params[1])) {
                $response = $this->_update($params, $request, $response);
            }

            // deleted with user accounts
            if(strtoupper($method) === "DELETE" && isset($params[1])) {
                $response = $this->_delete($params, $request, $response);
            }
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    /**
     * 
     * @param type $params
     * @param type $request
     * @param type $response
     * @return type
     */
    function _getAll($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // get fields for this endpoint from options
        $options = $this->_getOptions();
        $fields = $options[EventController::$genericName]["GET"]["response"]["list"][0];

        // automatically page the loaded items
        $limit_parameter = filter_input(INPUT_GET, "limit");
        $page_parameter = filter_input(INPUT_GET, "page");
        $limit = $limit_parameter == null ? 25 : intval($limit_parameter);
        $page = $page_parameter == null ? 0 : intval($page_parameter);
        $limitString = ($limit > 0 ? " LIMIT ".($page*($limit)).", $limit" : "");
        
        // ordering, check validity etc and parse for query
        $orderParameter = filter_input(INPUT_GET, "order");
        $orderParameter = $orderParameter == null ? "create_date:DESC" : $orderParameter;
        $order_split = explode(":",$orderParameter);
        if(array_search($order_split[0], array_keys($fields)) === false || array_search($order_split[1], array("DESC","ASC")) === false) {
            $orderParameter = "create_date:DESC";
        }
        $order = implode(" ",explode(":",$orderParameter))." ";

        // add filter
        $filterFields = array_keys($fields);
        $filters = isset($_GET["filter"]) ? array_filter($_GET["filter"]) : array();
        $filtersAnd = isset($_GET["filterAnd"]) ? array_filter($_GET["filterAnd"]) : array();
        $filterString = DS::gen_filters($filterFields, $filters, "OR");
        $filterStringAnd = DS::gen_filters($filterFields, $filtersAnd, "AND");
        $filterString = "($filterString AND $filterStringAnd)";

        $totalRead = DS::query("
            SELECT
                COUNT(id) as total
            FROM ".EventController::$tableName);

        $totalFiltered = DS::query("
            SELECT
                COUNT(id) as total
            FROM ".EventController::$tableName."
            WHERE ".$filterString);

        $generics = DS::query("SELECT
                ".implode(", ", array_keys($fields))."
            FROM ".EventController::$tableName."
            WHERE $filterString
            ORDER BY {$order} 
            $limitString");

        $responseBody["success"] = true;
        $responseBody["message"] = ucwords(str_camel_case_to_words(EventController::$genericName)). " list";
        $responseBody["data"] = array(
            "filter" => $filters,
            "filterAnd" => $filtersAnd,
            "order" => $orderParameter,
            "list" => $generics,
            "limit" => $limit,
            "page" => $page,
            "count" => count($generics),
            "total_filtered" => intval($totalFiltered[0]["total"]),
            "total" => intval($totalRead[0]["total"])
        );
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(EventController::$genericName,$request->getMethod(),0,$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getSpecific($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // get fields for this endpoint from options
        $options = $this->_getOptions();
        $fields = $options[EventController::$genericName."/{id}"]["GET"]["response"];

        $generics = DS::query("SELECT
                ".implode(", ", array_keys($fields))."
            FROM ".EventController::$tableName."
            WHERE id = ?i", intval($params[1]));

        if(count($generics)) {

            // found
            $responseBody["message"] = ucwords(str_camel_case_to_words(EventController::$genericName)). " loaded";
            $responseBody["success"] = true;
            $responseBody["data"] = $generics[0];

        } else {

            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(EventController::$genericName)). " not found";

        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(EventController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _add($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        if(count(array_intersect(["administrator","webmaster"], $scopes))) {
            
            // get all fields from request body
            $requestBody = file_get_contents("php://input");
            $requestObject = json_decode($requestBody, true);
            
            // get fields for this endpoint from options
            $options = $this->_getOptions();
            $fields = $options[EventController::$genericName]["POST"]["request"]["body"];
            
            // filter away bad fields
            $data = array();
            foreach(array_keys($fields) as $field) {
                if(isset($requestObject[$field])) {
                    $data[$field] = $requestObject[$field];
                }
            }

            // still have data?
            if(count($data)) {

                // some fields need to meet certain criteria - validate here
                $invalid = Forms::validateData($fields, $data);

                // valid?
                if(!count($invalid)) {
                    
                    if(isset($data["title"])) {
                        $data["title"] = str_slugify($data["title"]);
                    }
                    
                    // create image if not passed in:
                    /*$string = "spam@mvoncken.nl";                                             
                    $font   = 4;
                    $width  = ImageFontWidth($font) * strlen($string);
                    $height = ImageFontHeight($font);

                    $im = @imagecreate ($width,$height);
                    $background_color = imagecolorallocate ($im, 255, 255, 255); //white background
                    $text_color = imagecolorallocate ($im, 0, 0,0);//black text
                    imagestring ($im, $font, 0, 0,  $string, $text_color);
                    imagepng ($im);*/

                    if(count($generic = DS::insert(EventController::$tableName, $data))) {
                        
                        $responseBody["message"] = ucwords(str_camel_case_to_words(EventController::$genericName)). " added";
                        $responseBody["success"] = true;
                        $responseBody["data"] = $generic[0];
                    } else {
                        $responseBody["message"] = "Database insert error";
                    }

                } else {
                    
                    // had invalid fields
                    $responseBody["message"] = "Some fields are invalid";
                    $responseBody["data"] = array(
                        "values" => $data,
                        "invalid" => $invalid
                    );

                }
            } // else parameters are missing - default message
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(EventController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _update($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // get fields for update
        $options = $this->_getOptions();
        $fields = $options[EventController::$genericName."/{id}"]["PUT"]["request"]["body"];
        
        $generics = DS::query("SELECT
                ".implode(", ", array_keys($fields))."
            FROM ".EventController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // found?
        if(count($generics)) {
            
            // does the user have the correct scope
            if( count(array_intersect(["webmaster","administrator"], $scopes)) ) {

                $requestBody = file_get_contents("php://input");
                $requestObject = json_decode($requestBody, true);

                // filter away bad fields
                $data = array();
                $sparseFields = array();
                foreach(array_keys($fields) as $field) {
                    if(isset($requestObject[$field])) {
                        if(isset($requestObject[$field])) {
                            $data[$field] = $requestObject[$field];
                            $sparseFields[$field] = $fields[$field];
                        }
                    }
                }

                // still have data?
                if(count($data)) {

                    // some fields need to meet certain criteria - validate here
                    $invalid = Forms::validateData($sparseFields, $data, intval($params[1]));

                    // valid?
                    if(!count($invalid)) {
                        
                        if(isset($data["title"])) {
                            $data["title"] = str_slugify($data["title"]);
                        }

                        if(DS::update(EventController::$tableName, $data, "WHERE id = ?i", $params[1]) !== null) {

                            $responseBody["message"] = ucwords(str_camel_case_to_words(EventController::$genericName)). " updated";
                            $responseBody["success"] = true;
                            $responseBody["data"] = $data;
                        } else {
                            $responseBody["message"] = "Database update error";
                        }

                    } else {

                        // had invalid fields
                        $responseBody["message"] = "Some fields are invalid";
                        $responseBody["data"] = array(
                            "values" => $data,
                            "invalid" => $invalid
                        );

                    }

                } // else parameters are missing - default message
            
            } else {
            
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
                
            }

        } else {
            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(EventController::$genericName)). " not found";
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(EventController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _delete($params, $request, $response) {
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $generics = DS::query("SELECT
                id
            FROM ".EventController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // found?
        if(count($generics)) {
        
            // does the user have the correct scope
            if( count(array_intersect(["webmaster","administrator"], $scopes)) ) {
                
                if(DS::delete(EventController::$tableName, "WHERE id = ?i", $params[1]) !== null) {
                    $responseBody["message"] = ucwords(str_camel_case_to_words(EventController::$genericName)). " deleted";
                    $responseBody["success"] = true;
                } else {
                    $responseBody["message"] = "Database delete error";
                }
                
            } else {
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
            }
            
        } else {
            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(EventController::$genericName)). " not found";
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(EventController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    /**
     * 
     * @param type $slugTitle
     * @param type $userId
     * @param type $data
     * @param type $attachments
     * @param type $contactId
     * @return int Returns amount of queued emails
     */
    public static function trigger($slugTitle, $userId, $data, $attachments = array(), $contactId = null) {

        $queued = 0;

        $events = DS::query("SELECT * FROM " . EventController::$tableName . " WHERE slug_title = ?s", $slugTitle);

        LogController::log("EVENT", "TRIGGER_START", 0, $userId, 0, json_encode(compact("slugTitle", "userId", "data", "attachments", "contactId", "events")), "");

        foreach ($events as $event) {

            $eventRoles = explode(",", $event["roles"]);
            $sendToUsers = array();

            if (count(array_intersect($eventRoles, ["authenticated","anonymous"])) === 0) {
                // this event is to be sent to site admin
                LogController::log("EVENT", "TRIGGER_ADMIN", isset($event) ? $event["id"] : 0, $userId, 0, json_encode(array("data" => $data, "event" => $event, "contact_id" => $contactId)), "");

                $users = DS::query("
                        SELECT
                            u.id,
                            u.username,
                            r.role
                        FROM " . UserController::$tableName . " as u
                        LEFT JOIN user_roles as r ON r.user_id = u.id 
                        WHERE (r.role = 'administrator' OR (1 = ?i AND r.role = 'webmaster'))", (API_MODE === API_MODE_DEVELOPMENT ? 1 : 0));

                foreach ($users as $user) {

                    $userProfile = DS::select(UserProfileController::$tableName, "WHERE user_id = ?i", $user["id"]);
                    $userProfile = (count($userProfile) ? $userProfile[0] : null);

                    $sendToUsers[] = array_merge($user, $userProfile);
                }
            } elseif ($userId > 0) {

                // this event is sent to the specific user provided by userId
                LogController::log("EVENT", "TRIGGER_USER", isset($event) ? $event["id"] : 0, $userId, 0, json_encode(array("data" => $data, "event" => $event, "contact_id" => $contactId)), "");

                $user = DS::query("SELECT username FROM " . UserController::$tableName . " WHERE id = ?i", $userId);
                $user = (count($user) ? $user[0] : null);

                if ($user) {

                    $userRoles = DS::query("SELECT GROUP_CONCAT(role) as roles FROM user_roles WHERE user_id = ?i", $userId);
                    $userRoles = explode(",", (count($userRoles) ? $userRoles[0]["roles"] : ""));
                    $userRoles[] = "authenticated";

                    if (count(array_intersect($eventRoles, $userRoles)) > 0) {

                        $userProfile = DS::select(UserProfileController::$tableName, "WHERE user_id = ?i", $userId);
                        $userProfile = (count($userProfile) ? $userProfile[0] : null);

                        $sendToUsers[] = array_merge($user, $userProfile);
                    }
                }
            } elseif ($contactId !== null) {

                // this event is sent to the specific user provided by userId
                LogController::log("EVENT", "TRIGGER_CONTACT", isset($event) ? $event["id"] : 0, 0, 0, json_encode(array("data" => $data, "event" => $event, "contact_id" => $contactId)), "");

                $user = DS::query("SELECT *, email as username FROM " . ContactController::$tableName . " WHERE id = ?i", $contactId);
                $user = (count($user) ? $user[0] : null);

                if ($user) {

                    $userRoles = array("anonymous");

                    if (count(array_intersect($eventRoles, $userRoles)) > 0) {

                        $sendToUsers[] = $user;
                    }
                }
            }

            foreach ($sendToUsers as $user) {

                $subject = Template::dataValues($event["subject"], $data);
                $message = "";

                $combined = array("data" => $data, "user" => $user);

                if ($event["sub_template"] && file_exists("templates/emails/{$event["sub_template"]}.php")) {
                    $message = Template::load("templates/emails/{$event["sub_template"]}.php", $combined);
                } else {
                    $message = json_encode($combined);
                }

                $email_body = Template::load("templates/emails/base.php", compact("subject", "message"));
                if (EmailController::_queueEmail($combined["user"]["username"], "", "", VariableController::_getItemValue("Settings", "SITE_EMAIL_FROM"), $subject, $email_body, $attachments)) {

                    $queued++;
                    LogController::log("EVENT", "TRIGGER", isset($event) ? $event["id"] : 0, $userId, 0, json_encode(array("slugTitle" => $slugTitle, "user" => $user, "event" => $event, "data" => $data)), "");
                } else {

                    LogController::log("EVENT", "TRIGGER_QUEUE_ERROR", isset($event) ? $event["id"] : 0, $userId, 0, json_encode(array("slugTitle" => $slugTitle, "user" => $user, "event" => $event, "data" => $data)), "");
                }
            }
        }

        return $queued;
    }

    /**
     * 
     * @param type $title
     * @param type $subject
     * @param type $description
     * @param type $subTemplate
     * @param type $roles
     */
    public static function add($title, $subject, $description, $subTemplate, $roles) {
        DS::insert(EventController::$tableName, array(
            "title" => $title,
            "slug_title" => str_slugify($title),
            "subject" => $subject,
            "description" => $description,
            "sub_template" => $subTemplate,
            "roles" => (is_array($roles) ? implode(",", $roles) : $roles)
        ));
    }

    function _getOptions() {
        $weightItems = array();
        for($x=-25;$x<=25;$x++) {
            $weightItems[" ".$x] = array($x,true);
        }
        
        // use the following array to override the field details where necessary
        $replaceOptions = array(
            EventController::$genericName => array(
                "GET" => array(
                    "response" => array(
                        "list" => array(
                            array(
                            )
                        ),
                    )
                ),
                "POST" => array(
                    "request" => array(
                        "body" => array(
                        )
                    )
                ),
            ),
            EventController::$genericName."/{id}" => array(
                "PUT" => array(
                    "request" => array(
                        "body" => array(
                        )
                    )
                )
            )
        );
        
        // now generate the full options array
        $options = $this->_generateOptions(EventController::$genericName, EventController::$tableFields, $replaceOptions);
        
        // remove any fields that should not be returned
        
        //unset($options[EventController::$genericName]["GET"]["response"]["list"][0]["example_field_name"]);
        
        unset($options[EventController::$genericName]["POST"]["request"]["body"]["id"]);
        unset($options[EventController::$genericName]["POST"]["request"]["body"]["update_date"]);
        unset($options[EventController::$genericName]["POST"]["request"]["body"]["create_date"]);
        
        unset($options[EventController::$genericName."/{id}"]["PUT"]["request"]["body"]["id"]);
        unset($options[EventController::$genericName."/{id}"]["PUT"]["request"]["body"]["update_date"]);
        unset($options[EventController::$genericName."/{id}"]["PUT"]["request"]["body"]["create_date"]);
        unset($options[EventController::$genericName."/{id}"]["PUT"]["response"]["id"]);
        unset($options[EventController::$genericName."/{id}"]["PUT"]["response"]["update_date"]);
        unset($options[EventController::$genericName."/{id}"]["PUT"]["response"]["create_date"]);
        
        return $options;
    }
    
    function _options($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $options = $this->_getOptions();
        
        $responseBody["message"] = ucwords(str_camel_case_to_words(EventController::$genericName)). " options";
        $responseBody["success"] = true;
        $responseBody["data"] = $options;

        // prep output
        return $response->withPayload($responseBody);
    }
    
    
    
    public function install() {
        $tables = DS::list_tables();
        
        if(array_search(EventController::$tableName, $tables)===false) {
            
            // generate the create table query
            $query = "CREATE TABLE ".EventController::$tableName." (";
            
            $queryFields = "";
            $primaryKey = null;
            $uniqueKey = null;
            
            foreach(EventController::$tableFields as $field) {
                $queryFields.= ($queryFields ? ", " : "")."{$field["Field"]} {$field["Type"]} ".(strtolower($field["Null"]) === "no" ? "NOT NULL" : "")." ".($field["Default"] ? "DEFAULT ".($field["Default"] === "CURRENT_TIMESTAMP" ? $field["Default"] : "'{$field["Default"]}'") : "")." ".($field["Extra"] ? $field["Extra"] : "");
                if(strtolower($field["Key"]) === "pri") {
                    $primaryKey = ", PRIMARY KEY ({$field["Field"]})";
                } else if(strtolower($field["Key"]) === "uni") {
                    $uniqueKey = ", UNIQUE `{$field["Field"]}` (`{$field["Field"]}`)";
                }
            }
            
            $query.= $queryFields.($primaryKey ? $primaryKey : "").($uniqueKey ? ($primaryKey ? ", " : "").$uniqueKey : "").") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            if(DS::query($query)) {
                //message_add("The users table has been created.");
            }
        }
        
        if(($adminMenu = MenuController::getItem("Admin")) === null) {
            $adminMenu = MenuController::addItem("Admin", null, null, "", 0, ["administrator","webmaster"]);
        }
        if(($settingsMenu = MenuController::getItem("Settings")) === null) {
            $settingsMenu = MenuController::addItem("Settings", null, null, "", 3, ["administrator","webmaster"], $adminMenu["id"]);
        }
        MenuController::addItem("<i class='fa fa-flag'></i> Events", null, null, "event", 1, ["administrator","webmaster"], $settingsMenu["id"]);
    }
    
    /*public function update($from_version) {
        
        if($from_version < 1) {
            
            try {
                $query = "ALTER TABLE events ADD strategy_id INT AFTER identifier;";
                
                DS::query($query);
                
                $from_version ++;
                
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
        
        return $from_version;
    }*/
}