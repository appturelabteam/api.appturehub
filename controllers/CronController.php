<?php
class CronController extends Controller {
    static public $version = 0;
    static public $weight = 0;
    static public $genericName = "cron";
    static public $tableName = null;
    
    function __construct($route_name) {
        parent::__construct($route_name);
        
        //$this->permissions = Permissions::get($this->generic_name);
    }
    
    function view($params) {
        $responseBody = $this->_createJsonResponseBody();
        
        $responseBody["message"] = "Run cron";
        
        $emailController = new EmailController("email", array("generic_name"=>"email","table_name"=>"email_queue"));
        $responseBody["data"]["email"] = $emailController->_send();
        
        $responseBody["success"] = $responseBody["data"]["email"]["success"];
        $responseBody["time"] = (microtime(true) - $_SESSION["start_time"]);
        
        LogController::log("CRON", "RUN", 0, 0, 0, json_encode($responseBody), "");
        
        header("Content-type: application/json");
        print json_encode($responseBody);
        exit;
    }
}