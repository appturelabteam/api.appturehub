<?php
use League\OAuth2\Server\Exception\OAuthServerException;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Psr\Http\Message\ServerRequestInterface;

class MenuController extends Controller {
    static public $version = 0;
    static public $weight = 0;
    static public $genericName = "menu";
    static public $tableName = "menus";
    
    static public $tableFields = array(
        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
        "author_id" => array("Field" => "author_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "icon_image_id" => array("Field" => "icon_image_id", "Type" => "int(10) unsigned", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => "", "Label" => "Icon"),
        "parent_menu" => array("Field" => "parent_menu", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => 0, "Extra" => ""),
        "title" => array("Field" => "title", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "page_id" => array("Field" => "page_id", "Type" => "int", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
        "module" => array("Field" => "module", "Type" => "varchar(255)", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
        "url" => array("Field" => "url", "Type" => "varchar(255)", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
        "weight" => array("Field" => "weight", "Type" => "int(10)", "Null" => "NO", "Key" => "", "Default" => 0, "Extra" => ""),
        "roles" => array("Field" => "roles", "Type" => "varchar(255)", "Null" => "yes", "Key" => "", "Default" => "", "Extra" => "", "Items" => array(""=>array("All",true),"anonymous"=>array("Anonymous",true),"authenticated"=>array("Authenticated",true),"administrator"=>array("Administrator",true),"webmaster"=>array("Webmaster",true))),
        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
    );
    
    /**
     * 
     * @param type $params
     */
    function view($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {

            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response, $server);
            } else {
                $request = $server->validateAuthenticatedRequest($request);
            }
            
            if($method === "GET") {
                if(!isset($params[1])) {
                    $response = $this->_getAll($params, $request, $response, $server);
                } else if($params[1] === "hierarchy") {
                    $response = $this->_getMenuHierarchy($params, $request, $response, $server);
                } else {
                    $response = $this->_getSpecific($params, $request, $response, $server);
                }
            }

            // user profiles are created automatically on user registration
            if(strtoupper($method) === "POST") {
                $response = $this->_add($params, $request, $response, $server);
            }

            if(strtoupper($method) === "PUT" && isset($params[1])) {
                $response = $this->_update($params, $request, $response, $server);
            }

            // deleted with user accounts
            if(strtoupper($method) === "DELETE" && isset($params[1])) {
                $response = $this->_delete($params, $request, $response, $server);
            }
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    /**
     * 
     * @param type $params
     * @param type $request
     * @param type $response
     * @return type
     */
    function _getAll($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // get fields for this endpoint from options
        $options = $this->_getOptions();
        $fields = $options[MenuController::$genericName]["GET"]["response"]["list"][0];

        // automatically page the loaded items
        $limit_parameter = filter_input(INPUT_GET, "limit");
        $page_parameter = filter_input(INPUT_GET, "page");
        $limit = $limit_parameter == null ? 25 : intval($limit_parameter);
        $page = $page_parameter == null ? 0 : intval($page_parameter);
        $limitString = ($limit > 0 ? " LIMIT ".($page*($limit)).", $limit" : "");
        
        // ordering, check validity etc and parse for query
        $orderParameter = filter_input(INPUT_GET, "order");
        $orderParameter = $orderParameter == null ? "weight:ASC" : $orderParameter;
        $order_split = explode(":",$orderParameter);
        if(array_search($order_split[0], array_keys($fields)) === false || array_search($order_split[1], array("DESC","ASC")) === false) {
            $orderParameter = "weight:ASC";
        }
        $order = implode(" ",explode(":",$orderParameter))." ";

        // add filter
        $filterFields = array_keys($fields);
        $filters = isset($_GET["filter"]) ? array_filter($_GET["filter"]) : array();
        $filtersAnd = isset($_GET["filterAnd"]) ? array_filter($_GET["filterAnd"]) : array();
        $filterString = DS::gen_filters($filterFields, $filters, "OR");
        $filterStringAnd = DS::gen_filters($filterFields, $filtersAnd, "AND");
        $filterString = "($filterString AND $filterStringAnd)";

        $totalRead = DS::query("
            SELECT
                COUNT(id) as total
            FROM ".MenuController::$tableName);

        $totalFiltered = DS::query("
            SELECT
                COUNT(id) as total
            FROM ".MenuController::$tableName."
            WHERE ".$filterString);

        $generics = DS::query("SELECT
                ".implode(", ", array_keys($fields)).",
                IF(parent_menu != 0, (SELECT title FROM ".MenuController::$tableName." WHERE id = m.parent_menu LIMIT 1), '') as parent_menu
            FROM ".MenuController::$tableName." as m
            WHERE $filterString
            ORDER BY {$order} 
            $limitString");

        $responseBody["success"] = true;
        $responseBody["message"] = ucwords(str_camel_case_to_words(MenuController::$genericName)). " list";
        $responseBody["data"] = array(
            "filter" => $filters,
            "filterAnd" => $filtersAnd,
            "order" => $orderParameter,
            "list" => $generics,
            "limit" => $limit,
            "page" => $page,
            "count" => count($generics),
            "total_filtered" => intval($totalFiltered[0]["total"]),
            "total" => intval($totalRead[0]["total"])
        );
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(MenuController::$genericName,$request->getMethod(),0,$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getSpecific($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // get fields for this endpoint from options
        $options = $this->_getOptions();
        $fields = $options[MenuController::$genericName."/{id}"]["GET"]["response"];

        $generics = DS::query("SELECT
                ".implode(", ", array_keys($fields))."
            FROM ".MenuController::$tableName."
            WHERE id = ?i", intval($params[1]));

        if(count($generics)) {

            // found
            $responseBody["message"] = ucwords(str_camel_case_to_words(MenuController::$genericName)). " loaded";
            $responseBody["success"] = true;
            $responseBody["data"] = $generics[0];

        } else {

            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(MenuController::$genericName)). " not found";

        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(MenuController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _add($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        if(count(array_intersect(["administrator","webmaster"], $scopes))) {
            
            // get all fields from request body
            $requestBody = file_get_contents("php://input");
            $requestObject = json_decode($requestBody, true);
            
            // get fields for this endpoint from options
            $options = $this->_getOptions();
            $fields = $options[MenuController::$genericName]["POST"]["request"]["body"];
            
            // filter away bad fields
            $data = array();
            foreach(array_keys($fields) as $field) {
                if(isset($requestObject[$field])) {
                    $data[$field] = $requestObject[$field];
                }
            }

            // still have data?
            if(count($data)) {

                // some fields need to meet certain criteria - validate here
                $invalid = Forms::validateData($fields, $data);

                // valid?
                if(!count($invalid)) {
                    
                    $data["author_id"] = $user_id;

                    if(count($generic = DS::insert(MenuController::$tableName, $data))) {
                        
                        $responseBody["message"] = ucwords(str_camel_case_to_words(MenuController::$genericName)). " added";
                        $responseBody["success"] = true;
                        $responseBody["data"] = $generic[0];
                    } else {
                        $responseBody["message"] = "Database insert error";
                    }

                } else {
                    
                    // had invalid fields
                    $responseBody["message"] = "Some fields are invalid";
                    $responseBody["data"] = array(
                        "values" => $data,
                        "invalid" => $invalid
                    );

                }
            } // else parameters are missing - default message
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(MenuController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _update($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // get fields for update
        $options = $this->_getOptions();
        $fields = $options[MenuController::$genericName."/{id}"]["PUT"]["request"]["body"];
        
        $generics = DS::query("SELECT
                ".implode(", ", array_keys($fields))."
            FROM ".MenuController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // found?
        if(count($generics)) {
            
            // does the user have the correct scope
            if( count(array_intersect(["webmaster","administrator"], $scopes)) ) {

                $requestBody = file_get_contents("php://input");
                $requestObject = json_decode($requestBody, true);

                // filter away bad fields
                $data = array();
                $sparseFields = array();
                foreach(array_keys($fields) as $field) {
                    if(isset($requestObject[$field])) {
                        if(isset($requestObject[$field])) {
                            $data[$field] = $requestObject[$field];
                            $sparseFields[$field] = $fields[$field];
                        }
                    }
                }

                // still have data?
                if(count($data)) {

                    // some fields need to meet certain criteria - validate here
                    $invalid = Forms::validateData($sparseFields, $data, intval($params[1]));

                    // valid?
                    if(!count($invalid)) {

                        if(DS::update(MenuController::$tableName, $data, "WHERE id = ?i", $params[1]) !== null) {

                            $responseBody["message"] = ucwords(str_camel_case_to_words(MenuController::$genericName)). " updated";
                            $responseBody["success"] = true;
                            $responseBody["data"] = $data;
                        } else {
                            $responseBody["message"] = "Database update error";
                        }

                    } else {

                        // had invalid fields
                        $responseBody["message"] = "Some fields are invalid";
                        $responseBody["data"] = array(
                            "values" => $data,
                            "invalid" => $invalid
                        );

                    }

                } // else parameters are missing - default message
            
            } else {
            
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
                
            }

        } else {
            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(MenuController::$genericName)). " not found";
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(MenuController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _delete($params, $request, $response, $server) {
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $generics = DS::query("SELECT
                id
            FROM ".MenuController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // found?
        if(count($generics)) {
        
            // does the user have the correct scope
            if( count(array_intersect(["webmaster","administrator"], $scopes)) ) {
                
                if(DS::delete(MenuController::$tableName, "WHERE id = ?i", $params[1]) !== null) {
                    $responseBody["message"] = ucwords(str_camel_case_to_words(MenuController::$genericName)). " deleted";
                    $responseBody["success"] = true;
                } else {
                    $responseBody["message"] = "Database delete error";
                }
                
            } else {
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
            }
            
        } else {
            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(MenuController::$genericName)). " not found";
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(MenuController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    
    public static function getItem($title) {
        $menu = DS::select(MenuController::$tableName,"WHERE title = ?s", $title);
        return count($menu) ? $menu[0] : null;
    }
    
    /**
     * 
     * @param string $title
     * @param integer $pageId
     * @param string $module
     * @param string $url
     * @param integer $weight
     * @param mixed $roles Can be an array or a comma separated list.
     * @param integer $parent_menu Optional, Defaults to 0
     * @param integer $icon_image_id Optional, Defaults to null
     * @return mixed menu object or null if failed
     */
    public static function addItem($title,$pageId,$module,$url,$weight,$roles,$parent_menu = 0,$icon_image_id = null) {
		if(count($menu = DS::select(MenuController::$tableName, "WHERE title = ?s AND url = ?s AND page_id = ?i AND module = ?s", $title, $url, $pageId != null ? $pageId : "", $module != null ? $module : "")) === 0) {
			$menu = DS::insert(MenuController::$tableName,array(
				"author_id" => 0,
				"title" => $title,
				"page_id" => $pageId,
				"module" => $module,
				"url" => $url,
				"weight" => $weight,
				"roles" => is_array($roles) ? implode(",", $roles) : $roles,
				"parent_menu" => $parent_menu,
				"icon_image_id" => $icon_image_id
			));
		}
        return count($menu) ? $menu[0] : null;
    }
    
    function _createMenuItem($menu) {
        $link = array(
            "title" => $menu["title"],
            "url" => $menu["url"],
            "sub_menu" => array()
        );
        
        if($menu["module"] != "") {
            if(class_exists($menu["module"]."Controller")) {
                $moduleMenu = call_user_func($menu["module"]."Controller" .'::_getFrontendMenu');
                $link["sub_menu"] = $moduleMenu["sub_menu"];
            }
        } else if(intval($menu["page_id"]) > 0) {
            $page = DS::select("pages","WHERE id=?i",$menu["page_id"]);
            $link["url"] = "page/".$page[0]["slug_title"];
        }
        
        $link["weight"] = $menu["weight"];
        return $link;
    }
    
    function _loadMenuSubLinks($menu, $rolesWhere = "") {
        $links = array();
        foreach(DS::select("menus","WHERE parent_menu = ?i $rolesWhere ORDER BY weight ASC, title DESC",$menu["id"]) as $menu) {
            $item = $this->_createMenuItem($menu);
            if(isset($item["title"])) {
                $item["sub_menu"] = array_merge(isset($item["sub_menu"]) ? $item["sub_menu"] : array(),$this->_loadMenuSubLinks($menu, $rolesWhere));
                $links[] = $item;
            } else {
                $links = array_merge($links,$item);
            }
        }
        return $links;
    }
    
    /**
     * 
     * @param type $rolesArray List of roles for which to load menu items
     * @param type $title Load specific menu with its sub-menus or pass null to load full hierarchy.
     * @return type
     */
    function _loadMenu($rolesArray = null, $title = null) {
        $rolesWhere = "";
        if($rolesArray != null) {
            $roles = array();
            foreach($rolesArray as $role) {
                $roles[] = "FIND_IN_SET(".DS::escape($role).", roles)";
            }
            $rolesWhere = "AND ( ".implode(" OR ",$roles)." OR roles = '')";
        }
        
        $parent_menu = 0;
        if($title) {
            if(count($parent_menus = DS::select("menus","WHERE title LIKE ?s $rolesWhere",$title))) {
                $parent_menu = $parent_menus[0]["id"];
            }
        }
        $links = array();
        foreach(DS::select("menus","WHERE parent_menu = ?i $rolesWhere ORDER BY weight ASC, title DESC",$parent_menu) as $menu) {
            $item = $this->_createMenuItem($menu);
            if(isset($item["title"])) {
                $item["sub_menu"] = array_merge(isset($item["sub_menu"]) ? $item["sub_menu"] : array(),$this->_loadMenuSubLinks($menu, $rolesWhere));
                $links[] = $item;
            } else {
                $links = array_merge($links,$item);
            }
        }
        return $links;
    }
    
    function _getMenuHierarchy($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        $roles = isset($_GET["roles"]) ? array_filter($_GET["roles"]) : $scopes;
        $title = filter_input(INPUT_GET, "title");
        
        try {
            $responseBody["data"] = $this->_loadMenu($roles, $title);
            $responseBody["message"] = "Menu loaded";
            $responseBody["success"] = true;
        } catch (Exception $e) {
            $responseBody["message"] = "Error: ".$e->getMessage();
        }
        
        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _getSelectListItems($currentId = 0, $parentId = 0, $parentTitle = "") {
        //$time = microtime(true);
        $items = array();
        if($parentId === 0) {
            $items[] = array(
                "None", true
            );
        }
        $menus = DS::select(MenuController::$tableName,"WHERE parent_menu = ?i ORDER BY weight ASC, title DESC", $parentId);
        foreach($menus as $menu) {
            $title = $parentTitle.($parentTitle !== "" ? " > " : "").$menu["title"];
            $items[$menu["id"]] = array($title,$currentId == $menu["id"] ? false : true);
            $items = $items + $this->_getSelectListItems($currentId, $menu["id"], $title);
        }
        //var_dump("{$parentId}: ".(microtime(true)-$time));
        return $items;
    }
            
    
    function _getOptions() {
        $weightItems = array();
        for($x=-25;$x<=25;$x++) {
            $weightItems[" ".$x] = array($x,true);
        }
        
        $selectListItems = $this->_getSelectListItems();
        
        $moduleItems = array();
        $moduleItems[""] = array("Select...",true);
        foreach(Controller::getControllers() as $controller) {
            if(array_search("_getFrontendMenu", get_class_methods($controller["controller_class"]))!==false) {
                $moduleItems[str_replace("Controller", "", $controller["controller_class"])] = array(Template::toTitleCase(str_replace("Controller", "", $controller["controller_class"])),true);
            }
        }
        
        $pageItems = array();
        $pageItems[""] = array("Select...",true);
        foreach(DS::select("pages","ORDER BY title ASC") as $page) {
            $pageItems[$page["id"]] = array($page["title"],true);
        }
        
        // use the following array to override the field details where necessary
        $replaceOptions = array(
            MenuController::$genericName => array(
                "GET" => array(
                    "response" => array(
                        "list" => array(
                            array(
                                "icon_image_id" => array("Type" => "image_id"),
                                "roles" => array("Type" => "checkbox")
                            )
                        ),
                    )
                ),
                "POST" => array(
                    "request" => array(
                        "body" => array(
                            "icon_image_id" => array("Type" => "image_id"),
                            "parent_menu" => array("Type" => "select", "Items" => $selectListItems),
                            "page_id" => array("Type" => "select", "Label" => "Link to a Page", "Items" => $pageItems),
                            "module" => array("Type" => "select", "Label" => "Link to a Module", "Items" => $moduleItems),
                            "roles" => array("Type" => "checkbox"),
                            "weight" => array("Type" => "select", "Items" => $weightItems)
                        )
                    )
                ),
            ),
            MenuController::$genericName."/{id}" => array(
                "PUT" => array(
                    "request" => array(
                        "body" => array(
                            "icon_image_id" => array("Type" => "image_id"),
                            "parent_menu" => array("Type" => "select", "Items" => $selectListItems),
                            "page_id" => array("Type" => "select", "Label" => "Link to a Page", "Items" => $pageItems),
                            "module" => array("Type" => "select", "Label" => "Link to a Module", "Items" => $moduleItems),
                            "roles" => array("Type" => "checkbox"),
                            "weight" => array("Type" => "select", "Items" => $weightItems)
                        )
                    )
                )
            ),
            MenuController::$genericName."/hierarchy" => array(
                "GET" => array(
                    "title" => "Menu Hierarchy",
                    "description" => "Load and generate a menu hierarchy for specific roles or a specific menu.",
                    "request" => array(
                        "parameters" => array(
                            "roles[]" => array("Field" => "roles[]", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Description" => "Load menu hierarchy for specific roles."),
                            "title" => array("Field" => "title", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Description" => "Load a menu hierarchy starting with a specific title.")
                        ),
                        "body" => array()
                    )
                )
            )
        );
        
        // now generate the full options array
        $options = $this->_generateOptions(MenuController::$genericName, MenuController::$tableFields, $replaceOptions);
        
        // remove any fields that should not be returned
        
        //unset($options[MenuController::$genericName]["GET"]["response"]["list"][0]["example_field_name"]);
        
        unset($options[MenuController::$genericName]["POST"]["request"]["body"]["id"]);
        unset($options[MenuController::$genericName]["POST"]["request"]["body"]["author_id"]);
        unset($options[MenuController::$genericName]["POST"]["request"]["body"]["update_date"]);
        unset($options[MenuController::$genericName]["POST"]["request"]["body"]["create_date"]);
        
        unset($options[MenuController::$genericName."/{id}"]["PUT"]["request"]["body"]["id"]);
        unset($options[MenuController::$genericName."/{id}"]["PUT"]["request"]["body"]["author_id"]);
        unset($options[MenuController::$genericName."/{id}"]["PUT"]["request"]["body"]["update_date"]);
        unset($options[MenuController::$genericName."/{id}"]["PUT"]["request"]["body"]["create_date"]);
        unset($options[MenuController::$genericName."/{id}"]["PUT"]["response"]["id"]);
        unset($options[MenuController::$genericName."/{id}"]["PUT"]["response"]["author_id"]);
        unset($options[MenuController::$genericName."/{id}"]["PUT"]["response"]["update_date"]);
        unset($options[MenuController::$genericName."/{id}"]["PUT"]["response"]["create_date"]);
        
        return $options;
    }
    
    function _options($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $options = $this->_getOptions();
        
        $responseBody["message"] = ucwords(str_camel_case_to_words(MenuController::$genericName)). " options";
        $responseBody["success"] = true;
        $responseBody["data"] = $options;

        // prep output
        return $response->withPayload($responseBody);
    }
    
    
    
    public function install() {
        $tables = DS::list_tables();
        
        if(array_search(MenuController::$tableName, $tables)===false) {
            
            // generate the create table query
            $query = "CREATE TABLE ".MenuController::$tableName." (";
            
            $queryFields = "";
            $primaryKey = null;
            $uniqueKey = null;
            
            foreach(MenuController::$tableFields as $field) {
                $queryFields.= ($queryFields ? ", " : "")."{$field["Field"]} {$field["Type"]} ".(strtolower($field["Null"]) === "no" ? "NOT NULL" : "")." ".($field["Default"] ? "DEFAULT ".($field["Default"] === "CURRENT_TIMESTAMP" ? $field["Default"] : "'{$field["Default"]}'") : "")." ".($field["Extra"] ? $field["Extra"] : "");
                if(strtolower($field["Key"]) === "pri") {
                    $primaryKey = ", PRIMARY KEY ({$field["Field"]})";
                } else if(strtolower($field["Key"]) === "uni") {
                    $uniqueKey = ", UNIQUE `{$field["Field"]}` (`{$field["Field"]}`)";
                }
            }
            
            $query.= $queryFields.($primaryKey ? $primaryKey : "").($uniqueKey ? ($primaryKey ? ", " : "").$uniqueKey : "").") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            if(DS::query($query)) {
                //message_add("The users table has been created.");
            }
        }
        
        if(($adminMenu = MenuController::getItem("Admin")) === null) {
            $adminMenu = MenuController::addItem("Admin", null, null, "", 0, ["administrator","webmaster"]);
        }
        if(($siteContentMenu = MenuController::getItem("Content & Media")) === null) {
            $siteContentMenu = MenuController::addItem("Content & Media", null, null, "", 1, ["administrator","webmaster"], $adminMenu["id"]);
        }
        MenuController::addItem("<i class='fa fa-list'></i> Menu Links", null, null, "menu", 0, ["administrator","webmaster"], $siteContentMenu["id"]);
        
        if(($mainMenu = MenuController::getItem("Main")) === null) {
            $mainMenu = MenuController::addItem("Main", null, null, "", 0, ["anonymous","authenticated"]);
        }
        MenuController::addItem("Home", null, null, "./", 0, ["anonymous","authenticated"], $mainMenu["id"]);
        MenuController::addItem("Contact Us", null, null, "./contact-us", 2, ["anonymous","authenticated"], $mainMenu["id"]);
    }
    
    public function update($from_version) {
        
        /*if($from_version < 1) {
            
            try {
                $query = "ALTER TABLE menus ADD strategy_id INT AFTER identifier;";
                
                DS::query($query);
                
                $from_version ++;
                
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }*/
        
        return $from_version;
    }
}