<?php
use League\OAuth2\Server\Exception\OAuthServerException;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Psr\Http\Message\ServerRequestInterface;

class ContactController extends Controller {
    static public $version = 3;
    static public $weight = 10;
    static public $genericName = "contact";
    static public $tableName = "contacts";
    
    static public $tableFields = array(
        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
        "email" => array("Field" => "email", "Type" => "varchar(128)", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "phone" => array("Field" => "phone", "Type" => "varchar(128)", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "contact_name" => array("Field" => "contact_name", "Type" => "varchar(128)", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "message" => array("Field" => "message", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "width" => array("Field" => "width", "Type" => "varchar(32)", "Null" => "yes", "Key" => "", "Default" => "", "Extra" => ""),
        "profile" => array("Field" => "profile", "Type" => "varchar(32)", "Null" => "yes", "Key" => "", "Default" => "", "Extra" => ""),
        "rim_size" => array("Field" => "rim_size", "Type" => "varchar(32)", "Null" => "yes", "Key" => "", "Default" => "", "Extra" => ""),
        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
    );
    
    /**
     * 
     * @param type $params
     */
    function view($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {

            $request = $server->validateAuthenticatedRequest($request);
            
            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response, $server);
            }
            
            if($method === "GET") {
                if(!isset($params[1])) {
                    $response = $this->_getAll($params, $request, $response, $server);
                } else {
                    $response = $this->_getSpecific($params, $request, $response);
                }
            }

            // user profiles are created automatically on user registration
            if(strtoupper($method) === "POST") {
                $response = $this->_add($params, $request, $response);
            }

            if(strtoupper($method) === "PUT" && isset($params[1])) {
                $response = $this->_update($params, $request, $response);
            }

            // deleted with user accounts
            if(strtoupper($method) === "DELETE" && isset($params[1])) {
                $response = $this->_delete($params, $request, $response);
            }
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    /**
     * 
     * @param type $params
     * @param type $request
     * @param type $response
     * @return type
     */
    function _getAll($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // does the user have the correct scope
        if(count(array_intersect(["administrator","webmaster"], $scopes))) {

            // get fields for this endpoint from options
            $options = $this->_getOptions($request);
            $fields = $options[ContactController::$genericName]["GET"]["response"]["list"][0];

            // automatically page the loaded items
            $limit_parameter = filter_input(INPUT_GET, "limit");
            $page_parameter = filter_input(INPUT_GET, "page");
            $limit = $limit_parameter == null ? 25 : intval($limit_parameter);
            $page = $page_parameter == null ? 0 : intval($page_parameter);
            $limitString = ($limit > 0 ? " LIMIT ".($page*($limit)).", $limit" : "");

            // ordering, check validity etc and parse for query
            $orderParameter = filter_input(INPUT_GET, "order");
            $orderParameter = $orderParameter == null ? "create_date:DESC" : $orderParameter;
            $order_split = explode(":",$orderParameter);
            if(array_search($order_split[0], array_keys($fields)) === false || array_search($order_split[1], array("DESC","ASC")) === false) {
                $orderParameter = "create_date:DESC";
            }
            $order = implode(" ",explode(":",$orderParameter))." ";

            // add filter
            $filterFields = array_keys($fields);
            $filters = isset($_GET["filter"]) ? array_filter($_GET["filter"]) : array();
            $filtersAnd = isset($_GET["filterAnd"]) ? array_filter($_GET["filterAnd"]) : array();
            $filterString = DS::gen_filters($filterFields, $filters, "OR");
            $filterStringAnd = DS::gen_filters($filterFields, $filtersAnd, "AND");
            $filterString = "($filterString AND $filterStringAnd)";

            $totalRead = DS::query("
                SELECT
                    COUNT(id) as total
                FROM ".ContactController::$tableName);

            $totalFiltered = DS::query("
                SELECT
                    COUNT(id) as total
                FROM ".ContactController::$tableName."
                WHERE ".$filterString);

            $generics = DS::query("SELECT
                    ".implode(", ", array_keys($fields))."
                FROM ".ContactController::$tableName."
                WHERE $filterString
                ORDER BY {$order} 
                $limitString");

            $responseBody["success"] = true;
            $responseBody["message"] = ucwords(str_camel_case_to_words(ContactController::$genericName)). " list";
            $responseBody["data"] = array(
                "filter" => $filters,
                "filterAnd" => $filtersAnd,
                "order" => $orderParameter,
                "list" => $generics,
                "limit" => $limit,
                "page" => $page,
                "count" => count($generics),
                "total_filtered" => intval($totalFiltered[0]["total"]),
                "total" => intval($totalRead[0]["total"])
            );
               
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(ContactController::$genericName,$request->getMethod(),0,$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getSpecific($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // does the user have the correct scope
        if(count(array_intersect(["administrator","webmaster"], $scopes))) {
            
            // get fields for this endpoint from options
            $options = $this->_getOptions($request);
            $fields = $options[ContactController::$genericName."/{id}"]["GET"]["response"];

            $generics = DS::query("SELECT
                    ".implode(", ", array_keys($fields))."
                FROM ".ContactController::$tableName."
                WHERE id = ?i", intval($params[1]));

            if(count($generics)) {

                // found
                $responseBody["message"] = ucwords(str_camel_case_to_words(ContactController::$genericName)). " loaded";
                $responseBody["success"] = true;
                $responseBody["data"] = $generics[0];

            } else {

                // not found
                $responseBody["message"] = ucwords(str_camel_case_to_words(ContactController::$genericName)). " not found";

            }
               
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(ContactController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _add($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
            
        // get all fields from request body
        $requestBody = file_get_contents("php://input");
        $requestObject = json_decode($requestBody, true);

        // get fields for this endpoint from options
        $options = $this->_getOptions($request);
        $fields = $options[ContactController::$genericName]["POST"]["request"]["body"];

        // filter away bad fields
        $data = array();
        foreach(array_keys($fields) as $field) {
            if(isset($requestObject[$field])) {
                $data[$field] = $requestObject[$field];
            }
        }

        // still have data?
        if(count($data)) {
            
            // some fields need to meet certain criteria - validate here
            $invalid = Forms::validateData($fields, $data);
            
            if((!isset($data["email"]) || $data["email"] == "") && (!isset($data["phone"]) || $data["phone"] == "")) {
                $invalid["email"] = array(
                    "status" => "warning",
                    "message" => "Please provide either an email address or phone number"
                );
            }
            
            if(isset($data["question"]) && count(array_intersect(["anonymous"], $scopes))) {
                $ip_address = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "NA";
                if(count($logs = DS::select("logs","WHERE log_type = 'CONTACT' AND action = 'QUESTION' AND ip_address = ?s ORDER BY create_date DESC LIMIT 1", $ip_address))) {
                    list($question_a,$question_b) = explode(",",$logs[0]["description"]);
                    if(isset($data["question"]) && intval($data["question"]) !== intval($question_a) + intval($question_b)) {
                        $invalid["question"] = array(
                            "status" => "error",
                            "message" => "Please try again"
                        );
                    }
                } else {
                    $invalid["question"] = array(
                        "status" => "error",
                        "message" => "Session invalid"
                    );
                }
            }

            // valid?
            if(!count($invalid)) {
                
                unset($data["question"]);
                
                $contact = DS::select(ContactController::$tableName, "WHERE email = ?s", $data["email"]);
                
                if(count($contact)) {
                    
                    if(DS::update(ContactController::$tableName, $data, "WHERE id = ?i", $contact[0]["id"]) !== null) {

                if(count(array_intersect(["administrator","webmaster"], $scopes)) === 0) {
                    // send mail when anonymous user created this contact
                            EventController::trigger("contact-form-submission", 0, $data, null, $contact[0]["id"]);
                }
                
                        $responseBody["message"] = ucwords(str_camel_case_to_words(ContactController::$genericName)). " updated";
                        $responseBody["success"] = true;
                        $responseBody["data"] = $contact[0];
                    } else {
                        $responseBody["message"] = "Database insert error";
                    }
                
                } else {
                    
                if(count($generic = DS::insert(ContactController::$tableName, $data))) {

                        if(count(array_intersect(["administrator","webmaster"], $scopes)) === 0) {
                            // send mail when anonymous user created this contact
                            EventController::trigger("contact-form-submission", 0, $data, null, $generic[0]["id"]);
                        }

                    $responseBody["message"] = ucwords(str_camel_case_to_words(ContactController::$genericName)). " added";
                    $responseBody["success"] = true;
                    $responseBody["data"] = $generic[0];
                } else {
                    $responseBody["message"] = "Database insert error";
                }

                }
                
                

            } else {

                // had invalid fields
                $responseBody["message"] = "Some fields are invalid";
                $responseBody["data"] = array(
                    "values" => $data,
                    "invalid" => $invalid
                );

            }
        } // else parameters are missing - default message

        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(ContactController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _update($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // get fields for update
        $options = $this->_getOptions($request);
        $fields = $options[ContactController::$genericName."/{id}"]["PUT"]["request"]["body"];
        
        $generics = DS::query("SELECT
                ".implode(", ", array_keys($fields))."
            FROM ".ContactController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // found?
        if(count($generics)) {
            
            // does the user have the correct scope
            if( count(array_intersect(["webmaster","administrator"], $scopes)) ) {

                $requestBody = file_get_contents("php://input");
                $requestObject = json_decode($requestBody, true);

                // filter away bad fields
                $data = array();
                $sparseFields = array();
                foreach(array_keys($fields) as $field) {
                    if(isset($requestObject[$field])) {
                        if(isset($requestObject[$field])) {
                            $data[$field] = $requestObject[$field];
                            $sparseFields[$field] = $fields[$field];
                        }
                    }
                }

                // still have data?
                if(count($data)) {

                    // some fields need to meet certain criteria - validate here
                    $invalid = Forms::validateData($sparseFields, $data, intval($params[1]));

                    // valid?
                    if(!count($invalid)) {

                        if(DS::update(ContactController::$tableName, $data, "WHERE id = ?i", $params[1]) !== null) {

                            $responseBody["message"] = ucwords(str_camel_case_to_words(ContactController::$genericName)). " updated";
                            $responseBody["success"] = true;
                            $responseBody["data"] = $data;
                        } else {
                            $responseBody["message"] = "Database update error";
                        }

                    } else {

                        // had invalid fields
                        $responseBody["message"] = "Some fields are invalid";
                        $responseBody["data"] = array(
                            "values" => $data,
                            "invalid" => $invalid
                        );

                    }

                } // else parameters are missing - default message
            
            } else {
            
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
                
            }

        } else {
            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(ContactController::$genericName)). " not found";
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(ContactController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _delete($params, $request, $response) {
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $generics = DS::query("SELECT
                id
            FROM ".ContactController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // found?
        if(count($generics)) {
        
            // does the user have the correct scope
            if( count(array_intersect(["webmaster","administrator"], $scopes)) ) {
                
                if(DS::delete(ContactController::$tableName, "WHERE id = ?i", $params[1]) !== null) {
                    $responseBody["message"] = ucwords(str_camel_case_to_words(ContactController::$genericName)). " deleted";
                    $responseBody["success"] = true;
                } else {
                    $responseBody["message"] = "Database delete error";
                }
                
            } else {
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
            }
            
        } else {
            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(ContactController::$genericName)). " not found";
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(ContactController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getOptions($request) {
        
        list($scopes,$user_id) = $this->_getViewer($request);
        
        $question_a = rand(0, 9);
        $question_b = rand(0, 9);
        
        if(count(array_intersect(["anonymous"], $scopes))) {
            $ip_address = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "NA";
            
            if(count($logs = DS::select("logs","WHERE log_type = 'CONTACT' AND action = 'QUESTION' AND ip_address = ?s ORDER BY create_date DESC LIMIT 1", $ip_address))) {
                list($question_a,$question_b) = explode(",",$logs[0]["description"]);
            } else {
                LogController::log("CONTACT", "QUESTION", 0, 0, 0, "{$question_a},{$question_b}", "");
            }
        }
        
        // use the following array to override the field details where necessary
        $replaceOptions = array(
            ContactController::$genericName => array(
                "GET" => array(
                    "response" => array(
                        "list" => array(
                            array(
                                "email" => array("Type" => "email")
                            )
                        ),
                    )
                ),
                "POST" => array(
                    "request" => array(
                        "body" => array(
                            "email" => array("Type" => "email"),
                            "question" => array("Type" => "integer", "Null" => "yes", "Label" => "What is {$question_a} + {$question_b}?", "Extra" => "", "Default" => 0)
                        )
                    )
                ),
            ),
            ContactController::$genericName."/{id}" => array(
                "PUT" => array(
                    "request" => array(
                        "body" => array(
                            "email" => array("Type" => "email")
                        )
                    )
                )
            )
        );
        
        // now generate the full options array
        $options = $this->_generateOptions(ContactController::$genericName, ContactController::$tableFields, $replaceOptions);
        
        // remove any fields that should not be returned
        
        //unset($options[ContactController::$genericName]["GET"]["response"]["list"][0]["example_field_name"]);
        
        unset($options[ContactController::$genericName]["POST"]["request"]["body"]["id"]);
        unset($options[ContactController::$genericName]["POST"]["request"]["body"]["author_id"]);
        unset($options[ContactController::$genericName]["POST"]["request"]["body"]["question"]);
        unset($options[ContactController::$genericName]["POST"]["request"]["body"]["update_date"]);
        unset($options[ContactController::$genericName]["POST"]["request"]["body"]["create_date"]);
        
        unset($options[ContactController::$genericName."/{id}"]["PUT"]["request"]["body"]["id"]);
        unset($options[ContactController::$genericName."/{id}"]["PUT"]["request"]["body"]["author_id"]);
        unset($options[ContactController::$genericName."/{id}"]["PUT"]["request"]["body"]["update_date"]);
        unset($options[ContactController::$genericName."/{id}"]["PUT"]["request"]["body"]["create_date"]);
        unset($options[ContactController::$genericName."/{id}"]["PUT"]["response"]["id"]);
        unset($options[ContactController::$genericName."/{id}"]["PUT"]["response"]["author_id"]);
        unset($options[ContactController::$genericName."/{id}"]["PUT"]["response"]["update_date"]);
        unset($options[ContactController::$genericName."/{id}"]["PUT"]["response"]["create_date"]);
        
        return $options;
    }
    
    function _options($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $options = $this->_getOptions($request);
        
        $responseBody["message"] = ucwords(str_camel_case_to_words(ContactController::$genericName)). " options";
        $responseBody["success"] = true;
        $responseBody["data"] = $options;

        // prep output
        return $response->withPayload($responseBody);
    }
    
    
    
    public function install() {
        $tables = DS::list_tables();
        
        if(array_search(ContactController::$tableName, $tables)===false) {
            
            // generate the create table query
            $query = "CREATE TABLE ".ContactController::$tableName." (";
            
            $queryFields = "";
            $primaryKey = null;
            $uniqueKey = null;
            
            foreach(ContactController::$tableFields as $field) {
                $queryFields.= ($queryFields ? ", " : "")."{$field["Field"]} {$field["Type"]} ".(strtolower($field["Null"]) === "no" ? "NOT NULL" : "")." ".($field["Default"] ? "DEFAULT ".($field["Default"] === "CURRENT_TIMESTAMP" ? $field["Default"] : "'{$field["Default"]}'") : "")." ".($field["Extra"] ? $field["Extra"] : "");
                if(strtolower($field["Key"]) === "pri") {
                    $primaryKey = ", PRIMARY KEY ({$field["Field"]})";
                } else if(strtolower($field["Key"]) === "uni") {
                    $uniqueKey = ", UNIQUE `{$field["Field"]}` (`{$field["Field"]}`)";
                }
            }
            
            $query.= $queryFields.($primaryKey ? $primaryKey : "").($uniqueKey ? ($primaryKey ? ", " : "").$uniqueKey : "").") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            if(DS::query($query)) {
                //message_add("The users table has been created.");
            }
        }
        
        if(($adminMenu = MenuController::getItem("Admin")) === null) {
            $adminMenu = MenuController::addItem("Admin", null, null, "", 0, ["administrator","webmaster"]);
        }
        if(($communicationMenu = MenuController::getItem("Communication")) === null) {
            $communicationMenu = MenuController::addItem("Communication", null, null, "", 0, ["administrator","webmaster"], $adminMenu["id"]);
        }
        MenuController::addItem("<i class='fa fa-group'></i> Contacts", null, null, "contact", 1, ["administrator","webmaster"], $communicationMenu["id"]);
    }
    
    public function update($from_version) {
        
        if($from_version < 1) {
            
            try {
                EventController::add("Contact Form Submission","Contact Form","Sent when a contact form has been submitted", "contact/contact_admin", ["administrator"]);
                EventController::add("Contact Form Submission","Contact Form","Sent when you submit a contact form", "contact/contact", ["anonymous"]);
                
                $from_version ++;
                
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
        
        return $from_version;
    }
}