<?php
use League\OAuth2\Server\Exception\OAuthServerException;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Psr\Http\Message\ServerRequestInterface;

class UserProfileController extends Controller {
    static public $version = 1;
    static public $genericName = "user_profile";
    static public $tableName = "user_profiles";
    
    function __construct($route_name) {
        parent::__construct($route_name);
        
        $this->permissions = Permissions::get(UserProfileController::$genericName);
    }
    
    function view($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {
            if($method === "GET") {
                if(!isset($params[1])) {
                    $request = $server->validateAuthenticatedRequest($request);
                    $response = $this->_getAll($params, $request, $response, $server);
                } else {
                    $request = $server->validateAuthenticatedRequest($request);
                    $response = $this->_getSpecific($params, $request, $response, $server);
                }
            }

            // user profiles are created automatically on user registration
            /*if(strtoupper($method) === "POST") {
                $response = $this->_add($params, $request, $response);
            }*/

            if(strtoupper($method) === "PUT" && isset($params[1])) {
                $request = $server->validateAuthenticatedRequest($request);
                $response = $this->_update($params, $request, $response);
            }

            // deleted with user accounts
            /*if(strtoupper($method) === "DELETE" && isset($params[1])) {
                $response = $this->_delete($params, $request, $response);
            }*/

            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response);
            }
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    function _getAll($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if(count(array_intersect(["webmaster,administrator"],$scopes)) > 0) {
            
            $options = $this->_getOptions($user_id, $scopes);
            $fields = $options[UserProfileController::$genericName]["GET"]["response"]["list"][0];
            
            // automatically page the loaded items
            $limit_parameter = filter_input(INPUT_GET, "limit");
            $page_parameter = filter_input(INPUT_GET, "page");
            $limit = $limit_parameter === null ? 25 : intval($limit_parameter);
            $page = $page_parameter == null ? 0 : intval($page_parameter);
            $limitString = ($limit > 0 ? " LIMIT ".($page*($limit)).", $limit" : "");
            
            // add filter
            $filterFields = array_keys($fields);
            $passedFilter = filter_input(INPUT_GET, "filter");
            $filterString = "(1=1)";
            if($passedFilter) {
                $filterString = "";
                if(stripos($passedFilter,"+") !== false) {
                    // multiple filters
                    $filters = explode("+",$passedFilter);
                } else {
                    $filters = [$passedFilter];
                }

                foreach($filters as $filterGroup) {
                    $filterSubParts = explode(" ",$filterGroup);
                    
                    foreach($filterSubParts as $filter) {
                        $filterPart = "";

                        if(stripos($filter,"|")!==false) { // exact match
                            // filter on a specific column
                            $columnFilter = explode("|",$filter);
                            $filterVal = DS::escape("{$columnFilter[1]}");
                            $filterPart = ($filterPart !== "" ? " OR " : ""). "{$columnFilter[0]} = $filterVal";

                        } else if(stripos($filter,":")!==false) { // LIKE
                            // filter on a specific column
                            $columnFilter = explode(":",$filter);
                            $filterVal = DS::escape("%{$columnFilter[1]}%");
                            $filterPart = ($filterPart !== "" ? " OR " : ""). "{$columnFilter[0]} LIKE $filterVal";

                        } else {
                            // filter on all columns
                            $filterVal = DS::escape("%$filter%");
                            foreach($filterFields as $field) {
                                $filterPart.= ($filterPart !== "" ? " OR " : ""). ''.$field. " LIKE $filterVal";
                            }

                        }

                        $filterString.= ($filterString !== "" ? " OR " : ""). "($filterPart)";
                    }
                }

                $filterString = "($filterString)";
            }

            $totalRead = DS::query("
                SELECT
                    COUNT(id) as total
                FROM ".UserProfileController::$tableName."");
            
            $totalFiltered = DS::query("
                SELECT
                    COUNT(id) as total
                FROM ".UserProfileController::$tableName."
                WHERE $filterString");

            $generics = DS::query("SELECT
                    ".implode(", ", array_keys($fields))."
                FROM ".UserProfileController::$tableName."
                WHERE $filterString
                ORDER BY create_date DESC
                $limitString");

            $responseBody["success"] = true;
            $responseBody["message"] = ucwords(str_camel_case_to_words(UserProfileController::$genericName)). " list";
            $responseBody["data"] = array(
                "list" => $generics,
                "limit" => 0,
                "page" => 0,
                "count" => count($generics),
                "total_filtered" => intval($totalFiltered[0]["total"]),
                "total" => intval($totalRead[0]["total"])
            );
                   
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(UserProfileController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getSpecific($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        /*
         * 
         *  NOTE! we are going to load a profile given a user's id not a users profile
         *  id, so here we are going to switch from a profile id to a user id, or
         *  if own is passed just use the current user's id
         * 
         */
        
        if(count(array_intersect(["authenticated"], $scopes))) {
            if($params[1] === "own") {
                $params[1] = $user_id;
            } else {
                $generics = DS::query("SELECT
                        user_id
                    FROM user_profiles
                    WHERE id = ?i", $params[1]);
                if(count($generics)) {
                    $params[1] = $generics[0]["user_id"];
                }
            }
        }
        
        
            
        $options = $this->_getOptions($user_id, $scopes, 0);
        $fields = $options["".UserProfileController::$genericName."/{id}"]["GET"]["response"];

        $generics = DS::query("SELECT
                up.".implode(", up.", array_keys($fields)).",
                IFNULL(IF(up.first_name IS NOT NULL AND up.last_name IS NOT NULL, CONCAT(up.first_name,' ',up.last_name), up.first_name ), u.username) as username
            FROM oauth2_users as u
            LEFT JOIN user_profiles as up ON up.user_id = u.id
            WHERE u.id = ?i", $params[1]);

        if(count($generics)) {

            $isOwner = boolval(intval($generics[0]["user_id"]) === intval($user_id)) || count(array_intersect(["webmaster,administrator"],$scopes));
            if(!$isOwner) {
                unset($generics[0]["id"]);
                unset($generics[0]["user_id"]);
                unset($generics[0]["first_name"]);
                unset($generics[0]["last_name"]);
                unset($generics[0]["id_number"]);
                unset($generics[0]["id_number_type"]);
                unset($generics[0]["update_date"]);
                unset($generics[0]["create_date"]);
            }

            // found
            $responseBody["success"] = true;
            $responseBody["message"] = ucwords(str_camel_case_to_words(UserProfileController::$genericName)). " loaded";
            $responseBody["data"] = $generics[0];

        } else {

            $responseBody["message"] = ucwords(str_camel_case_to_words(UserProfileController::$genericName)). " not found";

        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(UserProfileController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    /*function _add($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if(!count(array_intersect(["authenticated","curator","artist"], $scopes)) || count(array_intersect(["webmaster","administrator"], $scopes))) {
            
            // get all fields from request body
            $requestBody = file_get_contents("php://input");
            $requestObject = json_decode($requestBody, true);

            $options = $this->_getOptions();
            $fields = $options["user_profile"]["POST"]["request"]["body"];//DS::table_info(UserProfileController::$tableName);
            
            // filter away bad fields
            $data = array();
            foreach(array_keys($fields) as $field) {
                if(isset($requestObject[$field])) {
                    $data[$field] = $requestObject[$field];
                }
            }

            // still have data?
            if(count($data)) {

                // some fields need to meet certain criteria - validate here
                $invalid = Forms::validateData($fields, $data);

                // valid?
                if(!count($invalid)) {

                    if(count($generic = DS::insert(UserProfileController::$tableName, $data))) {
                        
                        $responseBody["message"] = ucwords(str_camel_case_to_words(UserProfileController::$genericName)). " added";
                        $responseBody["success"] = true;
                        $responseBody["data"] = $generic[0];
                    } else {
                        $responseBody["message"] = "Database insert error";
                    }

                } else {
                    
                    // had invalid fields
                    $responseBody["message"] = "Some fields are invalid";
                    $responseBody["data"] = array(
                        "values" => $data,
                        "invalid" => $invalid
                    );

                }
            } // else parameters are missing - default message
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        return $response->withPayload($responseBody);
    }*/
    
    function _update($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if(count(array_intersect(["authenticated"], $scopes)) && $params[1] === "own") {
            $generics = DS::query("SELECT
                    up.id
                FROM user_profiles as up
                WHERE up.user_id = ?i", $user_id);
            if(count($generics)) {
                $params[1] = $generics[0]["id"];
            }
        }
        
        if(count(array_intersect(["authenticated"],$scopes)) > 0 || count(array_intersect(["webmaster,administrator"],$scopes)) > 0) {
            
            $generics = DS::query("SELECT
                    up.id
                FROM user_profiles as up
                WHERE up.id = ?i", $params[1]);
            
            if(count($generics)) {
                
                $requestBody = file_get_contents("php://input");
                $requestObject = json_decode($requestBody, true);

                $options = $this->_getOptions();
                $fields = $options[UserProfileController::$genericName."/{id}"]["PUT"]["request"]["body"];

                $isOwner = boolval(intval($generics[0]["user_id"]) === intval($user_id));

                // filter away bad fields
                $data = array();
                $sparseFields = array();
                foreach(array_keys($fields) as $field) {
                    if(isset($requestObject[$field])) {
                        $data[$field] = $requestObject[$field];
                        $sparseFields[$field] = $field;
                    }
                }

                // still have data?
                if(count($data)) {

                    // some fields need to meet certain criteria - validate here
                    $invalid = Forms::validateData($sparseFields, $data, intval($params[1]));

                    // valid?
                    if(!count($invalid)) {

                        if(DS::update(UserProfileController::$tableName, $data, "WHERE id = ?i", $params[1]) !== null) {

                            $responseBody["message"] = ucwords(str_camel_case_to_words(UserProfileController::$genericName)). " updated";
                            $responseBody["success"] = true;
                            $responseBody["data"] = $data;
                        } else {
                            $responseBody["message"] = "Database update error";
                        }

                    } else {

                        // had invalid fields
                        $responseBody["message"] = "Some fields are invalid";
                        $responseBody["data"] = array(
                            "values" => $data,
                            "invalid" => $invalid
                        );

                    }

                } // else parameters are missing - default message
                
            } else {
                
                // not found
                $responseBody["message"] = ucwords(str_camel_case_to_words(UserProfileController::$genericName)). " not found";
                
            }
            
        } else {

            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;

        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(UserProfileController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    /*function _delete($params, $request, $response) {
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $generics = DS::query("SELECT
                id,
                user_id,
                buyer_level,
                curator_level,
                artist_level,
                type_id,
                bio,
                hearts,
                update_date,
                create_date
            FROM user_profiles
            WHERE id = ?i", intval($params[1]));
        
        // does the user have the correct scope
        // can only be deleted by owner and admin
        if(count(array_intersect(["webmaster","administrator"], $scopes)) || 
                ( count($generics) && $generics[0]["id"] === $user_id ) ) {
            
            // found?
            if(count($generics)) {
                
                if(DS::delete(UserProfileController::$tableName, "WHERE id = ?i", $params[1]) !== null) {
                    $responseBody["message"] = ucwords(str_camel_case_to_words(UserProfileController::$genericName)). " deleted";
                    $responseBody["success"] = true;
                } else {
                    $responseBody["message"] = "Database delete error";
                }
                
            } else {
                // not found
                $responseBody["message"] = ucwords(str_camel_case_to_words(UserProfileController::$genericName)). " not found";
            }
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        return $response->withPayload($responseBody);
    }*/
    
    function _getOptions() {
        $idNumberTypeItems = array(
            "RSA ID" => array("RSA ID",true),
            "Passport Number" => array("Passport Number",true)
        );
        
        return array(
            "user_profile" => array(
                "GET" => array(
                    "title" => "List User Profiles",
                    "description" => "List resources.",
                    "request" => array(
                        "parameters" => array(
                            "limit" => array("Field" => "limit", "Type" => "int", "Null" => "yes", "Key" => "", "Default" => 25, "Extra" => "", "Description" => "Limit the number of items returned (default: 25)."),
                            "page" => array("Field" => "limit", "Type" => "int", "Null" => "yes", "Key" => "", "Default" => 0, "Extra" => "", "Description" => "Page through the returned items (default: 0)."),
                            "filter" => array("Field" => "limit", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Description" => "Filter the returned items ({field_name:value} or {value} for searching in all fields).")
                        ),
                        "body" => array()
                    ),
                    "response" => array(
                        "list" => array(
                            array(
                                "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
                                "user_id" => array("Field" => "user_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "first_name" => array("Field" => "first_name", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "last_name" => array("Field" => "last_name", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "tel_number" => array("Field" => "tel_number", "Type" => "varchar(12)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "cel_number" => array("Field" => "cel_number", "Type" => "varchar(12)", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                                "id_number" => array("Field" => "id_number", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "id_number_type" => array("Field" => "id_number_type", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "profile_image_id" => array("Field" => "profile_image_id", "Type" => "file_id", "Label" => "Logo Image", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                                "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                            )
                        ),
                        "limit" => null,
                        "page" => null,
                        "count" => null,
                        "total_filtered" => null,
                        "total" => null
                    )
                )/*,
                "POST" => array(
                    "description" => "Create a resource.",
                    "request" => array(
                        "parameters" => null,
                        "body" => array(
                            "type_id" => array("Field" => "type_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => 0, "Extra" => ""),
                            "bio" => array("Field" => "bio", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => "", "Extra" => "")
                        )
                    ),
                    "response" => array(
                        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
                        "user_id" => array("Field" => "user_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "buyer_level" => array("Field" => "buyer_level", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => 0, "Extra" => ""),
                        "curator_level" => array("Field" => "curator_level", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => 0, "Extra" => ""),
                        "artist_level" => array("Field" => "artist_level", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => 0, "Extra" => ""),
                        "type_id" => array("Field" => "type_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => 0, "Extra" => ""),
                        "first_name" => array("Field" => "first_name", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "last_name" => array("Field" => "last_name", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "tel_number" => array("Field" => "tel_number", "Type" => "varchar(12)", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "cel_number" => array("Field" => "cel_number", "Type" => "varchar(12)", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "bio" => array("Field" => "bio", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => "", "Extra" => ""),
                        "hearts" => array("Field" => "hearts", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => 0, "Extra" => ""),
                        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                    )
                ),*/
            ),
            "user_profile/{id}" => array(
                "GET" => array(
                    "title" => "Get User Profile",
                    "description" => "Get a resource.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array()
                    ),
                    "response" => array(
                        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
                        "user_id" => array("Field" => "user_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "first_name" => array("Field" => "first_name", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "last_name" => array("Field" => "last_name", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "tel_number" => array("Field" => "tel_number", "Type" => "varchar(12)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "cel_number" => array("Field" => "cel_number", "Type" => "varchar(12)", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "id_number" => array("Field" => "id_number", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "id_number_type" => array("Field" => "id_number_type", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "profile_image_id" => array("Field" => "profile_image_id", "Type" => "file_id", "Label" => "Logo Image", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                    )
                ),
                "PUT" => array(
                    "title" => "Update User Profile",
                    "description" => "Update a resource. Allows sparse payloads.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array(
                            "first_name" => array("Field" => "first_name", "Type" => "varchar(128)", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
                            "last_name" => array("Field" => "last_name", "Type" => "varchar(128)", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
                            "tel_number" => array("Field" => "tel_number", "Type" => "varchar(12)", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
                            "cel_number" => array("Field" => "cel_number", "Type" => "varchar(12)", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                            "id_number" => array("Field" => "id_number", "Type" => "varchar(128)", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
                            "id_number_type" => array("Field" => "id_number_type", "Type" => "select", "Null" => "no", "Key" => "", "Default" => "RSA ID", "Extra" => "", "Items" => $idNumberTypeItems),
                            "profile_image_id" => array("Field" => "profile_image_id", "Type" => "file_id", "Label" => "Logo Image", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        )
                    ),
                    "response" => array(
                        "first_name" => array("Field" => "first_name", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "last_name" => array("Field" => "last_name", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "tel_number" => array("Field" => "tel_number", "Type" => "varchar(12)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "cel_number" => array("Field" => "cel_number", "Type" => "varchar(12)", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "id_number" => array("Field" => "id_number", "Type" => "varchar(128)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "id_number_type" => array("Field" => "id_number_type", "Type" => "select", "Null" => "NO", "Key" => "", "Default" => "RSA ID", "Extra" => ""),
                        "profile_image_id" => array("Field" => "profile_image_id", "Type" => "file_id", "Label" => "Logo Image", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                    )
                )/*,
                "DELETE" => array(
                    "description" => "Permanently remove a resource from the database.",
                    "request" => array(
                        "parameters" => null,
                        "body" => array()
                    ),
                    "response" => array()
                )*/
            )
        );
    }
    
    function _options($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $options = $this->_getOptions();
        
        $responseBody["message"] = ucwords(str_camel_case_to_words(UserProfileController::$genericName)). " options";
        $responseBody["success"] = true;
        $responseBody["data"] = $options;

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _getOwnerId($generic) {
        return $generic["owner_id"]; // this generic does not keep an owner id
    }
    
    public function install() {
        $tables = DS::list_tables();
        
        // making use of user_profiles, created by the AuthController 
        
        if(array_search('user_profiles',$tables)===false) {
            // generate the create table query
            $query = "CREATE TABLE `user_profiles` (
                        `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                        `user_id` int(10) UNSIGNED NOT NULL,
                        `first_name` varchar(128) DEFAULT NULL,
                        `last_name` varchar(128) DEFAULT NULL,
                        `tel_number` varchar(12) DEFAULT NULL,
                        `cel_number` varchar(12) DEFAULT NULL,
                        `id_number` varchar(128) DEFAULT NULL,
                        `id_number_type` varchar(128) DEFAULT 'RSA ID',
                        `profile_image_id` int(10) DEFAULT NULL,
                        `update_date` datetime DEFAULT NULL,
                        `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (`id`),
                        UNIQUE `user_id` (`user_id`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            if(DS::query($query)) {
                //message_add("The users table has been created.");
            }
        }
        
        $pages = array();
        /*
        * user_profile/listing
        */

        // add user page permissions including core admin
        $pages[] = array(
            'name'=>'getAll',
            'category'=>'user_profile',
            'subcat'=>'pages',
            'ptype'=>2,
            'pview'=>'webmaster,administrator');
        
        foreach($pages as $key=>$fieldData) {
            Permissions::set($fieldData);
        }

        // add user content permissions
        $content = array();
        $content[] = array(
            'name'=>'user_profile',
            'category'=>'user_profile',
            'subcat'=>'content',
            'ptype'=>0,
            'pview'=>'own',
            'pedit'=>'own',
            'padd'=>'system',
            'pdel'=>'system');
        foreach($content as $key=>$fieldData) {
            Permissions::set($fieldData);
        }

        // add user field permissions
        /*$fields = array();
        $fields[] = array(
            'name'=>'active',
            'category'=>'user',
            'subcat'=>'fields',
            'ptype'=>1,
            'pview'=>'webmaster',
            'pedit'=>'webmaster');
        foreach($fields as $key=>$fieldData) {
            Permissions::set($fieldData);
        }*/
    }
    
    public function update($from_version) {
        
        try {
            
            if($from_version < 1) {
                DS::insert("user_profiles", array(
                    "user_id" => 1
                ));

                $from_version ++;
            }
            
        } catch (Exception $e) {
            print $e->getMessage();
        }
        
        return $from_version;
    }
}