<?php
use League\OAuth2\Server\Exception\OAuthServerException;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Psr\Http\Message\ServerRequestInterface;

class ClientController extends Controller {
    static public $version = 1;
    static public $genericName = "client";
    static public $tableName = "oauth2_clients";
    
    function __construct($route_name) {
        parent::__construct($route_name);
        
        $this->permissions = Permissions::get(ClientController::$genericName);
    }
    
    function view($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {
            
            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response);
            } else {
                $request = $server->validateAuthenticatedRequest($request);
            }

            if($method === "GET") {
                if(!isset($params[1])) {
                    $response = $this->_getAll($params, $request, $response);
                } else {
                    $response = $this->_getSpecific($params, $request, $response);
                }
            }

            if(strtoupper($method) === "POST") {
                $response = $this->_add($params, $request, $response);
            }

            if(strtoupper($method) === "PUT" && isset($params[1])) {
                $response = $this->_update($params, $request, $response);
            }

            if(strtoupper($method) === "DELETE" && isset($params[1])) {
                $response = $this->_delete($params, $request, $response);
            }
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // log response
        // TODO: log needs to be moved into the individual endpoints so we can discern success
        LogController::log(ClientController::$genericName,$method,0,0,true,json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    function _getAll($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // does the user have the correct scope
        if(count(array_intersect(["authenticated"], $scopes))) {
            
            $options = $this->_getOptions($user_id, $scopes);
            $fields = $options[ClientController::$genericName]["GET"]["response"]["list"][0];

            // automatically page the loaded items
            $limit_parameter = filter_input(INPUT_GET, "limit");
            $page_parameter = filter_input(INPUT_GET, "page");
            $limit = $limit_parameter == null ? 25 : intval($limit_parameter);
            $page = $page_parameter == null ? 0 : intval($page_parameter);
            $limitString = " LIMIT ".($page*($limit)).", $limit";
            
            // add filter
            $filterFields = array_keys($fields);
            $passedFilter = filter_input(INPUT_GET, "filter");
            $filterString = "(1=1)";
            if($passedFilter) {
                $filterString = "";
                if(stripos($passedFilter,"+") !== false) {
                    // multiple filters
                    $filters = explode("+",$passedFilter);
                } else {
                    $filters = [$passedFilter];
                }

                foreach($filters as $filterGroup) {
                    $filterSubParts = explode(" ",$filterGroup);
                    
                    foreach($filterSubParts as $filter) {
                        $filterPart = "";

                        if(stripos($filter,"|")!==false) { // exact match
                            // filter on a specific column
                            $columnFilter = explode("|",$filter);
                            $filterVal = DS::escape("{$columnFilter[1]}");
                            $filterPart = ($filterPart !== "" ? " OR " : ""). "{$columnFilter[0]} = $filterVal";

                        } else if(stripos($filter,":")!==false) { // LIKE
                            // filter on a specific column
                            $columnFilter = explode(":",$filter);
                            $filterVal = DS::escape("%{$columnFilter[1]}%");
                            $filterPart = ($filterPart !== "" ? " OR " : ""). "{$columnFilter[0]} LIKE $filterVal";

                        } else {
                            // filter on all columns
                            $filterVal = DS::escape("%$filter%");
                            foreach($filterFields as $field) {
                                $filterPart.= ($filterPart !== "" ? " OR " : ""). ''.$field. " LIKE $filterVal";
                            }

                        }

                        $filterString.= ($filterString !== "" ? " OR " : ""). "($filterPart)";
                    }
                }

                $filterString = "($filterString)";
            }
            
            $isAdmin = intval(count(array_intersect(["administrator","webmaster"], $scopes)) > 0);

            $totalRead = DS::query("
                SELECT
                    COUNT(identifier) as total
                FROM oauth2_clients
                WHERE (owner_id = ?i OR 1 = ?i)", $user_id, $isAdmin);

            $totalFiltered = DS::query("
                SELECT
                    COUNT(identifier) as total
                FROM oauth2_clients
                WHERE $filterString AND (owner_id = ?i OR 1 = ?i)", $user_id, $isAdmin);

            $generics = DS::query("SELECT
                    ".implode(", ",array_keys($fields))."
                FROM oauth2_clients
                WHERE $filterString AND (owner_id = ?i OR 1 = ?i)
                ORDER BY create_date DESC".$limitString, $user_id, $isAdmin);

            $responseBody["success"] = true;
            $responseBody["message"] = ucwords(str_camel_case_to_words(ClientController::$genericName)). " list";
            $responseBody["data"] = array(
                "list" => $generics,
                "limit" => $limit,
                "page" => $page,
                "count" => count($generics),
                "total_filtered" => intval($totalFiltered[0]["total"]),
                "total" => intval($totalRead[0]["total"])
            );
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _getSpecific($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if(count(array_intersect(["authenticated"], $scopes))) {
            
            $options = $this->_getOptions($user_id, $scopes);
            $fields = $options["".ClientController::$genericName."/{id}"]["GET"]["response"];
            
            $isAdmin = intval(count(array_intersect(["administrator","webmaster"], $scopes)) > 0);
            
            $generics = DS::query("SELECT
                    ".implode(",", array_keys($fields))."
                FROM oauth2_clients
                WHERE (id = ?i || identifier = ?s) AND (owner_id = ?i OR 1 = ?i)", intval($params[1]), $params[1], $user_id, $isAdmin);
            
            // does the user have the correct scope
            if(count($generics)) {
                
                // found
                $responseBody["success"] = true;
                $responseBody["message"] = ucwords(str_camel_case_to_words(ClientController::$genericName)). " loaded";
                $responseBody["data"] = $generics[0];
                
            } else {
                
                $responseBody["message"] = ucwords(str_camel_case_to_words(ClientController::$genericName)). " not found";
                
            }
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _add($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if(count(array_intersect(["authenticated"], $scopes))) {
            
            // get all fields from request body
            $requestBody = file_get_contents("php://input");
            $requestObject = json_decode($requestBody, true);

            $options = $this->_getOptions();
            $fields = $options[ClientController::$genericName]["POST"]["request"]["body"];

            // filter away bad fields
            $data = array();
            foreach(array_keys($fields) as $field) {
                if(isset($requestObject[$field])) {
                    $data[$field] = $requestObject[$field];
                }
            }

            // still have data?
            if(count($data)) {
                
                if($data["identifier"] == "") {
                    $data["identifier"] = $data["name"];
                }

                $data["identifier"] = str_replace("-","_",str_slugify($data["identifier"]));

                if($data["is_confidential"] == 1 && $data["secret"] == "") {
                    $data["secret"] = str_guid();
                }

                // some fields need to meet certain criteria - validate here
                $invalid = Forms::validateData($fields, $data);
                
                // valid?
                if(!count($invalid)) {
                    
                    $isAdmin = intval(count(array_intersect(["administrator","webmaster"], $scopes)) > 0);
                    
                    if(!$isAdmin || !isset($data["owner_id"])) {
                        $data["owner_id"] = $user_id;
                    }

                    if(count($generic = DS::insert(ClientController::$tableName, $data))) {
                        
                        $responseBody["message"] = ucwords(str_camel_case_to_words(ClientController::$genericName)). " added";
                        $responseBody["success"] = true;
                        $responseBody["data"] = $generic[0];
                        
                    } else {
                        $responseBody["message"] = "Database insert error";
                    }

                } else {
                    
                    // had invalid fields
                    $responseBody["message"] = "Some fields are invalid";
                    $responseBody["data"] = array(
                        "values" => $data,
                        "invalid" => $invalid
                    );

                }
                
            } // else parameters are missing - default message
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _update($params, $request, $response) {
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        if(count(array_intersect(["authenticated"], $scopes))) {
            
            $isAdmin = intval(count(array_intersect(["administrator","webmaster"], $scopes)) > 0);
            
            $generics = DS::query("SELECT
                    *
                FROM oauth2_clients
                WHERE (id = ?i || identifier = ?s) AND (owner_id = ?i OR 1 = ?i)", intval($params[1]), $params[1], $user_id, $isAdmin);
            
            // found?
            if(count($generics)) {
                
                $requestBody = file_get_contents("php://input");
                $requestObject = json_decode($requestBody, true);
                
                $options = $this->_getOptions();
                $fields = $options[ClientController::$genericName."/{id}"]["PUT"]["request"]["body"];
                
                // filter away bad fields
                $data = array();
                $passedFields = array();
                foreach(array_keys($fields) as $field) {
                    if(isset($requestObject[$field])) {
                        $passedFields[$field] = $fields[$field];
                        $data[$field] = $requestObject[$field];
                    }
                }

                // still have data?
                if(count($data)) {
                    
                    if(isset($data["identifier"])) {
                        if($data["identifier"] == "") {
                            $data["identifier"] = isset($data["name"]) ? $data["name"] : $generics[0]["name"];
                        }

                        $data["identifier"] = str_replace("-","_",str_slugify($data["identifier"]));
                    }
                    
                    if(isset($data["is_confidential"])) {
                        if($data["is_confidential"] == 1 && $generics[0]["secret"] == "") {
                            $data["secret"] = str_guid();
                        }
                    }

                    // some fields need to meet certain criteria - validate here
                    $invalid = Forms::validateData($passedFields, $data, intval($params[1]));

                    // valid?
                    if(!count($invalid)) {
                        
                        if($data["secret"] == "") {
                            unset($data["secret"]);
                        }
                        
                        if(isset($data["secret"])) {
                            $data["secret"] = password_hash($data["secret"], PASSWORD_BCRYPT);
                        }

                        if(DS::update(ClientController::$tableName, $data, "WHERE (id = ?i || identifier = ?s)", intval($params[1]), $params[1]) !== null) {
                            
                            if(intval($generics[0]["is_confidential"]) !== intval($data["is_confidential"])) {
                                DS::delete("oauth2_access_tokens", "WHERE client_id = ?s", $generics[0]["identifier"]);
                            }

                            $responseBody["message"] = ucwords(str_camel_case_to_words(ClientController::$genericName)). " updated";
                            $responseBody["success"] = true;
                            $responseBody["data"] = $data;
                            
                        } else {
                            
                            $responseBody["message"] = "Database update error";
                            
                        }

                    } else {

                        // had invalid fields
                        $responseBody["message"] = "Some fields are invalid";
                        $responseBody["data"] = array(
                            "values" => $data,
                            "invalid" => $invalid
                        );

                    }

                } // else parameters are missing - default message
                
            } else {
                
                // not found
                $responseBody["message"] = ucwords(str_camel_case_to_words(ClientController::$genericName)). " not found";
            }
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _delete($params, $request, $response) {
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $isAdmin = intval(count(array_intersect(["administrator","webmaster"], $scopes)) > 0);
        
        $generics = DS::query("SELECT
                *
            FROM oauth2_clients
            WHERE (id = ?i || identifier = ?s) AND (owner_id = ?i OR 1 = ?i)", intval($params[1]), $params[1], $user_id, $isAdmin);
        
        // does the user have the correct scope
        if(count(array_intersect(["authenticated"], $scopes))) {
            
            // found?
            if(count($generics)) {
                
                if(DS::delete(ClientController::$tableName, "WHERE (id = ?i || identifier = ?s)", intval($params[1]), $params[1]) !== null) {
                    
                    $responseBody["message"] = ucwords(str_camel_case_to_words(ClientController::$genericName)). " deleted";
                    $responseBody["success"] = true;
                } else {
                    $responseBody["message"] = "Database delete error";
                }
                
            } else {
                // not found
                $responseBody["message"] = ucwords(str_camel_case_to_words(ClientController::$genericName)). " not found";
            }
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _getOptions() {
        return array(
            ClientController::$genericName => array(
                "GET" => array(
                    "title" => "List Clients",
                    "description" => "List resources.",
                    "request" => array(
                        "parameters" => array(
                            "limit" => array("Field" => "limit", "Type" => "int", "Null" => "yes", "Key" => "", "Default" => 25, "Extra" => "", "Description" => "Limit the number of items returned (default: 25)."),
                            "page" => array("Field" => "limit", "Type" => "int", "Null" => "yes", "Key" => "", "Default" => 0, "Extra" => "", "Description" => "Page through the returned items (default: 0)."),
                            "filter" => array("Field" => "limit", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Description" => "Filter the returned items ({field_name:value} or {value} for searching in all fields).")
                        ),
                        "body" => array()
                    ),
                    "response" => array(
                        "list" => array(
                            array(
                                "id" => array("Field" => "id", "Type" => "int", "Null" => "NO", "Key" => "pri", "Default" => null, "Extra" => "auto_increment"),
                                "name" => array("Field" => "name", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "identifier" => array("Field" => "identifier", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "owner_id" => array("Field" => "owner_id", "Type" => "INT", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "redirect_uri" => array("Field" => "redirect_uri", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "is_confidential" => array("Field" => "is_confidential", "Type" => "TINYINT", "Null" => "YES", "Key" => "", "Default" => 1, "Extra" => ""),
                                "is_active" => array("Field" => "is_active", "Type" => "TINYINT", "Null" => "YES", "Key" => "", "Default" => 1, "Extra" => ""),
                                "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                                "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                            )
                        ),
                        "limit" => null,
                        "page" => null,
                        "count" => null,
                        "total_filtered" => null,
                        "total" => null
                    )
                ),
                "POST" => array(
                    "title" => "Create Client",
                    "description" => "Create a resource.",
                    "request" => array(
                        "parameters" => null,
                        "body" => array(
                            "name" => array("Field" => "name", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                            "identifier" => array("Field" => "identifier", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                            "secret" => array("Field" => "secret", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => str_guid(), "Extra" => ""),
                            "redirect_uri" => array("Field" => "redirect_uri", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                            "is_confidential" => array("Field" => "is_confidential", "Type" => "TINYINT", "Null" => "YES", "Key" => "", "Default" => 1, "Extra" => ""),
                            "is_active" => array("Field" => "is_active", "Type" => "TINYINT", "Null" => "YES", "Key" => "", "Default" => 1, "Extra" => ""),
                        )
                    ),
                    "response" => array(
                        "name" => array("Field" => "name", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "identifier" => array("Field" => "identifier", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "owner_id" => array("Field" => "owner_id", "Type" => "INT", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "redirect_uri" => array("Field" => "redirect_uri", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "is_confidential" => array("Field" => "is_confidential", "Type" => "TINYINT", "Null" => "YES", "Key" => "", "Default" => 1, "Extra" => ""),
                        "is_active" => array("Field" => "is_active", "Type" => "TINYINT", "Null" => "YES", "Key" => "", "Default" => 1, "Extra" => ""),
                        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                    )
                ),
            ),
            ClientController::$genericName."/{id}" => array(
                "GET" => array(
                    "title" => "Get Client",
                    "description" => "Get a resource.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array()
                    ),
                    "response" => array(
                        "name" => array("Field" => "name", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "identifier" => array("Field" => "identifier", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "owner_id" => array("Field" => "owner_id", "Type" => "INT", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "redirect_uri" => array("Field" => "redirect_uri", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "is_confidential" => array("Field" => "is_confidential", "Type" => "TINYINT", "Null" => "YES", "Key" => "", "Default" => 1, "Extra" => ""),
                        "is_active" => array("Field" => "is_active", "Type" => "TINYINT", "Null" => "YES", "Key" => "", "Default" => 1, "Extra" => ""),
                        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                    )
                ),
                "PUT" => array(
                    "title" => "Update Client",
                    "description" => "Update a resource. Allows sparse payloads.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array(
                            "name" => array("Field" => "name", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                            "identifier" => array("Field" => "identifier", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                            "secret" => array("Field" => "secret", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => str_guid(), "Extra" => ""),
                            "redirect_uri" => array("Field" => "redirect_uri", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                            "is_confidential" => array("Field" => "is_confidential", "Type" => "TINYINT", "Null" => "YES", "Key" => "", "Default" => 1, "Extra" => ""),
                            "is_active" => array("Field" => "is_active", "Type" => "TINYINT", "Null" => "YES", "Key" => "", "Default" => 1, "Extra" => ""),
                        )
                    ),
                    "response" => array(
                        "name" => array("Field" => "name", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "identifier" => array("Field" => "identifier", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "redirect_uri" => array("Field" => "redirect_uri", "Type" => "VARCHAR", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "is_confidential" => array("Field" => "is_confidential", "Type" => "TINYINT", "Null" => "YES", "Key" => "", "Default" => 1, "Extra" => ""),
                        "is_active" => array("Field" => "is_active", "Type" => "TINYINT", "Null" => "YES", "Key" => "", "Default" => 1, "Extra" => ""),
                    )
                ),
                "DELETE" => array(
                    "title" => "Delete Client",
                    "description" => "Permanently remove a resource from the database.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array()
                    ),
                    "response" => array()
                )
            )
        );
    }
    
    function _options($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $options = $this->_getOptions();
        
        $responseBody["message"] = ucwords(str_camel_case_to_words(ClientController::$genericName)). " options";
        $responseBody["success"] = true;
        $responseBody["data"] = $options;

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _getOwnerId($generic) {
        return $generic["owner_id"]; // this generic does not keep an owner id
    }
    
    public function install() {
        // making use of oauth2_clients, created by the AuthController
        
        $pages = array();
        /*
        * client/listing
        */

        // add client page permissions including core admin
        $pages[] = array(
            'name'=>'getAll',
            'category'=>'client',
            'subcat'=>'pages',
            'ptype'=>2,
            'pview'=>'authenticated');
        
        foreach($pages as $key=>$fieldData) {
            Permissions::set($fieldData);
        }

        // add client content permissions
        $content = array();
        $content[] = array(
            'name'=>'client',
            'category'=>'client',
            'subcat'=>'content',
            'ptype'=>0,
            'pview'=>'own',
            'pedit'=>'own',
            'padd'=>'authenticated',
            'pdel'=>'own');
        foreach($content as $key=>$fieldData) {
            Permissions::set($fieldData);
        }

        // add client field permissions
        $fields = array(
            array( 'name'=>'is_confidential', 'category'=>'client', 'subcat'=>'fields', 'ptype'=>1, 'pview'=>'webmaster,administrator', 'pedit'=>'webmaster,administrator' )
        );
        foreach($fields as $key=>$fieldData) {
            Permissions::set($fieldData);
        }
        
        if(($adminMenu = MenuController::getItem("Admin")) === null) {
            $adminMenu = MenuController::addItem("Admin", null, null, "", 0, ["administrator","webmaster"]);
        }
        if(($accessControlMenu = MenuController::getItem("Access Control")) === null) {
            $accessControlMenu = MenuController::addItem("Access Control", null, null, "", 2, ["administrator","webmaster"], $adminMenu["id"]);
        }
        MenuController::addItem("<i class='fa fa-server'></i> API Clients", null, null, "client", 1, ["webmaster"], $accessControlMenu["id"]);
    }
    
    public function update($from_version) {
        
        if($from_version < 1) {
            
            try {
                DS::insert("oauth2_clients", array(
                    "identifier" => API_DEFAULT_CLIENT_ID,
                    "owner_id" => 1,
                    "secret" => "",
                    "name" => VariableController::_getItemValue("Settings", "SITE_NAME_SHORT"). " Web",
                    "redirect_uri" => UI_URL,
                    "is_confidential" => false,
                    "is_active" => true));
                
                $from_version ++;
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
        
        return $from_version;
    }
}