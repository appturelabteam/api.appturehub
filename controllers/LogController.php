<?php
use League\OAuth2\Server\Exception\OAuthServerException;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;

class LogController extends Controller {
    static public $version = 2;
    static public $genericName = "log";
    static public $tableName = "logs";
    
    function __construct($route_name) {
        parent::__construct($route_name);
        
        //$this->permissions = Permissions::get(LogController::$genericName);
    }
    
    function view($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {
            $request = $server->validateAuthenticatedRequest($request);
            
            if($method === "GET") {
                if(!isset($params[1])) {
                    $response = $this->_getAll($params, $request, $response, $server);
                } else {
                    if($params[1] === 'visitors_per_day') {
                        $response = $this->_getVisitorsPerDay($params, $request, $response, $server);
                    } else if($params[1] === 'views_per_day') {
                        $response = $this->_getViewsPerDay($params, $request, $response, $server);
                    }
                }
            }

            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response, $server);
            }
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    function _getAll($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if(count(array_intersect(["webmaster","administrator"],$scopes)) > 0) {
            
            $options = $this->_getOptions($user_id, $scopes);
            $fields = $options[LogController::$genericName]["GET"]["response"]["list"][0];
            
            // automatically page the loaded items
            $limit_parameter = filter_input(INPUT_GET, "limit");
            $page_parameter = filter_input(INPUT_GET, "page");
            $limit = $limit_parameter === null ? 25 : intval($limit_parameter);
            $page = $page_parameter == null ? 0 : intval($page_parameter);
            $limitString = ($limit > 0 ? " LIMIT ".($page*($limit)).", $limit" : "");
            
            // add filter
            $filterFields = array_keys($fields);
            $passedFilter = filter_input(INPUT_GET, "filter");
            $filterString = "(1=1)";
            if($passedFilter) {
                $filterString = "";
                if(stripos($passedFilter,"+") !== false) {
                    // multiple filters
                    $filters = explode("+",$passedFilter);
                } else {
                    $filters = [$passedFilter];
                }

                foreach($filters as $filterGroup) {
                    $filterSubParts = explode(" ",$filterGroup);
                    
                    foreach($filterSubParts as $filter) {
                        $filterPart = "";

                        if(stripos($filter,"|")!==false) { // exact match
                            // filter on a specific column
                            $columnFilter = explode("|",$filter);
                            $filterVal = DS::escape("{$columnFilter[1]}");
                            $filterPart = ($filterPart !== "" ? " OR " : ""). "{$columnFilter[0]} = $filterVal";

                        } else if(stripos($filter,":")!==false) { // LIKE
                            // filter on a specific column
                            $columnFilter = explode(":",$filter);
                            $filterVal = DS::escape("%{$columnFilter[1]}%");
                            $filterPart = ($filterPart !== "" ? " OR " : ""). "{$columnFilter[0]} LIKE $filterVal";

                        } else {
                            // filter on all columns
                            $filterVal = DS::escape("%$filter%");
                            foreach($filterFields as $field) {
                                $filterPart.= ($filterPart !== "" ? " OR " : ""). ''.$field. " LIKE $filterVal";
                            }

                        }

                        $filterString.= ($filterString !== "" ? " OR " : ""). "($filterPart)";
                    }
                }

                $filterString = "($filterString)";
            }
            
            $totalRead = DS::query("
                SELECT
                    COUNT(id) as total
                FROM ".LogController::$tableName."");
            
            $totalFiltered = DS::query("
                SELECT
                    COUNT(id) as total
                FROM ".LogController::$tableName."
                WHERE $filterString");
            
            $generics = DS::query("
                SELECT
                    ".implode(", ",array_keys($fields))."
                FROM ".LogController::$tableName."
                WHERE $filterString
                ORDER BY create_date DESC
                $limitString");
            
            $responseBody["success"] = true;
            $responseBody["message"] = ucwords(str_camel_case_to_words(LogController::$genericName)). " list";
            $responseBody["data"] = array(
                "list" => $generics,
                "limit" => $limit,
                "page" => $page,
                "count" => count($generics),
                "total_filtered" => intval($totalFiltered[0]["total"]),
                "total" => intval($totalRead[0]["total"])
            );
            
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(LogController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getVisitorsPerDay($params, $request, $response, $server) {
        
        // SELECT COUNT(id) as visits, log_type, create_date FROM `logs` WHERE log_type <> 'file' AND action = 'GET' and item_id <> 0 GROUP BY log_type, DAY(create_date) ORDER BY create_date DESC 
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if(count(array_intersect(["webmaster","administrator"],$scopes)) > 0) {
            
            //$options = $this->_getOptions($user_id, $scopes);
            //$fields = $options[LogController::$genericName]["GET"]["response"]["list"][0];
            
            // automatically page the loaded items
            $fromDate = filter_input(INPUT_GET, "from");
            $toDate = filter_input(INPUT_GET, "to");
            
            $generics = DS::query("
                SELECT
                    DATE(create_date) as date,
                    COUNT(DISTINCT user_id, ip_address, browser) as visitors
                FROM `logs`
                WHERE
                        log_type <> 'file'
                    AND action = 'GET'
                    AND item_id <> 0
                    AND DATE(create_date) >= ?s
                    AND DATE(create_date) <= ?s
                GROUP BY DATE(create_date)
                ORDER BY create_date DESC", $fromDate, $toDate);
            
            $responseBody["success"] = true;
            $responseBody["message"] = ucwords(str_camel_case_to_words(LogController::$genericName)). " list";
            $responseBody["data"] = array(
                "list" => $generics,
                "total" => count($generics)
            );
            
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(LogController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getViewsPerDay($params, $request, $response, $server) {
        
        // SELECT COUNT(id) as visits, log_type, create_date FROM `logs` WHERE log_type <> 'file' AND action = 'GET' and item_id <> 0 GROUP BY log_type, DAY(create_date) ORDER BY create_date DESC 
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if(count(array_intersect(["webmaster","administrator"],$scopes)) > 0) {
            
            //$options = $this->_getOptions($user_id, $scopes);
            //$fields = $options[LogController::$genericName]["GET"]["response"]["list"][0];
            
            // automatically page the loaded items
            $fromDate = filter_input(INPUT_GET, "from");
            $toDate = filter_input(INPUT_GET, "to");
            
            $generics = DS::query("
                SELECT
                    DATE(create_date) as date,
                    COUNT(id) as views,
                    log_type as type
                FROM `logs`
                WHERE
                        log_type <> 'file'
                    AND action = 'GET'
                    AND item_id <> 0
                    AND DATE(create_date) >= ?s
                    AND DATE(create_date) <= ?s
                GROUP BY DATE(create_date)
                ORDER BY create_date DESC", $fromDate, $toDate);
            
            $responseBody["success"] = true;
            $responseBody["message"] = ucwords(str_camel_case_to_words(LogController::$genericName)). " list";
            $responseBody["data"] = array(
                "list" => $generics,
                "total" => count($generics)
            );
            
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(LogController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    /**
     * 
     * @param type $LogType generic name or textual identifier
     * @param type $Action form method or action - optional
     * @param type $ItemId generic item id - optional
     * @param type $UserId user responsible
     * @param type $success
     * @param type $Description more info - optional
     * @param type $Url optional
     * @return type
     */
    static public function log($LogType,$Action,$ItemId,$UserId,$success,$Description,$Url) {
        $ip_address = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "NA";
        $browser = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "NA";
        
        $log = array(
            "log_type" => $LogType,
            "action" => $Action,
            "item_id" => intval($ItemId),
            "user_id" => intval($UserId),
            "success" => $success,
            "browser" => "",//$browser,
            "ip_address" => $ip_address,
            "description" => $Description,
            "url" => $Url
        );
        //print DS::gen_insert("logs",$log);
        if($result = DS::insert("logs",$log)) {
            return $result[0];
        }
        return null;
    }
    
    function _getOptions($user_id, $scopes, $owner_id = null) {
        $options = array(
            "log" => array(
                "GET" => array(
                    "description" => "List resources.",
                    "request" => array(
                        "parameters" => array(
                            "limit" => "Limit the number of items returned (default: 25).",
                            "page" => "Page through the returned items (default: 0).",
                            "filter" => "Filter the returned items."
                        ),
                        "body" => array()
                    ),
                    "response" => array(
                        "list" => array(
                            array(
                                "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
                                "log_type" => array("Field" => "log_type", "Type" => "varchar", "Null" => "NO", "Default" => null, "Extra" => ""),
                                "action" => array("Field" => "action", "Type" => "varchar", "Null" => "NO", "Default" => null, "Extra" => ""),
                                "item_id" => array("Field" => "item_id", "Type" => "varchar", "Null" => "yes", "Default" => null, "Extra" => ""),
                                "user_id" => array("Field" => "user_id", "Type" => "varchar", "Null" => "no", "Default" => null, "Extra" => ""),
                                "success" => array("Field" => "success", "Type" => "tinyint", "Null" => "no", "Default" => 0, "Extra" => ""),
                                "description" => array("Field" => "description", "Type" => "text", "Null" => "yes", "Default" => null, "Extra" => ""),
                                "browser" => array("Field" => "browser", "Type" => "varchar", "Null" => "no", "Default" => null, "Extra" => ""),
                                "ip_address" => array("Field" => "ip_address", "Type" => "varchar", "Null" => "no", "Default" => null, "Extra" => ""),
                                "url" => array("Field" => "url", "Type" => "varchar", "Null" => "yes", "Default" => null, "Extra" => ""),
                                "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                            )
                        ),
                        "limit" => null,
                        "page" => null,
                        "count" => null,
                        "total_filtered" => null,
                        "total" => null
                    )
                )
            )
        );
        
        /*if(!count(array_intersect(["anonymous"], $scopes))) {
            // modify depending on ownership
            if($owner_id != null && $owner_id != $user_id) {
//
//                $options["order/{id}"]["PUT"]["request"]["body"] = array(
//                    "hearts" => array("Field" => "hearts", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => "0", "Extra" => "")
//                );
//                $options["order/{id}"]["PUT"]["response"] = array(
//                    "hearts" => array("Field" => "hearts", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => "0", "Extra" => "")
//                );
                
            }
        }*/
        
        return $options;
    }
    
    function _options($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        $options = $this->_getOptions($user_id, $scopes);
        
        $responseBody["message"] = ucwords(str_camel_case_to_words(LogController::$genericName)). " options";
        $responseBody["success"] = true;
        $responseBody["data"] = $options;
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        //LogController::log(LogController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    public function install() {
        //DS::query("DROP TABLE IF EXISTS jobs");
        $tables = DS::list_tables();
        
        // check if the logs table exists, if not create it.
        if(array_search('logs',$tables)===false) {
            // generate the create table query
            $query = "CREATE TABLE `logs` (
                        `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
                        `log_type` VARCHAR(45) NOT NULL,
                        `action` VARCHAR(45),
                        `item_id` varchar(32) NOT NULL,
                        `user_id` INTEGER UNSIGNED NOT NULL,
                        `success` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
                        `description` LONGTEXT,
                        `browser` VARCHAR(512) NOT NULL,
                        `ip_address` VARCHAR(128) NOT NULL,
                        `url` TEXT,
                        `create_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (`id`,`item_id`,`user_id`,`success`),
                        INDEX(`log_type`)
                      )
                      ENGINE = InnoDB DEFAULT CHARSET=utf8;";

            if(DS::query($query)) {
                //message_add("The system_log table has been created.");
            }
        }
        
        $pages = array();
        /*
        * system_log/%          // this is governed by the content permissions
        * system_log/add
        * system_log/listing
        */

        // add job page permissions including core admin
        $pages[] = array(
            'name'=>'getAll',
            'category'=>'system_log',
            'subcat'=>'pages',
            'ptype'=>2,
            'pview'=>'administrator');
        $pages[] = array(
            'name'=>'getVisitorsPerDay',
            'category'=>'system_log',
            'subcat'=>'pages',
            'ptype'=>2,
            'pview'=>'administrator');
        $pages[] = array(
            'name'=>'getViewsPerDay',
            'category'=>'system_log',
            'subcat'=>'pages',
            'ptype'=>2,
            'pview'=>'administrator');
        
        foreach($pages as $key=>$fieldData) {
            Permissions::set($fieldData);
        }

        // add job content permissions
        $content = array();
        $content[] = array(
            'name'=>'system_log',
            'category'=>'system_log',
            'subcat'=>'content',
            'ptype'=>0,
            'pview'=>'administrator',
            'pedit'=>'system',
            'padd'=>'system',
            'pdel'=>'system');
        foreach($content as $key=>$fieldData) {
            Permissions::set($fieldData);
        }

        // add job field permissions
        // there are no special permissions needed so leaving it for now
        /*$fields = array();
        $fields[] = array(
            'name'=>'roles',
            'category'=>'client',
            'subcat'=>'fields',
            'ptype'=>1,
            'pview'=>array(),
            'pedit'=>'admin');
        foreach($fields as $key=>$fieldData) {
            Permissions::set($fieldData);
        }*/
    }
    
    public function update($from_version) {
        
        if($from_version < 1) {
            
            try {
                $query = "ALTER TABLE `logs` ADD INDEX(`log_type`);";
                
                DS::query($query);
                
                $from_version ++;
                
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
        
        return $from_version;
    }
}