<?php
use League\OAuth2\Server\Exception\OAuthServerException;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Psr\Http\Message\ServerRequestInterface;

class VariableController extends Controller {
    static public $version = 5;
    static public $genericName = "variable";
    static public $tableName = "variables";
    
    static private $cache = array();
    
    static public $tableFields = array(
        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
        "author_id" => array("Field" => "author_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "var_group_name" => array("Field" => "var_group_name", "Label" => "Group Name", "Type" => "varchar(45)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "var_name" => array("Field" => "var_name", "Label" => "Name", "Type" => "varchar(45)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "var_value" => array("Field" => "var_value", "Label" => "Value", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "var_default_value" => array("Field" => "var_default_value", "Label" => "Default Value", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "var_type" => array("Field" => "var_type", "Label" => "Type", "Type" => "varchar(32)", "Null" => "no", "Key" => "", "Default" => "text", "Extra" => ""),
        "var_sequence" => array("Field" => "var_sequence", "Label" => "Sequence", "Type" => "int(10) unsigned", "Null" => "no", "Key" => "", "Default" => 0, "Extra" => ""),
        "required" => array("Field" => "required", "Label" => "Required", "Type" => "tinyint(1)", "Null" => "yes", "Key" => "", "Default" => 1, "Extra" => ""),
        "use_type" => array("Field" => "use_type", "Label" => "Usage", "Type" => "VARCHAR(12)", "Null" => "yes", "Key" => "", "Default" => 'system', "Extra" => ""),
        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
    );
    
    /**
     * 
     * @param type $params
     */
    function view($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {

            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response, $server);
            } else {
                $request = $server->validateAuthenticatedRequest($request);
            }
            
            if($method === "GET") {
                if(!isset($params[1])) {
                    $response = $this->_getAll($params, $request, $response);
                } else {
                    $response = $this->_getSpecific($params, $request, $response);
                }
            }

            // user profiles are created automatically on user registration
            if(strtoupper($method) === "POST") {
                $response = $this->_add($params, $request, $response);
            }

            if(strtoupper($method) === "PUT" && isset($params[1])) {
                $response = $this->_update($params, $request, $response);
            }

            // deleted with user accounts
            if(strtoupper($method) === "DELETE" && isset($params[1])) {
                $response = $this->_delete($params, $request, $response);
            }
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    /**
     * 
     * @param type $params
     * @param type $request
     * @param type $response
     * @return type
     */
    function _getAll($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // get fields for this endpoint from options
        $options = $this->_getOptions();
        $fields = $options[VariableController::$genericName]["GET"]["response"]["list"][0];

        // automatically page the loaded items
        $limit_parameter = filter_input(INPUT_GET, "limit");
        $page_parameter = filter_input(INPUT_GET, "page");
        $limit = $limit_parameter == null ? 25 : intval($limit_parameter);
        $page = $page_parameter == null ? 0 : intval($page_parameter);
        $limitString = ($limit > 0 ? " LIMIT ".($page*($limit)).", $limit" : "");
        
        // ordering, check validity etc and parse for query
        $orderParameter = filter_input(INPUT_GET, "order");
        $orderParameter = $orderParameter == null ? "create_date:DESC" : $orderParameter;
        $order_split = explode(":",$orderParameter);
        if(array_search($order_split[0], array_keys($fields)) === false || array_search($order_split[1], array("DESC","ASC")) === false) {
            $orderParameter = "create_date:DESC";
        }
        $order = implode(" ",explode(":",$orderParameter))." ";

        // add filter
        $filterFields = array_keys($fields);
        $filters = isset($_GET["filter"]) ? array_filter($_GET["filter"]) : array();
        $filtersAnd = isset($_GET["filterAnd"]) ? array_filter($_GET["filterAnd"]) : array();
        $filterString = DS::gen_filters($filterFields, $filters, "OR");
        $filterStringAnd = DS::gen_filters($filterFields, $filtersAnd, "AND");
        $filterString = "($filterString AND $filterStringAnd)";

        $totalRead = DS::query("
            SELECT
                COUNT(id) as total
            FROM ".VariableController::$tableName);

        $totalFiltered = DS::query("
            SELECT
                COUNT(id) as total
            FROM ".VariableController::$tableName."
            WHERE ".$filterString);

        $generics = DS::query("SELECT
                ".implode(", ", array_keys($fields))."
            FROM ".VariableController::$tableName."
            WHERE $filterString
            ORDER BY {$order} 
            $limitString");

        $responseBody["success"] = true;
        $responseBody["message"] = ucwords(str_camel_case_to_words(VariableController::$genericName)). " list";
        $responseBody["data"] = array(
            "filter" => $filters,
            "filterAnd" => $filtersAnd,
            "order" => $orderParameter,
            "list" => $generics,
            "limit" => $limit,
            "page" => $page,
            "count" => count($generics),
            "total_filtered" => intval($totalFiltered[0]["total"]),
            "total" => intval($totalRead[0]["total"])
        );
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(VariableController::$genericName,$request->getMethod(),0,$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getSpecific($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // get fields for this endpoint from options
        $options = $this->_getOptions();
        $fields = $options[VariableController::$genericName."/{id}"]["GET"]["response"];

        $generics = DS::query("SELECT
                ".implode(", ", array_keys($fields))."
            FROM ".VariableController::$tableName."
            WHERE id = ?i", intval($params[1]));

        if(count($generics)) {

            // found
            $responseBody["message"] = ucwords(str_camel_case_to_words(VariableController::$genericName)). " loaded";
            $responseBody["success"] = true;
            $responseBody["data"] = $generics[0];

        } else {

            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(VariableController::$genericName)). " not found";

        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(VariableController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _add($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        if(count(array_intersect(["administrator","webmaster"], $scopes))) {
            
            // get all fields from request body
            $requestBody = file_get_contents("php://input");
            $requestObject = json_decode($requestBody, true);
            
            // get fields for this endpoint from options
            $options = $this->_getOptions();
            $fields = $options[VariableController::$genericName]["POST"]["request"]["body"];
            
            // filter away bad fields
            $data = array();
            foreach(array_keys($fields) as $field) {
                if(isset($requestObject[$field])) {
                    $data[$field] = $requestObject[$field];
                }
            }

            // still have data?
            if(count($data)) {

                // some fields need to meet certain criteria - validate here
                $invalid = Forms::validateData($fields, $data);

                // valid?
                if(!count($invalid)) {
                    
                    $data["author_id"] = $user_id;

                    if(count($generic = DS::insert(VariableController::$tableName, $data))) {
                        
                        $responseBody["message"] = ucwords(str_camel_case_to_words(VariableController::$genericName)). " added";
                        $responseBody["success"] = true;
                        $responseBody["data"] = $generic[0];
                    } else {
                        $responseBody["message"] = "Database insert error";
                    }

                } else {
                    
                    // had invalid fields
                    $responseBody["message"] = "Some fields are invalid";
                    $responseBody["data"] = array(
                        "values" => $data,
                        "invalid" => $invalid
                    );

                }
            } // else parameters are missing - default message
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(VariableController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _update($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // get fields for update
        $options = $this->_getOptions();
        $fields = $options[VariableController::$genericName."/{id}"]["PUT"]["request"]["body"];
        
        $generics = DS::query("SELECT
                ".implode(", ", array_keys($fields))."
            FROM ".VariableController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // found?
        if(count($generics)) {
            
            // does the user have the correct scope
            if( count(array_intersect(["webmaster","administrator"], $scopes)) ) {

                $requestBody = file_get_contents("php://input");
                $requestObject = json_decode($requestBody, true);

                // filter away bad fields
                $data = array();
                $sparseFields = array();
                foreach(array_keys($fields) as $field) {
                    if(isset($requestObject[$field])) {
                        if(isset($requestObject[$field])) {
                            $data[$field] = $requestObject[$field];
                            $sparseFields[$field] = $fields[$field];
                        }
                    }
                }

                // still have data?
                if(count($data)) {

                    // some fields need to meet certain criteria - validate here
                    $invalid = Forms::validateData($sparseFields, $data, intval($params[1]));

                    // valid?
                    if(!count($invalid)) {

                        if(DS::update(VariableController::$tableName, $data, "WHERE id = ?i", $params[1]) !== null) {

                            $responseBody["message"] = ucwords(str_camel_case_to_words(VariableController::$genericName)). " updated";
                            $responseBody["success"] = true;
                            $responseBody["data"] = $data;
                        } else {
                            $responseBody["message"] = "Database update error";
                        }

                    } else {

                        // had invalid fields
                        $responseBody["message"] = "Some fields are invalid";
                        $responseBody["data"] = array(
                            "values" => $data,
                            "invalid" => $invalid
                        );

                    }

                } // else parameters are missing - default message
            
            } else {
            
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
                
            }

        } else {
            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(VariableController::$genericName)). " not found";
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(VariableController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _delete($params, $request, $response) {
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $generics = DS::query("SELECT
                id
            FROM ".VariableController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // found?
        if(count($generics)) {
        
            // does the user have the correct scope
            if( count(array_intersect(["webmaster","administrator"], $scopes)) ) {
                
                if(DS::delete(VariableController::$tableName, "WHERE id = ?i", $params[1]) !== null) {
                    $responseBody["message"] = ucwords(str_camel_case_to_words(VariableController::$genericName)). " deleted";
                    $responseBody["success"] = true;
                } else {
                    $responseBody["message"] = "Database delete error";
                }
                
            } else {
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
            }
            
        } else {
            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(VariableController::$genericName)). " not found";
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(VariableController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getOptions() {
        
        $useTypeItems = array(
            "user" => array("User",true),
            "system" => array("System",true)
        );
        
        $typeItems = array(
            "varchar" => array("Text Field",true),
            "longtext" => array("Long Text Field",true),
            "html" => array("HTML Field",true),
            "email" => array("Email Field",true),
            "tel" => array("Telephone Number",true),
            "int" => array("Integer Field",true),
            "float" => array("Float Field",true),
            "double" => array("Double Field",true),
            "bool" => array("Boolean Field",true),
            "color" => array("Color Field",true),
        );
        
        $groups = DS::select(VariableController::$tableName,"GROUP BY var_group_name ORDER BY var_group_name ASC");
        $groupItems = array();
        foreach($groups as $group) {
            $groupItems[$group["var_group_name"]] = array($group["var_group_name"],true);
        }
        
        // use the following array to override the field details where necessary
        $replaceOptions = array(
            VariableController::$genericName => array(
                "GET" => array(
                    "response" => array(
                        "list" => array(
                            array(
                                "var_group_name" => array("Type" => "typeahead", "Items" => $groupItems),
                                "var_type" => array("Type" => "select", "Items" => $typeItems, "Default" => "text"),
                                "use_type" => array("Type" => "select", "Items" => $useTypeItems, "Default" => "user")
                            )
                        ),
                    )
                ),
                "POST" => array(
                    "request" => array(
                        "body" => array(
                            "var_group_name" => array("Type" => "typeahead", "Items" => $groupItems),
                            "var_type" => array("Type" => "select", "Items" => $typeItems, "Default" => "text"),
                            "use_type" => array("Type" => "select", "Items" => $useTypeItems, "Default" => "user")
                        )
                    )
                ),
            ),
            VariableController::$genericName."/{id}" => array(
                "PUT" => array(
                    "request" => array(
                        "body" => array(
                            "var_group_name" => array("Type" => "typeahead", "Items" => $groupItems),
                            "var_type" => array("Type" => "select", "Items" => $typeItems, "Default" => "text"),
                            "use_type" => array("Type" => "select", "Items" => $useTypeItems, "Default" => "user")
                        )
                    )
                )
            )
        );
        
        // now generate the full options array
        $options = $this->_generateOptions(VariableController::$genericName, VariableController::$tableFields, $replaceOptions);
        
        // remove any fields that should not be returned
        
        //unset($options[VariableController::$genericName]["GET"]["response"]["list"][0]["example_field_name"]);
        
        unset($options[VariableController::$genericName]["POST"]["request"]["body"]["id"]);
        unset($options[VariableController::$genericName]["POST"]["request"]["body"]["author_id"]);
        unset($options[VariableController::$genericName]["POST"]["request"]["body"]["update_date"]);
        unset($options[VariableController::$genericName]["POST"]["request"]["body"]["create_date"]);
        
        unset($options[VariableController::$genericName."/{id}"]["PUT"]["request"]["body"]["id"]);
        unset($options[VariableController::$genericName."/{id}"]["PUT"]["request"]["body"]["author_id"]);
        unset($options[VariableController::$genericName."/{id}"]["PUT"]["request"]["body"]["update_date"]);
        unset($options[VariableController::$genericName."/{id}"]["PUT"]["request"]["body"]["create_date"]);
        unset($options[VariableController::$genericName."/{id}"]["PUT"]["response"]["id"]);
        unset($options[VariableController::$genericName."/{id}"]["PUT"]["response"]["author_id"]);
        unset($options[VariableController::$genericName."/{id}"]["PUT"]["response"]["update_date"]);
        unset($options[VariableController::$genericName."/{id}"]["PUT"]["response"]["create_date"]);
        
        return $options;
    }
    
    /**
     * 
     * @param type $group
     * @param type $name
     * @param type $value
     * @param type $defaultValue
     * @param type $type see Endpoint OPTIONS
     * @param type $sequence Add multiple values to the same group and name pair by adding a sequence number
     * @param type $required
     * @param type $use
     */
    static function _setItem($group, $name, $value, $defaultValue, $type, $sequence = 0, $required = 1, $use = 'system') {
        if(count($variable = DS::select(VariableController::$tableName, "WHERE var_group_name = ?s AND var_name = ?s AND var_sequence = ?i", $group, $name, $sequence)) > 0) {
            DS::update(VariableController::$tableName, array(
                "var_value" => $value,
                "var_default_value" => $defaultValue,
                "var_type" => $type,
                "required" => $required,
                "use_type" => $use
            ), "WHERE var_group_name = ?s AND var_name = ?s AND var_sequence = ?i", $group, $name, $sequence);
        } else {
            DS::insert(VariableController::$tableName, array(
                "author_id" => 0,
                "var_group_name" => $group,
                "var_name" => $name,
                "var_value" => $value,
                "var_default_value" => $defaultValue,
                "var_type" => $type,
                "required" => $required,
                "var_sequence" => $sequence,
                "use_type" => $use
            ));
        }
        
        if(!isset(VariableController::$cache[$group])) {
            VariableController::$cache[$group] = array();
        }
        if(!isset(VariableController::$cache[$group][$name])) {
            VariableController::$cache[$group][$name] = array();
        }
        VariableController::$cache[$group][$name][$sequence] = $value;
    }
    
    static function _getItemValue($group, $name, $sequence = null, $skipCache = false) {
        
        if(!$skipCache) {
            if(isset(VariableController::$cache[$group][$name])) {
                if($sequence === null && count(VariableController::$cache[$group][$name])) {
                    return VariableController::$cache[$group][$name][array_keys(VariableController::$cache[$group][$name])[0]];
                } else if(isset(VariableController::$cache[$group][$name][$sequence])){
                    return VariableController::$cache[$group][$name][$sequence];
                }
            }
        }
        
        $variable = array();
        
        if($sequence === null) {
            $variable = DS::select(VariableController::$tableName, "WHERE var_group_name = ?s AND var_name = ?s", $group, $name);
        } else {
            $variable = DS::select(VariableController::$tableName, "WHERE var_group_name = ?s AND var_name = ?s AND var_sequence = ?i", $group, $name, $sequence);
        }
        
        return count($variable) ? $variable[0]["var_value"] : null;
    }
    
    static function _getItemValues($group, $name, $skipCache = false) {
        
        if(!$skipCache) {
            if(isset(VariableController::$cache[$group][$name]) && count(VariableController::$cache[$group][$name])) {
                return VariableController::$cache[$group][$name];
            }
        }
        
        $variable = DS::select(VariableController::$tableName, "WHERE var_group_name = ?s AND var_name = ?s", $group, $name);
        $values = array();
        foreach($variable as $var) {
            $values[$var["var_sequence"]] = $var["var_value"];
        }
        
        return $values;
    }
    
    function _options($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $options = $this->_getOptions();
        
        $responseBody["message"] = ucwords(str_camel_case_to_words(VariableController::$genericName)). " options";
        $responseBody["success"] = true;
        $responseBody["data"] = $options;

        // prep output
        return $response->withPayload($responseBody);
    }
    
    
    
    public function install() {
        $tables = DS::list_tables();
        
        if(array_search(VariableController::$tableName, $tables)===false) {
            
            // generate the create table query
            $query = "CREATE TABLE ".VariableController::$tableName." (";
            
            $queryFields = "";
            $primaryKey = null;
            $uniqueKey = null;
            
            foreach(VariableController::$tableFields as $field) {
                $queryFields.= ($queryFields ? ", " : "")."{$field["Field"]} {$field["Type"]} ".(strtolower($field["Null"]) === "no" ? "NOT NULL" : "")." ".($field["Default"] ? "DEFAULT ".($field["Default"] === "CURRENT_TIMESTAMP" ? $field["Default"] : "'{$field["Default"]}'") : "")." ".($field["Extra"] ? $field["Extra"] : "");
                if(strtolower($field["Key"]) === "pri") {
                    $primaryKey = ", PRIMARY KEY ({$field["Field"]})";
                } else if(strtolower($field["Key"]) === "uni") {
                    $uniqueKey = ", UNIQUE `{$field["Field"]}` (`{$field["Field"]}`)";
                }
            }
            
            $query.= $queryFields.($primaryKey ? $primaryKey : "").($uniqueKey ? ($primaryKey ? ", " : "").$uniqueKey : "").") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            if(DS::query($query)) {
                //message_add("The users table has been created.");
            }
        }
        
        if(($adminMenu = MenuController::getItem("Admin")) === null) {
            $adminMenu = MenuController::addItem("Admin", null, null, "", 0, ["administrator","webmaster"]);
        }
        if(($settingsMenu = MenuController::getItem("Settings")) === null) {
            $settingsMenu = MenuController::addItem("Settings", null, null, "", 3, ["administrator","webmaster"], $adminMenu["id"]);
        }
        MenuController::addItem("<i class='fa fa-gears'></i> Variables", null, null, "variable", 0, ["webmaster"], $settingsMenu["id"]);
        
    }
    
    public function update($from_version) {
        
        if($from_version < 1) {
            
            try {
                $query = "CREATE UNIQUE INDEX i_variable ON variables ( var_group_name, var_name, var_sequence );";
                
                DS::query($query);
                
                $from_version ++;
                
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
        
        if($from_version < 2) {
            
            try {
                
                VariableController::_setItem("Settings", "SITE_NAME", "Another Appture Hub", "", "varchar");
                VariableController::_setItem("Settings", "SITE_NAME_SHORT", "Another Hub", "", "varchar");
                VariableController::_setItem("Settings", "SITE_EMAIL_ADMIN", "admin@appturehub.com", "", "email");
                VariableController::_setItem("Settings", "SITE_EMAIL_FROM", "website@appturehub.com", "", "email");
                VariableController::_setItem("Settings", "SITE_EMAIL_NO_REPLY", "noreply@appturehub.com", "", "email");
                VariableController::_setItem("Settings", "SITE_TEL", "123 123 1234", "", "tel");
                VariableController::_setItem("Settings", "SITE_COLOR", "#509958", "", "color");
                
                $from_version ++;
                
            } catch (Exception $ex) {
                print $e->getMessage();
            }
        }
        
        return $from_version;
    }
}