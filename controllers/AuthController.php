<?php
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\Stream;
use League\OAuth2\Server\Exception\OAuthServerException;

class AuthController extends Controller {
    static public $version = 0;
    static public $genericName = "auth";
    static public $tableName = null;
    
    function __construct($route_name) {
        parent::__construct($route_name);
        
        //$this->permissions = Permissions::get(AuthController::$genericName);
    }
    
    function view($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {
            //$request = $server->validateAuthenticatedRequest($request);

            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response);
            }
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // log response
        // TODO: log needs to be moved into the individual endpoints so we can discern success
        LogController::log(AuthController::$genericName,$method,0,0,true,json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    function access_token($params) {
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // init auth server
        $server = OAuth2Wrap\OAuth2Wrap::getAuthorizationServer();
        
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        try {
            
            // Try to respond to the request
            $body = $request->getParsedBody();
            $request = $request->withAttribute("account_type",!isset($body["account_type"]) ? "Local" : $body["account_type"]);
            $response = $server->respondToAccessTokenRequest($request, $response);

        } catch (\League\OAuth2\Server\Exception\OAuthServerException $exception) {

            // All instances of OAuthServerException can be formatted into a HTTP response
            $response = $exception->generateHttpResponse($response);

        } catch (\Exception $exception) {

            // Unknown exception
            $body = new Stream('php://temp', 'r+');
            $body->write($exception->getMessage());
            $response = $response->withStatus(500)->withBody($body);

        }
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    function revoke_token($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {
            $request = $server->validateAuthenticatedRequest($request);
            
            // get access token
            $accessToken = $request->getAttribute("oauth_access_token_id");
            
            // expire the access and refresh tokens
            DS::update("oauth2_access_tokens", array("expiry_date_time" => date(DATE_DB)), "WHERE access_token = ?s", $accessToken);
            DS::update("oauth2_refresh_tokens", array("expiry_date_time" => date(DATE_DB)), "WHERE access_token = ?s", $accessToken);
            
            // prep json response
            $responseBody = $this->_createJsonResponseBody();
            
            $responseBody["success"] = true;
            $responseBody["message"] = "Access and refresh tokens revoked";

            // prep output
            $response = $response->withPayload($responseBody);
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // log response
        //LogController::log(AuthController::$genericName,$method,0,0,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    function google_token ($params) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // Get $id_token via HTTPS POST.
        $id_token = filter_input(INPUT_POST, "id_token");
        $platform_id = filter_input(INPUT_POST, "platform_id");
        
        // Replace these with your token settings
        // Create a project at https://console.developers.google.com/
        $clientId = (strtolower($platform_id) === 'ios' ? API_GOOGLE_IOS_CLIENT_ID : API_GOOGLE_WEB_CLIENT_ID);

        $client = new Google_Client(['client_id' => $clientId]);  // Specify the CLIENT_ID of the app that accesses the backend
        $payload = $client->verifyIdToken($id_token);
        if ($payload) {
            if($payload["aud"] === $clientId) {
                
                $email = $payload['email'];
                $external_id = $payload['sub'];
                $provider_name = "Google";
                
                // see if we have an account with this email
                $user = DS::select("oauth2_users", "WHERE username = ?s AND is_active = 1", $email);
                $user = count($user) ? $user[0] : null;
                
                // if the used didn't authenticate using the selected provider before
                // we create a new entry on database.users for him
                if($user) {
                    
                    if($user["account_type"] == $provider_name && $user["external_id"] == $external_id) {
                        
                        // success
                        
                        // generate pass and hash
                        $salt = base64_encode(random_bytes(32));
                        $password = base64_encode(random_bytes(32));

                        // register new user
                        DS::update("oauth2_users", array(
                            "password" => password_hash($password.$salt, PASSWORD_BCRYPT),
                            "salt" => $salt
                        ), "WHERE id = ?i", $user["id"]);
                        
                    } else {
                        // previously signed in with different provider
                        $readableProvider = ucfirst($user["account_type"]);
                        $readableProvider = (stripos($readableProvider, "Linked") !== false ? str_replace("in"," In",$readableProvider) : $readableProvider);
                        $responseBody["message"] = "You previously used your ".$readableProvider." account to sign in.";
                        $user = null;
                    }
                    
                } else {
                    
                    // account not found
                    $responseBody["message"] = "Account not found. Please sign up first.";
                    $responseBody["data"] = array(
                        "email" => $email
                    );
                    
                }
                
                if($user) {
                    // redirect to access_token with username and password
                    
                    /*
                     * method: "POST",
            headers: {"Authorization" : "Basic "+btoa(self.client_id+":")},
            data: {
                grant_type: "password",
                username: username,
                password: password
            },
            dataType:"json",
            contentType: 'application/x-www-form-urlencoded',
                     */
                    $ch = curl_init(Router::getIndex()."/auth/access_token");
                    
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/x-www-form-urlencoded',
                        "Authorization: Basic ".base64_encode(API_DEFAULT_CLIENT_ID.":")
                    ));
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=password&account_type=".urlencode($provider_name)."&username=".urlencode($email)."&password=".urlencode($password));
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    
                    echo $result;
                    exit();
                }
            }
          
        } else {
            // Invalid ID token
            $responseBody["message"] = "Invalid ID token";
        }
        
        // prep output
        echo json_encode($responseBody);
        exit();
    }
    
    function google_signup ($params) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // Get $id_token via HTTPS POST.
        $id_token = filter_input(INPUT_POST, "id_token");
        $platform_id = filter_input(INPUT_POST, "platform_id");
        
        // Replace these with your token settings
        // Create a project at https://console.developers.google.com/
        $clientId = (strtolower($platform_id) === 'ios' ? API_GOOGLE_IOS_CLIENT_ID : API_GOOGLE_WEB_CLIENT_ID);
        
        $payload = null;
        
        if($id_token) {

            $client = new Google_Client(['client_id' => $clientId]);  // Specify the CLIENT_ID of the app that accesses the backend
            $payload = $client->verifyIdToken($id_token);
            
        }
        
        if ($payload) {
            if($payload["aud"] === $clientId) {
                
                $email = $payload['email'];
                $external_id = $payload['sub'];
                $provider_name = "Google";
                
                // see if we have an account with this email
                $user = DS::select("oauth2_users", "WHERE username = ?s AND is_active = 1", $email);
                $user = count($user) ? $user[0] : null;
                
                // if the used didn't authenticate using the selected provider before
                // we create a new entry on database.users for him
                if($user) {
                    
                    /*if($user["account_type"] == $provider_name && $user["external_id"] == $external_id) {
                        
                        // success
                        
                        // generate pass and hash
                        $salt = base64_encode(random_bytes(32));
                        $password = base64_encode(random_bytes(32));

                        // register new user
                        DS::update("oauth2_users", array(
                            "password" => password_hash($password.$salt, PASSWORD_BCRYPT),
                            "salt" => $salt
                        ), "WHERE id = ?i", $user["id"]);
                        
                    } else {
                        // previously signed in with different provider
                        $readableProvider = ucfirst($user["account_type"]);
                        $readableProvider = (stripos($readableProvider, "Linked") !== false ? str_replace("in"," In",$readableProvider) : $readableProvider);
                        $responseBody["message"] = "You previously used your ".$readableProvider." account to sign in.";
                        $user = null;
                    }*/
                    
                    $responseBody["message"] = "You are already signed up.";
                    
                } else {
                    // generate pass and hash
                    $salt = base64_encode(random_bytes(32));
                    $password = base64_encode(random_bytes(32));
                    
                    // register new user
                    $user = DS::insert("oauth2_users", array(
                        "username" => $email,
                        "password" => password_hash($password.$salt, PASSWORD_BCRYPT),
                        "salt" => $salt,
                        "account_type" => $provider_name,
                        "external_id" => $external_id,
                        "is_active" => 1,
                        "is_email_valid" => 1
                    ));
                    $user = $user[0];
                    
                    $avatar = null;
                    if($payload['picture']) {
                        $avatar = FileController::addExternalImage($payload['picture'], $user["id"]);
                    }
                    
                    $user_profile = DS::insert("user_profiles", array(
                        "user_id" => $user["id"],
                        "first_name" => $payload['given_name'],
                        "last_name" => $payload['family_name'],
                        "profile_image_id" => ($avatar ? $avatar["id"] : 0)
                    ));
                    
                    $subject = "Welcome to ".VariableController::_getItemValue("Settings", "SITE_NAME");
                    $message = Template::load("templates/emails/registered.php",array("account_type" => $provider_name, "username" => $email));
                    $email_body = Template::load("templates/emails/base.php",compact("subject","message"));
                    EmailController::_queueEmail($email, "", "", VariableController::_getItemValue("Settings", "SITE_EMAIL_FROM"), $subject, $email_body);
                    //Mailer::send($email, "", "", "noreply@click-a-tyre.co.za", $subject, $email_body);
                    
                    if($user) {
                        // redirect to access_token with username and password

                        /*
                         * method: "POST",
                        headers: {"Authorization" : "Basic "+btoa(self.client_id+":")},
                        data: {
                            grant_type: "password",
                            username: username,
                            password: password
                        },
                        dataType:"json",
                        contentType: 'application/x-www-form-urlencoded',
                         */
                        $ch = curl_init(Router::getIndex()."/auth/access_token");

                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/x-www-form-urlencoded',
                            "Authorization: Basic ".base64_encode(API_DEFAULT_CLIENT_ID.":")
                        ));
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=password&account_type=".urlencode($provider_name)."&username=".urlencode($email)."&password=".urlencode($password));
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                        $result = curl_exec($ch);
                        curl_close($ch);

                        echo $result;
                        exit();
                    }
                }
                
            }
          
        } else {
            // Invalid ID token
            $responseBody["message"] = "Invalid ID token";
        }
        
        // prep output
        echo json_encode($responseBody);
        exit();
    }
    
    function _getOptions() {
        return array(
            "".AuthController::$genericName."/access_token" => array(
                "POST" => array(
                    "title" => "Access Token",
                    "description" => "<p>Request an OAuth2 access token (Client or Password Grants).</p><p>Note: the Authorization header, containing the Client credentials, is required for both Grants. The <code>client_secret</code> may be left blank for non-confidential clients.",
                    "request" => array(
                        "headers" => array(
                            "Authorization" => "Required for both Grants. Plain JS example value: <code>'Basic '+btoa('client_id'+':'+'client_secret')</code>",
                            "Content-Type" => "<code>application/x-www-form-urlencoded</code>"
                        ),
                        "parameters" => array(
                            "client_id" => array("Field" => "client_id", "Type" => "varchar", "Label" => "Client ID (for Authorization header)", "Null" => "no", "Default" => "", "Extra" => "", "Description" => "Required for the Client Credentials Grant as part of the Authorization header."),
                            "client_secret" => array("Field" => "client_secret", "Type" => "password", "Label" => "Client Secret (for Authorization header)", "Null" => "yes", "Default" => "", "Extra" => "", "Description" => "Optional for the Client Credentials Grant as part of the Authorization header."),
                        ),
                        "body" => array(
                            "grant_type" => array("Field" => "grant_type", "Type" => "select", "Null" => "no", "Default" => "password", "Extra" => "", "Items" => array("client_credentials"=>array("client_credentials",true),"password"=>array("password",true)), "Description" => "Either 'client_credentials' or 'password'"),
                            "username" => array("Field" => "username", "Type" => "varchar", "Null" => "yes", "Default" => "", "Extra" => "", "Description" => "Required for the Password Grant"),
                            "password" => array("Field" => "password", "Type" => "password", "Null" => "yes", "Default" => "", "Extra" => "", "Description" => "Required for the Password Grant"),
                        )
                    ),
                    "response" => array(
                        "success" => array(
                            "scope" => "An array of scopes",
                            "notifications_token" => "Does the current user have an App notification token",
                            "token_type" => "Bearer",
                            "expires_in" => "Seconds",
                            "access_token" => "Generated JTW auth token",
                            "refresh_token" => "Returned for Password Grant",
                        ),
                        "error" => array(
                            "readyState" => 4,
                            "responseText" => "json string",
                            "responseJSON" => array(
                                "error" => "invalid_credentials or invalid_client",
                                "message" => ""
                            ),
                            "status" => 401,
                            "statusText" => "Unauthorized"
                        )
                    )
                )
            ),
            "".AuthController::$genericName."/revoke_token" => array(
                "GET" => array(
                    "title" => "Revoke Token",
                    "description" => "Revoke a previously granted OAuth2 access token.",
                    "request" => array(
                        "headers" => array(
                            "Authorization" => "Required. Plain JS example value: 'Basic '+btoa('client_id'+':'+'client_secret')"
                        ),
                        "parameters" => array(

                        ),
                        "body" => array()
                    ),
                    "response" => array(

                    )
                )
            ),
            "".AuthController::$genericName."/google_token" => array(
                "GET" => array(
                    "title" => "Google Token",
                    "description" => "Request an OAuth2 access token for a user linked to a Google Account.",
                    "request" => array(
                        "headers" => array(
                            "Authorization" => "Required. Plain JS example value: 'Basic '+btoa('client_id'+':'+'client_secret')"
                        ),
                        "parameters" => array(

                        ),
                        "body" => array()
                    ),
                    "response" => array(

                    )
                )
            ),
            "".AuthController::$genericName."/google_signup" => array(
                "GET" => array(
                    "title" => "Google Signup",
                    "description" => "Signup with a Google Account. Creates a user account linked to a Google Account.",
                    "request" => array(
                        "headers" => array(
                            "Authorization" => "Required. Plain JS example value: 'Basic '+btoa('client_id'+':'+'client_secret')"
                        ),
                        "parameters" => array(

                        ),
                        "body" => array()
                    ),
                    "response" => array(

                    )
                )
            )
        );
    }
    
    function _options($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $options = $this->_getOptions();
        
        $responseBody["message"] = ucwords(str_camel_case_to_words(AuthController::$genericName)). " options";
        $responseBody["success"] = true;
        $responseBody["data"] = $options;

        // prep output
        return $response->withPayload($responseBody);
    }
    
    public function install() {
        $tables = DS::list_tables();
        
        // check if the oauth2_access_tokens table exists, if not create it.
        if(array_search('oauth2_access_tokens',$tables)===false) {
            // generate the create table query
            $query = "CREATE TABLE oauth2_access_tokens (
                        access_token VARCHAR(128) NOT NULL,
                        expiry_date_time DATETIME NOT NULL,
                        user_id INTEGER UNSIGNED NOT NULL,
                        scopes VARCHAR(512) NOT NULL,
                        client_id VARCHAR(128) NOT NULL,
                        update_date DATETIME,
                        create_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (access_token),
                        UNIQUE KEY `access_token` (`access_token`)
                    ) ENGINE = InnoDB;";

            if(DS::query($query)) {
                //message_add("The products table has been created.");
            }
        }
        
        // check if the oauth2_refresh_tokens table exists, if not create it.
        if(array_search('oauth2_refresh_tokens',$tables)===false) {
            // generate the create table query
            $query = "CREATE TABLE oauth2_refresh_tokens (
                        refresh_token VARCHAR(128) NOT NULL,
                        access_token VARCHAR(128) NOT NULL,
                        expiry_date_time DATETIME NOT NULL,
                        update_date DATETIME,
                        create_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (refresh_token),
                        UNIQUE KEY `refresh_token` (`refresh_token`)
                    ) ENGINE = InnoDB;";

            if(DS::query($query)) {
                //message_add("The products table has been created.");
            }
        }
        
        // check if the oauth2_clients table exists, if not create it.
        if(array_search('oauth2_clients',$tables)===false) {
            // generate the create table query
            $query = "CREATE TABLE oauth2_clients (
                        `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                        `identifier` VARCHAR(128) NOT NULL,
                        `owner_id` int(10) UNSIGNED NOT NULL,
                        `secret` VARCHAR(60) NOT NULL,
                        `name` VARCHAR(128) NOT NULL,
                        `redirect_uri` VARCHAR(512) NOT NULL,
                        `is_confidential` TINYINT(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 1,
                        `is_active` TINYINT(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 1,
                        `update_date` DATETIME,
                        `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (id),
                        UNIQUE `identifier` (`owner_id`,`identifier`)
                    ) ENGINE = InnoDB;";

            if(DS::query($query)) {
                //message_add("The products table has been created.");
            }
        }
        
        // check if the oauth2_users table exists, if not create it.
        if(array_search('oauth2_users',$tables)===false) {
            // generate the create table query
            $query = "CREATE TABLE `oauth2_users` (
                        `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                        `username` varchar(128) NOT NULL,
                        `password` varchar(60) NOT NULL COMMENT 'Generated with PASSWORD_BCRYPT',
                        `salt` varchar(128) NOT NULL,
                        `app_platform` varchar(64) DEFAULT NULL,
                        `notifications_token` varchar(2048) DEFAULT NULL,
                        `account_type` varchar(64) DEFAULT 'Local',
                        `external_id` text,
                        `is_active` tinyint(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 1,
                        `is_email_valid` tinyint(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 0,
                        `status` varchar(64) DEFAULT 'ACTIVE',
                        `update_date` datetime DEFAULT NULL,
                        `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (id),
                        UNIQUE `username` (`username`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            if(DS::query($query)) {
                //message_add("The products table has been created.");
            }
        }
    }
    
    /*public function update($from_version) {
        
        if($from_version < 1) {
            
            try {
                this has been moved to ClientController
                DS::insert("oauth2_clients", array(
                    "identifier" => API_DEFAULT_CLIENT_ID,
                    "owner_id" => 1,
                    "secret" => "",
                    "salt" => "",
                    "name" => VariableController::_getItemValue("Settings", "SITE_NAME_SHORT"). " Web",
                    "redirect_uri" => UI_URL,
                    "is_confidential" => false,
                    "is_active" => true));
                
                $from_version ++;
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
        
        return $from_version;
    }*/
}