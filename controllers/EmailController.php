<?php
use League\OAuth2\Server\Exception\OAuthServerException;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\ServerRequest;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;

class EmailController extends Controller {
    static public $version = 0;
    static public $genericName = "email";
    static public $tableName = "email_queue";
    private $key = "jasdf983245jn98saduf";
    
    static public $tableFields = array(
        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
        
        "email_from" => array("Field" => "email_from", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "email_to" => array("Field" => "email_to", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "email_cc" => array("Field" => "email_cc", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "email_bcc" => array("Field" => "email_bcc", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "is_draft" => array("Field" => "is_draft", "Type" => "tinyint(1)", "Null" => "yes", "Key" => "", "Default" => 0, "Extra" => ""),
        "subject" => array("Field" => "subject", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "message" => array("Field" => "message", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "attachments" => array("Field" => "attachments", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        
        "scheduled_date" => array("Field" => "scheduled_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
        "sent_date" => array("Field" => "sent_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
        
        "sent" => array("Field" => "sent", "Type" => "tinyint(1)", "Null" => "yes", "Key" => "", "Default" => 0, "Extra" => ""),
        "error" => array("Field" => "error", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "tries" => array("Field" => "tries", "Type" => "int(10) unsigned", "Null" => "yes", "Key" => "", "Default" => 0, "Extra" => ""),
        
        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
    );
    
    function __construct($route_name) {
        parent::__construct($route_name);
        
        //$this->permissions = Permissions::get(EmailController::$genericName);
    }
    
    /**
     * 
     * @param type $params
     */
    function view($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {

            $request = $server->validateAuthenticatedRequest($request);
            
            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response, $server);
            }
            
            if($method === "GET") {
                if(!isset($params[1])) {
                    $response = $this->_getAll($params, $request, $response, $server);
                } else {
                    $response = $this->_getSpecific($params, $request, $response);
                }
            }

            // user profiles are created automatically on user registration
            if(strtoupper($method) === "POST") {
                $response = $this->_add($params, $request, $response);
            }

            if(strtoupper($method) === "PUT" && isset($params[1])) {
                $response = $this->_update($params, $request, $response);
            }

            // deleted with user accounts
            if(strtoupper($method) === "DELETE" && isset($params[1])) {
                $response = $this->_delete($params, $request, $response);
            }
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    /**
     * 
     * @param type $params
     * @param type $request
     * @param type $response
     * @return type
     */
    function _getAll($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // does the user have the correct scope
        if(count(array_intersect(["administrator","webmaster"], $scopes))) {

            // get fields for this endpoint from options
            $options = $this->_getOptions($request);
            $fields = $options[EmailController::$genericName]["GET"]["response"]["list"][0];

            // automatically page the loaded items
            $limit_parameter = filter_input(INPUT_GET, "limit");
            $page_parameter = filter_input(INPUT_GET, "page");
            $limit = $limit_parameter == null ? 25 : intval($limit_parameter);
            $page = $page_parameter == null ? 0 : intval($page_parameter);
            $limitString = ($limit > 0 ? " LIMIT ".($page*($limit)).", $limit" : "");

            // ordering, check validity etc and parse for query
            $orderParameter = filter_input(INPUT_GET, "order");
            $orderParameter = $orderParameter == null ? "create_date:DESC" : $orderParameter;
            $order_split = explode(":",$orderParameter);
            if(array_search($order_split[0], array_keys($fields)) === false || array_search($order_split[1], array("DESC","ASC")) === false) {
                $orderParameter = "create_date:DESC";
            }
            $order = implode(" ",explode(":",$orderParameter))." ";

            // add filter
            $filterFields = array_keys($fields);
            $filters = isset($_GET["filter"]) ? array_filter($_GET["filter"]) : array();
            $filtersAnd = isset($_GET["filterAnd"]) ? array_filter($_GET["filterAnd"]) : array();
            $filterString = DS::gen_filters($filterFields, $filters, "OR");
            $filterStringAnd = DS::gen_filters($filterFields, $filtersAnd, "AND");
            $filterString = "($filterString AND $filterStringAnd)";

            $totalRead = DS::query("
                SELECT
                    COUNT(id) as total
                FROM ".EmailController::$tableName);

            $totalFiltered = DS::query("
                SELECT
                    COUNT(id) as total
                FROM ".EmailController::$tableName."
                WHERE ".$filterString);

            $generics = DS::query("SELECT
                    ".implode(", ", array_keys($fields))."
                FROM ".EmailController::$tableName."
                WHERE $filterString
                ORDER BY {$order} 
                $limitString");

            $responseBody["success"] = true;
            $responseBody["message"] = ucwords(str_camel_case_to_words(EmailController::$genericName)). " list";
            $responseBody["data"] = array(
                "filter" => $filters,
                "filterAnd" => $filtersAnd,
                "order" => $orderParameter,
                "list" => $generics,
                "limit" => $limit,
                "page" => $page,
                "count" => count($generics),
                "total_filtered" => intval($totalFiltered[0]["total"]),
                "total" => intval($totalRead[0]["total"])
            );
               
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(EmailController::$genericName,$request->getMethod(),0,$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getSpecific($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // does the user have the correct scope
        if(count(array_intersect(["administrator","webmaster"], $scopes))) {
            
            // get fields for this endpoint from options
            $options = $this->_getOptions($request);
            $fields = $options[EmailController::$genericName."/{id}"]["GET"]["response"];

            $generics = DS::query("SELECT
                    ".implode(", ", array_keys($fields))."
                FROM ".EmailController::$tableName."
                WHERE id = ?i", intval($params[1]));

            if(count($generics)) {

                // found
                $responseBody["message"] = ucwords(str_camel_case_to_words(EmailController::$genericName)). " loaded";
                $responseBody["success"] = true;
                $responseBody["data"] = $generics[0];

            } else {

                // not found
                $responseBody["message"] = ucwords(str_camel_case_to_words(EmailController::$genericName)). " not found";

            }
               
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(EmailController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _add($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
            
        // get all fields from request body
        $requestBody = file_get_contents("php://input");
        $requestObject = json_decode($requestBody, true);

        // get fields for this endpoint from options
        $options = $this->_getOptions($request);
        $fields = $options[EmailController::$genericName]["POST"]["request"]["body"];

        // filter away bad fields
        $data = array();
        foreach(array_keys($fields) as $field) {
            if(isset($requestObject[$field])) {
                $data[$field] = $requestObject[$field];
            }
        }

        // still have data?
        if(count($data)) {
            
            // some fields need to meet certain criteria - validate here
            $invalid = Forms::validateData($fields, $data);
            
            // valid?
            if(!count($invalid)) {
                
                if(isset($data["is_draft"]) && $data["is_draft"] == 0) {
                    $data["message"] = Template::load("templates/emails/base.php",$data);
                }
                
                if(count($generic = DS::insert(EmailController::$tableName, $data))) {
                    $responseBody["message"] = ucwords(str_camel_case_to_words(EmailController::$genericName)). " added";
                    $responseBody["success"] = true;
                    $responseBody["data"] = $generic[0];
                } else {
                    $responseBody["message"] = "Database insert error";
                }

            } else {

                // had invalid fields
                $responseBody["message"] = "Some fields are invalid";
                $responseBody["data"] = array(
                    "values" => $data,
                    "invalid" => $invalid
                );

            }
        } // else parameters are missing - default message

        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(EmailController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _update($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // get fields for update
        $options = $this->_getOptions($request);
        $fields = $options[EmailController::$genericName."/{id}"]["PUT"]["request"]["body"];
        
        $generics = DS::query("SELECT
                ".implode(", ", array_keys($fields))."
            FROM ".EmailController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // found?
        if(count($generics)) {
            
            // does the user have the correct scope
            if( count(array_intersect(["webmaster","administrator"], $scopes)) ) {

                $requestBody = file_get_contents("php://input");
                $requestObject = json_decode($requestBody, true);

                // filter away bad fields
                $data = array();
                $sparseFields = array();
                foreach(array_keys($fields) as $field) {
                    if(isset($requestObject[$field])) {
                        if(isset($requestObject[$field])) {
                            $data[$field] = $requestObject[$field];
                            $sparseFields[$field] = $fields[$field];
                        }
                    }
                }

                // still have data?
                if(count($data)) {

                    // some fields need to meet certain criteria - validate here
                    $invalid = Forms::validateData($sparseFields, $data, intval($params[1]));

                    // valid?
                    if(!count($invalid)) {
                        
                        if(isset($data["is_draft"]) && $data["is_draft"] == 0) {
                            $data["message"] = Template::load("templates/emails/base.php",$data);
                        }

                        if(DS::update(EmailController::$tableName, $data, "WHERE id = ?i", $params[1]) !== null) {

                            $responseBody["message"] = ucwords(str_camel_case_to_words(EmailController::$genericName)). " updated";
                            $responseBody["success"] = true;
                            $responseBody["data"] = $data;
                        } else {
                            $responseBody["message"] = "Database update error";
                        }

                    } else {

                        // had invalid fields
                        $responseBody["message"] = "Some fields are invalid";
                        $responseBody["data"] = array(
                            "values" => $data,
                            "invalid" => $invalid
                        );

                    }

                } // else parameters are missing - default message
            
            } else {
            
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
                
            }

        } else {
            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(EmailController::$genericName)). " not found";
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(EmailController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _delete($params, $request, $response) {
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $generics = DS::query("SELECT
                id
            FROM ".EmailController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // found?
        if(count($generics)) {
        
            // does the user have the correct scope
            if( count(array_intersect(["webmaster","administrator"], $scopes)) ) {
                
                if(DS::delete(EmailController::$tableName, "WHERE id = ?i", $params[1]) !== null) {
                    $responseBody["message"] = ucwords(str_camel_case_to_words(EmailController::$genericName)). " deleted";
                    $responseBody["success"] = true;
                } else {
                    $responseBody["message"] = "Database delete error";
                }
                
            } else {
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
            }
            
        } else {
            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(EmailController::$genericName)). " not found";
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(EmailController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getOptions($request) {
        
        list($scopes,$user_id) = $this->_getViewer($request);
        
        $emailItems = array();
        
        $users = DS::query("
            SELECT
                u.id,
                u.username,
                CONCAT_WS(' ', IF(first_name = '',NULL,first_name), IF(last_name = '',NULL,last_name)) as contact_name
            FROM ".UserController::$tableName." as u
            JOIN user_profiles as p ON p.user_id = u.id
            ORDER BY u.username");
        foreach($users as $user) {
            $emailItems[$user["username"]] = array(
                $user["username"].($user["contact_name"] ? " ({$user["contact_name"]})" : ""),
                true
            );
            if($user["id"] == $user_id) {
                $currentUserEmail = $user["username"];
            }
        }
        
        $contacts = DS::select(ContactController::$tableName, "WHERE email IS NOT NULL AND email <> '' ORDER BY email");
        foreach($contacts as $contact) {
            $emailItems[$contact["email"]] = array(
                $contact["email"].($contact["contact_name"] ? " ({$contact["contact_name"]})" : ""),
                true
            );
        }
        
        $messageTemplate = Template::load("templates/emails/base_html.php",array());
        
        // use the following array to override the field details where necessary
        $replaceOptions = array(
            EmailController::$genericName => array(
                "GET" => array(
                    "response" => array(
                        "list" => array(
                            array(
                                "email_to" => array("Type" => "email",),
                                "email_cc" => array("Type" => "email"),
                                "email_bcc" => array("Type" => "email"),
                                "email_from" => array("Type" => "email")
                            )
                        ),
                    )
                ),
                "POST" => array(
                    "request" => array(
                        "body" => array(
                            "email_from" => array("Type" => "email", "Type" => "select", "Items" => $emailItems, "Default" => $currentUserEmail),
                            "email_to" => array("Type" => "email", "Type" => "multi_select", "Items" => $emailItems),
                            "email_cc" => array("Type" => "email", "Type" => "multi_select", "Items" => $emailItems),
                            "email_bcc" => array("Type" => "email", "Type" => "multi_select", "Items" => $emailItems),
                            "is_draft" => array("Type" => "hidden"),
                            "subject" => array("Type" => "varchar"),
                            "message" => array("Type" => "ckeditor", "Template" => $messageTemplate),
                            "attachments" => array("Type" => "multi_file_id"),
                        )
                    )
                ),
            ),
            EmailController::$genericName."/{id}" => array(
                "PUT" => array(
                    "request" => array(
                        "body" => array(
                            "email_from" => array("Type" => "email", "Type" => "select", "Items" => $emailItems, "Default" => $currentUserEmail),
                            "email_to" => array("Type" => "email", "Type" => "multi_select", "Items" => $emailItems),
                            "email_cc" => array("Type" => "email", "Type" => "multi_select", "Items" => $emailItems),
                            "email_bcc" => array("Type" => "email", "Type" => "multi_select", "Items" => $emailItems),
                            "is_draft" => array("Type" => "hidden"),
                            "subject" => array("Type" => "varchar"),
                            "message" => array("Type" => "ckeditor", "Template" => $messageTemplate),
                            "attachments" => array("Type" => "multi_file_id"),
                        )
                    )
                )
            )
        );
        
        // now generate the full options array
        $options = $this->_generateOptions(EmailController::$genericName, EmailController::$tableFields, $replaceOptions);
        
        // remove any fields that should not be returned
        
        //unset($options[EmailController::$genericName]["GET"]["response"]["list"][0]["example_field_name"]);
        
        unset($options[EmailController::$genericName]["POST"]["request"]["body"]["id"]);
        unset($options[EmailController::$genericName]["POST"]["request"]["body"]["sent"]);
        unset($options[EmailController::$genericName]["POST"]["request"]["body"]["error"]);
        unset($options[EmailController::$genericName]["POST"]["request"]["body"]["tries"]);
        unset($options[EmailController::$genericName]["POST"]["request"]["body"]["author_id"]);
        unset($options[EmailController::$genericName]["POST"]["request"]["body"]["sent_date"]);
        unset($options[EmailController::$genericName]["POST"]["request"]["body"]["update_date"]);
        unset($options[EmailController::$genericName]["POST"]["request"]["body"]["create_date"]);
        
        unset($options[EmailController::$genericName."/{id}"]["PUT"]["request"]["body"]["id"]);
        unset($options[EmailController::$genericName."/{id}"]["PUT"]["request"]["body"]["sent"]);
        unset($options[EmailController::$genericName."/{id}"]["PUT"]["request"]["body"]["error"]);
        unset($options[EmailController::$genericName."/{id}"]["PUT"]["request"]["body"]["tries"]);
        unset($options[EmailController::$genericName."/{id}"]["PUT"]["request"]["body"]["author_id"]);
        unset($options[EmailController::$genericName."/{id}"]["PUT"]["request"]["body"]["sent_date"]);
        unset($options[EmailController::$genericName."/{id}"]["PUT"]["request"]["body"]["update_date"]);
        unset($options[EmailController::$genericName."/{id}"]["PUT"]["request"]["body"]["create_date"]);
        unset($options[EmailController::$genericName."/{id}"]["PUT"]["response"]["id"]);
        unset($options[EmailController::$genericName."/{id}"]["PUT"]["response"]["author_id"]);
        unset($options[EmailController::$genericName."/{id}"]["PUT"]["response"]["update_date"]);
        unset($options[EmailController::$genericName."/{id}"]["PUT"]["response"]["create_date"]);
        
        unset($options[EmailController::$genericName]["GET"]["response"]["list"][0]["email_cc"]);
        unset($options[EmailController::$genericName]["GET"]["response"]["list"][0]["email_bcc"]);
        unset($options[EmailController::$genericName]["GET"]["response"]["list"][0]["email_from"]);
        unset($options[EmailController::$genericName]["GET"]["response"]["list"][0]["message"]);
        unset($options[EmailController::$genericName]["GET"]["response"]["list"][0]["attachments"]);
        
        return $options;
    }
    
    function _options($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $options = $this->_getOptions($request);
        
        $responseBody["message"] = ucwords(str_camel_case_to_words(EmailController::$genericName)). " options";
        $responseBody["success"] = true;
        $responseBody["data"] = $options;

        // prep output
        return $response->withPayload($responseBody);
    }
    
    /**
     * Send
     * Loads all mails that have been queued and not sent.
     * If failing, it will retry 4 times.
     * 
     * @global type $router
     * @param type $params
     */
    function _send() {
        global $router;
        
        $startTime = microtime(true);
        
        $responseBody = $this->_createJsonResponseBody();
        $responseBody["data"] = array();
        
        $emails = array();
        
        try {

            $emails = DS::query("SELECT * FROM email_queue WHERE (is_draft = 0 || is_draft IS NULL) AND (sent = 0 || sent IS NULL) AND (tries < 5 OR tries IS NULL) AND CURDATE() >= IFNULL(scheduled_date, CURDATE())");

            $responseBody["message"] = "Sending emails";
            $responseBody["data"]["queue_size"] = count($emails);

            $success = true;
            foreach($emails as $email) {
                $responseBody["data"][$email["id"]] = array();

                $sent = false;
                $error = "";

                try {

                    $sent = Mailer::send($email["email_to"], $email["email_cc"], $email["email_bcc"], array($email["email_from"] => VariableController::_getItemValue("Settings", "SITE_NAME_SHORT")), $email["subject"], $email["message"], null, explode(",",$email["attachments"]));

                    if(!$sent) {

                        $success = false;
                        $error = "No error message";

                    }

                } catch(Exception $e) {

                    $success = false;
                    $error = $e->getMessage();

                }

                $responseBody["data"][$email["id"]]["send_error"] = $error;
                $responseBody["data"][$email["id"]]["sent"] = $sent;

                try {

                    DS::update("email_queue",array(
                        "sent" => $sent,
                        "error" => is_string($error) ? $error : json_encode($error),
                        "tries" => (intval($email["tries"]) + 1),
                        "sent_date" => ($sent ? date(DATE_DB) : null)
                    ),"WHERE id = ?i", $email["id"]);

                } catch(Exception $e) {
                    $success = false;
                    $responseBody["data"][$email["id"]]["update_error"] = $error;
                }

                $responseBody["data"][$email["id"]]["try"] = $email["tries"];
            }

            $responseBody["success"] = true;
            $responseBody["data"]["date"] = date(DATE_DB);

        } catch (Exception $e) {

            $responseBody["data"] = $e->getMessage();
            $responseBody["message"] = "Could not load email queue";

        }
        
        $responseBody["time"] = (microtime(true) - $startTime);
        
        //$jsonReturn = json_encode($responseBody);
        LogController::log(EmailController::$genericName,"SEND",0,0,1, json_encode($responseBody),$router->getRoute());
        
        return $responseBody;
    }
    
    /**
     * Router endpoint
     * 
     * @global type $router
     * @param type $params
     */
    function send($params) {
        $responseBody = $this->_send();
        
        header("Content-type: application/json");
        print json_encode($responseBody);
        exit;
    }
    
    function _sendCron($params, $request, $response, $server) {
        set_time_limit ( 65 );
        
        $time = microtime(true);
        
        $responseBody = $this->_createJsonResponseBody();
        
        $responseBody["data"] = array();
        
        /*while(microtime(true) < $time + (60)) {
            $startTime = microtime(true);
            $responseBody["data"][] = $this->_send($params, $request, $response, $server);
            sleep(15-($startTime - microtime(true)));
        }*/
        
        $responseBody["data"][] = $this->_send($params, $request, $response, $server);
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        return $response;
    }
    
    /**
     * _queueEmail
     * To be used from other Controllers to queue an email to be sent in a separate thread.
     * 
     * @param type $to
     * @param type $cc
     * @param type $bcc
     * @param type $from
     * @param type $subject
     * @param type $message
     */
    static function _queueEmail($to,$cc,$bcc,$from,$subject,$message,$attachments = "",$scheduledDate = null) {
        $to = ($to === null ? "" : (is_array($to) ? implode(",",$to) : $to));
        $cc = ($cc === null ? "" : (is_array($cc) ? implode(",",$cc) : $cc));
        $bcc = ($bcc === null ? "" : (is_array($bcc) ? implode(",",$bcc) : $bcc));
        $from = ($from === null ? "" : (is_array($from) ? implode(",",$from) : $from));
        
        try {
            
            $duplicates = DS::select("email_queue", "WHERE email_to = ?s AND email_cc = ?s AND email_bcc = ?s AND email_from = ?s AND subject = ?s AND LENGTH(message) = ?i AND sent = 0 AND tries < 5", $to, $cc, $bcc, $from, $subject, strlen($message));
            
            LogController::log("EMAIL_QUEUE", "QUEUE", 0, 0,1, json_encode(compact("to","cc","bcc","from","subject","duplicates")), "");
            
            if(count($duplicates)===0) {
                
                $data = array(
                    "email_to" => $to,
                    "email_cc" => $cc,
                    "email_bcc" => $bcc,
                    "email_from" => $from,
                    "subject" => $subject,
                    "message" => $message,
                    "attachments" => is_array($attachments) ? implode(",",$attachments) : $attachments
                );
                
                if($scheduledDate !== null) {
                    $data["scheduled_date"] = $scheduledDate;
                }
                
                DS::insert("email_queue", $data);

                return true;
            }
        } catch (Exception $e) {
            
            LogController::log("EMAIL_QUEUE", "QUEUE", 0, 0,0, "EXCEPTION: ".$e->getMessage(), "");
            
        }
        
        return false;
    }
    
    function _getOwnerId($generic) {
        return null; // this generic does not keep an owner id
    }
    
    public function install() {
        $tables = DS::list_tables();
        
        if(array_search(EmailController::$tableName, $tables)===false) {
            
            // generate the create table query
            $query = "CREATE TABLE ".EmailController::$tableName." (";
            
            $queryFields = "";
            $primaryKey = null;
            $uniqueKey = null;
            
            foreach(EmailController::$tableFields as $field) {
                $queryFields.= ($queryFields ? ", " : "")."{$field["Field"]} {$field["Type"]} ".(strtolower($field["Null"]) === "no" ? "NOT NULL" : "")." ".($field["Default"] ? "DEFAULT ".($field["Default"] === "CURRENT_TIMESTAMP" ? $field["Default"] : "'{$field["Default"]}'") : "")." ".($field["Extra"] ? $field["Extra"] : "");
                if(strtolower($field["Key"]) === "pri") {
                    $primaryKey = ", PRIMARY KEY ({$field["Field"]})";
                } else if(strtolower($field["Key"]) === "uni") {
                    $uniqueKey = ", UNIQUE `{$field["Field"]}` (`{$field["Field"]}`)";
                }
            }
            
            $query.= $queryFields.($primaryKey ? $primaryKey : "").($uniqueKey ? ($primaryKey ? ", " : "").$uniqueKey : "").") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            if(DS::query($query)) {
                //message_add("The users table has been created.");
            }
        }
        
        if(($adminMenu = MenuController::getItem("Admin")) === null) {
            $adminMenu = MenuController::addItem("Admin", null, null, "", 0, ["administrator","webmaster"]);
        }
        if(($communicationMenu = MenuController::getItem("Communication")) === null) {
            $communicationMenu = MenuController::addItem("Communication", null, null, "", 0, ["administrator","webmaster"], $adminMenu["id"]);
        }
        MenuController::addItem("<i class='fa fa-send'></i> Mailer", null, null, "email", 1, ["administrator","webmaster"], $communicationMenu["id"]);
    }
}
?>