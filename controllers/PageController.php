<?php
use League\OAuth2\Server\Exception\OAuthServerException;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\ServerRequest;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;

class PageController extends Controller {
    static public $version = 1;
    static public $weight = 5;
    static public $genericName = "page";
    static public $tableName = "pages";
    
    static public $tableFields = array(
        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
        "author_id" => array("Field" => "author_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "images" => array("Field" => "images", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "title" => array("Field" => "title", "Type" => "VARCHAR(256)", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
        "slug_title" => array("Field" => "slug_title", "Type" => "VARCHAR(256)", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
        "body" => array("Field" => "body", "Type" => "longtext", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
    );
    
    function view($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {
            
            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response, $server);
            } else {
                $request = $server->validateAuthenticatedRequest($request);
            }
            
            if($method === "GET") {
                if(!isset($params[1])) {
                    $response = $this->_getAll($params, $request, $response, $server);
                } else {
                    $response = $this->_getSpecific($params, $request, $response, $server);
                }
            }

            if(strtoupper($method) === "POST") {
                $response = $this->_add($params, $request, $response, $server);
            }

            if(strtoupper($method) === "PUT" && isset($params[1])) {
                $response = $this->_update($params, $request, $response, $server);
            }

            if(strtoupper($method) === "DELETE" && isset($params[1])) {
                $response = $this->_delete($params, $request, $response, $server);
            }

            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response, $server);
            }
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    function _getAll($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if(count(array_intersect(["webmaster","administrator"],$scopes)) > 0) {
            
            $options = $this->_getOptions($user_id, $scopes);
            $fields = $options[PageController::$genericName]["GET"]["response"]["list"][0];
            
            // automatically page the loaded items
            $limit_parameter = filter_input(INPUT_GET, "limit");
            $page_parameter = filter_input(INPUT_GET, "page");
            $limit = $limit_parameter === null ? 25 : intval($limit_parameter);
            $page = $page_parameter == null ? 0 : intval($page_parameter);
            $limitString = ($limit > 0 ? " LIMIT ".($page*($limit)).", $limit" : "");
            
            // add filter
            $filterFields = array_keys($fields);
            $passedFilter = filter_input(INPUT_GET, "filter");
            $filterString = "(1=1)";
            if($passedFilter) {
                $filterString = "";
                if(stripos($passedFilter,"+") !== false) {
                    // multiple filters
                    $filters = explode("+",$passedFilter);
                } else {
                    $filters = [$passedFilter];
                }

                foreach($filters as $filterGroup) {
                    $filterSubParts = explode(" ",$filterGroup);
                    
                    foreach($filterSubParts as $filter) {
                        $filterPart = "";

                        if(stripos($filter,"|")!==false) { // exact match
                            // filter on a specific column
                            $columnFilter = explode("|",$filter);
                            $filterVal = DS::escape("{$columnFilter[1]}");
                            $filterPart = ($filterPart !== "" ? " OR " : ""). "{$columnFilter[0]} = $filterVal";

                        } else if(stripos($filter,":")!==false) { // LIKE
                            // filter on a specific column
                            $columnFilter = explode(":",$filter);
                            $filterVal = DS::escape("%{$columnFilter[1]}%");
                            $filterPart = ($filterPart !== "" ? " OR " : ""). "{$columnFilter[0]} LIKE $filterVal";

                        } else {
                            // filter on all columns
                            $filterVal = DS::escape("%$filter%");
                            foreach($filterFields as $field) {
                                $filterPart.= ($filterPart !== "" ? " OR " : ""). ''.$field. " LIKE $filterVal";
                            }

                        }

                        $filterString.= ($filterString !== "" ? " OR " : ""). "($filterPart)";
                    }
                }

                $filterString = "($filterString)";
            }

            $totalRead = DS::query("
                SELECT
                    COUNT(id) as total
                FROM ".PageController::$tableName."");
            
            $totalFiltered = DS::query("
                SELECT
                    COUNT(id) as total
                FROM ".PageController::$tableName."
                WHERE $filterString");

            $generics = DS::query("SELECT
                    ".implode(", ", array_keys($fields))."
                FROM ".PageController::$tableName."
                WHERE $filterString
                ORDER BY create_date DESC
                $limitString");

            $responseBody["success"] = true;
            $responseBody["message"] = ucwords(str_camel_case_to_words(PageController::$genericName)). " list";
            $responseBody["data"] = array(
                "list" => $generics,
                "limit" => 0,
                "page" => 0,
                "count" => count($generics),
                "total_filtered" => intval($totalFiltered[0]["total"]),
                "total" => intval($totalRead[0]["total"])
            );
                   
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(PageController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getSpecific($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        $options = $this->_getOptions($user_id, $scopes, 0);
        $fields = $options[PageController::$genericName."/{id}"]["GET"]["response"];
        
        if(intval($params[1])) {
            $generics = DS::query("SELECT
                    ".implode(", ", array_keys($fields)).",
                    IFNULL(update_date,create_date) as update_date
                FROM ".PageController::$tableName."
                WHERE id = ?i", $params[1]);
        } else {
            $generics = DS::query("SELECT
                    ".implode(", ", array_keys($fields)).",
                    IFNULL(update_date,create_date) as update_date
                FROM ".PageController::$tableName."
                WHERE slug_title = ?s", $params[1]);
        }
        
        if(count($generics)) {
            // found
            
            $responseBody["success"] = true;
            $responseBody["message"] = ucwords(str_camel_case_to_words(PageController::$genericName)). " loaded";
            $responseBody["data"] = $generics[0];
            
            
        } else {
            $responseBody["message"] = ucwords(str_camel_case_to_words(PageController::$genericName)). " not found";
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(PageController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _add($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // does the user have the correct scope
        if(count(array_intersect(["webmaster","administrator"], $scopes))) {
            
            // get all fields from request body
            $requestBody = file_get_contents("php://input");
            $requestObject = json_decode($requestBody, true);
            
            $options = $this->_getOptions($user_id, $scopes);
            $fields = $options[PageController::$genericName]["POST"]["request"]["body"];

            // filter away bad fields
            $data = array();
            foreach(array_keys($fields) as $field) {
                if(isset($requestObject[$field])) {
                    $data[$field] = $requestObject[$field];
                }
            }

            // still have data?
            if(count($data)) {

                // some fields need to meet certain criteria - validate here
                $invalid = Forms::validateData($fields, $data);

                // valid?
                if(!count($invalid)) {

                    // set current user as owner
                    $data["author_id"] = $user_id;
                    
                    if(isset($data["title"])) {
                        $data["slug_title"] = str_slugify($data["title"]);
                    }
                    
                    /*if(isset($data["body"])) {
                        $data["body"] = base64_encode($data["body"]);
                    }*/

                    if(count($generic = DS::insert(PageController::$tableName, $data))) {
                        
                        $responseBody["message"] = ucwords(str_camel_case_to_words(PageController::$genericName)). " added";
                        $responseBody["success"] = true;
                        $responseBody["data"] = $generic[0];
                    } else {
                        $responseBody["message"] = "Database insert error";
                    }

                } else {

                    // had invalid fields
                    $responseBody["message"] = "Some fields are invalid";
                    $responseBody["data"] = array(
                        "values" => $data,
                        "invalid" => $invalid
                    );

                }
            } // else parameters are missing - default message
               
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(PageController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _update($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        $generics = DS::query("SELECT
                *
            FROM ".PageController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // does the user have the correct scope
        if(count(array_intersect(["webmaster","administrator"], $scopes))) {
            
            $requestBody = file_get_contents("php://input");
            $requestObject = json_decode($requestBody, true);
            
            $options = $this->_getOptions($user_id, $scopes, $generics[0]["author_id"]);
            $fields = $options[PageController::$genericName."/{id}"]["PUT"]["request"]["body"];//DS::table_info(PageController::$tableName);
            
            // filter away bad fields
            $data = array();
            $sparseFields = array();
            foreach(array_keys($fields) as $field) {
                if(isset($requestObject[$field])) {
                    $data[$field] = $requestObject[$field];
                    $sparseFields[$field] = $fields[$field];
                }
            }
            
            // still have data?
            if(count($data)) {
                
                // some fields need to meet certain criteria - validate here
                $invalid = Forms::validateData($sparseFields, $data, $generics[0]["author_id"]);

                // valid?
                if(!count($invalid)) {
                    
                    if(isset($data["title"])) {
                        $data["slug_title"] = str_slugify($data["title"]);
                    }
                    
                    /*if(isset($data["body"])) {
                        $data["body"] = base64_encode($data["body"]);
                    }*/
                    
                    if(DS::update(PageController::$tableName, $data, "WHERE id = ?i", $params[1]) !== null) {
                        $responseBody["message"] = ucwords(str_camel_case_to_words(PageController::$genericName)). " updated";
                        $responseBody["success"] = true;
                        $responseBody["data"] = $data;
                    } else {
                        $responseBody["message"] = "Database update error";
                    }
                    
                } else {
                    
                    // had invalid fields
                    $responseBody["message"] = "Some fields are invalid";
                    $responseBody["data"] = array(
                        "values" => $data,
                        "invalid" => $invalid
                    );
                    
                }
                
            } // else parameters are missing - default message
            
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(PageController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _delete($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        $generics = DS::query("SELECT
                *
            FROM ".PageController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // does the user have the correct scope
        if(count(array_intersect(["webmaster","administrator"], $scopes))) {
            
            if(DS::delete(PageController::$tableName, "WHERE id = ?i", $params[1]) !== null) {
                $responseBody["message"] = ucwords(str_camel_case_to_words(PageController::$genericName)). " deleted";
                $responseBody["success"] = true;
            } else {
                $responseBody["message"] = "Database delete error";
            }
            
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(PageController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getOptions() {
        // use the following array to override the field details where necessary
        $replaceOptions = array(
            PageController::$genericName => array(
                "GET" => array(
                    "response" => array(
                        "list" => array(
                            array(
                                "images" => array("Type" => "multi_image"),
                                "body" => array("Type" => "html")
                            )
                        ),
                    )
                ),
                "POST" => array(
                    "request" => array(
                        "body" => array(
                            "images" => array("Type" => "multi_image"),
                            "body" => array("Type" => "html")
                        )
                    )
                ),
            ),
            PageController::$genericName."/{id}" => array(
                "PUT" => array(
                    "request" => array(
                        "body" => array(
                            "images" => array("Type" => "multi_image"),
                            "body" => array("Type" => "html")
                        )
                    )
                )
            )
        );
        
        // now generate the full options array
        $options = $this->_generateOptions(PageController::$genericName, PageController::$tableFields, $replaceOptions);
        
        // remove any fields that should not be returned
        
        //unset($options[PageController::$genericName]["GET"]["response"]["list"][0]["example_field_name"]);
        
        unset($options[PageController::$genericName]["POST"]["request"]["body"]["id"]);
        unset($options[PageController::$genericName]["POST"]["request"]["body"]["author_id"]);
        unset($options[PageController::$genericName]["POST"]["request"]["body"]["update_date"]);
        unset($options[PageController::$genericName]["POST"]["request"]["body"]["create_date"]);
        
        unset($options[PageController::$genericName."/{id}"]["PUT"]["request"]["body"]["id"]);
        unset($options[PageController::$genericName."/{id}"]["PUT"]["request"]["body"]["author_id"]);
        unset($options[PageController::$genericName."/{id}"]["PUT"]["request"]["body"]["update_date"]);
        unset($options[PageController::$genericName."/{id}"]["PUT"]["request"]["body"]["create_date"]);
        unset($options[PageController::$genericName."/{id}"]["PUT"]["response"]["id"]);
        unset($options[PageController::$genericName."/{id}"]["PUT"]["response"]["author_id"]);
        unset($options[PageController::$genericName."/{id}"]["PUT"]["response"]["update_date"]);
        unset($options[PageController::$genericName."/{id}"]["PUT"]["response"]["create_date"]);
        
        return $options;
    }
    
    function _options($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        $options = $this->_getOptions($user_id, $scopes);
        
        $responseBody["message"] = ucwords(str_camel_case_to_words(PageController::$genericName)). " options";
        $responseBody["success"] = true;
        $responseBody["data"] = $options;
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        //LogController::log(PageController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    public function install() {
        $tables = DS::list_tables();
        
        if(array_search(PageController::$tableName, $tables)===false) {
            
            // generate the create table query
            $query = "CREATE TABLE ".PageController::$tableName." (";
            
            $queryFields = "";
            $primaryKey = null;
            $uniqueKey = null;
            
            foreach(PageController::$tableFields as $field) {
                $queryFields.= ($queryFields ? ", " : "")."{$field["Field"]} {$field["Type"]} ".(strtolower($field["Null"]) === "no" ? "NOT NULL" : "")." ".($field["Default"] ? "DEFAULT ".($field["Default"] === "CURRENT_TIMESTAMP" ? $field["Default"] : "'{$field["Default"]}'") : "")." ".($field["Extra"] ? $field["Extra"] : "");
                if(strtolower($field["Key"]) === "pri") {
                    $primaryKey = ", PRIMARY KEY ({$field["Field"]})";
                } else if(strtolower($field["Key"]) === "uni") {
                    $uniqueKey = ", UNIQUE `{$field["Field"]}` (`{$field["Field"]}`)";
                }
            }
            
            $query.= $queryFields.($primaryKey ? $primaryKey : "").($uniqueKey ? ($primaryKey ? ", " : "").$uniqueKey : "").") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            if(DS::query($query)) {
                //message_add("The users table has been created.");
            }
        }
        
        if(($adminMenu = MenuController::getItem("Admin")) === null) {
            $adminMenu = MenuController::addItem("Admin", null, null, "", 0, ["administrator","webmaster"]);
        }
        if(($siteContentMenu = MenuController::getItem("Content & Media")) === null) {
            $siteContentMenu = MenuController::addItem("Content & Media", null, null, "", 1, ["administrator","webmaster"], $adminMenu["id"]);
        }
        MenuController::addItem("<i class='fa fa-file-o'></i> Page Manager", null, null, "page", 0, ["administrator","webmaster"], $siteContentMenu["id"]);
        
    }
    
    /*public function update($from_version) {
            
        try {
            
            if($from_version < 1) {
                
                

                $from_version ++;
                
            }
            
        } catch (Exception $e) {
            print $e->getMessage();
        }
        
        return $from_version;
    }*/
}