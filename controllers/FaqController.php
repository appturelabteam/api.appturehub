<?php
use League\OAuth2\Server\Exception\OAuthServerException;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Psr\Http\Message\ServerRequestInterface;

class FaqController extends Controller {
    static public $version = 0;
    static public $weight = 10;
    static public $genericName = "faq";
    static public $tableName = "faqs";
    
    static public $tableFields = array(
        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
        "author_id" => array("Field" => "author_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
        "title" => array("Field" => "title", "Type" => "VARCHAR(256)", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
        "slug_title" => array("Field" => "slug_title", "Type" => "VARCHAR(256)", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
		"body" => array("Field" => "body", "Type" => "longtext", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
		"keywords" => array("Field" => "keywords", "Type" => "text", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
		"attachments" => array("Field" => "attachments", "Type" => "text", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
    );
    
    /**
     * 
     * @param type $params
     */
    function view($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {

            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response, $server);
            } else {
                
            }
            
            if($method === "GET") {
                if(!isset($params[1])) {
                    $response = $this->_getAll($params, $request, $response);
                } else {
                    $response = $this->_getSpecific($params, $request, $response);
                }
            }

            // user profiles are created automatically on user registration
            if(strtoupper($method) === "POST") {
                $request = $server->validateAuthenticatedRequest($request);
                $response = $this->_add($params, $request, $response);
            }

            if(strtoupper($method) === "PUT" && isset($params[1])) {
                $request = $server->validateAuthenticatedRequest($request);
                $response = $this->_update($params, $request, $response);
            }

            // deleted with user accounts
            if(strtoupper($method) === "DELETE" && isset($params[1])) {
                $request = $server->validateAuthenticatedRequest($request);
                $response = $this->_delete($params, $request, $response);
            }
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    /**
     * 
     * @param type $params
     * @param type $request
     * @param type $response
     * @return type
     */
    function _getAll($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // get fields for this endpoint from options
        $options = $this->_getOptions();
        $fields = $options[FaqController::$genericName]["GET"]["response"]["list"][0];

        // automatically page the loaded items
        $limit_parameter = filter_input(INPUT_GET, "limit");
        $page_parameter = filter_input(INPUT_GET, "page");
        $limit = $limit_parameter == null ? 25 : intval($limit_parameter);
        $page = $page_parameter == null ? 0 : intval($page_parameter);
        $limitString = ($limit > 0 ? " LIMIT ".($page*($limit)).", $limit" : "");
        
        // ordering, check validity etc and parse for query
        $orderParameter = filter_input(INPUT_GET, "order");
        $orderParameter = $orderParameter == null ? "create_date:DESC" : $orderParameter;
        $order_split = explode(":",$orderParameter);
        if(array_search($order_split[0], array_keys($fields)) === false || array_search($order_split[1], array("DESC","ASC")) === false) {
            $orderParameter = "create_date:DESC";
        }
        $order = implode(" ",explode(":",$orderParameter))." ";

        // add filter
        $filterFields = array_keys($fields);
        $filters = isset($_GET["filter"]) ? array_filter($_GET["filter"]) : array();
        $filtersAnd = isset($_GET["filterAnd"]) ? array_filter($_GET["filterAnd"]) : array();
        $filterString = DS::gen_filters($filterFields, $filters, "OR");
        $filterStringAnd = DS::gen_filters($filterFields, $filtersAnd, "AND");
        $filterString = "($filterString AND $filterStringAnd)";

        $totalRead = DS::query("
            SELECT
                COUNT(id) as total
            FROM ".FaqController::$tableName);

        $totalFiltered = DS::query("
            SELECT
                COUNT(id) as total
            FROM ".FaqController::$tableName."
            WHERE ".$filterString);

        $generics = DS::query("SELECT
                ".implode(", ", array_keys($fields))."
            FROM ".FaqController::$tableName."
            WHERE $filterString
            ORDER BY {$order} 
            $limitString");

        $responseBody["success"] = true;
        $responseBody["message"] = ucwords(str_camel_case_to_words(FaqController::$genericName)). " list";
        $responseBody["data"] = array(
            "filter" => $filters,
            "filterAnd" => $filtersAnd,
            "order" => $orderParameter,
            "list" => $generics,
            "limit" => $limit,
            "page" => $page,
            "count" => count($generics),
            "total_filtered" => intval($totalFiltered[0]["total"]),
            "total" => intval($totalRead[0]["total"]),
            "order_options" => array(
                array("id" => "create_date:DESC", "title" => "Date: newest first"),
                array("id" => "create_date:ASC", "title" => "Date: oldest first"),
            )
        );
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(FaqController::$genericName,$request->getMethod(),0,$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getSpecific($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // get fields for this endpoint from options
        $options = $this->_getOptions();
        $fields = $options[FaqController::$genericName."/{id}"]["GET"]["response"];

        if(intval($params[1])) {
            $generics = DS::query("SELECT
                    ".implode(", ", array_keys($fields)).",
                    IFNULL(update_date,create_date) as update_date
                FROM ".FaqController::$tableName."
                WHERE id = ?i", $params[1]);
        } else {
            $generics = DS::query("SELECT
                    ".implode(", ", array_keys($fields)).",
                    IFNULL(update_date,create_date) as update_date
                FROM ".FaqController::$tableName."
                WHERE slug_title = ?s", $params[1]);
        }

        if(count($generics)) {

			//$generics[0]["body"] = base64_encode($generics[0]["body"]);

            // found
            $responseBody["message"] = ucwords(str_camel_case_to_words(FaqController::$genericName)). " loaded";
            $responseBody["success"] = true;
            $responseBody["data"] = $generics[0];

        } else {

            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(FaqController::$genericName)). " not found";

        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(FaqController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _add($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        if(count(array_intersect(["administrator","webmaster"], $scopes))) {
            
            // get all fields from request body
            $requestBody = file_get_contents("php://input");
            $requestObject = json_decode($requestBody, true);
            
            // get fields for this endpoint from options
            $options = $this->_getOptions();
            $fields = $options[FaqController::$genericName]["POST"]["request"]["body"];
            
            // filter away bad fields
            $data = array();
            foreach(array_keys($fields) as $field) {
                if(isset($requestObject[$field])) {
                    $data[$field] = $requestObject[$field];
                }
            }

            // still have data?
            if(count($data)) {

                // some fields need to meet certain criteria - validate here
                $invalid = Forms::validateData($fields, $data);

                // valid?
                if(!count($invalid)) {
                    
                    $data["author_id"] = $user_id;
                    
                    if(isset($data["title"])) {
                        $data["slug_title"] = str_slugify($data["title"]);
                    }

					if(isset($data["body"])) {
                        $data["body"] = base64_encode($data["body"]);
                    }

                    if(count($generic = DS::insert(FaqController::$tableName, $data))) {
                        
                        $responseBody["message"] = ucwords(str_camel_case_to_words(FaqController::$genericName)). " added";
                        $responseBody["success"] = true;
                        $responseBody["data"] = $generic[0];
                    } else {
                        $responseBody["message"] = "Database insert error";
                    }

                } else {
                    
                    // had invalid fields
                    $responseBody["message"] = "Some fields are invalid";
                    $responseBody["data"] = array(
                        "values" => $data,
                        "invalid" => $invalid
                    );

                }
            } // else parameters are missing - default message
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(FaqController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _update($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // get fields for update
        $options = $this->_getOptions();
        $fields = $options[FaqController::$genericName."/{id}"]["PUT"]["request"]["body"];
        
        $generics = DS::query("SELECT
                ".implode(", ", array_keys($fields))."
            FROM ".FaqController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // found?
        if(count($generics)) {
            
            // does the user have the correct scope
            if( count(array_intersect(["webmaster","administrator"], $scopes)) ) {

                $requestBody = file_get_contents("php://input");
                $requestObject = json_decode($requestBody, true);

                // filter away bad fields
                $data = array();
                $sparseFields = array();
                foreach(array_keys($fields) as $field) {
                    if(isset($requestObject[$field])) {
                        if(isset($requestObject[$field])) {
                            $data[$field] = $requestObject[$field];
                            $sparseFields[$field] = $fields[$field];
                        }
                    }
                }

                // still have data?
                if(count($data)) {

                    // some fields need to meet certain criteria - validate here
                    $invalid = Forms::validateData($sparseFields, $data, intval($params[1]));

                    // valid?
                    if(!count($invalid)) {

						if(isset($data["title"])) {
							$data["slug_title"] = str_slugify($data["title"]);
						}
                    
						if(isset($data["body"])) {
							$data["body"] = base64_encode($data["body"]);
						}

                        if(DS::update(FaqController::$tableName, $data, "WHERE id = ?i", $params[1]) !== null) {

                            $responseBody["message"] = ucwords(str_camel_case_to_words(FaqController::$genericName)). " updated";
                            $responseBody["success"] = true;
                            $responseBody["data"] = $data;
                        } else {
                            $responseBody["message"] = "Database update error";
                        }

                    } else {

                        // had invalid fields
                        $responseBody["message"] = "Some fields are invalid";
                        $responseBody["data"] = array(
                            "values" => $data,
                            "invalid" => $invalid
                        );

                    }

                } // else parameters are missing - default message
            
            } else {
            
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
                
            }

        } else {
            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(FaqController::$genericName)). " not found";
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(FaqController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _delete($params, $request, $response) {
        
        // get current user scopes
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $generics = DS::query("SELECT
                id
            FROM ".FaqController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // found?
        if(count($generics)) {
        
            // does the user have the correct scope
            if( count(array_intersect(["webmaster","administrator"], $scopes)) ) {
                
                if(DS::delete(FaqController::$tableName, "WHERE id = ?i", $params[1]) !== null) {
                    $responseBody["message"] = ucwords(str_camel_case_to_words(FaqController::$genericName)). " deleted";
                    $responseBody["success"] = true;
                } else {
                    $responseBody["message"] = "Database delete error";
                }
                
            } else {
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
            }
            
        } else {
            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(FaqController::$genericName)). " not found";
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(FaqController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getOptions() {
        // use the following array to override the field details where necessary
        $replaceOptions = array(
            FaqController::$genericName => array(
                "GET" => array(
                    "response" => array(
                        "list" => array(
                            array(
								"title" => array("Label" => "Question"),
								"body" => array("Label" => "Answer"),
                                "attachments" => array("Type" => "multi_file_id"),
								"keywords" => array("Type" => "varchar")
                            )
                        ),
                    )
                ),
                "POST" => array(
                    "request" => array(
                        "body" => array(
							"title" => array("Label" => "Question"),
							"body" => array("Label" => "Answer"),
                            "attachments" => array("Type" => "multi_file_id"),
							"keywords" => array("Type" => "varchar")
                        )
                    )
                ),
            ),
            FaqController::$genericName."/{id}" => array(
                "PUT" => array(
                    "request" => array(
                        "body" => array(
							"title" => array("Label" => "Question"),
							"body" => array("Label" => "Answer"),
                            "attachments" => array("Type" => "multi_file_id"),
							"keywords" => array("Type" => "varchar")
                        )
                    )
                )
            )
        );
        
        // now generate the full options array
        $options = $this->_generateOptions(FaqController::$genericName, FaqController::$tableFields, $replaceOptions);
        
        // remove any fields that should not be returned
        
        //unset($options[FaqController::$genericName]["GET"]["response"]["list"][0]["slug_title"]);
		unset($options[FaqController::$genericName]["GET"]["response"]["list"][0]["body"]);
		unset($options[FaqController::$genericName]["GET"]["response"]["list"][0]["attachments"]);
        
        unset($options[FaqController::$genericName]["POST"]["request"]["body"]["id"]);
        unset($options[FaqController::$genericName]["POST"]["request"]["body"]["author_id"]);
		unset($options[FaqController::$genericName]["POST"]["request"]["body"]["slug_title"]);
        unset($options[FaqController::$genericName]["POST"]["request"]["body"]["update_date"]);
        unset($options[FaqController::$genericName]["POST"]["request"]["body"]["create_date"]);
        
        unset($options[FaqController::$genericName."/{id}"]["PUT"]["request"]["body"]["id"]);
        unset($options[FaqController::$genericName."/{id}"]["PUT"]["request"]["body"]["author_id"]);
		unset($options[FaqController::$genericName."/{id}"]["PUT"]["request"]["body"]["slug_title"]);
        unset($options[FaqController::$genericName."/{id}"]["PUT"]["request"]["body"]["update_date"]);
        unset($options[FaqController::$genericName."/{id}"]["PUT"]["request"]["body"]["create_date"]);
        unset($options[FaqController::$genericName."/{id}"]["PUT"]["response"]["id"]);
        unset($options[FaqController::$genericName."/{id}"]["PUT"]["response"]["author_id"]);
        unset($options[FaqController::$genericName."/{id}"]["PUT"]["response"]["update_date"]);
        unset($options[FaqController::$genericName."/{id}"]["PUT"]["response"]["create_date"]);
        
        return $options;
    }
    
    function _options($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $options = $this->_getOptions();
        
        $responseBody["message"] = ucwords(str_camel_case_to_words(FaqController::$genericName)). " options";
        $responseBody["success"] = true;
        $responseBody["data"] = $options;

        // prep output
        return $response->withPayload($responseBody);
    }
    
    
    
    public function install() {
        $tables = DS::list_tables();
        
        if(array_search(FaqController::$tableName, $tables)===false) {
            
            // generate the create table query
            $query = "CREATE TABLE ".FaqController::$tableName." (";
            
            $queryFields = "";
            $primaryKey = null;
            $uniqueKey = null;
            
            foreach(FaqController::$tableFields as $field) {
                $queryFields.= ($queryFields ? ", " : "")."{$field["Field"]} {$field["Type"]} ".(strtolower($field["Null"]) === "no" ? "NOT NULL" : "")." ".($field["Default"] ? "DEFAULT ".($field["Default"] === "CURRENT_TIMESTAMP" ? $field["Default"] : "'{$field["Default"]}'") : "")." ".($field["Extra"] ? $field["Extra"] : "");
                if(strtolower($field["Key"]) === "pri") {
                    $primaryKey = ", PRIMARY KEY ({$field["Field"]})";
                } else if(strtolower($field["Key"]) === "uni") {
                    $uniqueKey = ", UNIQUE `{$field["Field"]}` (`{$field["Field"]}`)";
                }
            }
            
            $query.= $queryFields.($primaryKey ? $primaryKey : "").($uniqueKey ? ($primaryKey ? ", " : "").$uniqueKey : "").") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            if(DS::query($query)) {
                //message_add("The users table has been created.");
            }
        }

		if(($adminMenu = MenuController::getItem("Admin")) === null) {
            $adminMenu = MenuController::addItem("Admin", null, null, "", 0, ["administrator","webmaster"]);
        }
        if(($siteContentMenu = MenuController::getItem("Content & Media")) === null) {
            $siteContentMenu = MenuController::addItem("Content & Media", null, null, "", 1, ["administrator","webmaster"], $adminMenu["id"]);
        }
        MenuController::addItem("<i class='fa fa-question-circle'></i> FAQs", null, null, "faq", 2, ["administrator","webmaster"], $siteContentMenu["id"]);
        
    }
    
    public function update($from_version) {
        
        /*if($from_version < 1) {
            
            try {
                // import faqs from csv
		$fileHandle = fopen("uploads/faqs.csv", "r");
		$count = 0;
		$faqQueries = array();
		$fields = array();
		while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
			// first row contains field names
			if($count === 0) {
				foreach($row as $field) {
					$fields[] = $field;
				}
			} else {
				$faq = array();
				foreach($fields as $x=>$field) {
					$faq[$field] = $field === "body" ? base64_encode($row[$x]) : $row[$x];
				}
				$faq["slug_title"] = str_slugify($faq["title"]);
				$faq["author_id"] = 0;
				$faqQueries[] = DS::gen_insert(FaqController::$tableName, $faq);
			}
			$count++;
		}

		if(count($faqQueries)) {
			DS::query(implode(" ", $faqQueries));
		}
                
                $from_version ++;
                
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }*/
        
        return $from_version;
    }
}
?>