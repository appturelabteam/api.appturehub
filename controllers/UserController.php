<?php
use League\OAuth2\Server\Exception\OAuthServerException;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Psr\Http\Message\ServerRequestInterface;

class UserController extends Controller {
    static public $version = 1;
    static public $genericName = "user";
    static public $tableName = "oauth2_users";
    
    function __construct($route_name) {
        parent::__construct($route_name);
        
        $this->permissions = Permissions::get(UserController::$genericName);
    }
    
    function view($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {
            
            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response);
            } else if($method === "GET" && isset($params[1]) && $params[1] === "validate_email") {
                $response = $this->_validateEmail($params, $request, $response);
            } else {
                $request = $server->validateAuthenticatedRequest($request);
            }
            
            if($method === "GET") {
                if(!isset($params[1])) {
                    $response = $this->_getAll($params, $request, $response);
                } else if(isset($params[2]) && $params[2] === "resend_validation_email") {
                    $response = $this->_resendValidationEmail($params, $request, $response);
                } else {
                    $response = $this->_getSpecific($params, $request, $response);
                }
            }

            if(strtoupper($method) === "POST") {
                if(isset($params[1])) {
                    if($params[1] === "reset_password") {
                        $response = $this->_resetPassword($params, $request, $response);
                    } else {
                        $response = $this->_message($params, $request, $response);
                    }
                } else {
                    $response = $this->_add($params, $request, $response);
                }
            }

            if(strtoupper($method) === "PUT" && isset($params[1])) {
                $response = $this->_update($params, $request, $response);
            }

            if(strtoupper($method) === "DELETE" && isset($params[1])) {
                $response = $this->_delete($params, $request, $response);
            }
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // log response
        // TODO: log needs to be moved into the individual endpoints so we can discern success
        LogController::log(UserController::$genericName,$method,0,0,true,json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    function _getAll($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // does the user have the correct scope
        if(count(array_intersect(["webmaster","administrator"], $scopes))) {
            
            $options = $this->_getOptions($user_id, $scopes);
            $fields = $options[UserController::$genericName]["GET"]["response"]["list"][0];

            // automatically page the loaded items
            $limit_parameter = filter_input(INPUT_GET, "limit");
            $page_parameter = filter_input(INPUT_GET, "page");
            $limit = $limit_parameter == null ? 25 : intval($limit_parameter);
            $page = $page_parameter == null ? 0 : intval($page_parameter);
            $limitString = " LIMIT ".($page*($limit)).", $limit";
            
            // add filter
            $filterFields = array("id","username");
            $passedFilter = filter_input(INPUT_GET, "filter");
            $filterString = "(1=1)";
            if($passedFilter) {
                $filterString = "";
                if(stripos($passedFilter,"+") !== false) {
                    // multiple filters
                    $filters = explode("+",$passedFilter);
                } else {
                    $filters = [$passedFilter];
                }

                foreach($filters as $filterGroup) {
                    $filterSubParts = explode(" ",$filterGroup);
                    
                    foreach($filterSubParts as $filter) {
                        $filterPart = "";

                        if(stripos($filter,"|")!==false) { // exact match
                            // filter on a specific column
                            $columnFilter = explode("|",$filter);
                            $filterVal = DS::escape("{$columnFilter[1]}");
                            $filterPart = ($filterPart !== "" ? " OR " : ""). "{$columnFilter[0]} = $filterVal";

                        } else if(stripos($filter,":")!==false) { // LIKE
                            // filter on a specific column
                            $columnFilter = explode(":",$filter);
                            $filterVal = DS::escape("%{$columnFilter[1]}%");
                            $filterPart = ($filterPart !== "" ? " OR " : ""). "{$columnFilter[0]} LIKE $filterVal";

                        } else {
                            // filter on all columns
                            $filterVal = DS::escape("%$filter%");
                            foreach($filterFields as $field) {
                                $filterPart.= ($filterPart !== "" ? " OR " : ""). ''.$field. " LIKE $filterVal";
                            }

                        }

                        $filterString.= ($filterString !== "" ? " OR " : ""). "($filterPart)";
                    }
                }

                $filterString = "($filterString)";
            }

            $totalRead = DS::query("
                SELECT
                    COUNT(id) as total
                FROM oauth2_users");

            $totalFiltered = DS::query("
                SELECT
                    COUNT(id) as total
                FROM oauth2_users as u
                WHERE $filterString");

            
            $generics = DS::query("SELECT
                    u.id,
                    u.username,
                    CONVERT(CAST(IF(up.last_name != '',CONCAT(up.first_name, ' ', up.last_name),up.first_name) AS BINARY) USING utf8) as contact_name,
                    a.contact_number_1,
                    a.contact_number_2,
                    u.is_active,
                    u.is_email_valid,
                    u.account_type,
                    u.status,
                    u.app_platform,
                    u.update_date,
                    u.create_date
                FROM oauth2_users as u
                JOIN user_profiles as up ON up.user_id = u.id
                LEFT JOIN (SELECT contact_number_1, contact_number_2, owner_id FROM addresses GROUP BY owner_id) as a ON a.owner_id = u.id
                WHERE $filterString
                ORDER BY u.create_date DESC".$limitString);

            $responseBody["success"] = true;
            $responseBody["message"] = ucwords(str_camel_case_to_words(UserController::$genericName)). " list";
            $responseBody["data"] = array(
                "list" => $generics,
                "limit" => $limit,
                "page" => $page,
                "count" => count($generics),
                "total_filtered" => intval($totalFiltered[0]["total"]),
                "total" => intval($totalRead[0]["total"])
            );
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _getSpecific($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if($params[1] === "own") {
            $generics = DS::query("SELECT
                    id
                FROM oauth2_users
            WHERE id = ?i", $user_id);
            if(count($generics)) {
                $params[1] = $generics[0]["id"];
            }
        }
        
        $options = $this->_getOptions($user_id, $scopes);
        $fields = $options[UserController::$genericName."/{id}"]["GET"]["response"];
        
        $generics = DS::query("SELECT
                ".implode(",", array_keys($fields))."
            FROM oauth2_users
            WHERE id = ?i", intval($params[1]));
        
        // does the user have the correct scope
        if(count($generics)) {
            
            if((count(array_intersect(["authenticated"], $scopes)) && $generics[0]["id"] === $user_id) || count(array_intersect(["administrator","webmaster"], $scopes)) ) {
                
                // found
                $responseBody["success"] = true;
                $responseBody["message"] = ucwords(str_camel_case_to_words(UserController::$genericName)). " loaded";
                $responseBody["data"] = $generics[0];
                
            } else {
                
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
                
            }
            
        } else {
            $responseBody["message"] = ucwords(str_camel_case_to_words(UserController::$genericName)). " not found";
        }

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _add($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if(!count(array_intersect(["authenticated"], $scopes)) || count(array_intersect(["webmaster","administrator"], $scopes))) {
            
            // get all fields from request body
            $requestBody = file_get_contents("php://input");
            $requestObject = json_decode($requestBody, true);

            $options = $this->_getOptions();
            $fields = $options["user"]["POST"]["request"]["body"];//DS::table_info(UserController::$tableName);
            /*
             * Allow these fields:
            array(
                "username",
                "password",
                "confirm_password"
            );
             */

            // filter away bad fields
            $data = array();
            foreach(array_keys($fields) as $field) {
                if(isset($requestObject[$field])) {
                    $data[$field] = $requestObject[$field];
                }
            }

            // still have data?
            if(count($data)) {

                // some fields need to meet certain criteria - validate here
                $invalid = Forms::validateData($fields, $data);

                // valid?
                if(!count($invalid)) {
                    
                    $generatedPassword = false;
                    if($data["password"] === "{generate}") {
                        $generatedPassword = UserController::generatePassword(12);
                        $data["password"] = $generatedPassword;
                    }

                    $data["salt"] = base64_encode(random_bytes(32));
                    $data["password"] = password_hash($data["password"].$data["salt"], PASSWORD_BCRYPT);
                    
                    unset($data["confirm_password"]);

                    if(count($generic = DS::insert(UserController::$tableName, $data))) {
                        
                        // create empty user profile
                        DS::insert("user_profiles",array(
                            "user_id" => $generic[0]["id"],
                            "profile_image_id" => 0
                        ));

                        unset($generic[0]["salt"]);
                        unset($generic[0]["password"]);

                        $responseBody["message"] = ucwords(str_camel_case_to_words(UserController::$genericName)). " added";
                        $responseBody["success"] = true;
                        $responseBody["data"] = $generic[0];
                        
                        try {
                            
                            // create validation link which is only valid for a short period
                            $guid = str_guid();
                            
                            LogController::log(UserController::$genericName, "VALIDATION_LINK", 0, $generic[0]["id"], 1, $guid, "");
                            
                            $subject = "Welcome to ". VariableController::_getItemValue("Settings", "SITE_NAME_SHORT");
                            $message = Template::load("templates/emails/registered".($generatedPassword === false ? "" : "_password").".php",array("account_type" => "Local", "username" => $data["username"], "validation_guid" => $guid, "password" => $generatedPassword));
                            $email_body = Template::load("templates/emails/base.php",compact("subject","message"));
                            //EmailController::_queueEmail($data["username"], "", "", "noreply@craftedarts.co.za", $subject, $email_body);
                            Mailer::send($data["username"], "", "", array(VariableController::_getItemValue("Settings", "SITE_NAME_SHORT") => VariableController::_getItemValue("Settings", "SITE_EMAIL_FROM")), $subject, $email_body);
                            
                        } catch (Exception $ex) {
                            
                        }
                        
                    } else {
                        $responseBody["message"] = "Database insert error";
                    }

                } else {

                    $data["password"] = "";
                    $data["confirm_password"] = "";
                    
                    // had invalid fields
                    $responseBody["message"] = "Some fields are invalid";
                    $responseBody["data"] = array(
                        "values" => $data,
                        "invalid" => $invalid
                    );

                }
            } // else parameters are missing - default message
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _update($params, $request, $response) {
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        if($params[1] === "own") {
            $generics = DS::query("SELECT
                    id
                FROM oauth2_users
            WHERE id = ?i", $user_id);
            if(count($generics)) {
                $params[1] = $generics[0]["id"];
            }
        }
        
        $options = $this->_getOptions($user_id, $scopes);
        $fields = $options[UserController::$genericName."/{id}"]["GET"]["response"];
        
        $generics = DS::query("SELECT
                ".implode(",", array_keys($fields))."
            FROM oauth2_users
            WHERE id = ?i", intval($params[1]));
        
        
        // found?
        if(count($generics)) {
            
            // does the user have the correct scope
            if((count(array_intersect(["authenticated"], $scopes)) && $generics[0]["id"] === $user_id) || count(array_intersect(["administrator","webmaster"], $scopes)) ) {
            
                $requestBody = file_get_contents("php://input");
                $requestObject = json_decode($requestBody, true);

                $options = $this->_getOptions();
                $fields = $options["user/{id}"]["PUT"]["request"]["body"];//DS::table_info(UserController::$tableName);
                
                if(count(array_intersect(["webmaster","administrator"], $scopes)) === 0) {
                    unset($fields["is_active"]);
                    unset($fields["status"]);
                }
                
                /*
                 * Allow these fields:
                array(
                    "username",
                    "password",
                    "confirm_password",
                    "is_active"
                );
                 */
                
                // add account field user id
                $fields["username"]["AccountsUserID"] = $generics[0]["id"];

                // filter away bad fields
                $data = array();
                $passedFields = array();
                foreach(array_keys($fields) as $field) {
                    if(isset($requestObject[$field])) {
                        $passedFields[$field] = $fields[$field];
                        $data[$field] = $requestObject[$field];
                    }
                }

                // still have data?
                if(count($data)) {
                    
                    // some fields need to meet certain criteria - validate here
                    $invalid = Forms::validateData($passedFields, $data, $generics[0]["id"]);
                    
                    // valid?
                    if(!count($invalid)) {
                        
                        if(isset($data["password"])) {
                            $data["salt"] = base64_encode(random_bytes(32));
                            $data["password"] = password_hash($data["password"].$data["salt"], PASSWORD_BCRYPT);
                            //unset($data["password"]);
                            unset($data["confirm_password"]);
                        }
                        
                        if(DS::update(UserController::$tableName, $data, "WHERE id = ?i", $params[1]) !== null) {
                            
                            unset($data["salt"]);
                            unset($data["password"]);

                            $responseBody["message"] = ucwords(str_camel_case_to_words(UserController::$genericName)). " updated";
                            $responseBody["success"] = true;
                            $responseBody["data"] = $data;
                        } else {
                            $responseBody["message"] = "Database update error";
                        }

                    } else {

                        // had invalid fields
                        $responseBody["message"] = "Some fields are invalid";
                        $responseBody["data"] = array(
                            "values" => $data,
                            "invalid" => $invalid
                        );

                    }
                    
                } // else parameters are missing - default message
                
            } else {
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
            }
            
        } else {
            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(UserController::$genericName)). " not found";
        }

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _delete($params, $request, $response) {
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $generics = DS::query("SELECT
                id,
                username,
                is_active,
                update_date,
                create_date
                FROM oauth2_users
            WHERE id = ?i", intval($params[1]));
        
        // does the user have the correct scope
        if(count(array_intersect(["webmaster","administrator"], $scopes)) || 
                ( count($generics) && $generics[0]["id"] === $user_id ) ) {
            
            // found?
            if(count($generics)) {
                
                if(DS::delete(UserController::$tableName, "WHERE id = ?i", $params[1]) !== null) {
                    
                    // also delete user's profile
                    DS::delete("user_profiles", "WHERE user_id = ?i", $params[1]);
                    
                    $responseBody["message"] = ucwords(str_camel_case_to_words(UserController::$genericName)). " deleted";
                    $responseBody["success"] = true;
                } else {
                    $responseBody["message"] = "Database delete error";
                }
                
            } else {
                // not found
                $responseBody["message"] = ucwords(str_camel_case_to_words(UserController::$genericName)). " not found";
            }
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _resendValidationEmail($params, $request, $response) {
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        if($params[1] === "own") {
            $generics = DS::query("SELECT
                    id
                FROM oauth2_users
            WHERE id = ?i", $user_id);
            if(count($generics)) {
                $params[1] = $generics[0]["id"];
            }
        }
        
        $options = $this->_getOptions($user_id, $scopes);
        $fields = $options["".UserController::$genericName."/{id}"]["GET"]["response"];
        
        $generics = DS::query("SELECT
                ".implode(",", array_keys($fields))."
            FROM oauth2_users
            WHERE id = ?i", intval($params[1]));
        
        // found?
        if(count($generics)) {
            
            // does the user have the correct scope
            if((count(array_intersect(["authenticated"], $scopes)) && $generics[0]["id"] === $user_id) || count(array_intersect(["administrator","webmaster"], $scopes)) ) {
                
                if($generics[0]["is_email_valid"] == 0) {
                    
                    try {
                        
                        // create validation link which is only valid for a short period
                        $guid = str_guid();
                        
                        LogController::log(UserController::$genericName, "VALIDATION_LINK", 0, $generics[0]["id"], 1, $guid, "");
                        
                        $subject = "Validate your email address";
                        $message = Template::load("templates/emails/validate_email.php",array("username" => $generics[0]["username"], "validation_guid" => $guid));
                        $email_body = Template::load("templates/emails/base.php",compact("subject","message"));
                        //EmailController::_queueEmail($data["username"], "", "", "noreply@craftedarts.co.za", $subject, $email_body);
                        if(Mailer::send($generics[0]["username"], "", "", array(VariableController::_getItemValue("Settings", "SITE_NAME_SHORT") => VariableController::_getItemValue("Settings", "SITE_EMAIL_FROM")), $subject, $email_body)) {
                            
                            $responseBody["message"] = "Validation email sent";
                            $responseBody["success"] = true;
                            
                        }
                        
                    } catch (Exception $ex) {
                        
                        $responseBody["message"] = "Error: ".$ex->getMessage();
                        
                    }
                    
                } else {
                    
                    $responseBody["message"] = "Email address has already been validated";
                    
                }
                
            } else {
                // access denied
                $responseBody["message"] = "Access denied.";
                $responseBody["access"] = false;
            }
            
        } else {
            // not found
            $responseBody["message"] = ucwords(str_camel_case_to_words(UserController::$genericName)). " not found";
        }

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _validateEmail($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $encodedValidationGuid = filter_input(INPUT_GET, "id");
        
        if($encodedValidationGuid) {
            $validationGuid = base64_decode($encodedValidationGuid);
            
            // if still within 1 hour of link creation
            // DS::select("logs","WHERE description = ?s AND DATE_ADD(create_date, INTERVAL 1 HOUR) < CURDATE()", $validationGuid);
            if(count($logs = DS::select("logs","WHERE description = ?s", $validationGuid))) {
                
                if(count($user = DS::select("oauth2_users", "WHERE id = ?i", $logs[0]["user_id"]))) {
                    
                    if($user[0]["is_email_valid"] == 0) {

                        DS::update("oauth2_users", array("is_email_valid" => 1), "WHERE id = ?i", $logs[0]["user_id"]);
                        DS::delete("logs","WHERE description = ?s", $validationGuid);

                        $subject = "Email address validated";
                        $message = Template::load("templates/emails/validated.php",array());
                        $email_body = Template::load("templates/emails/base.php",compact("subject","message"));
                        //EmailController::_queueEmail($data["username"], "", "", "noreply@craftedarts.co.za", $subject, $email_body);
                        Mailer::send($user[0]["username"], "", "", array(VariableController::_getItemValue("Settings", "SITE_NAME_SHORT") => VariableController::_getItemValue("Settings", "SITE_EMAIL_FROM")), $subject, $email_body);

                        $responseBody["message"] = "Email address validated successfully";
                        $responseBody["success"] = true;
                        
                    } else {
                        
                        $responseBody["message"] = "Your Email address has already been validated";
                        $responseBody["success"] = true;
                        
                    }
                }
                
            } else {
                
                $responseBody["message"] = "Invalid validation link";
                $responseBody["success"] = false;
                
            }
            
        } else {
            
            $responseBody["message"] = "Invalid validation link";
            $responseBody["success"] = false;
            
        }
        
        header("Location: ".UI_URL."/user/validated?payload=".base64_encode(json_encode($responseBody)));
        exit();
    }
    
    function _resetPassword($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if(count(array_intersect(["anonymous"], $scopes))) {
            
            // get all fields from request body
            $requestBody = file_get_contents("php://input");
            $requestObject = json_decode($requestBody, true);

            $options = $this->_getOptions();
            $fields = $options["user"]["POST"]["request"]["body"];//DS::table_info(UserController::$tableName);
            $fields["username"]["Extra"] = "";
            unset($fields["password"]);
            unset($fields["confirm_password"]);
            /*
             * Allow these fields:
            array(
                "username",
                "password",
                "confirm_password"
            );
             */

            // filter away bad fields
            $data = array();
            foreach(array_keys($fields) as $field) {
                if(isset($requestObject[$field])) {
                    $data[$field] = $requestObject[$field];
                }
            }
            
            // still have data?
            if(count($data)) {

                // some fields need to meet certain criteria - validate here
                $invalid = Forms::validateData($fields, $data);
                
                // valid?
                if(!count($invalid)) {
                    
                    if(count($user = DS::select(UserController::$tableName, "WHERE username = ?s", $data["username"]))) {
                        $user = $user[0];
                        
                        if($user["account_type"] === "Local") {
                            
                            $password = UserController::generatePassword(10);
                            $update["salt"] = base64_encode(random_bytes(32));
                            $update["password"] = password_hash($password.$update["salt"], PASSWORD_BCRYPT);
                            
                            if(DS::update(UserController::$tableName, $update, "WHERE username = ?s", $data["username"]) !== null) {
                                
                                try {
                                    
                                    $subject = VariableController::_getItemValue("Settings", "SITE_NAME_SHORT"). " Password Reset";
                                    $message = Template::load("templates/emails/reset_password.php",array("account_type" => "Local", "username" => $data["username"], "password" => $password));
                                    $email_body = Template::load("templates/emails/base.php",compact("subject","message"));
                                    //EmailController::_queueEmail($data["username"], "", "", "noreply@craftedarts.co.za", $subject, $email_body);
                                    Mailer::send($data["username"], "", "", array(VariableController::_getItemValue("Settings", "SITE_EMAIL_FROM") => VariableController::_getItemValue("Settings", "SITE_NAME_SHORT")), $subject, $email_body);
                                    
                                    $responseBody["message"] = "Request sent. Check your mailbox for further details.";
                                    $responseBody["success"] = true;
                                    $responseBody["data"] = $data;
                                    
                                } catch (Exception $ex) {
                                    
                                    $responseBody["message"] = "Request failed";
                                    $responseBody["success"] = false;
                                    $responseBody["data"] = Mailer::$lastError;
                                    
                                }
                                
                            } else {
                                
                                $responseBody["message"] = "Database insert error";
                                
                            }
                            
                        } else {
                            
                            $responseBody["message"] = "Please sign in using your {$user["account_type"]} account. If you have forgot your password for the relevant account, you will need to use their facilities to reset your password.";
                            
                        }
                        
                    } else {
                        
                        $responseBody["message"] = "Account not found.";
                        
                    }

                } else {
                    
                    // had invalid fields
                    $responseBody["message"] = "Some fields are invalid";
                    $responseBody["data"] = array(
                        "values" => $data,
                        "invalid" => $invalid
                    );

                }
            } // else parameters are missing - default message
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _message($params, $request, $response) {
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $generics = DS::query("SELECT
                *
            FROM oauth2_users
            WHERE id = ?i", intval($params[1]));
        
        // does the user have the correct scope
        if(count(array_intersect(["webmaster","administrator"], $scopes)) ) {
            
            // found?
            if(count($generics)) {
                
                $user = $generics[0];
                
                $requestBody = file_get_contents("php://input");
                $requestObject = json_decode($requestBody, true);

                $options = $this->_getOptions();
                $fields = $options["user/{id}"]["POST"]["request"]["body"];
                
                // filter away bad fields
                $data = array();
                foreach(array_keys($fields) as $field) {
                    if(isset($requestObject[$field])) {
                        $data[$field] = $requestObject[$field];
                    }
                }

                // still have data?
                if(count($data)) {
                    
                    // some fields need to meet certain criteria - validate here
                    $invalid = Forms::validateData($fields, $data, intval($params[1]));
                    
                    // ensure correct type was passed
                    if($data["type"] !== "push" && $data["type"] !== "email") {
                        $invalid["type"] = array(
                            "message" => "Invalid type passed",
                            "status" => "danger"
                        );
                    }
                    
                    // valid?
                    if(!count($invalid)) {
                        
                        if($data["type"] === "push") {
                            
                            if($user["notifications_token"]) {

                                $push = new PushNotifications();

                                $responseBody["data"] = $push->SendToDevice($user["notifications_token"], array(
                                    "title" => $data["title"],
                                    "body" => $data["body"],
                                    "icon" => $data["icon"],
                                    "hash" => $data["hash"]
                                ));

                                $responseBody["message"] = "Push notification sent";
                                $responseBody["success"] = true;
                                
                            } else {
                                
                                $responseBody["message"] = "User has no token";
                                
                            }
                            
                        } else if($data["type"] === "email") {
                            
                            $message = Template::load("templates/emails/base.php",array(
                                //"iconPath" => UI_URL.'img/icons/'.$data["icon"].'.svg',
                                "subject" => $data["title"],
                                "message" => $data["body"]."<br>"
                            ));
                            
                            if(EmailController::_queueEmail($user["username"], "", "", VariableController::_getItemValue("Settings", "SITE_EMAIL_FROM"), $data["title"]." - ".VariableController::_getItemValue("Settings", "SITE_NAME_SHORT"), $message)) {
                                
                                $responseBody["message"] = "Email queued";
                                $responseBody["success"] = true;
                                
                            } else {
                                
                                $responseBody["message"] = "Could not queue email";
                                
                            }
                            
                        }
                        
                    } else {

                        // had invalid fields
                        $responseBody["message"] = "Some fields are invalid";
                        $responseBody["data"] = array(
                            "values" => $data,
                            "invalid" => $invalid
                        );

                    }
                    
                } // default message some parameters are missing
                
            } else {
                // not found
                $responseBody["message"] = ucwords(str_camel_case_to_words(UserController::$genericName)). " not found";
            }
            
        } else {
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
        }

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _getOptions() {
        $iconItems = array(
            "notice_icon"=>array("Site Logo",true)
        );
        
        $priorityItems = array(
            "high"=>array("high",true),
            "normal"=>array("normal",true)
        );
        
        $hashItems = array(
            "order"=>array("Buyer's Orders",true),
            "user"=>array("My Account",true),
            "address"=>array("My Addresses",true),
            "faq"=>array("FAQ",true),
            "privacy-policy"=>array("Privacy Policy",true),
            "terms-and-conditions"=>array("Terms and Conditions",true)
        );
        
        return array(
            "user" => array(
                "GET" => array(
                    "title" => "List Users",
                    "description" => "List resources.",
                    "request" => array(
                        "parameters" => array(
                            "limit" => array("Field" => "limit", "Type" => "int", "Null" => "yes", "Key" => "", "Default" => 25, "Extra" => "", "Description" => "Limit the number of items returned (default: 25)."),
                            "page" => array("Field" => "limit", "Type" => "int", "Null" => "yes", "Key" => "", "Default" => 0, "Extra" => "", "Description" => "Page through the returned items (default: 0)."),
                            "filter" => array("Field" => "limit", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Description" => "Filter the returned items ({field_name:value} or {value} for searching in all fields).")
                        ),
                        "body" => array()
                    ),
                    "response" => array(
                        "list" => array(
                            array(
                                "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
                                "username" => array("Field" => "title", "Type" => "email", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "contact_name" => array("Field" => "contact_name", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                                "contact_number_1" => array("Field" => "contact_number_1", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                                "contact_number_2" => array("Field" => "contact_number_2", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                                "is_active" => array("Field" => "is_active", "Type" => "tinyint(1) unsigned zerofill", "Null" => "NO", "Key" => "", "Default" => "1", "Extra" => ""),
                                "is_email_valid" => array("Field" => "is_email_valid", "Type" => "tinyint(1) unsigned zerofill", "Null" => "NO", "Key" => "", "Default" => "1", "Extra" => ""),
                                "account_type" => array("Field" => "account_type", "Type" => "varchar", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                                "status" => array("Field" => "status", "Type" => "varchar", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                                "app_platform" => array("Field" => "app_platform", "Type" => "varchar", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                                //"notifications_token" => array("Field" => "notifications_token", "Type" => "varchar", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                                "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                                "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                            )
                        ),
                        "limit" => null,
                        "page" => null,
                        "count" => null,
                        "total_filtered" => null,
                        "total" => null
                    )
                ),
                "POST" => array(
                    "title" => "Create User",
                    "description" => "Create a resource.",
                    "request" => array(
                        "parameters" => null,
                        "body" => array(
                            "username" => array("Field" => "username", "Type" => "email", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => "account", "AccountsTable" => "oauth2_users", "AccountsEmailField" => "username", "AccountsUserIDField" => "id", "Label" => "Email address (Username)"),
                            "password" => array("Field" => "password", "Type" => "password", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                            "confirm_password" => array("Field" => "confirm_password", "Type" => "password", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => "")
                        )
                    ),
                    "response" => array(
                        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
                        "username" => array("Field" => "username", "Type" => "email", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "is_active" => array("Field" => "is_active", "Type" => "tinyint(1) unsigned zerofill", "Null" => "NO", "Key" => "", "Default" => "1", "Extra" => ""),
                        "is_email_valid" => array("Field" => "is_email_valid", "Type" => "tinyint(1) unsigned zerofill", "Null" => "NO", "Key" => "", "Default" => "1", "Extra" => ""),
                        "account_type" => array("Field" => "account_type", "Type" => "varchar", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "status" => array("Field" => "status", "Type" => "varchar", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "app_platform" => array("Field" => "app_platform", "Type" => "varchar", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        //"notifications_token" => array("Field" => "notifications_token", "Type" => "varchar", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                    )
                ),
            ),
            "user/{id}" => array(
                "GET" => array(
                    "title" => "Get User",
                    "description" => "Get a resource.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array()
                    ),
                    "response" => array(
                        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
                        "username" => array("Field" => "username", "Type" => "email", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "is_active" => array("Field" => "is_active", "Type" => "tinyint(1) unsigned zerofill", "Null" => "NO", "Key" => "", "Default" => "1", "Extra" => ""),
                        "is_email_valid" => array("Field" => "is_email_valid", "Type" => "tinyint(1) unsigned zerofill", "Null" => "NO", "Key" => "", "Default" => "1", "Extra" => ""),
                        "account_type" => array("Field" => "account_type", "Type" => "varchar", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "status" => array("Field" => "status", "Type" => "varchar", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "app_platform" => array("Field" => "app_platform", "Type" => "varchar", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        //"notifications_token" => array("Field" => "notifications_token", "Type" => "varchar", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                    )
                ),
                "PUT" => array(
                    "title" => "Update User",
                    "description" => "Update a resource. Allows sparse payloads.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array(
                            "username" => array("Field" => "username", "Type" => "email", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "account", "AccountsTable" => "oauth2_users", "AccountsEmailField" => "username", "AccountsUserIDField" => "id", "AccountsUserID" => null, "Label" => "Email address (Username)"),
                            "password" => array("Field" => "password", "Type" => "password", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                            "confirm_password" => array("Field" => "confirm_password", "Type" => "password", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                            "is_active" => array("Field" => "is_active", "Type" => "tinyint(1) unsigned zerofill", "Null" => "yes", "Key" => "", "Default" => "1", "Extra" => ""),
                            //"status" => array("Field" => "status", "Type" => "varchar", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        )
                    ),
                    "response" => array(
                        "username" => array("Field" => "title", "Type" => "email", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                        "password" => array("Field" => "password", "Type" => "password", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                        "confirm_password" => array("Field" => "confirm_password", "Type" => "password", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                        "is_active" => array("Field" => "is_active", "Type" => "tinyint(1) unsigned zerofill", "Null" => "yes", "Key" => "", "Default" => "1", "Extra" => ""),
                        //"status" => array("Field" => "status", "Type" => "varchar", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                    )
                ),
                "DELETE" => array(
                    "title" => "Delete User",
                    "description" => "Permanently remove a resource from the database.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array()
                    ),
                    "response" => array()
                ),
                "POST" => array(
                    "title" => "Message User",
                    "description" => "Email or message this user.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array(
                            "type" => array("Field" => "type", "Type" => "varchar", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                            "title" => array("Field" => "title", "Label" => "Subject", "Type" => "varchar", "Null" => "NO", "Key" => "", "Default" => "", "Extra" => ""),
                            "body" => array("Field" => "body", "Label" => "Message", "Type" => "text", "Null" => "NO", "Key" => "", "Default" => "", "Extra" => ""),
                            "icon" => array("Field" => "icon", "Type" => "select", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => "", "Items" => $iconItems),
                            "priority" => array("Field" => "priority", "Type" => "select", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => "", "Items" => $priorityItems),
                            "hash" => array("Field" => "hash", "Label" => "Link to page", "Type" => "select", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => "", "Items" => $hashItems)
                        )
                    ),
                    "response" => array(
                        // result
                    )
                )
            ),
            "user/validate_email" => array(
                "GET" => array(
                    "title" => "Validate User",
                    "description" => "Validate a user email address.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "varchar(36)", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => "", "Description" => "Generated validation id"),
                        ),
                        "body" => array()
                    ),
                    "response" => array()
                )
            ),
            "user/{id}/resend_validation_email" => array(
                "GET" => array(
                    "title" => "Resend Validation",
                    "description" => "Resend a validation link in an email to a user.",
                    "request" => array(
                        "parameters" => array(),
                        "body" => array()
                    ),
                    "response" => array()
                )
            )
        );
    }
    
    function _options($params, $request, $response) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        $options = $this->_getOptions();
        
        $responseBody["message"] = ucwords(str_camel_case_to_words(UserController::$genericName)). " options";
        $responseBody["success"] = true;
        $responseBody["data"] = $options;

        // prep output
        return $response->withPayload($responseBody);
    }
    
    function _getOwnerId($generic) {
        return $generic["owner_id"]; // this generic does not keep an owner id
    }
    
    static public function generatePassword($length) {
        $pass = "";
        for($x = 1; $x < $length; $x++) {
            if($x % ceil(rand(1,2))) {
                $pass.=rand(0, 9);
            } else if($x % ceil(rand(3,6))) {
                $pass.=chr(rand(65, 90)); // 65 - 90, 97 - 122
            } else {
                $pass.=chr(rand(97, 122)); // 65 - 90, 97 - 122
            }
        }
        return $pass;
    }
    
    public function install() {
        $tables = DS::list_tables();
        
        // making use of oauth2_users, created by the AuthController 
        
        if(array_search('roles',$tables)===false) {
            // generate the create table query
            $query = "CREATE TABLE `roles` (
                        `role` varchar(128) NOT NULL,
                        `description` varchar(512) NOT NULL,
                        PRIMARY KEY (`role`),
                        UNIQUE KEY `role` (`role`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            if(DS::query($query)) {
                //message_add("The users table has been created.");
            }
        }
        
        if(array_search('user_roles',$tables)===false) {
            // generate the create table query
            $query = "CREATE TABLE user_roles (
                        id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
                        role VARCHAR(128) NOT NULL,
                        user_id INTEGER UNSIGNED NOT NULL,
                        PRIMARY KEY (id),
                        UNIQUE `user_role` (`role`,`user_id`)
                    ) ENGINE = InnoDB;";

            if(DS::query($query)) {
                //message_add("The users table has been created.");
            }
        }
        
        // create menu links; these need to line up with the front end views/pages
        $userMenu = MenuController::addItem("User", null, null, "", 0, "");
        if($userMenu) {
            MenuController::addItem("Sign in", null, null, "user/signin", 0, "anonymous", $userMenu["id"]);
            //MenuController::addItem("Register", null, null, "user/register", 1, "anonymous", $userMenu["id"]); // for click-a-tyre no anonymous registration will be allowed
            MenuController::addItem("My Account", null, null, "user/own/edit", 0, "authenticated", $userMenu["id"]);
            MenuController::addItem("Sign out", null, null, "user/signout", 1, "authenticated", $userMenu["id"]);
        }
        
        if(($adminMenu = MenuController::getItem("Admin")) === null) {
            $adminMenu = MenuController::addItem("Admin", null, null, "", 0, ["administrator","webmaster"]);
        }
        if(($accessControlMenu = MenuController::getItem("Access Control")) === null) {
            $accessControlMenu = MenuController::addItem("Access Control", null, null, "", 2, ["administrator","webmaster"], $adminMenu["id"]);
        }
        MenuController::addItem("<i class='fa fa-group'></i> User Accounts", null, null, "user", 0, ["administrator","webmaster"], $accessControlMenu["id"]);
        
        $pages = array();
        /*
        * user/listing
        */

        // add user page permissions including core admin
        $pages[] = array(
            'name'=>'getAll',
            'category'=>'user',
            'subcat'=>'pages',
            'ptype'=>2,
            'pview'=>'webmaster,administrator');
        
        foreach($pages as $key=>$fieldData) {
            Permissions::set($fieldData);
        }

        // add user content permissions
        $content = array();
        $content[] = array(
            'name'=>'user',
            'category'=>'user',
            'subcat'=>'content',
            'ptype'=>0,
            'pview'=>'own',
            'pedit'=>'own',
            'padd'=>'anonymous,webmaster,administrator',
            'pdel'=>'own');
        foreach($content as $key=>$fieldData) {
            Permissions::set($fieldData);
        }

        // add user field permissions
        /*$fields = array();
        $fields[] = array(
            'name'=>'active',
            'category'=>'user',
            'subcat'=>'fields',
            'ptype'=>1,
            'pview'=>'webmaster',
            'pedit'=>'webmaster');
        foreach($fields as $key=>$fieldData) {
            Permissions::set($fieldData);
        }*/
    }
    
    public function update($from_version) {
        
        if($from_version < 1) {
            
            try {
                $salt = base64_encode(random_bytes(32));
                
                $users = DS::insert("oauth2_users", array(
                            "username" => "admin@appturehub.com",
                            "password" => password_hash("AnotherHubPass".$salt, PASSWORD_BCRYPT),
                            "salt" => $salt,
                            "is_active" => true));
                
                DS::insert("roles", array("role" => "authenticated", "description" => "Authenticated"));
                DS::insert("roles", array("role" => "administrator", "description" => "Administrator"));
                DS::insert("roles", array("role" => "webmaster", "description" => "Webmaster"));
                
                DS::insert("user_roles", array("role" => "webmaster", "user_id" => $users[0]["id"]));
                
                $from_version ++;
            } catch (Exception $e) {
                print $e->getMessage();
            }
        }
        
        return $from_version;
    }
}