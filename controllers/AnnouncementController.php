<?php
use League\OAuth2\Server\Exception\OAuthServerException;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\Diactoros\ServerRequest;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;

class AnnouncementController extends Controller {
    static public $version = 0;
    static public $genericName = "announcement";
    static public $tableName = "announcements";
    
    function __construct($route_name) {
        parent::__construct($route_name);
        
        $this->permissions = Permissions::get(AnnouncementController::$genericName);
    }
    
    function view($params) {
        // generate request and response
        $request = ServerRequestFactory::fromGlobals();
        $response = new JsonResponse("");
        
        // get HTTP Method
        $method = $request->getMethod();
        
        // init OAuth2 resource server
        $server = OAuth2Wrap\OAuth2Wrap::getResourceServer();
        
        try {
            $request = $server->validateAuthenticatedRequest($request);
            
            if($method === "GET") {
                if(!isset($params[1])) {
                    $response = $this->_getAll($params, $request, $response, $server);
                } else {
                    if($params[1] === "for_display") {
                        $response = $this->_getAllForDisplay($params, $request, $response, $server);
                    } else {
                        $response = $this->_getSpecific($params, $request, $response, $server);
                    }
                }
            }

            if(strtoupper($method) === "POST") {
                $response = $this->_add($params, $request, $response, $server);
            }

            if(strtoupper($method) === "PUT" && isset($params[1])) {
                $response = $this->_update($params, $request, $response, $server);
            }

            if(strtoupper($method) === "DELETE" && isset($params[1])) {
                $response = $this->_delete($params, $request, $response, $server);
            }

            if(strtoupper($method) === "OPTIONS") {
                $response = $this->_options($params, $request, $response, $server);
            }
            
        } catch (OAuthServerException $exception) {
            $response = $exception->generateHttpResponse($response);
            // @codeCoverageIgnoreStart
        } catch (\Exception $exception) {
            $response = (new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500))
                ->generateHttpResponse($response);
            // @codeCoverageIgnoreEnd
        }
        
        // output the response
        $emitter = new SapiEmitter();
        $emitter->emit($response);
        exit();
    }
    
    function _getAll($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if(count(array_intersect(["webmaster","administrator"],$scopes)) > 0) {
            
            $options = $this->_getOptions($user_id, $scopes);
            $fields = $options[AnnouncementController::$genericName]["GET"]["response"]["list"][0];
            
            // automatically page the loaded items
            $limit_parameter = filter_input(INPUT_GET, "limit");
            $page_parameter = filter_input(INPUT_GET, "page");
            $limit = $limit_parameter === null ? 25 : intval($limit_parameter);
            $page = $page_parameter == null ? 0 : intval($page_parameter);
            $limitString = ($limit > 0 ? " LIMIT ".($page*($limit)).", $limit" : "");
            
            // add filter
            unset($fields["group_id"]);
            unset($fields["display_type"]);
            $filterFields = array_keys($fields);
            $passedFilter = filter_input(INPUT_GET, "filter");
            $filterString = "(1=1)";
            if($passedFilter) {
                $filterString = "";
                if(stripos($passedFilter,"+") !== false) {
                    // multiple filters
                    $filters = explode("+",$passedFilter);
                } else {
                    $filters = [$passedFilter];
                }

                foreach($filters as $filterGroup) {
                    $filterSubParts = explode(" ",$filterGroup);
                    
                    foreach($filterSubParts as $filter) {
                        $filterPart = "";

                        if(stripos($filter,"|")!==false) { // exact match
                            // filter on a specific column
                            $columnFilter = explode("|",$filter);
                            $filterVal = DS::escape("{$columnFilter[1]}");
                            $filterPart = ($filterPart !== "" ? " OR " : ""). "{$columnFilter[0]} = $filterVal";

                        } else if(stripos($filter,":")!==false) { // LIKE
                            // filter on a specific column
                            $columnFilter = explode(":",$filter);
                            $filterVal = DS::escape("%{$columnFilter[1]}%");
                            $filterPart = ($filterPart !== "" ? " OR " : ""). "{$columnFilter[0]} LIKE $filterVal";

                        } else {
                            // filter on all columns
                            $filterVal = DS::escape("%$filter%");
                            foreach($filterFields as $field) {
                                $filterPart.= ($filterPart !== "" ? " OR " : ""). ''.$field. " LIKE $filterVal";
                            }

                        }

                        $filterString.= ($filterString !== "" ? " OR " : ""). "($filterPart)";
                    }
                }

                $filterString = "($filterString)";
            }

            $totalRead = DS::query("
                SELECT
                    COUNT(id) as total
                FROM ".AnnouncementController::$tableName."");
            
            $totalFiltered = DS::query("
                SELECT
                    COUNT(id) as total
                FROM ".AnnouncementController::$tableName."
                WHERE $filterString");

            $generics = DS::query("SELECT
                    a.".implode(", a.", array_keys($fields)).",
                    ag.title as group_title,
                    ag.display_type as display_type
                FROM ".AnnouncementController::$tableName." as a
                LEFT JOIN announcement_groups as ag ON ag.id = a.group_id
                WHERE $filterString
                ORDER BY create_date DESC
                $limitString");

            $responseBody["success"] = true;
            $responseBody["message"] = ucwords(str_camel_case_to_words(AnnouncementController::$genericName)). " list";
            $responseBody["data"] = array(
                "list" => $generics,
                "limit" => 0,
                "page" => 0,
                "count" => count($generics),
                "total_filtered" => intval($totalFiltered[0]["total"]),
                "total" => intval($totalRead[0]["total"])
            );
                   
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(AnnouncementController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getAllForDisplay($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
            
        $options = $this->_getOptions($user_id, $scopes);
        $fields = $options[AnnouncementController::$genericName]["GET"]["response"]["list"][0];

        unset($fields["group_id"]);
        unset($fields["display_type"]);

        $generics = DS::query("SELECT
                a.".implode(", a.", array_keys($fields)).",
                ag.title as group_title,
                ag.display_type as display_type
            FROM ".AnnouncementController::$tableName." as a
            LEFT JOIN announcement_groups as ag ON ag.id = a.group_id
            WHERE
                a.enabled AND ag.enabled AND
                (a.expire_date IS NULL OR CURDATE() < DATE(a.expire_date)) AND
                (ag.expire_date IS NULL OR CURDATE() < DATE(ag.expire_date))
            ORDER BY ag.order_number ASC, a.order_number ASC");

        $responseBody["success"] = true;
        $responseBody["message"] = ucwords(str_camel_case_to_words(AnnouncementController::$genericName)). " list";
        $responseBody["data"] = array(
            "list" => $generics,
            //"limit" => 0,
            //"page" => 0,
            "count" => count($generics),
            //"total_filtered" => intval($totalFiltered[0]["total"]),
            //"total" => intval($totalRead[0]["total"])
        );
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(AnnouncementController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getSpecific($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        if(intval($params[1])) {
            $generics = DS::query("SELECT
                    *,
                    IFNULL(update_date,create_date) as update_date
                FROM ".AnnouncementController::$tableName."
                WHERE id = ?i", $params[1]);
        } else {
            $generics = DS::query("SELECT
                    *,
                    IFNULL(update_date,create_date) as update_date
                FROM ".AnnouncementController::$tableName."
                WHERE slug_title = ?s", $params[1]);
        }
        
        if(count($generics)) {
            // found
            
            $responseBody["success"] = true;
            $responseBody["message"] = ucwords(str_camel_case_to_words(AnnouncementController::$genericName)). " loaded";
            $responseBody["data"] = $generics[0];
            
            
        } else {
            $responseBody["message"] = ucwords(str_camel_case_to_words(AnnouncementController::$genericName)). " not found";
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(AnnouncementController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _add($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        // does the user have the correct scope
        if(count(array_intersect(["webmaster","administrator"], $scopes))) {
            
            // get all fields from request body
            $requestBody = file_get_contents("php://input");
            $requestObject = json_decode($requestBody, true);
            
            $options = $this->_getOptions($user_id, $scopes);
            $fields = $options[AnnouncementController::$genericName]["POST"]["request"]["body"];

            // filter away bad fields
            $data = array();
            foreach(array_keys($fields) as $field) {
                if(isset($requestObject[$field])) {
                    $data[$field] = $requestObject[$field];
                }
            }

            // still have data?
            if(count($data)) {

                // some fields need to meet certain criteria - validate here
                $invalid = Forms::validateData($fields, $data);

                // valid?
                if(!count($invalid)) {

                    // set current user as owner
                    $data["owner_id"] = $user_id;
                    
                    if(isset($data["title"])) {
                        $data["slug_title"] = str_slugify($data["title"]);
                    }
                    
                    if(isset($data["body"])) {
                        $data["body"] = base64_encode($data["body"]);
                    }

                    if(count($generic = DS::insert(AnnouncementController::$tableName, $data))) {
                        
                        $responseBody["message"] = ucwords(str_camel_case_to_words(AnnouncementController::$genericName)). " added";
                        $responseBody["success"] = true;
                        $responseBody["data"] = $generic[0];
                    } else {
                        $responseBody["message"] = "Database insert error";
                    }

                } else {

                    // had invalid fields
                    $responseBody["message"] = "Some fields are invalid";
                    $responseBody["data"] = array(
                        "values" => $data,
                        "invalid" => $invalid
                    );

                }
            } // else parameters are missing - default message
               
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(AnnouncementController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _update($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        $generics = DS::query("SELECT
                *
            FROM ".AnnouncementController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // does the user have the correct scope
        if(count(array_intersect(["webmaster","administrator"], $scopes))) {
            
            $requestBody = file_get_contents("php://input");
            $requestObject = json_decode($requestBody, true);
            
            $options = $this->_getOptions($user_id, $scopes, $generics[0]["owner_id"]);
            $fields = $options["".AnnouncementController::$genericName."/{id}"]["PUT"]["request"]["body"];//DS::table_info(AnnouncementController::$tableName);
            
            $isOwner = boolval(intval($generics[0]["owner_id"]) === intval($user_id));

            // filter away bad fields
            $data = array();
            foreach(array_keys($fields) as $field) {
                if(isset($requestObject[$field])) {
                    $data[$field] = $requestObject[$field];
                }
            }
            
            // still have data?
            if(count($data)) {
                
                // some fields need to meet certain criteria - validate here
                $invalid = Forms::validateData($fields, $data, $generics[0]["owner_id"]);

                // valid?
                if(!count($invalid)) {
                    
                    if(isset($data["title"])) {
                        $data["slug_title"] = str_slugify($data["title"]);
                    }
                    
                    if(isset($data["body"])) {
                        $data["body"] = base64_encode($data["body"]);
                    }
                    
                    if(DS::update(AnnouncementController::$tableName, $data, "WHERE id = ?i", $params[1]) !== null) {
                        $responseBody["message"] = ucwords(str_camel_case_to_words(AnnouncementController::$genericName)). " updated";
                        $responseBody["success"] = true;
                        $responseBody["data"] = $data;
                    } else {
                        $responseBody["message"] = "Database update error";
                    }
                    
                } else {
                    
                    // had invalid fields
                    $responseBody["message"] = "Some fields are invalid";
                    $responseBody["data"] = array(
                        "values" => $data,
                        "invalid" => $invalid
                    );
                    
                }
                
            } // else parameters are missing - default message
            
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(AnnouncementController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _delete($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        $generics = DS::query("SELECT
                *
            FROM ".AnnouncementController::$tableName."
            WHERE id = ?i", intval($params[1]));
        
        // does the user have the correct scope
        if(count(array_intersect(["webmaster","administrator"], $scopes))) {
            
            if(DS::delete(AnnouncementController::$tableName, "WHERE id = ?i", $params[1]) !== null) {
                $responseBody["message"] = ucwords(str_camel_case_to_words(AnnouncementController::$genericName)). " deleted";
                $responseBody["success"] = true;
            } else {
                $responseBody["message"] = "Database delete error";
            }
            
        } else {
            
            // access denied
            $responseBody["message"] = "Access denied.";
            $responseBody["access"] = false;
            
        }
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        LogController::log(AnnouncementController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    function _getOptions($user_id, $scopes, $owner_id = null) {
        $groups = DS::select("announcement_groups");
        $groupItems = array();
        foreach($groups as $group) {
            $groupItems[$group["id"]] = array($group["title"]." (".($group["enabled"] ? "Enabled" : "Disabled").")",true);
        }
        $options = array(
            "announcement" => array(
                "GET" => array(
                    "description" => "List resources.",
                    "request" => array(
                        "parameters" => array(
                            "limit" => "Limit the number of items returned (default: 25).",
                            "page" => "Announcement through the returned items (default: 0).",
                            "filter" => "Filter the returned items."
                        ),
                        "body" => array()
                    ),
                    "response" => array(
                        "list" => array(
                            array(
                                "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
                                "owner_id" => array("Field" => "owner_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                                "title" => array("Field" => "title", "Type" => "VARCHAR", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
                                "group_id" => array("Field" => "group_id", "Type" => "varchar", "Null" => "no", "Key" => "", "Default" => 0, "Extra" => "", "Label" => "Group"),
                                "display_type" => array("Field" => "display_type", "Type" => "varchar", "Null" => "no", "Key" => "", "Default" => 0, "Extra" => ""),
                                "body" => array("Field" => "body", "Type" => "ckeditor", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                                "order_number" => array("Field" => "order_number", "Type" => "int", "Null" => "NO", "Key" => "", "Default" => 0, "Extra" => ""),
                                "link_url" => array("Field" => "link_url", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                                "slug_title" => array("Field" => "slug_title", "Type" => "VARCHAR", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
                                "image_id" => array("Field" => "image_id", "Type" => "image", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Label" => "Image"),
                                "enabled" => array("Field" => "enabled", "Type" => "TINYINT", "Null" => "yes", "Key" => "", "Default" => 1, "Extra" => ""),
                                "expire_date" => array("Field" => "expire_date", "Type" => "datetime", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                                "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                                "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                            )
                        ),
                        "limit" => null,
                        "page" => null,
                        "count" => null,
                        "total_filtered" => null,
                        "total" => null
                    )
                ),
                "POST" => array(
                    "description" => "Create a resource.",
                    "request" => array(
                        "parameters" => null,
                        "body" => array(
                            "title" => array("Field" => "title", "Type" => "VARCHAR", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
                            "group_id" => array("Field" => "group_id", "Type" => "select", "Null" => "no", "Key" => "", "Default" => 0, "Extra" => "", "Items" => $groupItems, "Label" => "Group"),
                            "body" => array("Field" => "body", "Type" => "ckeditor", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                            "order_number" => array("Field" => "order_number", "Type" => "int", "Null" => "NO", "Key" => "", "Default" => 0, "Extra" => ""),
                            "link_url" => array("Field" => "link_url", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                            "image_id" => array("Field" => "image_id", "Type" => "image", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Label" => "Image"),
                            "enabled" => array("Field" => "enabled", "Type" => "TINYINT", "Null" => "yes", "Key" => "", "Default" => 1, "Extra" => ""),
                            "expire_date" => array("Field" => "expire_date", "Type" => "datetime", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                        )
                    ),
                    "response" => array(
                        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
                        "owner_id" => array("Field" => "owner_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "title" => array("Field" => "title", "Type" => "VARCHAR", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
                        "group_id" => array("Field" => "group_id", "Type" => "select", "Null" => "no", "Key" => "", "Default" => 0, "Extra" => "", "Items" => $groupItems, "Label" => "Group"),
                        "body" => array("Field" => "body", "Type" => "ckeditor", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                        "order_number" => array("Field" => "order_number", "Type" => "int", "Null" => "NO", "Key" => "", "Default" => 0, "Extra" => ""),
                        "link_url" => array("Field" => "link_url", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                        "slug_title" => array("Field" => "slug_title", "Type" => "VARCHAR", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
                        "image_id" => array("Field" => "image_id", "Type" => "image", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Label" => "Image"),
                        "enabled" => array("Field" => "enabled", "Type" => "TINYINT", "Null" => "yes", "Key" => "", "Default" => 1, "Extra" => ""),
                        "expire_date" => array("Field" => "expire_date", "Type" => "datetime", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                    )
                ),
            ),
            "announcement/{id}" => array(
                "GET" => array(
                    "description" => "Get a resource.",
                    "request" => array(
                        "parameters" => null,
                        "body" => array()
                    ),
                    "response" => array(
                        "id" => array("Field" => "id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "PRI", "Default" => null, "Extra" => "auto_increment"),
                        "owner_id" => array("Field" => "owner_id", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => null, "Extra" => ""),
                        "title" => array("Field" => "title", "Type" => "VARCHAR", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
                        "group_id" => array("Field" => "group_id", "Type" => "select", "Null" => "no", "Key" => "", "Default" => 0, "Extra" => "", "Items" => $groupItems, "Label" => "Group"),
                        "body" => array("Field" => "body", "Type" => "ckeditor", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                        "order_number" => array("Field" => "order_number", "Type" => "int", "Null" => "NO", "Key" => "", "Default" => 0, "Extra" => ""),
                        "link_url" => array("Field" => "link_url", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                        "slug_title" => array("Field" => "slug_title", "Type" => "VARCHAR", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
                        "image_id" => array("Field" => "image_id", "Type" => "image", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Label" => "Image"),
                        "enabled" => array("Field" => "enabled", "Type" => "TINYINT", "Null" => "yes", "Key" => "", "Default" => 1, "Extra" => ""),
                        "expire_date" => array("Field" => "expire_date", "Type" => "datetime", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                        "update_date" => array("Field" => "update_date", "Type" => "datetime", "Null" => "YES", "Key" => "", "Default" => null, "Extra" => ""),
                        "create_date" => array("Field" => "create_date", "Type" => "timestamp", "Null" => "NO", "Key" => "", "Default" => "CURRENT_TIMESTAMP", "Extra" => "")
                    )
                ),
                "PUT" => array(
                    "description" => "Update a resource. Allows sparse payloads.",
                    "request" => array(
                        "parameters" => null,
                        "body" => array(
                            "title" => array("Field" => "title", "Type" => "VARCHAR", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
                            "group_id" => array("Field" => "group_id", "Type" => "select", "Null" => "no", "Key" => "", "Default" => 0, "Extra" => "", "Items" => $groupItems, "Label" => "Group"),
                            "body" => array("Field" => "body", "Type" => "ckeditor", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                            "order_number" => array("Field" => "order_number", "Type" => "int", "Null" => "NO", "Key" => "", "Default" => 0, "Extra" => ""),
                            "link_url" => array("Field" => "link_url", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                            "image_id" => array("Field" => "image_id", "Type" => "image", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Label" => "Image"),
                            "enabled" => array("Field" => "enabled", "Type" => "TINYINT", "Null" => "yes", "Key" => "", "Default" => 1, "Extra" => ""),
                            "expire_date" => array("Field" => "expire_date", "Type" => "datetime", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                        )
                    ),
                    "response" => array(
                        "title" => array("Field" => "title", "Type" => "VARCHAR", "Null" => "no", "Key" => "", "Default" => null, "Extra" => ""),
                        "group_id" => array("Field" => "group_id", "Type" => "select", "Null" => "no", "Key" => "", "Default" => 0, "Extra" => "", "Items" => $groupItems, "Label" => "Group"),
                        "body" => array("Field" => "body", "Type" => "ckeditor", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                        "order_number" => array("Field" => "order_number", "Type" => "int", "Null" => "NO", "Key" => "", "Default" => 0, "Extra" => ""),
                        "link_url" => array("Field" => "link_url", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                        "image_id" => array("Field" => "image_id", "Type" => "image", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Label" => "Image"),
                        "enabled" => array("Field" => "enabled", "Type" => "TINYINT", "Null" => "yes", "Key" => "", "Default" => 1, "Extra" => ""),
                        "expire_date" => array("Field" => "expire_date", "Type" => "datetime", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => ""),
                    )
                ),
                "DELETE" => array(
                    "description" => "Permanently remove a resource from the database.",
                    "request" => array(
                        "parameters" => null,
                        "body" => array()
                    ),
                    "response" => array()
                )
            )
        );
        
        if(!count(array_intersect(["anonymous"], $scopes))) {
            // modify depending on ownership
            if($owner_id != null && $owner_id != $user_id) {
//
//                $options["announcement/{id}"]["PUT"]["request"]["body"] = array(
//                    "hearts" => array("Field" => "hearts", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => "0", "Extra" => "")
//                );
//                $options["announcement/{id}"]["PUT"]["response"] = array(
//                    "hearts" => array("Field" => "hearts", "Type" => "int(10) unsigned", "Null" => "NO", "Key" => "", "Default" => "0", "Extra" => "")
//                );
                
            } else {
                unset($options[AnnouncementController::$genericName]["GET"]["response"]["list"][0]["owner_id"]);
            }
        }
        
        return $options;
    }
    
    function _options($params, $request, $response, $server) {
        
        // prep json response
        $responseBody = $this->_createJsonResponseBody();
        
        // get current user scopes
        list($scopes,$user_id) = $this->_getViewer($request);
        
        $options = $this->_getOptions($user_id, $scopes);
        
        $responseBody["message"] = ucwords(str_camel_case_to_words(AnnouncementController::$genericName)). " options";
        $responseBody["success"] = true;
        $responseBody["data"] = $options;
        
        // prep output
        $response = $response->withPayload($responseBody);
        
        // log response
        //LogController::log(AnnouncementController::$genericName,$request->getMethod(),$params[1],$user_id,$responseBody["success"],json_encode($response->getPayload()),$request->getUri()->getHost().$request->getUri()->getPath()."?".$request->getUri()->getQuery());
        
        return $response;
    }
    
    public function install() {
        $tables = DS::list_tables();
        
        // check if the announcements table exists, if not create it.
        if(array_search('announcements',$tables)===false) {
            // generate the create table query
            $query = "CREATE TABLE `announcements` (
                `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `owner_id` int(10) UNSIGNED NOT NULL,
                `title` varchar(256) NOT NULL,
                `body` text,
                `group_id` varchar(128) NOT NULL,
                `order_number` int(11) NOT NULL DEFAULT '0',
                `link_url` text,
                `slug_title` varchar(256) NOT NULL,
                `image_id` int(10) UNSIGNED DEFAULT NULL,
                `enabled` tinyint(1) DEFAULT '1',
                `expire_date` datetime DEFAULT NULL,
                `update_date` datetime DEFAULT NULL,
                `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";

            if(DS::query($query)) {
                //message_add("The announcements table has been created.");
            }
        }
        
        $pages = array();
        /*
        * announcement/listing
        */

        // add user page permissions including core admin
        $pages[] = array(
            'name'=>'getAll',
            'category'=>'announcement',
            'subcat'=>'pages',
            'ptype'=>2,
            'pview'=>'webmaster,administrator');
        $pages[] = array(
            'name'=>'getAllForDisplay',
            'category'=>'announcement',
            'subcat'=>'pages',
            'ptype'=>2,
            'pview'=>'anonymous,authenticated');
        
        foreach($pages as $key=>$fieldData) {
            Permissions::set($fieldData);
        }

        // add user content permissions
        $content = array();
        $content[] = array(
            'name'=>'announcement',
            'category'=>'announcement',
            'subcat'=>'content',
            'ptype'=>0,
            'pview'=>'webmaster,administrator',
            'pedit'=>'webmaster,administrator',
            'padd'=>'webmaster,administrator',
            'pdel'=>'webmaster,administrator');
        foreach($content as $key=>$fieldData) {
            Permissions::set($fieldData);
        }

        // add user field permissions
        /*$fields = array();
        $fields[] = array(
            'name'=>'active',
            'category'=>'user',
            'subcat'=>'fields',
            'ptype'=>1,
            'pview'=>'webmaster',
            'pedit'=>'webmaster');
        foreach($fields as $key=>$fieldData) {
            Permissions::set($fieldData);
        }*/
    }
    
    /*public function update($from_version) {
            
        try {
            
            if($from_version < 1) {
                $query = "ALTER TABLE announcements CHANGE video_url link_url TEXT AFTER body;";

                DS::query($query);

                $from_version ++;
            }
            
        } catch (Exception $e) {
            print $e->getMessage();
        }
        
        return $from_version;
    }*/
}